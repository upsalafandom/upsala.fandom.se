+++
title = "E-postlista"
path = "epostlista"
+++

Upsala sf-fans har en gemensam sändlista för att vi ska kunna informera varandra om vad som händer och diskutera vad vi Upsalabor som är intresserade av sf och fantasy kan göra tillsammans.

För att gå med, [följ den här länken till listans webgränssnitt](https://mail.fandom.se/mailman/listinfo/upsala_fandom.se) och följ anvisningarna:

# Vi finns också på:

* [Facebook](https://www.facebook.com/groups/upsalafandom/)
* [LibraryThing](https://www.librarything.com/groups/upsalafandom)
