+++
title = "Pubmöten"
path = "pubmoten"
+++

Första helgfria tisdagen varje månad träffas vi på [O'Neill's Irish Pub](https://oneillsirishpub.se/) på Dragarbrunnsgatan&nbsp;53, ett stenkast från Resecentrum. Vi sitter i rummet nedanför trappan som du hittar längst bak i puben.

Vi sitter där från kl. 18.00 och större delen av kvällen och pratar om science fiction, fantasy och [allmänt strunt](/taggar/pubmoten/). Varför inte komma förbi och ta en matbit och ett glas öl eller juice och våga dig in i fandoms spännande värld?

![Bild från pubmöte](buddys.jpeg)

[Rapporter från gångna pubmöten hittar du här](/taggar/pubmoten/).

[Här](https://fandom.se/pub/) kan du få information om pubmöten i andra städer.
