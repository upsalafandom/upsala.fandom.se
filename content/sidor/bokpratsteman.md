+++
title = "Bokpratsteman"
path = "bokpratsteman"
+++

Vi arrangerar bokprat. Det är informella möten då vi diskuterar en bok vi
alla läst. 2009 till 2017 hölls bokpraten i samarbete med
[Catahya](https://catahya.net/).

# Tidigare teman

## 2016-2017

* Ann Leckie – Ancillary Justice 
* Elizabeth Bear – Karen Memory
* Saladin Ahmed – Throne of the Crescent Moon
* Kameron Hurley – ?

## 2013

### Vår

* Libba Bray – Beauty Queens

## 2012

### Höst

* Sheri S. Tepper – Grass
* Clifford Simak – City
* Hope Mirrlees – Lud-in-the-Mist
* Robert Silverberg – The Book of Skulls

### Vår

* Pat Cadigan – Fools
* Shirley Jackson – We Have Always Lived in the Castle
* Catherynne M. Valente – Deathless
* Helen Oyeyemi – Mr. Fox
* Tansy Raynor Roberts – Love and Romanpunk

## 2011

### Höst

* Lauren Beukes – Zoo City
* Kage Baker – The Empress of Mars
* Nnedi Okorafor – Who Fears Death
* Amelia Beamer – The Loving Dead

### Vår

* Cherie Priest – Boneshaker
* Kit Whitfield – In Great Waters
* China Miéville – The City & the City
* Martin Engberg – Stjärnpalatset
* Junot Díaz – The Brief Wondrous Life of Oscar Wao

## 2010

### Vår

Temat var bortglömda mästare inom science fiction.

* Thomas M. Disch – Camp Concentration
* Robert Silverberg — Dying Inside
* Norman Spinrad — Bug Jack Barron
* Christopher Priest — The Affirmation

## 2009

### Höst

* Margaret Atwood – The Handmaid's Tale
* Kalle Dixelius – Toffs bok
* Paul McAuley – Fairyland
* Gene Wolfe – The Fifth Head of Cerberus
* Best of the Best: 20 Years of the Year's Best Science Fiction (En antologi redigerad av Gardner Dozois. Vi valde ut några av novellerna för diskussion.)

### Vår

Temat var vinnare av World Fantasy Award för bästa roman (igen).

* Dan Simmons – Song of Kali
* Tim Powers – Last Call
* Jeffrey Ford – The Physiognomy
* Jo Walton – Tooth and Claw
* Patricia A. McKillip – Ombria in Shadow

## 2008

Temat var hedersgäster på årets kongresser.

* Ian McDonald – River of Gods
* Cory Doctorow – Someone Comes to Town, Someone Leaves Town
* Adam Roberts – Swiftly
* Diane Duane – So You Want To Be A Wizard?
* John Courtenay Grimwood – Stamping Butterflies

## 2007

Temat var vinnare av World Fantasy Award för bästa roman.

* John M. Ford – The Dragon Waiting
* Robert Holdstock – Mythago Wood
* James Morrow – Only Begotten Daughter
* Ursula K. Le Guin – The Other Wind
* Haruki Murakami – Kafka on the Shore

## 2005

* J. Sheridan LeFanu – Carmilla
* Robert W. Chambers – The King in Yellow
* Bram Stoker – Dracula
* E. R. Edison – The Worm Ouroboros
* Charles Stross – Iron Sunrise
* Umberto Eco – Rosens namn
* Joanna Russ – The Female Man

## 2004

* Novellantologin _Cities_
* I. B. Singer – Familjen Moskat
* Ted Chiang – Stories of Your Life and Others
* Robert A. Heinlein – Starship Troopers
* Justina Robson – Natural History
* Samuel R. Delany – The Einstein Intersection
* M. John Harrison – Light
* Roger Zelazny – Lord of Light
* George McDonald – Lilith, a romance
* H. Rider Haggard – She

## 2003

* Ken MacLeod – The Stone Canal
* Alastair Reynolds – Chasm City
* Ken MacLeod – The Cassini Division
* Ken MacLeod – The Sky Road
* Ken MacLeod – Cosmonaut Keep
* Ken MacLeod – Dark Light
* Ken MacLeod – Engine City
* Michael Moorcock – The Final Programme/A Cure for Cancer
* Umberto Eco – Foucaults pendel
* Michael Moorcock – The English Assassin/The Condition of Muzak

## 2002

* Hope Mirrlees – Lud-in-the-Mist
