+++
title = "Björntjänsten"
path = "bjorntjansten"
+++

Björntjänsten, dvs *Upsalafandoms Rese-Stipendium för Unga SF-fans* (URSUS),
är en fond som förvaltas av sf-fansen i Uppsala, och vars syfte är att hjälpa
unga och nykomlingar att ta sig till sf-kongresser och träffa andra. Om man
går i skolan eller läser på universitet eller högskola har man ofta lite
skralt med pengar. Bor man långt från orten där kongressen hålls blir det
lätt ganska dyrt att resa dit. Har man inte varit på kongresser förr kanske
man drar sig lite extra för den investeringen, eftersom man inte vet om man
kommer att gilla det. Om pengabrist är det som hindrar att man är med vill vi
hjälpa till.

Vi vill kunna ge bidrag till resan till och från kongressen, så att nya unga
fans lättare kan komma till kongresser och lära känna fandom IRL. Vi vill
knyta kontakter mellan sf-fans!

Pengarna samlas in genom donationer och genom försäljningar och auktioner på
kongresser och andra fanmöten. Grundplåten lades på Swecon 2005 i Göteborg.

# Kan jag få pengar?

Vill du åka på en sf-kongress eller sf-relaterade aktiviteter med anknytning
till svensk fandom kan du söka resebidrag från URSUS. Vi kommer att bedöma
ansökningarna antingen på något av våra vanliga möten eller på ett särskilt
sammankallat möte. Finns det flera ansökningar kommer vi att prioritera dem
så här:

1. personer i högstadiet eller gymnasiet
1. studenter eller andra under 25 som har dålig ekonomi
1. övriga

Lite beroende på hur mycket pengar fonden får in hoppas vi kunna ge bidrag
till 2-4 personer om året. Storleken på bidraget kan också anpassas till hur
långt från kongressen eller evenemanget man bor.

Vi kommer att begära någon form av redovisning för att pengarna använts till
det stipendiet är avsett för. Allra helst vill vi ha en liten skriven
berättelse om hur det var! Kan man inte redogöra för resan vill vi ha
pengarna tillbaka.

# Hur söker man?

Skriv ett brev och förklara vem du är och motivera varför du behöver ett
bidrag för att åka på kongress eller liknande. Skicka brevet till Anna
Davour.

Snigelpost:

> Anna Davour \
> Marmorv. 9 A \
> 752 44 Uppsala

E-post: `landetannien kringla gmail.com` (du vet vad man gör med adressen)
