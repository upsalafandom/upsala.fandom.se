+++
title = "Vad är fandom?"
path = "vad_ar_fandom"
+++

_Science fiction-fandom_ är ett nätverk av science fiction-intresserade med
rötterna i 1920-talets USA. Det uppstod då de första sf-tidskrifterna tryckte
namn och adress på dem som publicerades i brevspalterna, varpå läsarna
började skriva direkt till varandra och lärde känna varandra. Umgänget
försiggick via brevväxling samt i amatörtidningar, s.k. _fanzines_ och
sf-klubbar. Fandom finns idag även utanför USA, inklusive i de flesta
europeiska länder.

Utanför sf-fandom kallar man gärna alla som gillar sf, Star Trek, Star Wars
osv för sf-fans, men inom fandom syftar ordet bara på medlemmarna inom
kontaktnätet. Även om man har hyllorna fulla av science fiction räknas man
bara som "sf-läsare" om man inte går på sf-kongresser eller är med i en
sf-förening. Lackmustestet är enkelt: Om man vet vad fandom är och anser sig
vara sf-fan, så är man förmodligen det.

Om man en gång har blivit sf-fan fortsätter man alltså att räknas som sf-fan
så länge man går på sf-kongresser, läser fanzines eller umgås med andra
sf-fans, även om man slutar intressera sig för sf.

# Varför blir man sf-fan?

Hur kommer det sig att det för vissa sf-älskare inte räcker med att läsa
eller titta på sf och fantasy, utan att de engagerar sig på något sätt i
fandom? Är fandom en klubb för inbördes beundran och skvaller?

Fandom i första hand en grupp människor, ett socialt kontaktnät. Inom gruppen
finns både de som mest är intresserade av science fiction och de som i första
hand är intresserade av fandom i sig. De flesta faller mellan de bägge
ytterligheterna, eller rör sig mellan dem. Det gemensamma intresset för
fantastisk litteratur bildar bakgrund, oavsett om fans babblar om böcker,
religion, sport, film eller politik så har de en gemensam referensram: De
känner till robotikens tre lagar, de minns den svindlande känslan första
gången de upplevde sense of wonder och sådant där. De hittade till fandom
tack vare att de fascineras av science fiction, men de stannar kvar eftersom
de trivs med varandra.

Det kan ibland vara svårt att tränga in i det nätverk som är fandom. Det är
inte alla som är intresserade av att umgås med andra sf-läsare eller som
gillar betoningen på verbal prestation eller den geekmentalitet som ibland
råder.

# Vad utmärker sf-fans?

Kärnan bakom det ursprungliga engagemanget i fandom är driften att hitta
andra med samma intresse. Det drev tonåringarna i trettiotalets USA att utge
sina första amatörtidskrifter fyllda med visioner av framtiden, teknikfixerad
prosa och handfasta beskrivningar av hur man bygger sina egna raketer;
driften att få dela med sig av sitt intresse och diskutera det med andra.
Samma drift var det som gjorde att de första organiserade sf-träffarna, som i
svensk sf-fandom går under namnet _kongresser_ trots att de är föga
kongresslika, utlystes i slutet av trettiotalet och sedan spred sig över
världen.

Det är detta som skiljer sf-läsaren från sf-fanen. När sf-fans träffas och
umgås över ett glas öl så spekuleras det djärvt och fabuleras det vilt om
vartannat, och halsbrytande litterära teorier ser dagens ljus för första
gången, som när det uppdagas att det finns en undergenre inom
fantasylitteraturen - främst representerad av Robert Jordan - som benämns
homeopatisk litteratur, eller att Roy Andersson är Sveriges främsta
företrädare för den socialsurrealistiska skolan inom filmkonsten.

Föga förvånande för en rörelse som sprang ur brevspalter, tidskriftsutgivning
och läsande, är dess medlemmar ofta textfixerade och engagerade i det skrivna
ordet. Skriftlig kommunikation har alltid varit det sätt som fandom har
hållit samman på och flera av de främsta utmärkelserna inom fandom genom
tiderna har varit avsedda för duktiga skribenter eller getts till människor
som de flesta bara känner genom de texter han eller hon producerar. I den
engelsktalande världen, där den mesta av all sf publiceras, är det snarare
regel än undantag att sf-författarna har en bakgrund i fandom och debuterade
i sf-fanzines. Fans bryr sig om skrift och det är viktigt i fandom att skriva
med omsorg.

# Vad är ett sf-fanzine?

Väldigt länge var utgivandet av _sf-fanzines_ fandoms kärnverksamhet. Det var
i fanzines man övade upp sina skrivtalanger och det var genom fanzinen och
deras brevspalter man höll kontakten med resten av fandom.

En strikt definition på vad som kännetecknar ett science fiction-fanzine kan
vara svår att ge, men i lösa ordalag kan det sägas vara en hobbytidskrift
(oftast billigt reproducerad) utgiven av sf-fans med andra sf-fans som
främsta målgrupp. Innehållet har i regel anknytning till antingen science
fiction och/eller science fiction-fandom. Utgivarna tjänar inga pengar på
utgivningen och bidragsgivare får sällan betalt på något sätt.

**Not:** Ordet _fanzine_ är en sammandragning av _fan magazine_ och myntades
i början på 1940-talet i amerikansk fandom, många år innan det spred sig
utanför fandom till sammanhang som musik, rollspel eller tecknade serier.
Ibland försvenskas det till _fansin_.

# Vad är en science fiction-kongress?

Mycket enkelt i teorin; det är en stor träff med program (föredrag,
filmvisning, paneldiskussioner, frågesport och liknande) för science
fiction-fans. Om träffens huvudsyfte är något annat än att vara en social
träff med program för sf-fans så är det inte någon sf-kongress. Medlemmarna i
en sf-kongress är där för att träffa gamla vänner, lära känna nya vänner och
vanligen också lyssna på programmet. (På ens första kongresser är
naturligtvis programmet viktigast, innan man känner så många av de andra
deltagarna.) Medlemmarna är, kort sagt, där för att ha kul, vilket kan (men
inte måste) inkludera att prata om sf och fantasy. Om det finns författare
eller förlagsfolk där umgås de som vanligt folk med de andra
kongressmedlemmarna, ingen sätts på piedestal.

En sf-kongress är ett deltagarevenemang. Alla är med på samma villkor, som
deltagare som hjälps åt för att få arrangemanget att gå runt organisatoriskt
och ekonomiskt. En sf-kongress delas inte upp i arrangörer och kunder, man
köper inte biljett till kongressen. Man löser _medlemsavgift_ till den och
vem som helst kan bli tillfrågad om man har lust att hjälpa till med något
bakom kulisserna. Det här är inte en lek med ord; det är helt avgörande för
förståelsen av fenomenet _sf-kongress_ att förstå skillnaden mellan
inträdesbiljett och medlemsavgift.

Den första svenska sf-kongressen, _LunCon_, hölls i Lund 1956.

# Science fiction-föreningar inom fandom

**Club Cosmos** i Göteborg bildades 1954 av Gabriel Setterborg, Roland Adlerberth
och Lars Erik Helin och är Sveriges äldsta existerande sf-förening.
Verksamheten har varierat mycket genom åren och består numera mest i
gemensamma pubmöten och biobesök. Club Cosmos utgav det första svenska
sf-fanzinet, Cosmos News.

I Stockholm finns Sveriges största sf-förening, **Skandinavisk Förening för
Science Fiction**. Den föddes 1960 med ambitionen att bli en ledande
samlingsplats för science fiction-intresserade i hela Skandinavien.
Femtiotalet i svensk sf-fandom var en tid av yr skandinavism och flammande
storhetsvansinne, det grundades flera unioner och pangalaktiska sällskap med
grandiosa planer att ena sf-vännerna i Sverige och Skandinavien mot den
oförstående omvärlden. De hette SF-Union Göteborg, SF-Union Stockholm,
Luncon-Unionen, Stockon-Unionen och SF-Union Skandinavien. Alla dog de
emellertid inom några veckor eller i bästa fall efter några år.

När den sista av dessa unioner gått i graven beslöt några fans att bilda en
ny organisation. De kallade den Skandinavisk Förening för Science Fiction.
Föreningen var ­ och är fortfarande ­ baserad i Stockholm, och man ägnade sig
främst åt utgivning av fanzines, framför allt av flaggskeppet SF-Forum som
under flera perioder varit bland de mest ambitiösa tidskriftsprojekten i
svensk fandom. SFSF hade de första åren cirka femtio medlemmar, men växte
långsamt och hade vid 1970-talets mitt runt 200. Idag är SFSF:s verksamhet
måttlig. Man har möten då och då och diskuterar någon bok eller tittar på
lite video, och utgivningstakten på medlemstidningarna varierar. Genom åren
har SFSF stått bakom sf-kongresser, ofta i det lite större formatet.

Mindre till medlemsantalet än SFSF, och med avgjort kortare historia, är
**Linköpings Science Fiction-Förening**. LSFF grundades 1988 av Andreas och
Carina Björklind, ett par sörmlänningar som flyttade ner till Linköping för
att studera. I den rollspels- och teknologtäta östgötamyllan visade sig den
här idén med en sf-förening frodas, och medlemmarna ägnar sig i dag åt att ge
ut medlemstidningen Månblad Alfa med oregelbundna mellanrum och åt att ha
möten. Dessa möten lär ha inneburit ett uppsving för Linköpings tehandlare.
Föreningen har också ägnat sig åt bokmöten, och även åt något så exotiskt som
bokbokmöten efter att de vanliga bokmötena urvattnats till att mer handla om
allmänt fanniskt umgänge än om litterära diskussioner. Vartannat år
arrangerar de en sf-kongress vid namn Confuse.

Venerabla **Sigma Terra Corps** i Stockholmsförorterna Nacka och Saltsjöbaden
grundades i januari 1979 som en svensk avläggare till den två år gamla tyska
Perry Rhodan-Club Terra Corps. En av den tyska klubbens grundare, Wolf von
Witting, bodde vid den här tiden i Sverige. 1982, när föreningen var som
störst, hade den och och det tyska systersällskapet tillsammans fler än 200
medlemmar. Med början år 1980 arrangerade Sigma TC sf-kongressen NaSaCon elva
år i rad. Efter dessa krävande år, som slutade med en del interna
stridigheter, lades föreningen i malpåse. Den återupplivades i januari 1997,
och har arrangerat fyra kongresser till efter det. I skrivande stund verkar
dock aktiviteten på nytt ha mattats av något.

**Europeisk Förening för Science Fiction** grundades 1985 på en studentpub
i Uppsala och efter att namnförslaget SF-klubben Atom röstats ned, var
föreningen nästan namnlös i några månader tills medlemmarna i fick för sig
att pika Stockholmsföreningen SFSF en smula genom att ge EFSF dess nuvarande
namn. Föreningen saknar stadgar, styrelse och medlemsavgift. Allt som finns
är ett namn som används för de spridda aktiviteter man ägnar sig åt,
exempelvis gemensamma antikvariatsrundor, läsecirklar, pubmöten och
sf-kongresser.

**Sveriges Fanzineförening**s verksamhet består av distribution av sf-fanzines.
För medlemsavgiften får man rätten att få sitt fanzine distribuerat till alla
medlemmar, och omvänt får man då alla andras fanzines hem till sin egen
brevlåda. Det är emellertid inget krav på att man som medlem måste utge några
fanzines, de flesta medlemmarna är med bara för att få andras fanzines hem i
brevlådan. Med tanke på dagens brevporto så sparar de medverkande
fanzineutgivarna stora pengar på detta distributionssätt. Det är till och med
så att det i dagens svenska fandom knappast förekommer någon fanzineutgivning
värd att nämnas utanför SFF.

**Stiftelsen Alvar Appeltoffts Minnesfond** (Alvarfonden), mest känd för att den
delar ut Alvar Appeltoffts minnespris, är något så unikt som en pengastark
stipendiefond för fandom. Alvarpriset är ett årligt stipendium till sf-fans
för betydande och berömvärda insatser av bestående värde i någon form inom
svensk fandom.
