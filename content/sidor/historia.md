+++
title = "Historia"
path = "historia"
+++

Ett par försök har gjorts att beskriva Upsalafandom tidigare, Magnus Erikssons [bakgrund](/arkiv/bakgrund.html) i förordet till kongressboken för Ghöstacon (1985) och Johan Anglemarks [ordlista](/arkiv/ordlista.html) i kongressboken för [Trøstcon 1988](/arkiv/kongresser/trostcon.html). Båda två är naturligtvis ganska föråldrade idag, men eftersom flera av de då tongivande fansen fortfarande bor i Uppsala är de en god introduktion för den som vill veta något om hur fansen hade det i Uppsala i mitten av 80-talet.

Mycket av den fanniska mytologi som skapades då är även idag levande i dessa fans huvuden. Däremot bränner vi sällan några flaggor utanför Carolina på sista april eller utbringar högröstade skålar för avlidna nazister nuförtiden. (Tur är väl det!)

Magnus Eriksson har även beskrivit [hur EFSF bildades](/arkiv/tillkomst.html). Det är en nedkortad version av en artikel i kongressboken till Konfekt 97. En rapsodisk [krönika över våra tidigare kongresser](/arkiv/sjatte_gangen.html) finns också tillgänglig. Det är en artikel ur programboken till kongressen _En bättre konfekt_.

Martin Andreasson skrev en kongressrapport från [Trøstcon 1988](/arkiv/kongresser/trostcon.html), som vi har stulit. Den är läsvärd.
