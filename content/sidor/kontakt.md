+++
title = "Kontakt"
path = "kontakt"
+++

Vill du ha kontakt med oss kan du [skriva till oss](mailto:info@fandom.se) (eller varför inte dyka upp på ett av våra pubmöten? Du lär nog lista ut vilka som är vi…)
