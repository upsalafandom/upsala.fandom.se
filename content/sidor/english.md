+++
title = "In English"
path = "english"
lang = "en"
+++

Welcome, alien visitor.

This is the Upsala chapter of the worldwide fannish conspiracy. We are the ones who are not content to sit down in our comfy chairs and read that subversive literature in solitude, but who insist that science fiction and fantasy is more fun when you talk about it. We have book circles, we watch dvds, we meet up at the pub, we go shopping for books and we arrange sf conventions.

The hub of our activities is the [pub meetings](/pubmoten) the first Tuesday every month at [O'Neill's Irish Pub](https://oneillsirishpub.se/) on Dragarbrunnsgatan 53 (quite close to the railway station), from 6:00 PM. We've held these meetings since January 1997. Drop in for a pint and some food. O'Neill's has a decent selection of beer and they do good food.

You might also want to join our mailing list, a fairly low-traffic list used mainly for discussing upcoming events. [Follow the instructions here](https://mail.fandom.se/mailman/listinfo/upsala_fandom.se).
