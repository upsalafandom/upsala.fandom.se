+++
title = "Om sidan"
path = "om_sidan"
+++

# Upphovsrätt

Upphovsrätt till texter och bilder tillhör respektive upphovsman.

# Mjukvara

Sidan har byggts med [Zola](https://www.getzola.org/).

# Webbhotell

Sidan drivs på [HCoop](https://hcoop.net), ett Internet-kooperativ.

# Källkod

Källkod till sidan finns [i det här GitLab-arkivet](https://gitlab.com/upsalafandom/upsala.fandom.se/tree/master).
