+++
title = "Kalendarium"
path = "kalendarium"
+++

Här är ett kalendarium över våra aktiviteter den närmaste tiden.

<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;hl=sv&amp;bgcolor=%23FFFFFF&amp;src=ctv4hnp38d0jf8a3pj38d30ovo%40group.calendar.google.com&amp;color=%23875509&amp;ctz=Asia%2FBangkok" style="border-width:0" width="400" height="600" frameborder="0" scrolling="no"></iframe>

[Tryck här](https://calendar.google.com/calendar/embed?src=ctv4hnp38d0jf8a3pj38d30ovo%40group.calendar.google.com&ctz=Europe/Stockholm) för att lägga till vårt kalendarium till din Google-kalender.

Har du ett annat kalenderprogram kan du förhoppningsvis också läsa in vårt kalendarium från vårt [iCalendar-flöde](https://calendar.google.com/calendar/ical/ctv4hnp38d0jf8a3pj38d30ovo%40group.calendar.google.com/public/basic.ics).
