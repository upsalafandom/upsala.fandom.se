+++
title = "Fanzines"
path = "fanzines"
+++

Här är ett antal fanzines som Johan Anglemark producerat genom åren:

* [SFF-bulletin 98](/arkiv/fanzines/sff-bulletin_98.pdf)
* [Tarabagatai!](/arkiv/fanzines/tarabagatai.html)
* [Tarabagatai! 2](/arkiv/fanzines/tarabagatai2.html)
* [Ett rum med utsikt över fandom 1-4. Fanspalt i Spektra SF 1998–99](/arkiv/fanzines/ett_rum_med_utsikt.pdf)
* [Interview with Iain Banks. Liverpool, April 1999](/arkiv/fanzines/banks.pdf)
* [Fandom runt. Fanspalt i Minotauren 2000–2001](/arkiv/fanzines/fandom_runt.pdf)
* [SFF-bulletin 126](/arkiv/fanzines/sff-bulletin_126.pdf)
* [SFF-bulletin 141](/arkiv/fanzines/sff-bulletin_141.pdf)

Här är ett par nummer av pubfanzinet Obduktion av Andreas Davour och Johan Anglemark:

* [Oktober 2002](/arkiv/fanzines/obduktion_200210.pdf)
* [November 2002](/arkiv/fanzines/obduktion_200211.pdf)

Anna Davour har också en hemsida för sitt fanzine [Folk och fans i Annien](https://annien.wordpress.com/).
