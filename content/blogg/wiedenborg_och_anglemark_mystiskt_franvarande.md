+++
title = "Wiedenborg och Anglemark mystiskt frånvarande"
slug = "wiedenborg_och_anglemark_mystiskt_franvarande"
date = 2008-03-05

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Klockan var ca 22.30 när jag behagade infinna mig till mars månads pubmöte. Eftersom jag var så sen kommer jag inte att stå till tjänst med någon närvarolista.

Vissa märktes dock genom sin frånvaro. Johan Anglemark hade försatt sig i dvala i sitt hem, i väntan på att framtiden ska infinna sig. Experimentet lyckades i begränsad omfattning.

Hur som helst var ett tiotal fantaster av den vetenskapliga vitterheten på plats när jag kom. Jag plockade fram mitt anteckningsblock och försökte samla in uppgifter om vad som försigått fram till dess. August började med att be mig rapportera att han för en gångs skull kommit dit i tid. Jesper kunde dock meddela att han varit där i tid, och att August minsann inte varit där då. Av detta drog jag slutsatsen att stora mängder öhl måste ha förtärits, och att ingen närvarande kunde betraktas som helt tillförlitlig.

Jag ska dock rapportera att innan jag kom hade Jolkkonen förklarat för Jesper hur man bygger en vätebomb, och August hade avlöjat sina fördomar mot medelålders new age-tanter.

Efter att jag fått detta berättat för mig gled konversationen naturligt in på kopplingarna mellan Marxismen och Darwinismen. Jolkkonen förklarade hur han låtit historiematerialismen genomsyra sin undervisning på den kurs om kärnbränslen han ger för kemistudenter. Närmare bestämt handlade det om hur tekniska uppfinningar driver fram vetenskapliga framsteg. [L. J. Henderson](https://en.wikipedia.org/wiki/Lawrence_Joseph_Henderson) citerades: "Science owes more to the steam engine than the steam engine owes to Science".

Ungefär här försvann jag och Biörn X in i en egen konversation rörande storleken, papperskvaliteten och användbarheten hos olika slags registerkort.

När vi återanslöt oss till huvudsamtalet handlade det om det tyska språkets skönhet. Henrik deklarerade att detta var något han insåg redan då han läste Goethe.

Jolkkonen efterfrågade nyskriven opera och teater, istället för "dövhomotransversioner av King Lear". Tyvärr kan vi inte vänta oss något sådant, då vi befinner oss i en dekadent fas av den västerländska kulturen.

Torbjörn planerar att anordna bokmat (alltså bokprat parat med lagande och ätande av god mat) igen. Möjligheten att få tag på råvaror för att göra kalvdans diskuterades. Frågan är dock om Torbjörn är mannen att hålla i detta – han vet ju inte ens vem [Fritz Leiber](https://en.wikipedia.org/wiki/Fritz_Leiber) är.
