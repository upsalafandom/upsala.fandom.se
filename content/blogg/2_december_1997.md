+++
title = "2 december 1997"
slug = "2_december_1997"
date = 1997-12-02

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Det Upsalafanniska gardets sista strid för året utkämpades igår tisdag på O'Connors pub i Uppsala när vi för tolfte gången i år samlades för att kannstöpa och betrakta världen med ömsom klarsynta, ömsom hemmablinda ögon.

<!-- more -->

När jag anlände, fryntligt och punktligt klockan 18.30 möttes jag av fyra par ögon som betraktade mig med de förtidigt anländas spridda intresse. Förmodligen hade de följt cigarettautomaten, som fortfarande gick på sommartid. Ett debutfanzine låg på bordet, utgivet av Anna Åkesson, en av nycklarna bakom Upsalafandoms fenixartade danse macabre (för närvarande utlånad till Bayern för att där kunna spå på corioliseffekten en smula). Det ska kopieras upp och skickas ut. (Jag ser just att det saknar avsändaradress. Eftersom hon flyttar från Bayern till västkusten för några månaders uppehåll där innan hon återvänder till Uppsala antar jag att vi får skriva en c/o-adress så att folk kan loc:a. Jag undrar hur många av dem som inte träffat henne som kommer att tro att hon är en hoax ;)

Halva Uppsala verkade ha gått till O'Connors, förmodligen för att göra slut på sina företags personalvårdskonton så här på sluttampen av det gamla budgetåret. Kruse avslöjade att hon en gång i tiden kunnat en komplicerad fras på ryska som innehöll ordet älg, och ett snabbt associationsskutt över indoeuropeiska ljudlagar förde oss till det ryska ordet för fitta, pizda, som förtjänstfullt illustrerar några av dessa ljudlagar.

Flyter glas eller inte? Jolkkonen förklarade detta ingående, men verkade komma fram till att det inte finns något entydigt svar. Jag undrar om inte "kallflytande" är något slags eufemism för "kallfusion". Det lät så. Jag och Kristin reagerade med en impromptuföreställning av Spancil Hill och Will Ye Go, Lassie Go innan vi avrundade med Oro sé do bheatha 'bhaile och By the Banks of the Roses eller vad den nu hette. (Jag kunde den inte.)

Kvällens trubadurer var ovanligt påfrestande, så det var tur att vi satt en bra bit bort bakom ett hörn och kunde prata på trots det. Jolkkonen undrade om syndikalisterna och höll ett improviserat försvarstal för oktoberrevolutionen som kanske inte övertygade, men ändå var bättre än många andra i den genren jag hört. Det är onekligen ganska svårt att förklara varför ett maktövertagande som ledde till många miljoner oskyldigas förtidiga död var en Bra Sak (till skillnad från en Kul Grej).

Sten berättade om sin favorithögstadielärare vars två motton gjort starkt intryck på honom: "Ät alltid efterrätten först" och "Ta inte så hårt på det där med städning". (Hon hade en gång tappat bort en trehjuling i ett kök. Onekligen imponerande. Jolkkonen höll på att göra något lika imponerande en gång när han trodde att han tappat bort en stol i sin lägenhet, men sedan mindes han att han hade lånat ut den till sin far.)

Contact hade folk sett och tyckt ganska bra om (utom slutet) och min fråga om Jody Foster var sexig i filmen besvarades med ett Ja. Matthias har nu tagit filmmaskinistcert, så här har ni kongressarrangörer någon att bjuda på gratis resa och medlemskap om ni vill visa 35 mm på era kongresser! 2001 på stor duk i Upsala år 2001, kanske?

C.S. Lewis fyller 100 nästa år, vilket fick oss att diskutera Perelandra, mardrömmar, kristendom och Rush Limbaugh. Vi hade högst skiftande åsikter om dessa ämnen... nä, kanske inte om Limbaugh.

MURPHYS!? Ska vi inte flytta verksamheten till, i alla fall inte utan att ha observerat deras krumsprång ett tag till först. Fellini, föreslog Jolkkonen, så det får ni gärna grunna på. Tills vidare förblir det O'Connors, på gott och ont.

ex pulpetra
— _Johan_


Vi var där: Johan Anglemark, Mikael Jolkkonen, Linnéa Jonsson, Karin Kruse, Jan Nyström, Matthias Palmér, John Sören Pettersson, Sten Thaning, Kristin Thorrud, Anders Wahlbom, Björn X Öqvist

Även nästa möte blir således på samma plats (O'Connors pub, ovanför Saffets på Stora torget) från 18.30 och framåt, tisdagen 6 januari.

Väl mött!
