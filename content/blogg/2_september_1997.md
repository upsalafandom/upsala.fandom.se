+++
title = "2 september 1997"
slug = "2_september_1997"
date = 1997-09-02

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Klockan slog tre glas, kapten, sade den undersätsige dvärgen och slog sig ner bakom rodret på skutan O'Connors då Uppsalas science fiction-fans avverkade sitt nionde Orvarsmöte för året. Innan kvällen hade hunnit förvandlas till dag igen, hade vi dessutom lyckats hemsöka såväl alla Orvarsmötens ursprung, Orvars krog, Norrlands nations stolthet (om nu inte det är HP Burman...), som mitt och Linnéas hem.

<!-- more -->

För kvällen hade O'Connors rea på Old Peculier, vilket är en klassisk dryck för brittiska kufar. Nu vet jag inte om vi är klassiska brittiska kufar, men vi lyckades i alla fall göra slut på deras lager av Old Peculier.

Med sömgångaraktig säkerhet svingade sig våra diskussioner mellan tyngdlöst sex, prinsessan Diana och Guinness framtid i Sverige. Lennart delade ut inbjudningar till sin brors vernissage på Galleri Dombron på lördag.

En något förminskad skara nådde senare Orvars krog, där vi satt ute tills det blev för kallt, och vi gick in. När så Orvarspersonalen brutalt körde ut oss (ahh, vissa saker har inte ändrats på tio år) drog vi hem till undertecknad, där whisky, drinkar och animerade diskussioner om sf, fandom samt skillnaden mellan upplevelsen av en god bok och upplevelsen av bra sex präglade de sista timmarna.

Vid halv sex gick jag och lade mig och jag vet inte när de sista lämnade mitt hem, men jag noterade dagen efter att Sten tydligen hade ringt vid halv åtta, så då måste åtminstone han ha varit hemma.

Gårdagen vill jag helst glömma, men jag hoppas att vi ses om en månad igen, om inte förr!

ex pulpetra
— _Johan_


Vi var där: Johan Anglemark, Magnus Eriksson, Andreas Gustafsson, Karin Kruse, Jan Nyström, Markus Persson, Lennart Svensson, Sten Thaning, Wahlbom, Anna Åkesson, Björn Öqvist, Tomas "Baron"

Nästa möte blir på samma plats (O'Connors pub, ovanför Saffets på Stora torget) från 18.30 och framåt, tisdagen 7 oktober.

Väl mött!
