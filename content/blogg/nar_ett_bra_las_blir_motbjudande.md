+++
title = "När ett bra läs blir motbjudande"
slug = "nar_ett_bra_las_blir_motbjudande"
date = 2008-07-05

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Recensioner"]
taggar = ["Chung Kuo", "David Wingrove", "R. Scott Bakker", "moral", "psykopater", "våld"]
+++

Som jag skrivit tidigare har jag tagit mig an att läsa R. Scott Bakkers
bokserie _The Prince of Nothing_. Nu har jag kommit en bit in i första
volymen, 90 % eller så, och börjar ha åsikter.

För ett antal år sedan, i början av nittiotalet, var det en hel del snack om
David Wingrove och hans monumentala sf-serie _Chung Kuo_. Nu, år 2008, verkar
inte längre en framtid dominerad av Kina och kinesisk kultur lika spekulativ.
Som vanligt hinner verkligheten ikapp dikten. Ännu har vi inte megastäder
över hela jorden och en kejsare över jorden. Kanske kommer det med. Jag blev
i alla fall fascinerad och började läsa första volymen. I den fanns det en
person som begick de vidrigaste handlingar. Han var motbjudande till den grad
att jag slutade läsa, av äckel. Något senare när alla böcker kommit ut,
undersökte jag den sista volymen på _Uppsala English Bookshop._ Genom att
bara skumma baksidestexten stod det klart att den person som berört mig så
illa uppenbarligen fortfarande var mitt i händelsernas centrum och verkade
åtnjuta lycka och välgång. Det fick mig att lägga ner boken, bestämma mig för
att aldrig läsa den samt att om jag någonsin träffade David Wingrove skulle
jag slå honom på käften. Han hade våldfört sig på mig med de bilder hans bok
planterade i mitt sinne.

<!-- more -->

Nu vet jag inte om jag fortfarande känner lika starkt för alla de
föresatserna. Dock tänker jag inte läsa böckerna. För mig är det faktiskt så
att en bok eller en film där någon skitstövel, en riktig vidrig typ, lyckas
och frodas när de hederliga och hyggliga går under, helt enkelt inte går hem.
Jag kräver någon sorts moralisk rättvisa. Kanske kan det anses både lite
omodernt och lite banalt, men sådan är jag. Om inte den person som utsätter
andra för lidande blir straffad eller konfronterad med det faktum att han
förlorat sin mänsklighet, kan jag inte se verket som konst. Även
underhållningsliteratur måste för mig ha någon sorts konstnärlig moral.

Som det sades på en panel under Ad Astra, om man sätter eld på en kartong med
ett spädbarn och filmar det så är det inte en skräckfilm. Det måste finnas en
moral. Nu kom jag åter att tänka på dessa ting efter att ha läst ett avsnitt
av _The Darkness That Comes Before_.

I denna bok finns det en man, en scylvendi, ett folk som påminner lite om
prärieindianer eller mongoler. Denne man är en stamhövding som heter Cnaiür
urs Skiötha. Han är en mördare, vilde och totalt psykpopatiskt paranoid. Det
är ingen spoiler, det framkommer direkt. Dessutom anser han, likt resten av
scylvendi, att de som inte är scylvendi är offerlamm på krigsgudens altare.
Om andra ord anser de att alla andra än de bara är till för att slaktas,
dräpas och offras. Sällan har man hört om en mer patologisk kultur, och jag
är inte ens säker på att Bakker inte hittat en sådan kultur på vår egen
planet. Detta kunde jag kanske fördra, men det sätt som Cnaiür beter sig, när
hans fruktan får utlopp i raseri och misshandel, fysisk och psykisk, så
börjar jag närma mig gränsen.

Hur långt kan en författare gå innan man som läsare tappar kontakten och
empatin med de man läser om? För mig känner jag att en person som är så
fixerad vid våld och död och med en så låg uppfattning om homosexuella och
kvinnor att han inte bara kokar inombords av tvångstankar, utan också vid
varje tillfälle lever ut dessa känslor i form av vidriga dåd av våldtäkt,
mord och tortyr, han är gränsen för mig. Misogyni samt sexuellt våld är
liksom inte underhållning. Våld mot kvinnor verkar vara ett snabbt sätt att
få mig att må illa.

Vad gör man när man känner att en person i en bok är en galen hund som borde
avlivas för att göra världen till ett bättre ställe? En del personer vill i
sådana här tillfällen säga att det bara en är främmande kultur, och att
poängen är att jag **ska** bli upprörd. I sådana fall börjar jag undra om
inte den moraliska poängen, om sådan finnes, dränks i den motvilja man känner
som läsare?

Jag har inte bestämt mig för att lägga ner boken än, eller att bestämma mig
för att lämna de fortsatta volymerna därhän. Det finns onekligen en
balansgång att gå som författare. När kan man fläska på med osmakligheter,
och när blir de helt enkelt för mycket?

Vi får se hur det går.
