+++
title = "Kanadensisk fantasy – inte som i Europa"
slug = "kanadensisk_fantasy_inte_som_i_europa"
date = 2008-06-25

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["böcker", "fantasy"]
+++

Jag har precis börjat läsa en nya bokserie, av en kanadensare som heter R.
Scott Bakker. Han är visst kompis med Steven Erikson, känd för sin bokserie
The Malazan Book of the Fallen, som även han är från Kanada. Den senare har
ju rönt viss uppmärksamhet för sin bokserie av flera skäl. Ett är ju förstås
att han skrivit kontrakt på tio böcker, och när den första kom ut och var
över tusen sidor tjock blev man ju mörkrädd. Han är produktiv, Steven. Vi
fick en CD på en påskkongress en gång där han läste ett kapitel från en av
sina böcker. Det befäste min åsikt att författarläsningar är sövande och
oerhört meningslösa.

Bakker är även han en av dessa författare som planerar en lång serie. Han har
inte ens bestämt hur många böcker det ska bli. Huga! Vi får se om jag orkar
läsa mer än en bok...

Nåja, i alla fall har den en sak gemensamt som jag finner lite intressant.
Bägge skriver fantasy, men de skriver om världar som inte bygger på någon
pseudomedeltida värld stöpt efter Europa. Istället är det Rom, Babylon och
diverse orientaliska och asiatiska kulturer som står som modell. Fantasy
handlar ju ganska mycket om att se och uppleva främmande och exotiska
miljöer, så jag finner det numera lockande när folk skriver om sådana
kulturer som för mig ger just dessa exotiska kickar.

Än har jag bara läst två kapitel och en prolog, men har redan flera gånger
tänkt på Glen Cooks _The Tower of Fear_ och Steven Brusts utmärkta böcker om
Vlad Taltos. Vi får se hur länge det är fräscht och intressant. Watch this
space.

Även jag undrar lite stilla hur det kommer sig att de som skapade denna
webbplats och ville ha en blog inte skriver nåt?

_För övrigt anser jag att regeringen bör avsättas och FRA sprängas, med
bomber, Al Aqaida, sprängämnen, atombomber och andra nyckelord som de lär
trigga på. Sug på den spiondjävlar!_
