+++
title = "Barsoon i våra hjärtan"
slug = "barsoon_i_vara_hjartan"
date = 2009-04-13

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Barssom", "Edgar Rice Burroughs"]
+++

Nu har jag läst två volymer av Edgar Rice Burroughs kända Barsoom serie. Det är märkligt vad autodidakter kan producera märkliga verk när de ger sig hän. Den första volymen skrevs av en Burroughs som hade prövat det mesta, och nu var pank och desillusionerad. Klart att då kan man ju kanske lika gärna skriva en bok!

Som förstligsverk är den märkvärdigt trollsk och ett intressant stycka eskapistisk pikaresk med märkliga främmande varelser och underliga seder. Den åstadkommer mycket i kraft av sin fantasi och den förundran över en främmande värld som han lyckas frammana av rent längtan efter nåt annat än den värld han befann sig i.

Dock märker man att de här gamla äventyshistorierna har lite av ett problem. När jag läste Williamsons förfärliga Legionböcker insåg jag att det hörn som han målat sig in i bara blev mer och mer trångt när han måste hitta på mer och mer fantastiska omöjligheter för hjältarna att besegra. Nu när jag har läst om inte bara gröna, svarta, vita utan ocksåröda marsianer så kommer jag åter att tänka på det där inmålade hörnet.

Det är inte utan man undrar om det inte är svårare att skriva billiga äventyr än seriösare saker, ibland. Dock har de något de här taffligt skrivna berättelserna om män som kan allt och fäktas med de värsta monster, blir kharg, jeddad, kung, gatsputj, kejsare eller bara rik som ett troll. De får princessan och alla älskar dem. Kanske behöver man inte mer av en bok ibland. Även om jag känner mig präktigt less på  Edgar Rice Burroughs så är det ju tack vare de här historierna som t.ex. Michael Moorcok började skriva fantasy. Det vore ju tråkigt att vara utan.
