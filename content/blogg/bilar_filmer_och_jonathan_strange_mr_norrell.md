+++
title = "Bilar, filmer och Jonathan Strange & Mr Norrell"
slug = "bilar_filmer_och_jonathan_strange_mr_norrell"
date = 2008-02-28

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Kåserier"]
taggar = ["Susanna Clarke", "bilar inom sf", "rysk sf"]
+++

Det är ju egentligen på Onsdagar som jag ska skriva mina blogginlägg här, men eftersom det redan ligger två Onsdagsinlägg på Bloggen tar jag och skriver nu istället...

Innan jag skriver har jag naturligtvis funderat på VAD jag ska skriva om. Jag kom på att jag på något vis måste låta det faktum att jag har köpt [en helt ny bil](http://www.renault.se/sv/Personbilar/Clio-Storia.aspx) framkomma och att detta, att jag idag ska hämta ut min lilla pärla, har fått mig att reflektera över bilar i SF. Och så skulle jag skriva ett briljant inlägg om "Bilarna inom SF". Där ni som läser detta skulle ha begripit dels att jag köpt en ny bil och dels att jag lagt märke till bilanvändningen inom SF. Men hur jag än funderar kommer jag inte på något speciellt SF-bilande förrutom den gula taxin och alla polisbilar i filmen "Det femte elementet".  De bilarna är ju inte speciellt annorlunda än dagens bilar förutom att de flyger då...

(Här skulle jag då lägga in en häftig bild från teknade filmskriptet med en bil på om jag begrep hur man får in bilder)

(Och här var det en bild på [Milla Jovovich](https://www.imdb.com/name/nm0000170) när hon ser grym ut...)

Nåväl, då skriver jag väl om filmerna då när nu jag inte lyckas alls med biluppslaget.

Jag var sugen på att se SF på film och gick till Akademibokhandeln här i Uppsala (eftersom jag missat vilken dag bokrean egentligen började på...). En hjälpsam man tipsade genast om två Ryska filmer, del I och del II som bägge fått bra kritik. "Lite lika the Matrix-filmerna men med ännu häftigare specialeffekter och dessutom Rysk" var de ord som fick mig att falla och köpa bägge två. Väl hemma från stan bänkade jag mig framför TVn, skruvade upp sound-surround-ljudet och beredde mig på att njuta.

– HJÄLP, vad ÄR detta?

– Vampyrer?

– Drickande av blod??

Ett slagsmål med en vampyr som slåss med en sax och flippar in och ut ur dimensionerna så att han inte syns mesta delen av slagsmålet? Jag tordes inte titta!

Efter att ha suttit och kikat på filmen genom springor mellan fingrarna så sjönk gradvis händerna ned i knät och jag var fångad. Vilken värld som målas upp, intelligent, rolig, spännande, skitig, fantasisk och helt annorlunda än vad jag någonsin sett. Så långt ifrån The Matrix som man kan komma? Filmerna heter "[Night watch](https://en.wikipedia.org/wiki/Night_Watch_%282004_film%29)" och "[Day watch](https://en.wikipedia.org/wiki/Day_Watch)" och baseras på böcker av författaren [Sergei Lukyanenko](https://en.wikipedia.org/wiki/Sergey_Lukyanenko). Jag såg bägge filmerna i ett sträck och hade det inte varit så sent efter jag var färdig med "Day watch" skulle jag ha sett om dem bägge två på en gång. Men HUR kunde försäljaren tro att det var SF? En gåta! På många av sidorna jag läst såhär i efterhand om filmerna så är de klassade som "Horror movies", och hade jag vetat DET skulle jag aldrig ha köpt dem. En värre mes än jag när det blir lite otäckt på film kan man svårligen finna. Jag är glad att jag inte visste det innan för filmerna var en upplevelse av stora mått att se och jag kan varmt rekommendera dem.

Och så lite om bokrean... Jag har ju fuskat och suttit och beställt böcker på nätet från årets bokrea. Bokus hade nämligen ett erbjudande att man fick "dubbla poäng på allt" och "gratis frakt" som var omöjligt för mig att motstå. Så när jag gick ned till bokrean (på rätt dag nu) hade jag bara tänkt "titta lite".

Jag läste häromdagen i ett [blogginlägg](./blogg/favoritforfattare.md) av [Johan Anglemark](/forfattare/anglemark)  att han läste allt av [Susanna Clarke](https://en.wikipedia.org/wiki/Susanna_Clarke) och när jag Googlade för att ta reda på mer om henne fann jag boken [Jonathan Strange and Mr Norrell](https://en.wikipedia.org/wiki/Jonathan_Strange_%26_Mr_Norrell) som genast intresserade mig. Döm om min glädje när jag ser den boken på bokrean när jag lite planlöst irrade omkring på Akademibokhandeln här i Uppsala. Jag tog det genast som en fingervisning att DEN boken måste jag köpa, glöm att jag går och väntar på ett stort paket från Bokia. Först skriver Johan Anglemark att författaren är en av de som han "läser allt när det kommer ut" av, sen ramlar jag på den på bokrean, och om inte DET är ett "tecken" får du kalla mig blind. Eftersom jag läser mina böcker på Engelska letade jag upp den Engelska versionen av boken för att köpa den. Den var inte på bokrean men ett tecken ÄR ett tecken. Men vilken bedrövelse. Boken är snudd på en decimeter tjock! Jag har tyvärr små händer som dessutom alltid är "onda" av artrit så jag kan inte hålla i böcker av den formidabla digniteten. Jag får vänta på en nyutgåva då de av sympati med mig kapar upp boken i en trilogi.

Så, det var mitt bloggiinlägg. Hur jag velade runt bland bilar, filmer och Jonathan Strange and Mr Norrell för att komma på vad jag skulle skriva om...

;o)
