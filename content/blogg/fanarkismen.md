+++
title = "Fanarkismen"
slug = "fanarkismen"
date = 2008-11-27

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["Imagine", "anarkism", "fanarki", "fandom", "föreningar", "organisation"]
+++

Varför detta motstånd inom fandom mot att organisera sig? Den unga svenska fandomen var under de första åren på femtiotalet fylld av entusiastiska ungdomar som gång på gång försökte att skapa rikstäckande/skandinaviska föreningar eller federationer. Samma fenomen drabbade den amerikanska fandomen, och där lever fortfarande det mest framgångsrika försöket kvar, i form av [National Fantasy Fan Federation](http://www.n3f.org) (som inte innebär att det är en organisation för fantasy, _fantasy_ var ett vanligt samlingsbegrepp på den tiden för det vi sedan några år tillbaka kallar fantastik, dvs sf, fantasy och övernaturlig skräck). Framgångsrikt, kallar jag det, och det var N3F förvisso, men det blev aldrig någon succé inom amerikansk fandom, utan började ganska snart föra ett eget liv, med medlemmar som ofta i övrigt inte hade någon kontakt med _mainstream fandom_.

<!-- more -->

Amerikanska N3F slog som sagt aldrig an bland fans i gemen, och detsamma gäller de olika svenska försöken. Bäst lyckades här Skandinavisk Förening för Science Fiction, men det beror nog mest på att föreningen hade lärt sig av de tidigare storvulna och överambitiösa projekten att samla alla fans i en och samma förening, och nöjde sig med att ge ut fanzines och ha små lokala möten. Dvs, fungerade som vilken förening som helst. [SFSF](http://sfsf.fandom.se) lever fortfarande men är idag i stort sett bara en Stockholmsförening med måttlig aktivitet.

Här i Uppsala har vi under många år diskuterat att bilda en rejäl förening. Vi bildade ett slags förening 1985, som aldrig organiserades formellt utan mest har existerat som ett alias för vårt kaotiska och anarkistiska nätverk av fans, utan organisationsnummer, styrelse och sådant där. Det har funnits förespråkare för en "riktig" förening, men de har alltid varit i minoritet. Frågan är varför. Det finns onekligen fördelar med en formell organisation. En förening har en klart definierad verksamhet och förhoppningsvis stabilitet. Den är lätt att förstå sig på. Man behöver inte fundera på hur man kan aktivera sig. Det finns något att syssla med, något att jobba sida vid sida med andra medlemmar om. Föreningar lever sitt eget liv.

Ändå har svensk (och angloamerikansk) fandom varit skeptiska mot det organiserade. Har vi en romantisk, kanske förljugen, självbild av att vara fria obyråkratiska själar? Är vi lata? Vill vi slippa strider om ur verksamheten ska utformas och om vem som ska bestämma? Eller inser vi helt enkelt bara inte vad vi kan uppnå med stadgar, organisationsnummer och styrelse som vi inte kan uppnå utan byråkrati? Jag vet inte. Jag är skeptisk till föreningar i fandom, och ändå har jag varit och är med i flera föreningar som aldrig hade fungerat som lösa nätverk (tror jag) - Tolkiensällskapen.

Dessa funderingar är naturligtvis föranledda av [diskussionen om Imagine](http://fandom.se/forum/viewtopic.php?t=1167) på fandom.se:s forum.
