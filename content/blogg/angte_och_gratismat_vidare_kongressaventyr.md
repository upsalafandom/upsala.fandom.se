+++
title = "Ångte och gratismat: vidare kongressäventyr"
slug = "angte_och_gratismat_vidare_kongressaventyr"
date = 2009-04-02

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Rapporter"]
taggar = ["kongresser", "skrivande", "steampunk"]
+++

Jag har förlorat en del av mitt [exended mind](http://annien.wordpress.com/2009/01/27/the-extended-mind): jag hittar inte min anteckningsbok. Jag brukar alltid bära med mig en sån, och där skriver jag ner alla saker som faller mig in: ideer, citat, att göra-listor, utkast till olika texter, minnesanteckningar, böcker jag vill läsa, och så vidare. Var är min anteckningsbok? Frustrerande. Nåja, några spridda noteringar från helgens kongress kan jag nog åstadkomma ändå.

För det första måste jag instämma med Ante om att det var mer välorganiserat i år. Alla verkade vara informerade om vilka paneler de skulle vara med i, och programmet ändrades inte mycket efter att kongressen börjat. Det var intressantare också (enligt min smak), med fler programpunkter som handlade om ideinnehåll och vad man kan göra med litteratur. Jag såg till exempel en panel som handlade om vad ondska egentligen är, och vad den har för roll i sf och fantasy. Och så var jag med i en panel om kolonialism och sf, som bland annat handlade om i vilken utsträckning sf är propaganda för kolonisering av rymden. Jag tycker förstås att paneler om att skriva och publicera är intressanta också.

Det var mycket steampunk i år, ännu mer än förra året. Volanger och mässingsknappar och mysko hembyggda manicker. Det fanns en ång-modevisning, som jag missade. Paret Penney hade konverterat sitt traditionella roomparty till "steam tea", och bjöd på kakor på silverfat i flera våningar. De hade till och med tagit med sig några fina koppar i tunt porslin med guldkant och blomdekorationer, fast inte tillräckligt till alla gäster. En snubbe som jag tror är någon sorts chef för vetenskapsmuseet demonstrerade sina hembyggda tebryggningsapparater -- mycket snygga, men inte särskilt praktiska. Han hade en strålpistol i bältet också, som han nog hade byggt själv.

En fördel med att vara på den här kontinenten är ju att det finns stort befolkningsunderlag inom rimligt avstånd och med minimala språkbarriärer. Det är många författare, många experter, och tillräckligt många fans. Även på en sån här rätt liten kongress (ett par hundra medlemmar, kanske) ser man alltid Robert Sawyer hålla hov (det är hans hemmakongress) och det finns alltid kontakter att ta av för att ha ett solitt vetenskapsprogram. Jag är lättimponerad och beundrar folk som kan kasta ur sig siffror och redogöra exakt för hur många ton per år man kan lyfta till omloppsbana med olika typer av teknologi, och sånt. 

Jag undrar fortfarande lite vad jag egentligen får ut av kongresser. Det känns som om jag tillbringar väldigt mycket tid med att irra omkring och känna mig vagt stressad över att jag inte utnyttjar tiden ordentligt när en kongress är så kort och snart över. Ibland råkar man ju ut för missöden också, som när hotellbarens personal var alldeles sönderjäktad, och jag inte fick min mat förrän dottern redan ätit färdigt -- och sen fick jag något som inte var vad jag hade beställt. De är nog inte vana vid att ha full ruljans i den där baren. Tyckte nästan synd om dem, men när de sade att jag inte behövde  betala lät jag gärna bli.

För övrigt tyckte jag att panelen om att redigera sina egna texter var väldigt bra. Jag tillämpar den inte just på sånt här skrivande, men ska nog ha nytta av ett eller annat knep framöver. Panelen rekommenderade också Robert Silverbergs bok Science Fiction 101, som Ante påpassligt köpte förra året. Den ligger nu på min allt högre och vingligare att läsa-hög brevid sängen.

Och dit går jag nu. Godnatt!
