+++
title = "Påsknoveller"
slug = "pasknoveller"
date = 2009-04-09

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier", "Recensioner"]
taggar = ["noveller"]
+++

Nu blir det påsk, och är man i närheten av de brittiska öarna kan det hända att man åker på [Eastercon](http://lx2009.com) över påsk (om man inte är en sån som alltid är på GothCon, kanske, eller stannar hemma och sparar pengar i väntan på bättre tider). Där kommer [BSFA](http://www.bsfa.co.uk) att dela ut sina priser. 

Jag hittade tre av de nominerade novellerna på StarShip Sofa, och [via Vector-bloggen](http://vectoreditors.wordpress.com/2009/03/28/the-rest-of-the-mailing) hittade jag även den fjärde från Transmissions from Beyond. Den här veckan har jag lyssnat på dem alla fyra på väg till och från jobbet. Jag gillade dem allihop. Jag har inte tid att skriva så mycket om dem nu, men jag länkar till vad Vector-redaktörerna har samlat ihop för kommentarer -- det räcker och blir över. Fast vill man verkligen inte ha några spoilers rekommenderar jag att man läser eller lyssnar först.

“Evidence of Love in a Case of Abandonment” av M. Rickert är en väldigt starkt berättad historia om en flicka i en ganska osannolik dystopisk framtid. Temat är väl lite politiskt tillspetsat, särskilt om man har kontakt med somliga kretsar som vi kanske inte ser mycket av i Sverige, men det kommer liksom inte i vägen för att den är väldigt bra skriven med ett beklämmande perspektiv inifrån. [Text här](http://www.sfsite.com/fsf/fiction/mr01.htm), [ljud här](http://www.starshipsofa.com/20090226/starshipsofa-bsfa-nominee-2008-mary-rickert). [Vad folk säger.](http://vectoreditors.wordpress.com/2009/04/02/bsfa-nominee-evidence-of-love-in-a-case-of-abandonment)

“Little Lost Robot” av Paul McAuley handlar om identitet och moral. Och om öde. Och en väldigt kraftfull robot. Mer säger jag inte. [Text (pdf) här](http://ttapress.com/LittleLostRobot.pdf), [ljud här](http://www.starshipsofa.com/20090227/starshipsofa-bsfa-nominee-2008-paul-mcauley). Och återigen: [vad folk säger](http://vectoreditors.wordpress.com/2009/04/03/bsfa-nominee-little-lost-robot).

“Exhalation” av Ted Chiang är en sån där typisk tankelek som han brukar skriva -- lite av ett pussel för läsaren att lista ut vad det är för sorts värld den utspelar sig i. Slutar med att berättaren utbrister i en utläggning av sense of wonder. Ganska charmig, men absolut inte någon av hans intressantaste noveller. [Text här](http://nightshadebooks.com/cart.php?m=product_detail&amp;p=124) (längst ner på sidan finns länkar till olika format), [ljud här](http://www.starshipsofa.com/20090227/starshipsofa-bsfa-nominee-2008-ted-chiang), och [vad folk säger](http://vectoreditors.wordpress.com/2009/03/31/bsfahugo-nominee-exhalation).

“Crystal Nights” av Greg Egan är en sån där historia om AI och önskan att skapa intelligent liv att efterträda oss. Vad kommer ens skapelser att tycka om en? Massor av etiska ställningstaganden och kompetent technobabble, som leder upp till ett typiskt sf-slut. [Text här](http://ttapress.com/553/crystal-nights-by-greg-egan), [ljud här](http://transmissionsfrombeyond.com/2008/09/transmission7), och [vad folk säger](http://vectoreditors.wordpress.com/2009/04/01/bsfa-nominee-crystal-nights).

Utan att säga särskilt mycket alls skulle jag vilja utnämna "Little Lost Robot" till min favorit och hoppas att den vinner. 

Håll till godo. (Den här postningen kom lite sent, eftersom jag var lite för trött igår. Eftersom ingen annan håller sitt schema för postningarna antar jag att det inte kommer surt efter.)
