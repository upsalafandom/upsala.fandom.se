+++
title = "Liverapport fran Montreal"
slug = "liverapport_fran_montreal"
date = 2008-10-18

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Rapporter"]
taggar = ["Montreal", "Worldcon", "fanzines", "kongress"]
+++

(Detta skrives på en snorlångsam lina och en offentlig dator utan svenska tecken.)

Nu är det kongress igen! Denna gång är familjen Davour i Montreal. Kongressen heter ConCept och de har ett dealers room med ganska lite böcker, men det finnes smycken, henna-"tatueringar" och en massa plastfigurer att köpa.

Folk ser ut som de brukar på kongresser med lite kostymer och märkliga knappar och "roliga" hattar. Dessutom en person som inte kan gå så bra med pokemonväska som sitter i programrummen och mummlar och pratar om dysfunktionella judar. Alla får vara med i fandom.

Programmet är lite försenat i en av lokalerna och just nu sitter David Brin där och bräker. Han var inte så rolig att lyssna på tyckte jag. Han verkar tycka om sin egen röst, dock.

Vi har sett lite på stan och vi bor på ett hotell en kort bit bort från kongresshotellet. Ganska lagom. När vi kom igår gick vi runt och tittade på medan de byggde ihop kongressen och flyttade bord. Vi hittade en lokal i källaren där de hade art show och fanzine lounge! Kul! Absurt nog visade sig när man på måfa tog ett fanzine (Prolapse) och bläddrade så fick jag syn på en bild på Sture Sedolin! Vi svenskar finns överallt.

Det är kul med kongress och på den här har de ett freebiebord med gamla nummer av Analog och Asimovs! Schysst.

Nu är vi också medlemmar av nästa Worldcon, i Montreal nästa år. Ses vi där?
