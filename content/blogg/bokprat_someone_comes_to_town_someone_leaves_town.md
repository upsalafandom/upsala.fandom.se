+++
title = "Bokprat – Someone Comes to Town, Someone Leaves Town"
slug = "bokprat_someone_comes_to_town_someone_leaves_town"
date = 2008-03-28

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Inbjudningar"]
taggar = ["bokprat"]
+++

Onsdagen 16/4 är datumet för nästa bokpratsmöte. Den aktuella boken den här
gången är Cory Doctorows _Someone Comes to Town, Someone Leaves Town_. Skulle
man inte vilja lägga sina surt förvärvade slantar på att köpa den, eller vill
beställa hem den men tycker att det är lite ont om tid, så har författaren
lagt upp den för gratis nedladdning i en lång rad format
[här](http://craphound.com/someone/download.php).

Bok: _Someone Comes to Town, Someone Leaves Town_, Cory Doctorow.
Tidpunkt: 16/4 klockan 19:00.
Plats: Hemma hos mig (Johan Jönsson), Marmorvägen 9A, femte våningen.
