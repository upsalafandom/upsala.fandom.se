+++
title = "Liverapport från Montreal del 2"
slug = "liverapport_fran_montreal_del_2"
date = 2008-10-19

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Rapporter"]
taggar = ["Montreal", "fandom", "kongresser"]
+++

Nu är det sista dagen på kongressen. Igår var det maskerad efter jag skrev sist. Rebecka hade fått vara i ett lekrum med de andra barnen under dagen. Det var en trevlig innovation som gjorde kongressbesökandet mer fritt för oss med barn. Barnen skulle också få vara med i maskeraden i en liten fashionshow. Rebecka fick vara prinsessa med sin lilla mjukiskatt i mantel och allt. Riktigt festligt.

<!-- more -->

Till skillnad från traditionen i många amerikanska kongresser är maten i con suit inte gratis, men trots detta och det faktum att den ar väldigt liten så verkar det vara det som är alternativet till den icke existerande baren. Det vill saga det finns en hotellbar, men där finns ju inte en käft. Det är annorlunda med kongresser på den har sidan polen.

Det känns lite som den har kongressen forsöker vara större än den är, med maskerad, con suite, flera programspår och grejer. Vi såg faktiskt en person på filkkonserten som hade en skylt med "5 minutes" på, så de har lite styrsel på programmet även om det ibland känns lite bångstyrigt.

Nu har jag sett David Brin några ganger. Han är onekligen väldigt bra på att ta alla tillfällen att promota sig själv. Jag kan fatta om man tycker han är lite odräglig. Han säger en hel del bra saker, men han gör det onekligen på ett ganska kantigt satt.

Igår var det room party for Seattle in 2011, men jag var toktrött så jag och Rebecka somnade. Åka gick tillbaka till kongresshotellet där de hade dans. Vi röjde loss på Octocon i Dublin for några år sedan. Onekligen en lite ovanlig sak för våra kongresser hemma.

Nere i lobbyn när vi kom in idag så kunde jag även se de sista 10 varven på kinas grand prix, det var ju lite skoj. Nu ska vi se om de vaknat och kickat igång den här kongressen snart. Det ryktas att dagens program ar försenat innan det startat. Sånt händer.

Mannen som sitter och pratar för sig själv har slutat prata om judar och han har en ny ful hatt.
