+++
title = "Bokrean – en kompromiss mellan förnuft och hagalenskap"
slug = "bokrean_en_kompromiss_mellan_fornuft_och_hagalenskap"
date = 2008-02-27

[taxonomies]
forfattare = ["Torbjörn"]
kategorier = ["Kåserier"]
taggar = ["böcker"]
+++

Så! Hur många av er andra känner som mig just nu:

* Lätt nöjdhet över vissa kap/semikap/roliga slumpfynd
* Viss skamsenhet över den stora chansen att vissa av de nyinköpta böckerna inte kommer att orkas läsas, och kanske inte var jätteprisvärda

Här är den något inkorrekta listan över de försyndelser jag begått mot min bokhylla och plånbok sedan igår, och deras diverse motivationer och framtidsutsikter:

* **Yoga {nånting nånting}** – 39 kr – Tja – jag gick ju på Yoga i ett år, så kanske kan jag kolla om det är nån vana jag kan få för mig att tänka mig att fundera på att utöva i hemmets ro – om bara katterna kan förmås att hålla sig utanför det hela
* **En kärleksförklaring till vårt svenska matarv** – 49 kr – Mycket seriöst prat och fina bilder runt klassisk svensk matlagning – man har säkert missat en massa höjdare! Kommer att slöläsas till helgfrukosten i min kökssoffa
* **{nånting} Art Nuoveu** – 149 kr – Det är alltid svårt att hitta bra konstböcker, och denna verkar vara en helt ok en, som handlar om en av mina absoluta favoritstilar. Samma som ovan; jag måste skaffa en bokhylla i köket också!
* **Riddarbageriets bröd** – 99 kr – En riktig "Manskokbok", med bistra men vänliga kockherrar som visar hur man gör Riktigt bröd. Snart är kvinnosläktet helt utträngt från köken, och då återstår bara att komma på hur vi gör barn själva för att äntligen få överhanden i den husliga maktkampen
* **Wokboken {typ}** – 99 kr (?) – Den vanliga samlingen med recept med allt för många pilliga ingredienser i för att man ska nyttja den annat än till fest; men när jag läser "karameliserat fläsk" plockar jag tydligen fram plånboken utan betänkligheter. Livet är kort, och "karameliserat fläsk" måste undersökas omedelbart
* **{nånting nånting} Tai Chi** – 39 kr – Tja, jag har alltid varit nyfiken, och kanske kan jag blanda ihop detta med Yogan och komma fram till ett helt nytt sätt att.. förbrylla mina katter
* **100 stora tänkare** – 99 kr – Ett uppslag om varje tänkare, typ. Antagligen tillräckligt mycket för att göra mig förvirrad angående vartenda en av dem. Minst två upplag om varje borde det väl nästan vara? – Men jag hoppas ändå få ut något av den.. hoppet om allmänbildning hägrar! Kanske kan jag lyssna på klassisk musik medans jag läser den?
* **Dag Hammarskjöld, biografi** – 69 kr(?) – Jag kan inte minnas att jag läst en enda biografi.. kanske är de jättebra? Dag Hammarskjöld borde väl vara en intressant person att läsa om i vilket fall
* **Human Rights, by {någon}** – 49 kr – Denna verkade seriös, undrar bara om jag är tillräckligt seriös för att ta mig igenom den? Köpet var dock spikat när jag såg att den fanns både i paperback och hardback för samma pris.. alltså måste det betyda att jag gör någon slags vinst om jag då köper den i hardback?!.. vi kan bara hoppas.
