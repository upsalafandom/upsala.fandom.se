+++
title = "Att finna sin plats — att hitta hem"
slug = "att_finna_sin_plats_att_hitta_hem"
date = 2008-11-26

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["fandom"]
+++

På engelska heter det _finding one's tribe_. Jag tror inte att alla människor
får uppleva den känslan, och jag undrar om den dessutom inte är knuten till
det ungdomliga psyket – en facett av nyfikenheten, naiviteten och den
stormande entusiasmen som sällan återfinns över 25 års ålder. Jag tror också
att den hör samman främst med tankens sysselsättningar.

<!-- more -->

Jag talar alltså om den där känslan av inte bara att en ny värld har öppnat
sig för en, utan att man har hittat ett sammanhang där man hör hemma. Att
finna tro är säkert besläktat; jag vet inte, jag har aldrig gjort det. Det
närmaste jag har kommit var känslan av meningsfullhet och samhörighet jag som
17-åring kände vid ett studiebesök på Krishnakollektivet. Den känslan tror
jag är snarlik den att finna tro, och det är inte riktigt detsamma som
_finding one's tribe_, som bättre beskriver det jag far efter än "att hitta
hem", eftersom det framför allt handlar om människorna man kommer i kontakt
med. (När man finner Gud tror jag däremot att "hitta hem" är klockrent rätt
beskrivning av känslan. Och "hitta hem" tror jag inte heller är lika
förknippat med ungdomen, tvärtom är det nog vanligare bland äldre människor –
"till slut har jag hittat hem" hör man ibland folk säga.)

Att hitta fandom var för många av oss denna känsla. Jag upplevde den nog
egentligen starkare när jag gick med i Forodrim, Stockholms Tolkiensällskap,
och sedan ännu starkare igen när jag gick med i Uppsala Tolkiensällskap. Jag
upplevde den inte alls på mitt första spelkonvent, däremot. Det är en stark
känsla av att man har hittat ett sammanhang och en grupp människor där man
till slut passar in och vars verksamhet upplevs som intressant, viktig och
oändligt fascinerande. Man vill veta allt och engagera sig helhjärtat och
göra gruppen till sin egen. Det är lite som en social sense of wonder.

Kan man känna den i ett idrottslag? På en arbetsplats? Jag har mycket svårt
att föreställa mig det; för mig är känslan intimt förknippad med geekdom av
olika slag. Tankens verksamheter, som sagt.

Vad tror ni? Hur många av er har upplevt det jag talar om, och vad tror ni om
mina resonemang om ålder, typ av sammanhang och personligt kynne som
fenomenet är förknippat med?
