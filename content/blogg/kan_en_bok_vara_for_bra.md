+++
title = "Kan en bok vara för bra?"
slug = "kan_en_bok_vara_for_bra"
date = 2008-09-16

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Recensioner"]
taggar = ["Douglas Clegg", "litterära kvaliteter", "skräck"]
+++

Jag läste ganska nyligen boken _Naomi_ av **Douglas Clegg**. Författaren har
vunnit både Bram Stoker och International Horror Guild Award, så jag blev
lite nyfiken. Dessutom var den ganska billig.

Det stod ganska snart klart varför han blivit belönad med horrorgenrens
finare priser, för han kan skriva riktigt bra. Ett par stycken tyckte jag
faktiskt var så bra att jag visade dem för Åka att läsa utan att försöka
förklara sammanhanget, utan bara låta henne se hur han uttryckte sig. Jag
blev imponerad.

Nu är förstås frågan vad man ska leta efter i en bok som denna. Den är
marknadsförd som skräck, och handlar definitivt om "mörka" teman och ting.
Ändå är den lite speciell, för flera gånger börjar Clegg bygga upp ett
mysterium och antyda att det finns något hotfult och dolt. Vid ett par
tillfällen lägger han till och med upp kapitelavsluten med en cliffhanger.
Men, sedan gör han inget av det!

Faktum är att detta nog är den _minst_ stämmningsfulla skräckroman jag läst
på länge, utan att detta berott på att den är dålig! Den är helt enkelt för
bra.

Kan man som skräckförfattare skriva en bok som är "för bra"? Kanske. Om man
har en utmärkt språkkänsla och kan skriva engagerande om människor och deras
öden, men väljer att göra detta med skräckens alla attribut som spöken och
gengångare utan att göra sitt yttersta för att bygga upp en olustig
stämmning, har man då skrivit en bra bok? Egentligen?

"Utmärkt! Men inte alls så värst bra." Det är mitt mycket paradoxala
utlåtande. Mycket märklig upplevelse.
