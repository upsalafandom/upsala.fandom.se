+++
title = "Klassifikationssystem"
slug = "klassifikationssystem"
date = 2008-07-08

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["Dewey", "SAB", "klassifikationssystem"]
+++

Tim Spalding, LibraryThings grundare, har länge gått i tankar och funderat på ett klassifikationssystem för att ersätta det proprietära, föråldrade och ganska krångliga Deweysystemet. Efter årets American Library Association-konferens har han fått för sig att sjösätta tanken: [Open Shelves Classification](http://www.librarything.com/thingology/2008/07/build-open-shelves-classification.php), ett opensourceprojekt som LibraryThing nu söker frivilliga att driva.

Nu använder vi i och för sig [SAB-systemet](https://sv.wikipedia.org/wiki/SAB) hemma men det skulle ändå vara kul att betatesta det system som de kommer fram till, även om en ständig omsortering av alla böcker skulle kunna tänkas sluka enorma mängder tid.
