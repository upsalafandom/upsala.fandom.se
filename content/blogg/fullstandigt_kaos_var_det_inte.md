+++
title = "Fullständigt kaos var det inte..."
slug = "fullstandigt_kaos_var_det_inte"
date = 2004-11-02

[taxonomies]
forfattare = ["Nicklas"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

...men någon spillde öl på protokollet. Det gick att rädda, hörnet som blev vått torkade på under en minut. Vilket firades med mer öl.

Protokoll för Upsala WilliamsMöte som hölls på Williams Pub den 2/11 -04

<!-- more -->

Närvarande: J Nicklas Andersson, August Aronsson, Åke Bertenstam, Ragnar Hedlund, Mikael Jolkkonen, Torbjörn Josefsson, Maria Jönsson, Daniel Luna, Gabriel Nilsson, Karl-Johan Norén, Therese Norén, Alexander Stamou, Sten Thaning, Erik Trulsson, Karolina Wennemyr, Samuel Åslund, Dessy, Torbjörns storvuxne vän

Noterade icke-närvarande: Johan Anglemark, Andreas Davour, Anna Davour, Björn Lindström, Biörn X Öqvist, Neo-Mattias

1. Mikael Jolkkonen förklarar Mötet öppnat klokcan 18:15.

2. Alla nyss inkomna från kylan skaffar öl och dricker.

3. Irländare bör sluta stjäla Jolkkonens öl. [f:2 / e:0 / a:1]

4. Protokollet från förra Mötet läses upp, men ett veto läggs in i sista stund.

5. Pikter ska få allmän rösträtt på Mötet, vilket innebär att de har rätt att rösta på allt som påverkar Mötet men inte mot det som gäller utanför. [f:2 / e:0 / a:2]

6. Debatt om huruvida Mötet öppnas 18:00 eller 18:30.

7. Även om all tillgänglig information säger 18:30 protesterar Jolkkonen i och med att det är tradtitionsbrott och att någon har förfalskat och ändrat Mötets konstitution utan regelrätt revolusion.

8. Sluten röstning om vem som är skyldig: 2 för "Johan Angelmark" motiverat av att det är han som underhåller hemsidan. 1 för "Issac Asimov" med motiveringen att han är en usel författare och att det därmed är i dennes karaktär att skriva fel på det viset och slutligen 1 röst för "de har sjukt många Avesta-flaskor i kylen".

9. Förslag läggs fram på att antibiotika är en metafor för läsandet av telefonkatalogen.

10. Något om tungmetaller och avlopp och komposter.

11. Jolkkonen erkände sitt förflutna som torskmassmördare.

12. Lever är gott.

13. Röstning om att kött och lever visst är gott. [f:11 / e:1 / a:4]

14. Therese lägger in ett veto till giltligheten av föregående röstningsresultat

15. Luna tycker att de som inte är där ändå ska få rösta och att deras röster ska vara för under resten av Mötet. [f:13 / e:2 / a:4]

16. Johan Anglemark bör inte få vara sjuk. [f:19 / e:0 / a:0]

17. Till Thereses stora förtjusning berättade Jolkkonen att de har en sällan använd mixer i köket, den användes betydligt oftare innan han berättade för sin fru att den tidigare användes till att mumsa sönder apor.

18. Är det lättare att besluta saker via kollektiv diktatur? Vi prövar. [f:19 / e:0 / a:0]

19. Förlovningsringar av järn borde vara mer romantiskt än guld – speciellt om man utvunnit järnet själv ur sitt blod. Man kan även pröva först om det går bra på andra; Samuel och Luna utsågs till frivilliga av den tillfällige diktatorn.

20. Onekligen ja, diktatur fungerade utmärkt om ingen klagar. Men det gav oss andra för lite att göra. Mosttåndsarmén vid det runda bordet införde åter igen demokratiröstning under mötet via en blodlös kupp och fanzineblockad.

21. Alexander bör komma till Mötet oftare.

22. Röstning om Alexanders framtida närvarofrekvens. [f:23 / e:1 / a:0]

23. Om man beställer en hamburgertallrik utan beanesesås efter alla andra så får man maten innan de som beställde med.

24. Äkta Bolsjeviker är nu mera en minioritet. Men frågan om de borde kvoteras in över hela världen kom aldrig till röstning.

25. Något om sadomiserade kaniner.

26. Om man tvingas bo i Stockholm skulle man få bygga mur och utropa sitt hus till egen stat.

27. Röstning om att Mötet ska avslutas när sekreteraren försvinner. [f:7 / e:6 / a:0]

/ Nicklas
