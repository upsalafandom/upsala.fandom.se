+++
title = "8 januari 2002"
slug = "8_januari_2002"
date = 2002-01-08

[taxonomies]
forfattare = ["Torbjörn"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

"Hur det går när man ska skriva om ett pubmöte och har lite feber och har druckit för mycket te"

Hmm.. fick mig mitt i mitt fredliga ölande en lapp och en kopp med småpengar levererad - trots den fasta hand detta levererades med, och den hotfulla blick som medföljde Kanske jag hade kunnat stå emot om inte min öl precis tagit slut.

Som sagt var jag rätt oförberedd, och hade därmed inte lyssnat så mycket till vad min omgivning yttrade, utan varit mer inriktad på mitt kall att Sprida Sanningen Om Science Fiction(!) i dess ädlare former.

<!-- more -->

En intressant bok från typ år 1941 flöt omkring och upplyste om hur ett Riktigt Hederligt Äktenskap ska gå till (speciellt till förmån
för de nygifta tu) - det finns säkert något slags hemligt tryckeri som HITTAR PÅ alla dessa gamla böcker och ger ut dem bara för att vi ska tycka att vi har det bra nu för tiden.

En stor pinsamhet för Mig: Jag tjatade HELA kvällen om en författare som heter Alexander Garland, och snackade om den coola, hårda, moderna rymdopera han skrivit.. i "Revelation Space" och "Chasm City" - de som har bättre ordning på huvudet kanske kommer ihåg att sagde författare (som förresten är väldigt bra) i Själva verket skrivit "The Beach", och INTE hittills gett sig in på science fiction! Den författare jag tänkte på heter ALASTAIR REYNOLDS!!!

Köp! Köp!

Eftersom jag inte kommer ihåg så mycket mer just nu så tänker jag hitta på resten (tips till andra sekreterare: skriv redovisningen Snart efter mötet)

Öööh.. Så ..då satt jag på denna pub ibland dessa personer, och kom att tänka på: ifall jag hade varit en infiltrerande rymdvarelse så skulle detta vara det Perfekta ställen att träffa mina med-infiltratörer på! Ingen skulle misstänka något - bland alla dessa amatörastronomer och klingonsk-talande och allmänt halvfulla småudda människor.. En hemsk tanke slår mig.. ÄR det i själva verket så det ligger till?!.. Kanske är det så att alla här utom jag är aliens.. eller är det Jag som är en alien?.. det skulle ju vara smart – eftersom den perfekte agenten ju borde vara den som tror helt på den roll han spelar.. jag har alltså blivit hjärn-(eller vad min ras nu använder för att tänka – låt oss kalla det för Haarnyer **notch-notch**)-tvättad att tro att jag är en science fiction-intresserad programmerare - ok.. Men vad gör jag här egentligen? Det måste vara ett infiltrations- och desinformationsuppdrag, det verkar vettigast - mitt uppdrag är alltså att sprida desinformation om science-fiction.. antagligen på grund av att människorna ju alltid verkar skriva om sin framtid innan den slår in - kanske är det så att science fiction skapar om deras framtid, och 'fantasy' skapar om deras förflutna?.. Det som idag står i historieböckerna stod igår i fantasyromaner?.

Trygg i min roll lutar jag mig tillbaks och säger något dumt om Vernor Vinge, och tänker tillfredsställt tillbaks på mitt briljanta drag med att lura i folk att Alexander Garland skriver science fiction - HAH! Om alla Fanniter börjar läsa Garland i tron att de läser om framtiden, så kanske all på denna planet kommer att bo på stränder om hudra år, vilket skulle underlätta en invasion avsevärt!

Ah! Just det ja, åsså var det nån slags möte i Stockholm helgen efter.. med nån stiftelse - fråga inte mig - jag är ny! Inte bara i föreningen, utan också på planeten, antagligen!

Följande personer misslyckades i varierande grad med sin frånvaro:

Samuel Åslund, Carl-Henrik Felth, Daniel Luna, Emanuel Berg, Andreas Gustafsson, Marie Engfors, Maria Jönsson ,Anna Åkesson, Simon Åslund, Magnus Eriksson, Åke Bertenstam, Björn Lindström, Johan Anglemark, Björn X Öqvist, Torbjörn Josefsson, Mikael Jolkkonen

Kopia bifogad till SÄPO, per innestående order.. eh.. jag menar.. inte alls..

Hade lite ont i Haarnyerna dagen efter
/Torbjörn Josefsson
