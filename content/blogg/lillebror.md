+++
title = "Lillebror"
slug = "lillebror"
date = 2008-05-08

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["Cory Doctorow", "böcker", "musik", "piratpartiet"]
+++

Nu har läst [Little Brother](http://craphound.com/littlebrother) av Cory Doctorow (som jag laddade ner från nätet, se länken), och jag måste säga att jag verkligen gillade den. Den har allt som fick mig intresserad av sf från första början: smarta killar och tjejer som begriper tekniken och gör den till sin, samt spänning och äventyr och relevans för hur jag ser på världen omkring mig. Som jag också av en slump upptäckte i morse passar låten Eldblommor med [Elektrubadur](https://www.jamendo.com/artist/4363/elektrubadur) ganska bra som soundtrack till både terroristattacken i början och en del av de mer dramatiska sammandrabbningarna senare.

Lite kul att svenska piratpartiet spelar en viss roll i boken. Med tanke på hur litet Sverige är få landet väldigt mycket uppmärksamhet ute i världen, tycker jag.  För övrigt vill jag till Sverige. Sista april gav mig hemlängtan, och jag tittade på webbkameran som visar universitetsparken flera gånger. Och så lyssnar jag på Elektrubadur, av liknande skäl. Jag är också lite avundsjuk på er som var på Åcon. Jag vill också!
