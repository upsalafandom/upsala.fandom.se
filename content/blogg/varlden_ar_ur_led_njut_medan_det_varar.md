+++
title = "Världen är ur led -- njut medan det varar"
slug = "varlden_ar_ur_led_njut_medan_det_varar"
date = 2008-05-01

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Hamlet", "kaos", "litteraturteori", "obehag", "skräck"]
+++

Har ni tänkt på det här med vad som anses vara "sci fi", eller annan fantastik som ligger en nära om hjärtat? Det är ganska mycket skit som skyfflats ut för konsumtion som stämplats med fantastikstämpeln för att det funnits en eller annan raket, strålpistol eller nåt annat fånigt tecken på att det tillhör genren. Själv insåg jag efter den litteraturkurs jag gick om sf, då vi konfronterats med några olika mer eller mindre fantasifulla försök att definiera science fiction, att det inte är en genre över huvud taget. Nu är det dock inte science fiction som jag tänkt på, utan en annan "genre" som jag sedan många år känt mig mycket förtjust i. Jag tänker på skräckgenren, och jag antar att ingen blir direkt förvånad om jag säger att jag inte anser att skräck är en genre det heller.

När det var Halloween här i landet så kunde man se folk spöka ut sina hus med små spökfigurer i rabatterna eller med plastskelett som hängde och vajade i vinden på verandan. Det var mycket tydligt att svart, orange och vissa typiska attribut som spindelväv och skelett var "skräck", enligt temat. Nu kanske någon vill påpeka att man inte ska dra alltför stora växlar på fenomenet Halloween. Det kan jag ju instämma i även om det var det som satte igång mina tankar, jag ska bredda mig en aning.

Eftersom jag är förtjust i att läsa saker som publicerats och marknadsförts som skräck, har jag med samma blick som granskade folks verandor vid Halloween tagit och skärskådat en del av de noveller och romaner jag läst. Tänker jag då tillbaka så finner jag att det inte är direkt ovanligt att man skriver berättelser som egentligen är synnerligen ordinära, men att huvudpersonen råkar vara vampyr. Framförallt i fallet med vampyrer så verkar det vara så utspritt, att de flesta vampyrhistorier jag läst har jag inte ansett vara skräck alls!

Naturligtvis har jag då ett annat sätt att ta mig an det hela. Skräck är för mig inte de yttre attributen, utan det är en stämning och en inställning till texten och ämnet. Då är det ganska naturligt att diverse texter som folk skrivit utan att egentligen betrakta dem som skräck, kan tillhöra den etiketten och en hel del som är utgivet som skräck inte alls är det. Nu inbillar jag mig naturligtvis inte att någon ska se detta som någon Viktig Insikt(tm), men det är en liten observation om att det ganska ofta är så att vi serveras nåt som på ytan ser rätt ut, men när man sätter huggtänderna i det så är det bara teaterblod inuti. Rekommendationer eller recensioner av någon som insett problemet är alltså ganska intressanta.

Under den panel om skräck som fanns i programmet på Ad Astra (en enda, och i en liten lokal, i slutet på programmet sent på kvällen. Det var en av de mer välbesökta paneler jag var på...) sa Brett Alexander Savory nåt som fastnade i mitt minne. Jag minns inte längre sammanhanget, men han nämnde ordet "despair" när han diskuterade vad som var bra respektive dåligt med diverse skräckfilmer. Det slog mig att detta kanske är ett bra ord för att visa på vad som är kärnan i upplevelsen och attityden jag far efter. Något annat som nämndes i samma konversation om jämförelse var att om det saknar en "moralisk poäng" så är det inte bra skräck. Nu ska vi se om jag kan få ihop det här till nåt.

Skräck handlar om hur det kontrollerade och ordnade vrids ur gängorna, och den känsla av förtvivlan som uppkommer vid denna förlust av kontroll leder av tvång till någon sorts situation då antingen personerna i texten, läsaren eller världen konfronteras med någon sorts moralisk poäng. Var det lite tungt? Är jag ens på rätt spår? Jag tror någon form av sådan process ligger bakom bra skräck.

Man kan ju säkert vrida på det hela ganska mycket, men jag vet hur det känns när jag läser en novell av den briljante Thomas Ligotti. Man får en känsla av overklighet, av drömsk förtrollning som visar på hur ingenting är riktigt pålitligt och solitt. Är det en känsla av förtvivlan jag försöker beskriva? Kanske inte, men det är något som beror totalt på hur författaren angripigt sitt ämne och inte på om genrekonventionerna följt och de traditionella monstren eller fenomenen är på plats. Det är sånt som gör mig till skräckläsare, och det är inte en direkt behaglig känsla. Av någon anledning är det dock nåt jag återvänder till och det kittlar mitt sinne på estetiskt tilltalande vis. Har du någon gång bestämt dig för att det där med skräck inte verkar vara din grej så kanske du helt enkelt inte gillade "genrekonventionerna"? Jag är inte så värst förtjust i dem.

Kanske är jag helt vilse, eller så är jag nåt på spåret. I alla fall är det klart att böcker om vampyrer som lika gärna kunde handlat om folkskollärare är nåt som tråkar ut mig, och det som jag gillar handlar om helt andra saker. Det vore kul att veta om någon annan tänkt nåt liknande.

Nu ska jag bara säga åt er att läsa [Borderlands](https://www.amazon.com/Borderlands-1-No/dp/1565041070/ref=sr_1_7?ie=UTF8&amp;s=books&amp;qid=1209670097&amp;sr=8-7) som ett ypperligt exempel på när skräck inte är genrekonventioner utan bara riktigt skruvande, välskrivna berättelser som inte ger dig en känsla av avslappnat behag. Utan en mer svårbeskrivlig känsla av obehag. Njut.
