+++
title = "Om sf-priser"
slug = "om_sf_priser"
date = 2008-02-24

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["böcker", "priser", "science fiction"]
+++

När den [preliminära listan](http://www.sfsignal.com/archives/006113.html) på
nomineringar till årets Nebula-pris offentliggjordes kände jag mig lite stolt
över att upptäcka att jag faktiskt läst tre av böckerna i roman-kategorin:
_Vellum_, _Mainspring_, och _Harry Potter and the Deathly Hallows_ (den sista
har jag visserligen inte avslutat ännu, men ändå). Alla tre har försvunnit på
den [slutliga
nomineringslistan](http://www.sfsignal.com/archives/006297.html), men det var
detsamma.

Det fick mig i alla fall att fundera lite på det här med litteraturpriser.
För den som eventuellt inte känner till det kan jag ju nämna att
[Nebula](http://www.sfwa.org/awards)n är det årliga pris som delas ut av
Science Fiction Writers of America för nyutkomna verk i olika kategorier. Det
andra stora sf-priset är [Hugo](http://www.worldcon.org/hugos.html)n, som
röstas fram av medlemmarna i sf-världskongressen varje år.

I nummer 7 av fanzinet Steam Engine Time (som man kan ladda ner
[här](http://efanzines.com/SFC/index.html)) fanns en gästledare som
kritiserade hela fenomenet med litteraturpriser och samtidigt sågade hela
systemet med citat som prisar böckerna (blurbar på omslaget). Det är bara en
tom marknadsföringsploj, det betyder ingenting för kvaliteten, menade
skribenten. Möjligen är det också svårt att bedöma kvaliteten på ett verk
eller ett författarskap när det är nytt, innan det har hunnit mogna och man
kan se ifall det höll för tidens tand.

Jag minns plötsligt boken Kirinyaga, som är en fixup av ett antal noveller.
Författaren [Mike Resnick](http://www.fantasticfiction.co.uk/r/mike-resnick)
skröt i förordet om att detta var världens mest prisbelönta bok, eller något
sådant, eftersom många av de ursprungliga novellerna fått ett eller annat
pris. Jag tyckte att det var lite fånigt, och boken var verkligen inte alls
så fantastiskt bra.

När jag lägger märke till hur jag själv faktiskt använder mig av vissa av de
här litteraturpriserna måste jag ändå invända lite mot kritiken. Saken är att
jag inte tror att de flesta priser egentligen handlar om att försöka välja ut
det objektivt bästa verket. Man väljer det som framstår som mest intressant
här och nu, enligt vissa kriterier och perspektiv som man kanske inte är
särskilt medveten om.

Inom science fiction tycker jag att det är särskilt uppenbart att
litteraturpriser är en sorts replik i en konversation som pågår i
litteraturen men också omkring den. Science fiction är på gott och ont en
väldigt självmedveten kulturströmning, där olika författare läser och tar
intryck av varandra, och träffar både varandra och sina läsare på kongresser.
Det är ett ständigt utbyte av idéer. Det kan bli inkrökt, men det kan också
bli väldigt intressant. Att dela ut ett litteraturpris av något slag är
förstås ett sätt att sätta strålkastaren på verk som man tycker är värda att
uppmärksamma just nu.

Jag som inte läser så där enormt mycket, och sällan hinner läsa böcker innan
de blir prisbelönta, har ganska god nytta av att följa nomineringar och sånt
för att hänga med i vad folk pratar om och vad som rör på sig i sf-fältet
just nu. Det finns många som följer specifika listor och läser de som
nomineras eller belönas just där, för att man har förtroende eller intresse
för den organisation eller de personer som är inblandade i att välja ut dem
som just då ska belönas. Till exempel vet jag folk som alltid ser till att
läsa årets [Tiptree](http://www.tiptree.org)-pristagare. Det här är ju
meningsfullt bara om man känner till priset i fråga, och gör man inte det är
det nästan meningslöst att det står på boken att författaren fått en eller
annan utmärkelse, ungefär som det inte är särskilt informativt att läsa
braskande blurbar ("a new Stephen King!") på ett bokomslag om man inte känner
till att den som skrivit det har bra smak och god urskiljning (fast det är en
bra ledtråd om han eller hon låter bli att fuska genom att jämföra
författaren med någon bästsäljande storhet).

Det är inte fråga om att försöka luska ut vad som är det objektivt bästa av
det som kommer ut, utan om att skaffa sig lite koll på vad det är folk
snackar om just nu. Tar man det för vad det är, och tycker att sånt är kul,
så kan det vara ganska användbart med ett eller annat pris.
