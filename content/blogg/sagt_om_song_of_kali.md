+++
title = "Sagt om Song of Kali"
slug = "sagt_om_song_of_kali"
date = 2009-01-21

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter", "Recensioner"]
taggar = ["Dan Simmons", "bokprat", "böcker", "skräck"]
+++

Ikväll möttes sex belevade Upsalafans hemma hos mig för det första bokpratet
i den nya serien.

Den bok som skulle diskuteras var Dan Simmons' _Song of Kali_, som vann World
Fantasy Award för bästa roman 1986. Handlingen i boken utspelar sig i
Calcutta – en otäck stad dominerad av Kali-kultister, i alla fall om man får
tro bokens berättarperson.

<!-- more -->

Vissa hade blivit riktigt äcklade av den ohygienska likhanteringen och annat
otäckt som förekommer i boken, medan andra verkade ha högre tolerans. Oavsett
det hade bokens upplägg som skräckroman förvånat de flesta med sin
upplösning, som är en sorts kärlekens triumf.

Det diskuterades vidare hur vi såg på skildringen av Calcutta i boken.
Berättarpersonen är rakt igenom boken fientligt inställd till staden, och man
får en vinklad och väldigt negativ skildring. Miljöbeskrivningarna är ganska
generiska, och förutom Kalikulten som spelar en roll i handlingen hade den
nog kunnat utspela sig i vilken stad som helst i tredje världen.

De fantastiska elementen i boken består av händelser relaterade till
Kalikulten och återuppväckande av döda. Vi tyckte att det fungerade bra som
en sorts bakgrund, men att själva de scener där personer i boken direkt
konfronteras med detta var tunt beskrivna. Det är nog bättre att inte läsa
den primärt som en fantasybok, utan att fokusera på de övriga elementen. Det
konstaterades att det gör boken till en ganska typisk WFA-pristagare.

Är det någon mer som läst boken och har något att tillägga?

Nästa bok till läsning är _Last Call_ av Tim Powers. Se [sidan om
bokprat](./sidor/bokprat/index.md) och [kalendariet](./sidor/kalendarium.md).
