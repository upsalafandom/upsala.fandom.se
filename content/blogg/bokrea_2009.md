+++
title = "Bokrea 2009"
slug = "bokrea_2009"
date = 2009-03-17

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Kåserier"]
taggar = ["bokrea", "epik"]
+++

Är det fler än jag som fyndat på bokrean? (Retorisk fråga.)

Själv har jag mestadels passat på att hamstra epik. Det blev _The Táin_, _Beowulf_, _Enuma elish_, med mera. När jag läst lite kan det hända att jag återkommer med en utläggning motiverad av [diskussionen om mytologi och SF](http://physicalityofwords.blogspot.com/2009/02/science-fiction-as-myth.html) på Åkas blogg för ett tag sedan.

Jag tycker att bokrean på nätbokhandlarna är mest intressant. De verkar passa på att tömma sina förråd, snarare än att marknadsföra bokreautgåvor. För den som fortfarande inte har kikat verkar rean fortfarande hålla på, åtminstone hos [AdLibris](http://www.adlibris.com).
