+++
title = "Riktig sf-film"
slug = "riktig_sf_film"
date = 2008-02-19

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Kåserier"]
taggar = ["film"]
+++

Det finns en tendens till förakt för science fiction-film inom fandom. Med
visst fog, kan man nog också säga. Ganska ofta betyder sf på film
melodramatisk katastroffilm eller rymdopera.

Jag tycker dock att de som totalt dömer ut "medie-sf" har fel. Det görs
ganska mycket film som måhända inte befinner sig i genrens utforskande
frontlinjer, men definitivt hanterar sf-teman på ett respektabelt sätt. Här
kommer nu några filmtips.

[Code 46](http://imdb.com/title/tt0345061) (2003)

Förbjuden kärlek i en futuristisk dystopi. Vi har sett det förr i [THX
1138](http://imdb.com/title/tt0066434) och
[Gattaca](http://imdb.com/title/tt0119177), men Michael Winterbottom gör det
bättre.

[Primer](http://imdb.com/title/tt0390384) (2004)

Något så ovanligt som en sf-berättelse om tidsresor, i filmform, som faktiskt
hanterar de paradoxer som uppstår, snarare än att handvifta bort dem. Det är
en lågbudgetfilm om några ingenjörer som mer eller mindre av en slump bygger
en tidsmaskin, och noggrant räknar ut hur de ska använda den för att bli
rika.

[A Scanner Darkly](http://imdb.com/title/tt0405296) (2006)

En animerad film baserad på romanen av Philip K. Dick, mycket trogen
originalet. Animationstekniken tjänar perfekt syftet att sätta bilder till
Dicks skildring av paranoida hallucinationer.

[Renaissance](http://imdb.com/title/tt0386741) (2006)

Fransk-brittisk animerad noir-sf. Precis som _A Scanner Darkly_ är den
rotoskoperad. De nästan monokroma bilderna är från och till rent av
hypnotiska.

[Sunshine](http://imdb.com/title/tt0448134) (2007)

En grupp astronauter färdas mot solen i ett enormt rymdskepp lastat med allt
klyvbart material man kunnat hitta på jorden, för att försöka fördröja dess
utslocknande. Låter det som en tramsig katastroffilm? I själva verket är det
_Heart of Darkness_ möter _Solaris_.
