+++
title = "Williams i tisdags"
slug = "williams_i_tisdags"
date = 2008-05-08

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["Williams"]
+++

Jag räknade till sammanlagt 14 vuxna och ett barn, men eftersom en del gick tidigt och andra kom sent var vi nog som flest 10 på plats samtidigt. Williams var tommare än det brukat vara de senaste månaderna, kanske fanns det 20 gäster förutom oss. Det gick alltså raskt att få mat efter beställning.

Tony avslöjade att hans småflickor drabbats av något slags elakartad mutation och plötsligt kommit in i tonåren; den äldsta av töserna ska till och med börja gymnasiet till hösten. Naturligtvis på samma skola och samma program där Tony undervisar, så han kommer att ta sig an tvåorna i höst istället för ettorna som det egentligen var tänkt, för att slippa ha dottern som elev. Vi väntar med spänning på hennes debutfanzine.

Vi pratade om junikongressen i Linköping, men det verkar inte som om särskilt många ska åka. Maria måste vara i Norge då, annars hade hon och Samuel åkt. Men några stycken blir vi i alla fall.

För andra gången i våra liv fick jag tillfälle att påpeka en otillåten talspråksform i Mikaels språkbruk; förra gången var 1991 trodde Mikael. Dessvärre minns jag inte längre vad det rörde sig om.

Nästa gång vi samlas på Williams är den 3 juni, men först är det pubkväll för oss med kårleg på Orvars den 20 maj.
