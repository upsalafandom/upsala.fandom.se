+++
title = "Neil Gaiman på påskkongressen"
slug = "neil_gaiman_pa_paskkongressen"
date = 2008-03-24

[taxonomies]
forfattare = ["Nea"]
kategorier = ["Rapporter"]
taggar = ["Eastercon"]
+++

En av de mest välbesökta programpunkterna under kongressen var Neil Gaimans
hedersgästtal på söndagseftermiddagen. Jag tror att alla som ville lyssna
fick plats i rummet (som rymmer ungefär 900 personer) men det var definitivt
några som stod längs väggarna. Gaiman är rätt charmig, och definitivt rolig
att lyssna på. Han inledde med högläsning: novellen "Orange", som han skrev
på flygplanet på väg till Australien, där han skulle träffa sin redaktör och
tala om att han ville dra tillbaka en novell från en antologi - så för att
redaktören skulle bli glad igen skrev han "Orange". Det lär ska finnas ett
videoklipp av Gaiman läsande "Orange" vid något tidigare tillfälle,
[här](http://video.google.com/videoplay?docid=4252431117888232778). Bra
novell. Han läser bra också.

Sedan pratade han en stund om hur han hittade till fandom, och om känslan av
att ha funnit sin grupp (your tribe), med anekdoter från hans första
påskkongresser. Påsken 1986 var Gaiman i Glasgow på påskkongressen där;
kongressen hölls i Central Hotel som ligger i centralstationen, så han klev
av tåget klockan fem på morgonen och snubblade in på hotellet i avsikt att
skaffa ett rum, sova några timmar, duscha och äta frukost. Förutom att det
fanns en bar mellan utgången och receptionen, så det blev inget rum på hela
kongressen. Så kan det (kanske) gå.

Under samma kongress satt Gaiman och pratade med en redaktör, som han
försökte övertyga om att det vore en god idé att någon skrev en magisk roman
om London, det vill säga om Londons magiska sidor. "Ja, gör det!" sa
redaktören, vilket förbluffade Gaiman en hel del, men det var det som blev
fröet till _Neverwhere_.

Saker ni inte visste om Neil Gaiman: av alla typer av saker han har skrivit,
romaner, noveller, serier och så vidare, tycker han mest om att skriva
radiopjäser. Och _American Gods_ kom till därför att han kände att kritikerna
höll på att stoppa in honom i ett fack, han-som-skriver-om-England-och-magi,
efter _Stardust_ och _Neverwhere_. Och när han sökte efter bra miljöer inför
filmatiseringen av _Neverwhere_ så fick han lifta med tunnelbanetåg som
stannade vid nedlagda stationer för hans skull. Och han har egentligen inget
alls emot att talböcker med hans verk sprids fritt, för han vill ju att folk
ska läsa, eller lyssna på, det han skriver, och det är faktiskt inte ofta som
man upptäcker helt nya författare genom att köpa deras böcker, utan man lånar
eller får någon bok av dem, och när man har fastnat så köper man fler.

Dessutom läste han en del av första kapitlet av sin nya bok, som kommer ut om
några månader. Den heter _The Graveyard Book_ och är ett slags modern
version av _The Jungle Book_ (Kiplings _Djungelboken_), men i stället
för att handla om en pojke som uppfostras i djungeln av vargar så handlar den
om en pojke som uppfostras på en kyrkogård av spöken. Och inspiration till
den boken fick han för många år sedan, när han bodde granne med en kyrkogård,
i ett hus som hade en igenmurad dörr som ledde till kyrkogården. Jag förstår
fullkomligt att det var fantasieggande. Ända sedan dess har han börjat skriva
boken med några års mellanrum, skrivit några sidor och sedan slutat för att
han inte tyckte att han var tillräckligt duktig som författare ännu, och bara
skulle förstöra historien om han började skriva den innan han blev duktig
nog. Men så för ett par år sedan insåg han att han visserligen inte är
tillräckligt bra men å andra sidan knappast kommer att bli bättre, så det är
lika bra att skriva boken nu. Det är trevligt med avväpnande självdistans.
