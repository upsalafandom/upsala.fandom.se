+++
title = "The Golden Age of Fandom"
slug = "the_golden_age_of_fandom"
date = 2008-03-17

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["fandom", "historia", "nostalgi"]
+++

Det har tack och lov avmattats nuförtiden, men på nittiotalet gick det inte att börja diskutera fandom med någon som var runt trettio och aktiv i fandom på tidigt åttiotal utan att få höra hur fantastiskt mycket aktivare och roligare fandom var på åttiotalet. Frågan är hur väl det stämmer. Det finns ett korn av sanning i det; åttiotalets fandom bestod till stor del av tjogtals begåvade gymnasister och studenter med gott om tid och ett behov att nå ut till andra utan tillgång till andra kommunikationsmedier än det tryckta ordet. Det sjöd med andra ord av aktivitet när de här människorna lade lika mycket tid på fandom som dagens 22-åringar lägger på att titta på DVD och spela onlinespel.

Men aktivare är inte samma sak som roligare. Som Upsalafan så får jag erkänna att jag tyckte att fandom var roligare på 80-talet, men det är jag helt säker på enbart beror på att jag då var student. Det jag minns som roligast var typiska studenteskapader som att driva runt på stan med andra fans i timmar och dagar, att fördriva dagarna på kafé och på nationspubarna, att sitta och spåna wittra vansinnigheter över vinflaska efter vinflaska. Det hade egentligen inget med fandom och allt med vår ålder att göra. Om man tittar efter hur aktiv Upsalafandom var på 80-talet och jämför det med hur det är idag, år 2008, så utfaller jämförelsen entydigt till 2008 års fördel.

Ett typiskt möte på puben anno 1986 drog kanske åtta-nio deltagare, och när det gäller övriga aktiviteter i form av att arrangera kongresser och skriva fanzines är det ingen större skillnad på då och nu. Visst har fanzinen bytts ut mot bloggar, webbforum och e-postlistor, och mycket sker i långsammare tempo. Det blir ju så när medelåldern ökar och folk får barn och jobb som knycker tid av dem. Men pubmöten med 20-25 deltagare, ett gruppfanzine (som vore motsvarigheten till den här bloggen) med ett dussin aktiva skribenter, regelbundna bokpratarmöten... nej, i mina stjärnblickande nostalgiska ögon går det inte upp mot att dra runt på stan och sätta upp affischer för Gotiska förbundet följt av en lång, irrande pubrunda, men det beror bara på att jag inte längre är 22.

Det är ju samma sak med det mesta andra i nöjesväg, det gör störst intryck på oss när vi är yngre och fortfarande som mjuka vaxtavlor. Jag kommer aldrig att ha samma fantastiska upplevelser med rollspel som hösten 1981, trots att jag i vuxen ålder spelat med bättre spelledare, kreativare medspelare och mer välskrivna äventyr. Jag kommer aldrig läsa en bok som gör större intryck på mig än vad Tolkiens ringtrilogi gjorde 1979, trots att jag läst skickligare skrivna böcker sedan dess.

Det är samma sak med fandom. The Golden Age of Fandom är de första åren. Det enda jag kommer på som inte passar i den här mallen är musik, musikupplevelser ger samma fräscha känsla av att vara unika hur många år som än förflyter. Jag undrar om musik tilltalar en annan del av hjärnan?

Och idag är det Saint Patrick's Day. Sláinte mhór, a cháirde uilig!
