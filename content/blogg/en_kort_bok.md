+++
title = "En kort bok"
slug = "en_kort_bok"
date = 2008-02-14

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Recensioner"]
taggar = ["böcker", "fantasy"]
+++

Igår gick jag förbi den där hyllan med nya böcker, och min blick fastnade på ett bekant namn. Steven Erikson.

Han är ju kanadensare, och skriver tjocka böcker. Här är en av hans böcker, i Kanada, och den är inte tjock alls! Eftersom så många lovprisat hans böcker bestämde jag mig för att ta en titt på den. Hans tegelstenar kan vara hur bra som helst, men jag kommer inte orka läsa dem ändå. Det här var dock en novella. Som jag fattade det skulle detta vara den första i en serie med kortare berättelser i samma värld som hans stora romancykel.

Ingen mindre än Glen Cook  skriver lovprisande på baksidan. Även om jag vet att dessa små blurbar många gånger skrivs som väntjänster, och inte mycket mer, så gjorde det mig nyfiken. Jag hade en liten stund tills jag skulle vara någon annan stans så jag satte mig och läste.

_Blood Follows_, som boken hette, visade sig vara en mördarhistoria i fantasymiljö. Den var hyggligt intressant, och miljön innehåll lite vandöda trollkarlar, gastar och nekromatisk magi. Inte en lugn natt på stan, om andra ord. Vad den dock inte innehöll var mycket till miljö och personbeskrivning. Många fantasyberättelser består av mycket reciterande av mer eller mindre medeltida klädesdetaljer och liknande. Så dock inte här. Om hans tegelstenar är liknande så blir jag imponerad, för då kommer han kunna klämma in synnerligen komplexa och invecklade intriger. Kanske är det sånt folk uppskattar som omväxling till "swirling cloaks" och liknande.

Sammanfattningsvis kan man väl säga att det var en hygglig plot och en hygglig twist, men inte var det så att man kom att tänka på Lankhmar ändå? Där får jag för att jag läser baksidesblurbar.
