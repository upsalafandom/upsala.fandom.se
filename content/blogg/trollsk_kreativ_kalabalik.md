+++
title = "Trolsk kreativ kalabalik"
slug = "trollsk_kreativ_kalabalik"
date = 2008-05-23

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["fandom", "rollspel", "sword &amp; sorcery", "tunnels &amp; trolls"]
+++

De som känner mig vet att jag samlar på rollspel, och att jag är rätt aktiv på en del epostlistor/webbforum och andra ställen. Nu har jag hittat ett nytt sätt att lägga tid på min hobby! Först lite bagrund innan jag berättar vad jag fastnat för den här gången. Om man inte vet så mycket om rollspelshobbyn kanske man inte hört talas om så mycket mer än namnet Dungeons &amp; Dragons. Eftersom det spelet för många blivit synonymt med hobbyn är det för en del nästan förvånande att det finns fler spel! Själv har jag nu snöat in på Tunnels &amp; Trolls. Nej, innan någon frågar, det måste inte heta Bla &amp; Bla, men det var lite populärt ett tag på sjuttiotalet. T&amp;T skapades av [Ken St. Andre](https://en.wikipedia.org/wiki/Ken_St_Andre) som ett alternativ till det i hans tycke trevliga konceptet i D&amp;D, men utan de mycket förbryllande och invecklade reglerna. För de som gillade tanken på Sword &amp; Sorcery-äventyr hemma i vardagsrummet var T&amp;T enklare och inte lika seriöst menat.

Ken har en liten klubb online som kallas [Trollhalla](http://www.trollhalla.com), och där har jag nu anslutit mig till de trogna skarorna kring "The Trollgod" som Ken är känd som. Man bygger sig en online-persona, inte olik den man spelar i T&amp;T. Sedan småpratar man om allt mellan himmel och jord samt kämpar om "trollish victory points" genom att dela med sig av skämt, anekdoter, hemmagjorda scenarier och illustrationer eller berättelser till fanzines. Jajamen! Det publiceras två fanzines om T&amp;T för tillfället, förutom en massa annat lustigt som novellsamlingar och blyertspennor som t.o.m. har ett anfallsvärde enligt T&amp;T reglerna.

Den vecka som gått har vi haft en "Kindred Competition", vilket var ganska skoj. Vi fick alla hitta på någon lite udda fakta om de varelser som befolkar Trollworld. Själv skrev jag om dvärgarnas förmåga att lukta sig till stulna dvärgaartefakter eftersom de kan känna doften av ädelmetall och juveler, samt märkliga fakta som att hobgoblins sjunger strupsång och lite annat smått och gott. Man känner sig verkligen kreativ och peppad när man får poäng för allt man bidrar med, och därför kan inspirera och peppa varandra i en positiv spiral av sällskaplig tävlan.

Jag gillar verkligen känslan av grupptillhörighet och den bubblande känsla av skämtsamt skojande där man tillsammans bygger saker och sedan spelar med dem i spelet. Det här givandet och tagandet är väl lite som i fandom när det är som bäst, med fans som skriver bloggar och fanzines tillsammans, läser böcker och utbyter erfarenheter med författare som är lika mycket fans de med. Vi får se när jag hittar nåt annat som jag kanske blir än mer entusiastisk över. Nu har jag i alla fall lagt ännu ett spel till samlingarna och haft en hel del kul med det. Nu skulle jag bara hitta en ny bra fantasyförfattare att läsa när man är i gasen...
