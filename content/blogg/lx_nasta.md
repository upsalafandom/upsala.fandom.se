+++
title = "LX nästa"
slug = "lx_nasta"
date = 2008-03-24

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["Eastercon", "påskkongressen"]
+++

Påskkongressen på Radisson Edwardian vid Heathrow ligger på dödsbädden. Hedersgästerna har varit utmärkta, China Miéville, Neil Gaiman, Tanith Lee, Charlie Stross och Rog Peyton. Det är svårt att komma ifrån att påskkongressen är höjdpunkten på kongressåret. Det är inte bara kvaliteten på programmet, utan också den starka traditionen bakåt. Här finns många fans som började gå på kongresser på 50- och 60-talen, samtidigt som den yngre generationen i 25-årsåldern är månghövdad och synlig. Jag träffar folk som jag bara träffar en gång om året eller vartannat år eller vart femte, som kan berätta om hur det var förr och andra som jag aldrig har sett förut. Det är inte väsensskilt från en svensk kongress, det är bara så mycket större. Större bredd, mer program, fler människor, bättre panelister. I år var vi dessutom fler svenskar än vanligt.

På lördagskvällen hade vi en synnerligen lyckad skandinavisk fest. Vi höll den i en del av en inglasad atriumgård med små vattenfall och annat och det var packat med folk som drack små muggar med brännvin och punsch av olika slag, drack öl som vi köpt under en djärv räd till ett lokalt Tesco och mumsade på lakritsbåtar, Ahlgrens bilar och pepparkakor. Geoff Ryman och Chris Priest hängde vid baren och pratade minnen från svenska kongresser med oss.

Jag tror inte på Gud men om han finns kan han vara China Miéville. Allt han säger är välformulerat och övertygande, och i likhet med en del andra välkända figurer i fandom talar han med grammatiskt perfekta meningar som håller ett LIX-tal på 74. En John Clute med ett mänskligt ansikte. Sedan håller man inte alltid med honom när man har fått en chans att fundera på saken, men så länge han pratar ifrågasätter man inte. Ungefär som jag föreställer mig att fallet är med Gud.

Nej, nu måste jag tillbaka till Dead dog-festen och hjälpa till att tömma baren. Linnéa är på Buffy singalong och jag tittar kanske in själv på vägen till baren. Cheers!
