+++
title = "Brädspel: En guide till ditt nya beroende!"
slug = "bradspel_en_guide_till_ditt_nya_beroende"
date = 2008-07-09

[taxonomies]
forfattare = ["Torbjörn"]
kategorier = ["Kåserier"]
taggar = ["brädspel", "spel"]
+++

Ja; jag verkar ha blivit insugen i brädspels(/sällskapsspels)-träsket! Det
finns ju så många spel nu som verkligen är Roliga att spela, till skillnad
från dammiga "klassiker" som Monopol (Urk!), Cluedo (Gäsp) och Trivial
Pursuit (Ack!) - En sak som är viktig för mig (lathuvud) är att spelen inte
är för jäkla jobbiga och fulla med konstiga regler; jag är för
**"Softcore Gaming Som Alla Kan Njuta Av"** :)

<!-- more -->

Ett verkligt "Gatewayspel", för att få folk att börja uppskatta brädspel i vuxen ålder, är det skojiga och enkla ["Ticket To Ride"](http://www.boardgamegeek.com/game/14996), där man bygger järnvägslinjer över olika kontinenter (beroende på vilken version av spelet man köpt), för att sammanlänka de städer man har nämnda på sina "tickets". Detta spel fick jag i julas min bror och hans (ickebrädspelande) familj att bli hookade på - inklusive min 9-årige brorson, som snart yttrade att han skulle ut och samla burkar så han kunde köpa ett eget. För att jäklas med de övriga spelarna kan man sno rutter man tror att de behöver, så håll dina ruttbehov hemliga! (2-5 spelare)

Nästa steg i ditt kommande missbruk är antagligen ["Settlers of Catan"](http://www.boardgamegeek.com/game/13), vilket innehåller lite fipplande med "resurskort" men i övrigt är snällt mot oss som är för lata för att orka spela jobbiga spel. -Man bygger i början av spelet en slumpmässig ö av hexagoner (ah! hexagoner!), vilka de olika spelarna sedan ska leka nybyggare på. Det gäller att placera sina byar och vägar på ett sådant sätt att man får gott om resurser för att.. bygga fler byar och vägar, och på så sätt få fler poäng än sina motspelare. Gott om utrymme för att jäklas med andra spelare finns, iom att man kan blockera dem på olika sätt. Man behöver också idka byteshandel med dem, vilket gör att man behöver vara minst 3 spelare (3-4 spelare möjliga, och det finns expansioner för allt mellan himmel och jord). Mycket roligt och omspelbart! Största nackdelen känns som minst-3-spelare tvånget.

Min senaste baby är dock ["Race for the Galaxy"](http://www.boardgamegeek.com/game/28143) - ett nästan helt kortbaserat spel (förutom "marker" som man använder ibland för att hålla koll på poäng), vilket är något nytt för mig. -Efter bara tre speltillfällen är jag dock redan ganska fast, och börjar redan undra när man kan köpa några expansioner. -Spelet går ut på att man ska plocka kort (planeter man kan bebygga/saker man kan bygga) och köpa ut dem för att placera dem i sitt imperium, där de ska ge en poäng som man vinner med. -Detta involverar olika möjliga "faser" som kan inträffa under varje "turn" - _Explore_ (plocka kort), _Develop_ (bygg ev saker), _Settle_ (bosätt ev planeter), _Trade_ (sälj ev resurser), _Consume_ (byt ev råvaror till "victory points") samt _Produce_ (vissa planeter producerar råvaror). Något mer komplext än ovanstående alltså, men  efter första rundan (45 min) hajar man 99%, så det är inte så farligt. 2-4 spelare.

Glad spelsommar! (hrrm.. när det Regnar, såklart) - fråga mig om du är med i uppsalafandom och vill dyka upp på någon lagom loj spelkväll :)
