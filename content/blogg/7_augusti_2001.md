+++
title = "7 augusti 2001"
slug = "7_augusti_2001"
date = 2001-08-07

[taxonomies]
forfattare = ["Luna"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Och ännu en tisdag har gått av stapeln. Den här gången pratades det om saker. Några av dessa saker var Anglemarks avundsjuka över att alla andras roliga tekniska saker var små och att de alltså kunde läggas fram nonchalant på bordet, medan hans egna coola saker var stora; hur man skulle kunna snabbt planera ihop en kongress bara för att; och huruvida det var bra eller inte att vara semikolonfetischist.

<!-- more -->

Kongressplanerna ser ut att bli en liten kongress i vårt kära Uppsala 2002 och en Riktig kongress 2003; semikolonen ser fortfarande ut att vara på utdöende - enligt Maria till talstreckets förmån.

Janne gjorde reklam för sin fankatalog vilket utan tvekan var för att kunna visa upp sin fina dator för alla närvarande (den hade ett tuggat äpple på ovansidan och var vit och vitt är ju fint). Mer om datorer blev det och under en liten period verkade det som om alla pratade om sådana, men sedan gick hysterin över och folk pratade om böcker igen.

En person som vill ha rykte om sig som snäll (och alltså vill vara anonym) nämnde att slug lingvist var något som han tyckte att det var bra att presentera sig som (fast på engelska). Jag föstod ingenting innan jag högt och tydligt testat att uttala det (fast jag tror att folk inte lyssnar på mig).

Marie skickade en lista som hon ville ha fyllt med boktips. Fyll på den mer via mail vet jag (ha! Nu kommer det att trilla in boktips resten av livet till dig (**ond** (**mys**))).

Ond som jag var bestämde jag att jag skulle skriva rapport för då kan jag bestämma vad som har sagts under kvällen. Observera dessutom att min tidsuppfattning inte är linjär (eller är det mitt minne månne?). Jag har dessutom ett dåligt minne så om någon kommer ihåg andra saker så tycker jag att ni ska komplettera.

Enligt den namnlista som gick runt så var följande personer närvarande: Sten, Åke, Marie Engfors, Maria o Samuel, Luna, Karolina Wennemyr, Tomas Winbladh, Gabriel Nilsson, Jolkkonen, Karl-Johan, Wahlbom, Johan Anglemark, Kurt, Nils, Janne Wallenius, Simon, Jonas.

— Daniel Luna
