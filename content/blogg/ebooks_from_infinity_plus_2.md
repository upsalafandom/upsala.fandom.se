+++
title = "Ebooks from Infinity Plus"
slug = "ebooks_from_infinity_plus_2"
date = 2011-03-28
lang = "en"

[taxonomies]
forfattare = ["George"]
kategorier = ["Länkar"]
taggar = ["ebooks", "sf", "web sites"]
+++

One of my first discoveries in the world of British Science Fiction was the sophisticated website of Keith Brooke and Nick Gevers, [Infinity Plus](http://www.infinityplus.co.uk). I liked it so much that I contacted Keith to express my appreciation. We have been in touch since then. The site was online from 1997 to 2006, and became a chief resource for reviews of the best in speculative fiction, as well as high-level articles and interviews. It is now an archive that is well-worth perusing and supporting by donation. Keith is a well-known British SF author, academic, running enthusiast, IT specialist, and most recently seller of ebooks. [His store is here.](http://www.infinityplus.co.uk/books)
