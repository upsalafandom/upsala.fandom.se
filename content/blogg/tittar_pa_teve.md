+++
title = "Tittar på teve"
slug = "tittar_pa_teve"
date = 2008-07-11

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Recensioner"]
taggar = ["Charlie Jade", "Doctor Who", "Firefly", "Life on Mars", "teve"]
+++

I [Riktig sf-film](./blogg/riktig_sf_film.md) tipsade jag om några filmer som trots valet av medium är god science fiction.

Nu tänkte jag göra samma sak igen, fast med teve.

<!-- more -->

[Firefly](https://www.imdb.com/title/tt0303461) (2002)

Det här är den enda USA-producerade serien i listan.

_Firefly_ är en rymdopera skapad av upphovsmannen till
[Buffy](https://www.imdb.com/title/tt0118276). En cynisk men karismatisk
krigsveteran, från ett inbördeskrig inte helt olikt det amerikanska, åker
runt i ett vilda västern-liknande solsystems utkanter, skrapande ihop sitt
levebröd genom att utföra olika skumma uppdrag. Inte ett helt obekant koncept
alltså.

Styrkan i serien ligger i dramat inom besättningen, och i den lätt
humoristiska dialogen. Efter att serien lades ner gjordes en biofilm också,
som blev en rejäl flopp (i min mening välförtjänt).

[Charlie Jade](https://www.imdb.com/title/tt0408378) (2005)

En privatdetektiv från en värld som tagen från en roman av Philip K. Dick finner sig fångad i en parallellvärld liknande vår egen. Gradvis uppdagas att ett företag från huvudpersonens hemvärld är inblandade i ohämmad resursöverföring från andra världar.

Mot denna bakgrund kommenterar denna kanadensisk-sydafrikanska samproduktion
personlig moral i relation till makt och exploatering.

Förutom sina filmiska kvaliteter har serien utmärkt musik.

[Doctor Who](https://www.imdb.com/title/tt0436992) (2005–)

Alla känner väl till BBC:s sf-flaggskepp _Doctor Who_. Den nya serien har
måhända lite högre budget än när det begav sig, men fortfarande ligger
tonvikten på en intressant historia. Sedan spelar det inte så stor roll om
det syns att utomjordlingarna är gjorda av gamla rengöringsmedelsflaskor.

[Life on Mars](https://www.imdb.com/title/tt0478942) (2006–2007)

I denna BBC-serie vaknar en polis i Manchester vaknar efter en trafikolycka
upp på 70-talet, där han fortsätter sitt arbete trots psykedeliska
meddelanden från verkligheten. Huvudpersonen påminner på många sätt om någon
av Philip K. Dicks (där är han igen) antihjältar, då han slits mellan att
acceptera sin nya miljö eller att försöka bryta sig ut och vakna upp.

Det var det. Nästa gång ska jag göra en lista över sf-romaner som trots att
de är romaner är usla.
