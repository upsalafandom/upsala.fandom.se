+++
title = "Utdrag ur EN TISDAGSKVÄLLS HÄMNINGSLÖSA UTSVÄVNING"
slug = "utdrag_ur_en_tisdagskvalls_hamningslosa_utsvavning"
date = 2006-04-06

[taxonomies]
forfattare = ["Nicklas"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

<pre>AKT 1, SCEN 1    WILLIAMS PUB
( Nicklas kommer in. Bartender finns redan där. )

BARTENDER       Ja, kan jag hjälpa till med något?

NICKLAS         Jag skulle nog vilja ha den där Oatmeal Stout:en.

BARTENDER       Den är nyss insatt, men det kanske inte gör något?

NICKALS         Attanns. Då väntar jag lite med den. Jag tar en
                Musketeer istället.

BARTENDER       Ska bli.

( Tony kommer in. )</pre>

<!-- more -->

<pre>TONY            Ursäkta. Det skulle vara ett science fiction-möte här.

NICKLAS         Ja, det är här.

TONY            Hej, Tony.

NICKLAS         Nicklas



FRÅN AKT 1, SCEN 3

KRUSE           Just nu håller Anna på med någon häxbarnbokserie. Det
                ska finnas en annan med glitter och rosa fluff på, men
                de hade inte den på UEB.

NICKLAS         Fluff? Tygfluff? Som är rosa?

KRUSE           Ja, så förstod jag det. Som en rattmuff fast för böcker.

THERESE         OH! Den måste jag skaffa, om så bara för fluffet!



FRÅN AKT 2, SCEN 1

JOPHAN          Va?!

KRUSE           Va?!

JOPHAN          Är det påhittat?!

KRUSE           Betyder detta att andra saker också är påhittat? Som
                Bosse-Nisse?

JOPHAN          Det låter skumt.

TONY            En massa människor från förr har faktiskt kommit fram
                till mig och frågat ”stämmer det här?”

JOPHAN          Det handlar ju om platser nära din uppväxt.Givetvis
                antar de att det bygger på sanningar.

TONY            När jag var i de trakterna upptäckte jag en sak. Visste
                ni att Världens Bästa Plats inte finns kvar?

JOPHAN          Va?!

KRUSE           Va?! Det är inte sant!

JOPHAN          Man kan inte bara vanhelga den så.

TONY            Jo. Det står en ful grå betongkloss till fabrik där nu.

JOPHAN          Världens Bästa Fabrik om jag får be. Du vet inte vad de
                gör där?

TONY            Nej. Kunde bara se att den var stor och ful.



FRÅN AKT 3, SCEN 1

JOPHAN          UHH! Varför dricker du sådant?

NICKLAS         Jag fick den av Maria.

JOPHAN          Men kunde du inte lagt till en tia och fått något gott?

NICKLAS         Den var halvt uppdrucken när jag fick den.

JOPHAN          Jaja.


/ Nicklas</pre>
