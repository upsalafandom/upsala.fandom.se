+++
title = "Vonda N. McIntyre's \"Dreamsnake\""
slug = "vonda_n_mcintyres_dreamsnake"
date = 2008-04-02

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Kåserier"]
taggar = ["saknat kapitel"]
+++

Jag läste "Om Gräs och Dis och Sand" av [Vonda N. McIntyre](http://www.vondanmcintyre.com) för en ohemult massa år sedan. En fantastisk spännande och annorlunda värld målades upp. Jag kan fortfarande "se" flera av scenerna i den boken, så fantasieggande var den. Men, den hade ett synnerligen mystiskt slut!

Huvudrollsinnehavarinnan bara står där framför porten till en stad och ska gå in och så slutar boken. Tvärt!

Eftersom jag hade alla dessa härliga (men lite suddiga) bilder i min skalle så tänkte jag härom året att jag skulle läsa om boken för att minnas lite mer. På äldre dagar läser jag ju böcker enbart på Engelska så jag köpte boken "Dreamsnake" .

Hupp!

Nu var slutet helt annorlunda!

Nu gjorde jag något riktigt ovanligt... Jag kunde inte låta bli att på vinst och förlust skriva till författarinnan, (berömma hennes författarskap) och kommentera att den Svenska boken nog tappat ganska så mycket i översättnigen.

Döm om min glädje när Vonda svarade! Och om min bryddhet när hon skrev att den svenske förläggaren typ tappat bort det sista kapitlet och tryckt boken utan detta. Om jag kommer ihåg rätt så sa hon att kapitlet trycktes separat (som ett fristående litet häfte) men att detta inte alltid åtföljt böckerna... Hon skrev också lite torrt att det kanske var det saknade sista kapitlet som gjort att hon inte fått så många svenska fans...

Nu till min undran. Finns det någon som vet mer om detta? Om varför det sista kapitlet saknas i den svenska översättnigen? Ni som är översättare, kan det ha varit översättarens fel? Eller finns det andra teorier? Jag kan inte låta bli att ibland fundera på hur en sådan sak kan hända...

;o)

Vonda har ju vunnit "Nebulapriset" för "Of Mist, and Grass, and Sand," och samma pris plus "Hugopriset" för "Dreamsnake" där ju "Of Mist, and Grass, and Sand," är en del och om detta kan man läsa "[överallt](https://en.wikipedia.org/wiki/Vonda_McIntyre)".  Men att sista kapitlet skulle saknas i den Svenska översättningen står ingenstans, vaffördådå?
