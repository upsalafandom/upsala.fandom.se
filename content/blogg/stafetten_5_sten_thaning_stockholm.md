+++
title = "Stafetten 5: Sten Thaning, Stockholm"
slug = "stafetten_5_sten_thaning_stockholm"
date = 2009-05-16

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Intervjuer"]
taggar = ["Alexander Petrov", "Andrew Wiles", "Dublin", "Sten Thaning", "Örebro"]
+++

> Detta är den femte av mina personliga intervjuer med människor inom
> svensk fandom eller som på annat vis är involverade i det här med sf och
> fantasy. Sten föreslogs av det förra intervjuoffret, _Matthias_, med
> frågan "är han fortfarande aktiv i fandom?"

# Uppväxt

Min familj flyttade till Örebro från Växjö när jag var sex år, och jag bodde
där tills det blev dags att flytta till Uppsala och bli student. Mina
föräldrar och min lillasyster med familj bor fortfarande i Örebro, så jag
besöker staden med jämna mellanrum. Den ser likadan ut som för tretton år
sedan. Slottet är mycket slottigare än Uppsalas, och det finns flera stycken
riktigt trevliga caféer. Detta är dock inte tillräckligt för att jag ska
känna något större behov av att flytta dit.

<!-- more -->

Min pappa är läkare, min mamma präst. Pappa tycker om fotografering och min
mamma ägnar just nu sin inte helt obegränsade fritid till att lära sig
bulgariska - hon har förmodligen ett större ordförråd än jag vid det här
laget, vilket är en smula pinsamt. Dessutom har jag en syster i Linköping, en
syster i Örebro och en bror som för närvarande bor i Norge. Och sammanlagt
fem syskonbarn, av vilka det yngsta är några dagar gammalt.

Tonåren ägnade jag åt att sitta framför en dator eller läsa böcker. Nästan
hela tiden. Ibland samtidigt. Fast jag har vissa minnen av att vara utomhus
också. Jag sysslade med orientering för länge sedan, tills det blev rätt
uppenbart att min totala avsaknad av lokalsinne ibland var till en viss
nackdel. Då började jag i stället friidrotta, tills jag insåg att det var
jobbigt att träna. Så jag började spela schack i stället. Jag slutade med att
försöka med schack på tävlingsnivå när det gick upp för mig att jag helt
enkelt inte har ett tillräckligt bra minne. Och så var jag inblandad i det
som nu kallas Svenska Kyrkans Unga. Och sådär.

Gymnasiet ägnade jag åt att spela bräd- och rollspel och att sitta på caféer.
Min minnesbild av Hamncaféet i närheten av min gymnasieskola är att jag
tillbringade betydligt mer tid där än på lektioner, men förmodligen
överdriver min minnesbild. Det livet tog en paus under ett år mitt i
gymnasiet, då jag lämnade landet och flyttade till Australien som
utbytesstudent.

# Uppsala

Jag ville bli student. Det vill säga, jag ville läsa på universitetet i
Uppsala. Jag har tyckt om Uppsala sedan jag var barn. Min mamma läste på
distans och då och då tog hon med mig dit när hon skrev tentor, och släppte
mig lös i staden under dagen. Det var jättebra, för jag kunde gå vilse utan
att det gjorde särskilt mycket, och plötsligt dök det upp ännu en bokhandel.
Sådant kan få vilken tioåring som helst att bli förälskad i en stad. Jag har
med andra ord velat bli uppsalastudent så länge jag kan minnas, så jag
skaffade en kurskatalog och valde bland programmen som verkade roligast. Fast
ämnet var i ärlighetens namn rätt sekundärt.

Huvudsakligen läste jag språkteknologi. Datalingvistik med språkteknologisk
inriktning, tror jag det står på min examen. Plus, förstås, roliga kurser.
Fysik för poeter, runkunskap, arkeoastronomi, personnamn och ortnamn, kemi i
köket, sådana där saker. Du vet, sådant där man tycker är fullständigt
självklart att göra när man bor i Uppsala. I början hade jag ambitiösa planer
på att läsa en matematikutbildning vid sidan om programmet. Det fungerade i
några veckor, tills jag började förstå att matematik också kräver att man
lägger ned en massa tid på det. Man kan antingen plugga dygnet runt eller
passa på att vara student lite av tiden. Och vid det här laget hade jag
precis börjat komma i kontakt med fandom, så jag lade ned projektet.

# Fandom

Det är i första hand Anna Davours förtjänst att jag kom i kontakt med fandom.
Fast då hette hon Åkesson. Vi hade en lära-känna-varandra-middag på
studenthemmet där jag ganska nyss hade flyttat in, det vill säga, um, 1996,
tror jag, och jag hamnade bredvid en sympatisk person som omedelbart började
diskutera science fiction med mig. Lite senare sprang jag in i henne när hon
var på väg till Biörn X Öqvist för att se på film, och så satt jag plötsligt
på hans golv och drack te och pratade med intressanta människor. Det var inte
den sista gången det hände – en skrämmande stor del av min tid i Uppsala har
bestått av att jag har suttit på Biörns golv och druckit te, inte sällan i
över ett dygn i sträck.

Ja, och sedan dök det upp ett pubmöte med Uppsalafandom. Där satt Johan
Anglemark, och efter ett par timmar yttrade jag de där ödesmättade orden:
"Anordna en kongress? Jag har ingen aning om vad en sf-kongress är för något,
men det låter roligt. Det är klart att jag är med i kommittén." Därefter
fanns det ju liksom ingen återvändo.

Fandom består av en massa personer. De flesta av dessa är intressanta att
prata med. Det beror dels på att en typisk fan är intelligent. Eller
intressant. Eller åtminstone i besittning av en massa mystisk och intressant
kunskap om sådant jag antingen är intresserad av (vilket är bra) eller inte
vet något om (vilket är ännu bättre), samt tycker om att diskutera. Ofta,
länge och högljutt. Det tog ett litet tag innan jag förstod kulturen med att
det är helt okej att tala om för folk att de har puckade åsikter utan att det
är meningen att de ska ta det personligt, och ännu längre innan jag förstod
att det inte är alla andra som har förstått detta. Ibland får jag en känsla
av att fanniska diskussioner, särskilt sådana som sker i realtid, gör mina
dåliga egenskaper mer framträdande. Som att det i många sammanhang är helt
okej att avbryta folk som pratar, eftersom det ofta är den enda möjligheten
man har att själv bli delaktig i monologen. Det är inte lätt att vara ny i
fandom. Jag försöker jobba på att dels inte prata oavbrutet och att dels inte
avbryta folk, med varierande resultat. Men, som sagt, jag tycker faktiskt om
kulturen.

Jag tycker om att kunna åka på en kongress utomlands, slå mig ned i närheten
av okända slumpmässiga personer och kunna anta att jag dels har något
gemensamt med dem och att jag dels vill prata med dem. Typ.

Om något skulle få mig att gafiera, skulle det möjligen vara om jag kom fram
till att det finns fler tröttsamma personer än intressanta personer, till den
grad att det helt enkelt inte var värt det längre. E-postlistor är praktiska
på det sättet, det är lättare att undvika jobbiga människor och diskussioner
när man har tröttnat på dem för stunden.

Ett mer sannolikt scenario är att jag inte skulle gafiera, men prioritera
andra saker. Det är illa nog att bo i en stockholmsförort där det är ett
projekt att ta sig någonstans (i Uppsala sätter man sig på cykeln och är
framme efter tio minuter), och mitt behov av att träffa andra människor går i
vågor. Så jag skulle kunna betrakta mig som fan men inte besöka någon
kongress eller pubmöte eller liknande. Räknas det som gafiering?

Tja, och så skulle jag kanske få för mig att flytta till yttre långtbortistan utan någon inhemsk fandom. Det finns folk som aktivt jobbar på att få igång en lokal fandom i sådana lägen, men jag är inte särskilt bra på sådant.


# Ute i världen

Jag har flyttat runt en del. Australien var ett bra ställe att bo på, men
ligger alldeles för långt bort från allt som inte är Australien, och
egentligen från det mesta som faktiskt är Australien också. Å andra sidan
bodde jag där 1994, när det inte fanns allmän tillgång till Internet. Det är
möjligt att det är närmare idag. Det mesta i världen är det.

Italien har den stora nackdelen att folk pratar italienska där. Italienska är
ett mystiskt språk – det verkar jätteenkelt, men när man väl ska försöka
kommunicera inser man att det inte alls är särskilt enkelt att göra sig
förstådd. Iallafall om man är jag. Världen skulle bli mycket bättre om jag
var mer språkbegåvad, åtminstone för mig.

Både England och Irland är mycket sympatiska länder. I största allmänhet
tycker jag bättre om den engelska kulturen än om den svenska, även om det
finns dåliga saker där också. Fast jag skulle nog inte välja att bosätta mig
i Sheffield igen. Det finns å andra sidan många städer i England.

Dublin är dock en fantastisk stad. Allt är för dyrt, men staden är
fantastisk. Och det finns en massa fans där, och de gör kongresser, och jag
förstår språket, och staden är mer full av pubar än vad som egentligen är
fysiskt möjligt... Hm. Varför bor jag inte i Dublin, egentligen? Nu när jag
tänker efter har jag svårt att hitta några nackdelar. Det skulle vara deras
kollektivtrafik då, men med en cykel skulle det gå att undvika det mesta av
den.

Finge jag välja skulle jag helst bo i Dublin, skulle mitt spontana svar bli
just nu. Fast det är antagligen för att jag just har suttit och tänkt på
Dublin en liten stund, staden gör sådant med min hjärna. Eller också London.
Jag tycker om London, även om jag aldrig har varit där mer än sådär en vecka
i taget. Det skulle vara roligt att bo i USA i några år. Jag har aldrig bott
i USA. Det är en brist i mitt liv.

# Stockholm

Dessy skaffade ett jobb söder om Stockholm medan vi bodde i Uppsala. Hon stod
ut med drygt två timmars restid till jobbet i ett halvår, och sedan bestämdes
det att vi ville flytta. Just nu jobbar jag också i Stockholm, vilket gör det
till ett ganska logiskt ställe att bo på. (Familjen består för närvarande av
mig, Dessy och vår guldfisk Sushi.)

Här lär vi bo tills vi inte längre arbetar här. Avstånd till jobbet är
egentligen den enda anledningen att bo i Stockholm. Jag har aldrig varit
särskilt främmande för att flytta med kort varsel när det har dykt upp något
intressant som kräver att jag bor någon annanstans. Det blir lite mer
pusslande när man är två personer, men det kommer säkert att gå bra.

Just nu är jag översättare och projektledare på ett översättningsföretag. Det
är ett vikariat som räcker till slutet av september, sedan får vi se vad som
händer. Annars är jag frilansande översättare, mest av datorprogram och
tekniska handböcker. De senaste åren har jag arbetat hemifrån, och jag hade
glömt hur praktiskt det kan vara med sådana där arbetskamrater. Fast det
finns stora fördelar med att jobba hemifrån också.

På fritiden sitter jag framför datorn och/eller läser böcker... Hm. En
nackdel med ett visst mått av arbetsnarkomani är att det inte blir så mycket
fritid alls. Det är alltid något jag borde göra – bokföring, eller skriva
fakturor, eller den där deadlinen som närmar sig med oroväckande hastighet,
och sådant där. Semestrar har en tendens att ägnas åt diverse kongresser. För
en tid sedan startade jag ett projekt som går ut på att lära mig hitta i
Stockholm. Man skulle kunna tro att alla de praktiska cykelvägarna, i
kombination med en GPS och en cykelkarta på ett par hundra sidor, skulle göra
att det går att cykla utan att åka vilse. I en perfekt värld skulle det
stämma. Lagom tills vi flyttar härifrån kommer jag säkert att ha lärt mig
geografin någotsånär.

Vilket år som helst ska jag nog göra ett nytt avsnitt av mitt fanzine. Eller
göra det webbaserat. Folk utanför fandom brukar i allmänhet kalla webbaserade
personliga fanzines för "bloggar", så steget kanske egentligen inte är så
långt.

Jag nämnde för en tid sedan för Anders Wahlbom att jag visste alldeles för
mycket om Doctor Who utan att någonsin ha sett något avsnitt av tv-serien.
Han blev bekymrad över denna brist i min allmänbildning och lånade ut en
halvmeter DVD-filmer med representativa avsnitt, så att jag skulle kunna
sluta vandra omkring i ignoransens illaluktande träsk (jag tror att det var
så han uttryckte det). Detta fick till följd att jag av bara farten beställde
hem ett par säsonger vardera av Doctor Who och Torchwood, och de senaste
veckorna har jag ägnat kvällarna åt att se ett par avsnitt per kväll. När
Dessy kommer in i rummet brukar jag entusiastiskt förklara handlingen för
henne. "Titta, där kommer onda mördarrobotar! Och där också! Tjoho!" Hon får
av någon anledning samma lätt överseende ansiktsuttryck som när jag leker
krokodil med guldfisken. Nåja, alla människor behöver inte tycka om allt.

# Entusiasmer

Det jag engagerar mig djupt i tenderar att gå i vågor. Jag brinner för saker
i korta (och ibland längre) perioder. Just nu har jag ärligt talat svårt att
komma på något speciellt jag brinner för den här veckan, men det kanske beror
på vad man menar. Jag brukade vara entusiastiskt intresserad av enskilda
politiska frågor, men jag har blivit allt mer desillusionerad när det gäller
hur mycket det går att påverka politik.

Jag hoppas att jag börjar brinna för något snart. Det är ofta rätt kul. För
tillfället är jag med i två kongresskommittéer, och förhoppningsvis kommer
min entusiasm att gå igång på högvarv lagom tills planeringen gör detsamma.

# Förebilder

Tre personer jag ser upp till? Hm. Det beror på vad man menar. Det finns
faktiskt politiker jag ser upp till, sådana som trots massivt motstånd
faktiskt lyckas göra världen märkbart bättre. Margaret Thatcher och Ronald
Reagan är två exempel på sådana. Det är bara tråkigt att de är i en sådan
minoritet.

Det finns människor som gör saker där jag dels är glad att någon gör saken
ifråga, och där jag dels blir oerhört imponerad av det faktum att någon kan
lägga ned så mycket energi på ett projekt. Andrew Wiles stötte på ett
matematiskt problem som ingen hade lyckats lösa, trots att världens bästa
matematiker hade försökt de senaste 350 åren. "Äsch", tyckte han. "Hur svårt
kan det vara?". Och så låste han in sig på sitt rum i sju år och kom inte ut
förrän han hade löst problemet. Alexander Petrov ville göra en filmatisering
av Den gamle och havet. Så han gör en animerad film. Genom att måla 29 000
filmrutor. I olja. Och fotografera dem, en ruta i taget. Jag ser upp till den
typen av hängivenhet.

Och så finns det människor som konstant gör saker som egentligen borde vara
totalt omöjliga, och som får mig att tänka "mmm, jag skulle vilja vara en
sådan person". Som Nikola Tesla och Richard Feynman. De gör världen bättre
bara genom att finnas till. (Ja ja, genom att ha funnits till, i just de två
fallen.)

# Irritationsmoment i livet

Jag retar mig alldeles för mycket på språkfel för mitt eget bästa, och
oproportionerligt mycket på saker som när människor inte kan skilja mellan
"de" och "dem". Den stora nackdelen med mitt jobb är att jag har tappat
förmågan att till exempel se en engelskspråkig film utan att störa mig på fel
i undertexterna. Det här är en irritationskälla som lätt blir ganska pinsam,
eftersom den ju liksom bygger på att jag själv inte ska göra mig skyldig till
några språkfel. Så är dock inte fallet. Universum är inte ordentligt styrt
(vilket är en annan sak jag ofta irriterar mig på). En annan sak som gör mig
alldeles för upprörd är när folk envisas med att tro på saker som alla borde
kunna inse är omöjliga, om man bara tänker efter i trettio sekunder. Som
homeopati, elallergi och sådant där. Ännu mer irriterad blir jag av att folk
tjänar pengar på folk som tror på sådant. Det skulle vara okej om de bara
kunde erkänna att det handlar om magi, eller åtminstone att det är en
religion, men det stör mig att folk tror att det har något med vetenskap att
göra. Hm. Samtidigt stör jag mig på människor som kritiserar en religion
utifrån sina fördomar, utan att vilja sätta sig in i vad det är man
kritserar.

Och så irriterar jag mig på smittsamma smaker. Det finns saker som dels är
totalt oätbara, till exempel bananer och lakrits, och dels smittar ned sin
omgivning. En hel skål med godis blir helt oätbar om man blandar ned några
bitar lakrits i den. En fruktsallad blir oätbar av uppskattningsvis fyra
bananmolekyler. Andra människor förstår inte detta, förmodligen för att de
inte delar min uppfattning om vad som är totalt oätbart. Andra människor har
fel.

Tja, jag irriterar mig på nästan allt politiker gör, förstås. Men det är inte
mitt fel, det är en inneboende egenskap hos politiker.

# Höjdpunkter i livet

Det finns olika typer av höjdpunkter. Vissa saker, som när jag träffade Dessy
och när jag kom i kontakt med fandom, är klart definierade tidpunkter som jag
såhär i efterhand har insett gjorde mitt liv betydligt bättre. Å andra sidan
finns det tidpunkter som kändes mycket höjdpunktigare än de var - som när jag
äntligen fick ut min examen, elva år efter det att jag hade börjat
utbildningen och sex år efter min sista kurs. Jag har inte haft någon som
helst nytta av det där papperet sedan jag fick det, men det var i alla fall
en alldeles underbar känsla. I flera år fick jag lite odefinierat ont i magen
varje gång jag tänkte "javisstja, den där utbildningen, den borde jag göra
något åt. Någon gång.". Jag är rätt bra på att ignorera sådana där magkänslor
och skjuta upp saken jag borde göra tills jag får mer tid. Det fungerar
nästan aldrig.

(Den enda gången det faktiskt fungerar att göra så är när det gäller
tandläkare. Allmänna jag-borde-göra-det-här-känslor håller sig på en konstant
bakgrundsnivå, men när man får ont i en tand ökar smärtan ganska linjärt
tills man slutligen blir tillräckligt motiverad för att övervinna sin
tandläkarovilja och går dit. Fast den strategin har andra nackdelar och
rekommenderas inte.)

Fast det finns ju utdragnare höjdpunkter också. Den tiden i mitt liv jag har
haft roligast var förmodligen när jag var student i England i ett halvår.
Förmodligen skulle jag inte tycka lika mycket om att bo i Sheffield i dag –
allt har sin tid, och det finns lägen i livet då man måste inse att man inte
vill ha olika typer av studentaktiviteter fyra-fem dagar i veckan – men just
då, och just där, hade jag oerhört roligt.

# Framtiden

Jag har ingen aning vad jag gör om tio år. Allvarligt talat vågar jag inte
ens komma med en kvalificerad gissning. Jag kanske bor i Älvsjö, Stockholm
och arbetar som frilansande översättare. Eller så har jag bytt karriär. Eller
så har jag bestämt mig för att doktorera. Eller så följer jag efter Dessy när
hon byter stad/land/kontinent och hittar något att göra beroende på
förutsättningarna där. Allt beror på en mängd saker som i sin tur beror på en
mängd saker. När jag blir stor ska jag skaffa mig en plan i livet. Jag har
hört att det finns folk som har en sådan.
