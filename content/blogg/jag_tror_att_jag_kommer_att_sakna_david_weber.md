+++
title = "Jag tror att jag kommer att sakna David Weber"
slug = "jag_tror_att_jag_kommer_att_sakna_david_weber"
date = 2008-05-22

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Recensioner"]
taggar = ["David Weber"]
+++

Jag har med oändligt tålamod snart läst alla [David Webers böcker](https://en.wikipedia.org/wiki/David_Weber). Jag är ju en sådan... En sådan "kuf" som skaffar alla böcker av en författare och sedan läser dem i kronologisk ordning och nu är jag inne på DWs näst senaste bok av totalt ett tjugotal (?). (Ni må tro att min man gillar mitt sätt att vara totalt borta under några veckor) (inte).

David Weber skriver ganska speciellt. Han börjar på A, sen tar han B, sen C och så i rak ordning "äter" han sig fram till Ö. Längst denna raka väg tuggar han fram som en ångvält. Allt löser sig relativt enkelt och inte många komplikationer antyds ens. Med andra ord skulle man kunna tro att hans böcker är tråkiga och fantasilösa. Av någon väldigt knepig (och av mig obegriplig) anledning är böckerna ändå läsvärda. De blir gedigna av alla historiska referenser han saxar in, och av hans politiska kunnande. Dessutom är ingenting svart eller vitt utan alla lever i en "verklig" värld bland alla rymdskeppen och alla olika planeter. Personerna i böckerna blir trovärdiga och inte allt för stereotypa. Jag kan inte säga att jag VARMT rekommenderar hans böcker, inte som jag utan tvekan kan göra med Tanya Huffs böcker. TH har det där lilla extra, knorren, saltet, innerligheten... David Weber Är underhållande och trivsam men när jag läst ut hans senaste kommer de att få ligga i bokhyllan Väldigt Lång Tid innan jag ens ska tänka på att läsa om alla...

Tanya Huff däremot ska jag börja läsa om nu när jag blir klar med DW... Den senaste boken i hennes serie om sergant Kerr kom levererad med posten igår och jag läser GÄRNA om alla hennes böcker som jag redan har innan jag kastar mig över den nya godbiten. Det gäller att suga på de goda karamellerna...
