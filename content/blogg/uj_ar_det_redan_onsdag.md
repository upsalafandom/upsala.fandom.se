+++
title = "Uj, är det redan Onsdag?"
slug = "uj_ar_det_redan_onsdag"
date = 2008-04-09

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Kåserier"]
taggar = ["David Weber", "hård sf", "rekommendationer"]
+++

Det är meningen att jag ska blogga här varje Onsdag. Det är inte klokt vad en vecka går fort!

Idag tänkte jag skriva lite om David Weber och hans serie om Honor Harrington.

Jag gillar ju "hård SF" med rymdresor, interstellära krig, mm och har till exempel svårt att vänta på [Tanya Huffs](http://www.fantasticfiction.co.uk/h/tanya-huff) nya bok om "Staff Sergeant Torin Kerr's" vidare äventyr i "Valor" serien. I väntan på att den senaste boken i den serien ska skickas ut (ja jag har beställt den) så nosade jag runt för att hitta något som fick höga betyg och trevliga recensioner. Jag  fann [David Weber](https://en.wikipedia.org/wiki/David_Weber). Klick, klick, (internethandel gillar jag) så använde jag en stor del av mitt senaste stipendium och SF-bokhandeln i Stockholm skickade ett jättepaket fullt med nya spännande böcker. Men nu, 3 böcker senare, börjar jag ana att de är som de är och blir inte bättre.  Ja, Honor är duktig, ja, hon är en riktigt bra officer, ja, hon... Men, NEJ, jag begriper inte haussen. Kan någon förklara för mig varför serien om Honor Harrington blivit SÅ populär?

Och kan någon rekommendera någonting annat som jag kan bita i när tanyahuffabstinensen blir för stark?
