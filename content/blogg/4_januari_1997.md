+++
title = "4 januari 1997"
slug = "4_januari_1997"
date = 1997-01-04

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Orvarsmöten säsong 11 inleddes igår tisdag 7/1 på O'Connor's. Tanken är att
de ska hållas på

**O'Connor's pub**

på Stora torget i Uppsala från

**18.30**

och framåt

**första tisdagen i varje månad.**

<!-- more -->

Följande fans (neofans och urgambla veteraner) visade sina nyllen:

Johan Anglemark, Joakim Bjelkås, Magnus Eriksson, Linnéa Jonsson, Karin
Kruse, Jan Nyström, Sten Thaning, Kristin Thorrud, Anna Åkesson, Björn X
Öqvist, Matthias Palmér

Vi rekommenderade böcker åt varandra, diskuterade programpunkter på vår
kommande oktoberkongress (bl a en där jag och Magnus får tala om varför vi
hatar maskerader på kongresser, och en McGyver-workshop). Vi diskuterade
också möjligheten och vitsen med att utge ett samhäftat Uppsala-fanzine. Sten
undrade varför han inte fått Lenins samlade verk än som han vill köpa och
Karin förklarade att hon hade en annan intressent (en Lenin-klon, 1.57 lång,
skallig med vegamössa och pipskägg) till denna aparta boksamling som vi
försökt sälja i snart 10 års tid. Månne är Nisse Lenin åter poppis? Karin
meddelade också att hennes Leninkopia kan vrida armarna ur led på sig själv,
vilket möttes med förundran.

Övrigt av intresse som diskuterades har fallit mig ur minnet, eller så
pratades det om det efter halv tio då jag och Magnus avtågade.

Den konversationsförsvårande funktion som alkohol brukar kunna stå till
tjänst med hade vid det laget ersatts av en ointressant trubadur. Vi beslöt
att försöka ta det stora bordet bakom garderoben, längst från scenen nästa
gång.

— _Johan_

PS. Jo, vi snackade också om att se på video tillsammans. Detta ledde till en
nygammal kvalitetsdiskussion.
