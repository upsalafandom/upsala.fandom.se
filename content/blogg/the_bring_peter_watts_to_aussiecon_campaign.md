+++
title = "The Bring Peter Watts To Aussiecon Campaign"
slug = "the_bring_peter_watts_to_aussiecon_campaign"
date = 2010-06-16
lang = "en"

[taxonomies]
forfattare = ["George"]
kategorier = ["Nyheter"]
taggar = ["Aussiecon", "Hugo", "Peter Watts"]
+++

Dr Peter Watts' story, "The Island," was nominated for a Hugo. Let's [support this initiative](http://catsparx.livejournal.com/200299.html) to get him to Aussiecon, so that he can attend the Hugos and the con. It's a raffle with an interesting prize.
