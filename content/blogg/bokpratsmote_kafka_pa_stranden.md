+++
title = "Bokpratsmöte – Kafka på stranden"
slug = "bokpratsmote_kafka_pa_stranden"
date = 2008-02-16

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Inbjudningar"]
taggar = ["bokprat", "böcker"]
+++

På onsdag är det, som synes i vårt [kalendarium](./sidor/kalendarium.md),
dags för den här omgånges sista bokpratsmöte. Den aktuella boken är Haruki
Murakamis _Kafka på stranden_ (_Umibe no Kafuka_), som vann World Fantasy
Award 2006. Det är en någorlunda komplex bok, men ändå inte svårtillgänglig.
Den som läser snabbt och har mycket tid över borde kunna hinna sluka den på
ett par dagar, även om den är tjock. Eller i alla fall har kraftig benstomme.

Onsdag 20/2 klockan 19:00, hemma hos Björn Lindström på Sysslomansgatan 12.
Oavsett om vi ses där eller inte så rekommenderar jag boken. Det är inte en
fantastisk, omskakande upplevelse, men det är en intressant och välskriven
bok med skönt flyt. Det är inte att förakta.
