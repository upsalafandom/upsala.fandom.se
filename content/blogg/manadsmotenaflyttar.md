+++
title = "Upsalafandoms månadsmöten flyttar"
slug = "manadsmotenaflyttar"
date = 2009-05-29

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Inbjudningar", "Nyheter"]
taggar = ["Pipes of Scotland", "Williams", "pubmöten"]
+++

Upsalafandoms första-tisdagen-i-månaden-pubmöten byter mötesplats från och med nästa möte, nu på tisdag 2 juni.

Den pub som vi brukade träffas på har bytt ägare och plötsligt stod den trevliga serveringspersonalen utan anställningsavtal, för att den nye ägaren förhalade undertecknandet av dem i all oändlighet. De sade upp sig och talade om för sina vänner vad som hade hänt.

Vi har inte varit odelat nöjda med puben i vilket fall efter ägarbytet, så vi flyttar. Vi får se var vi hamnar till slut, men till en början försöker vi med Pipes of Scotland (den pub där dead-doggen efter Kontext hölls).

**Vad:** Upsalafandoms 150:e månatliga pubmöte
**Var:** [Pipes of Scotland](http://www.pipesofscotland.se/uppsala), puben som ingår i Hotel Uppsala i korsningen Kungsgatan-S:t Olofsgatan.
**När:** Tisdag 2 juni, 18.30 och framåt

Vi får se om vi orkar hitta på något extra för att det både är 150:e mötet sedan traditionen återupptogs januari 1997 **och** inflyttningsfest på Pipes of Scotland!
