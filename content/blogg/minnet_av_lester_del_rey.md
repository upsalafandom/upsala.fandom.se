+++
title = "Minnet av Lester del Rey"
slug = "minnet_av_lester_del_rey"
date = 2009-03-02

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Lester del Rey", "historia", "litteratur", "science fiction"]
+++

Nyligen fick bokpratargruppen [Fearless Fantasy
Fans](http://fearlessfantasy.wordpress.com) ärva en boksamling. Jag råkar ju
ha lyckan att bo med en av de drivande krafterna i gruppen, och dessutom bo i
den lägenhet där bö§ckerna förvaras för vidare spridning i gruppen.
Möjligheterna att botanisera i samlingen har alltså varit goda, och jag har
funnit en del pärlor.

En sak som slog mig, var att det fanns många samlingar med "best of" diverse
mer eller mindre stora namn i genren. Det verkar som om The Science Fiction
Bookclub gett ut en hel del sådana. Flera av dessa var intressanta, och jag
kanske kommer att skriva mer om dem senare, men en som speciellt fångade min
blick var den som samlade berättelser av [Lester del
Rey.](https://en.wikipedia.org/wiki/Lester_Del_Rey)

<!-- more -->

Vilka av läsarna har hört talas om namnet del Rey? Är det via Del Rey Books,
månne? Själv hörde jag talas om namnet på det viset, och då fick jag även
veta att det var Lesters fru [Judy
Lynn](https://en.wikipedia.org/wiki/Judy-Lynn_del_Rey) som namngett Del Rey
Books. Lite efterforskningar (på Wikipedia, som man gör nu för tiden) gav vid
handa att han åstadkommit en hel del, och även fått fina priser för sina
insatser. Så, är det bara jag som har dålig koll, eller är han inte så känd
numera?

Nu kunde jag förstås inte låta bli att läsa lite i den _Best of Lester del
Rey_ volym som bor hemma hos oss. Ganska snart fick jag faktiskt en
hypotes om varför Lester kanske är en av de bortglömda författarna. Jag ska
presentera mina intryck, och har du några egna är jag sugen på att höra dem.
Vårt exemplar av John-Henris _Inre Landskap, och yttre rymd_ är ju
t.ex. i Upsala, så jag vet inte om han kanske skriver nåt insiktsfullt där.

Längst bak i boken fann jag det som verkade intressantast, så det löste jag
naturligtvis först! Lester gav där sina egna kommenterar och reflektioner
till de noveller som ingick i samlingen. Där lärde jag mig, och det
bekräftades av mina senare efterforskningar, att han var en av de författare
som ingick i John Campbells stall av regelbundna bidragsgivare till
_Astounding_ och hans andra magasin. Dessutom verkade det som av de
berättelser som här framställdes som Lesters bästa, de var i stor mån inte
hans egna idéer utan Campbells. Nu var ju det inget konstigt i den kretsen
men det är kanske en del till förklaringen till att han numera inte är en
författare som man kommer att tänka på så ofta.

Hur använde då Lester del Rey de här idéerna? Nu kommer det fram ett par
intressanta drag i de berättelser jag läst i samlingen. Några av de bästa
berättelserna är förvånansvärt klena på klassisk "sensawunda". Det betyder
dock inte att de är dåliga på något vis, men på nåt vis känns det typiskt att
en av de mer minnesvärda berättelser jag läst av Lester del Rey handlar om en
döende neanderthalares sista dagar och hans oförmåga att leva med det faktum
att hans värld har förändrats med ankomsten av de Cro Magnon som nu sprider
sig över jorden. Inget alternativhistoriskt, eller teknologiskt ingrepp i
historien alls. Det är bara en väldigt human berättelse om hur en gammal man
sörjer en svunnen tid och hur han som den siste av sin sort inte längre orkar
leva. Riktigt rörande och inte en berättelse som på något vis breddat genren
eller utvecklat dess vokabulär.

Kan det kanske vara så att det bästa Lester del Rey skrev var
hantverksmässigt och hyggligt jobb, baserat på Campbells sprudlande fantasi,
men att han själv inte hade de där stora visionerna? Skrev han när han skrev
som bäst, saker som var rörande och engagerande, men utan den där gnistrande
juvelen som folk långt senare återkom till och polerade, som ansibeln,
terraformning eller positronhjärnor med inbyggda moralregler? Osökt kommer
jag att tänka på Clifford D. Simak, en författare vars verk nu knappt står
att finna, och som så vitt jag vet aldrig skapade nya svindlande idéer som
sedan kommit att ingå i genrens gemensamma rekvisita. Simak var dock en
mästare på subtil stämning och hade ofta en human och lite existentiell ton i
sina bästa verk. Vårdar genren helt enkelt inte sina finstämda och lågmälda
berättare?

Det kan ju vara så att alla utom jag känner väl till Lester del Rey, och att
hans verk trycks om och läses om i kretsar där jag inte rör mig. Men, jag
undrar om det är så. Måste man kanske introducera något stort, svindlande och
konkret fär att inte riskera att bli bortglömd eller i alla fall falla ur
tryck? Hur många av de lågmälda berättarna från förr känner **du** till?
