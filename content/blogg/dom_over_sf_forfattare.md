+++
title = "Dom över sf-författare"
slug = "dom_over_sf_forfattare"
date = 2010-03-20

[taxonomies]
forfattare = ["Lennart Jansson"]
kategorier = ["Nyheter"]
taggar = ["Peter Watts"]
+++

Den kanadensiska sf-författaren Peter Watts förklarades i fredags skyldig
till överfall på en gränsvakt i USA. Exakt vad som har hänt är inte helt
klart men det verkar som om vakterna överreagerat rätt ordentligt. Det är
inte första gången de har uppträtt arrogant, de verkar vara närmast
slumpmässiga i sin ämbetsutövning. Domen kommer i slutet av april.
Förhoppningsvis inte det strängaste tänkbara straffet, vilket är två år, för
något som närmast motsvarar våld mot tjänsteman.

[Nyhetsartikel om händelsen](http://www.cbc.ca/canada/toronto/story/2010/03/19/watts-convicted.html).

Bakgrunden finns på [https://en.wikipedia.org/wiki/Peter_Watts](https://en.wikipedia.org/wiki/Peter_Watts).
