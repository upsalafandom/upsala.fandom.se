+++
title = "Stafetten 1: Gabriel Nilsson, Uppsala"
slug = "stafetten_1_gabriel_nilsson_uppsala"
date = 2008-07-30

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Intervjuer"]
taggar = ["författande", "rollspel", "speldesign", "uppväxt"]
+++

> Jag kommer under en tid framöver att utföra personliga intervjuer med ett
> antal människor inom svensk fandom eller som på annat vis är involverade i
> det här med sf och fantasy. Resultaten presenterar jag här på
> Upsalafandombloggen. Först ut är Gabriel Nilsson!

Jag föddes i Timrå i slutet av 70-talet men flyttade 1,5 år gammal till
Piteå, och jag bodde under hela min uppväxt i en radhuslänga i utkanten av
staden. Föräldrarna bor kvar där ännu. Min far var och förblir skäggigare än
jag är, och min mor har fortfarande inte haft mer än fjorton födelsedagar.
Pappa ägnar sig gärna åt släktforskning, till exempel, och mamma är förtjust
i musik. Syskon finns det två av, en yngre syster som regerar på Guitar Hero
och en ännu yngre bror som ser rolig ut på kort.

<!-- more -->

# Jag växte upp med spel

Tonåren ägnade jag i huvudsak åt rollspel. Några äldre kusiner drog ner
mig i träsket en sommar, jag bör ha varit runt sju, och sedan var jag fast.

När kusinerna lärde mig hade de inget rollspel med sig, så de konstruerade en
enkel regeluppsättning på plats, och den fortsatte jag att använda av bara
farten. Efter något inledande år med detta hemmagjorda rollspel fick jag ett
exemplar av Drakar och Demoner. Mina kompisar och jag skrattade gott åt den
rekommenderade åldersgränsen på elva år, har jag för mig att det var. Hela
gänget var yngre än så, och vi hade redan spelat länge. Vi var små, men vi
hade kul. Vad vi uppfattade som väsentligt skiftade väl så småningom. Vi
konstruerade/improviserade ändå fram alla våra äventyr själva, så de blev
naturligt anpassade efter vår ålder.

Senare samlade jag på mig fler och fler Drakar och Demoner-produkter och
började använda regler från Äventyrsspels tidskrift Sinkadus och dylikt. Med
tiden fick jag hitta på egna lösningar för att få det hela att gå ihop, men
jag konstruerade inga helt egna rollspelssystem förrän i övre tonåren. Innan
dess höftade jag nog oftast då de nedtecknade reglerna var otillräckliga
eller otillfredsställande.

Spelandet fortgick med oförminskad styrka i många år innan det tappade lite
fart på gymnasiet. Men egentligen var jag nog mer storyteller än spelledare.
Det roligaste var att konstruera världar, platser, personer och historier.
Att spela igenom äventyren var mer en formalitet. Jag är ganska säker på att
fröet till mina författarambitioner fanns där i rollspelandet. Någon gång vet
jag att jag skrev runt 40 A4-sidor inför ett äventyr vi skulle spela igenom.

På den tiden konstruerade jag mina världar allt eftersom utifrån de behov som
historierna gav upphov till. Det var äventyren som var motorn i skapandet.
Idag kan motorn vara lite vad som helst. Beroende på vilket hörn jag börjar
nysta i behöver det för min del inte inledningsvis vara så stor skillnad på
att skriva något avsett för läsning och något avsett för spel. Jag har flera
gånger konstruerat världar, myter, personer och historier som jag inte
riktigt vet vad jag ska ha till, och ett par av mina opublicerade romaner
utspelar sig i världar som började som rollspelsvärldar. Men material som jag
skapar i en sådan utvecklingsfas används och presenteras förstås på helt
olika sätt i romaner och i rollspelsäventyr. Jag är mer noggrann med språket
än jag behöver när jag skriver rollspelsmaterial, men vid skönlitterärt
författande finns det ytterligare nivåer i texten som ska planeras och
putsas. Det är inte lika komplext att skriva rollspelsmaterial, tycker jag,
men jag har i och för sig aldrig skrivit äventyr avsedda för andra
spelledare.

Egentligen finns det en ungefär lika påtaglig skiljelinje mellan att skriva
noveller och romaner som mellan att skriva romaner och rollspelsmaterial för
min del. Novellförfattandet har i stort sett inget gemensamt med
rollspelsförfattandet. Mellan dessa extremer ligger romanerna som en
förbindande länk.

Från övre tonåren och framåt har jag konstruerat högar av brädspels- och
rollspelssystem. Bara en bråkdel av dem har testats och använts. Till att
börja med drevs jag nog mest av irritation på brister i existerande system,
men senare har det lika mycket blivit ett avkopplande nöje.

När det gäller brädspel är nog favorittypen någorlunda snabbspelade
strategispel. Jag tenderar att koncentrera mig ganska intensivt, och tar det
längre tid än kanske fyra timmar att spela igenom protesterar hjärnan.
Idealspelet har ett enkelt grundläggande system, men med inbyggda finesser
som ger komplexa taktiska möjligheter. Blood Bowl är förmodligen det brädspel
som jag har spelat mest, men det är till stor del för att det kan spelas
online på fumbbl.com, där jag går under namnet Bonehead. Det är utmaningen i
att försöka överlista en mänsklig motståndare som lockar mig. När det gäller
datorspel handlar det dock inte bara om lugn kamp mellan hjärnor. Jag spelar
gärna actionfyllda krigsspel också. Helst i lag. Viktigast är att det finns
ett mänskligt motstånd att kämpa mot. Att spela mot datorer är för enformigt
i längden.

# Andra intressen

Vi spelade även en hel del innebandy under uppväxten, men det har jag nog
mindre att säga om. Fast en gång sköt jag stolpe-stolpe-in från motståndarnas
förlängda mållinje på en match. Målvakten stod aningen för långt ut. Gräsligt
snyggt mål, om jag får säga det själv. Nå, min klubba var väl någon decimeter
ut från linjen när jag sköt.

Innebandyn låg på is under många år men återupptogs igen för något år sedan.
Glatt överraskad upptäckte jag att mycket av spelsinnet fanns kvar, och med
tiden har konditionen närmat sig den från fornstora dar. Jag drivs nog av
samma tävlingsinstinkt när jag tampas med motståndarna på innebandyplanen som
när jag deltar i novelltävlingar.

Jag vill även minnas att jag till och från läste böcker under uppväxten. Mest
fantasytegelstenar på den tiden, men det slank ner en del sf också.

# Mitt liv i Uppsala

Till Uppsala flyttade jag direkt efter gymnasiet, 1997, och sedan har jag
blivit kvar. Det kunde lika gärna ha blivit Umeå. Någon gång får jag skriva
en alternativhistorisk novell om vad som hade hänt om jag hade flyttat dit
istället. Att det blev Uppsala berodde nog på att gubben som kom till vårt
gymnasium från Uppsala för att presentera universitetet var bättre än killen
från Umeå på att övertyga mig om ortens förträfflighet. Dessutom ligger
Uppsala längre från polcirkeln.

Tills vidare trivs jag bra här. Staden är inte större än att man kan ta sig
vart man vill med cykel, och det är nära till Stockholm när man vill dit.
Tillgång till ett universitet med ett brett sortiment av lustiga små kurser
är ett måste. Den ynkliga tillgången till vatten är visserligen en nackdel,
men eftersom jag ändå inte har tänkt använda vattnet till så mycket mer än
att titta på det går det bra ändå. Det kunde gott få vara sommar året runt
dock. Förr eller senare kommer jag nog att emigrera längre söderut.
Någonstans där det är 25 grader och sol året runt.

Här i Uppsala har jag pluggat teoretisk och praktisk filosofi, matematik,
datavetenskap, litteraturvetenskap, svenska, statskunskap, engelska, spanska,
konstvetenskap och en hög småkurser som exempelvis tidiga kulturer i östra
Medelhavsområdet, anatomi och Herren Gud 5p. Vissa kurser lästes i ett försök
att knyta ihop säcken till en lärarexamen, men när jag väl hoppade på
lärarprogrammet hösten 2007 insåg jag det är psykologiskt omöjligt att gå
det. I övrigt krävs det nog god fantasi för att se någon röd tråd.

Jag läser fortfarande böcker, så klart. I år har jag mest läst serier,
eftersom jag har för avsikt att prova på att åstadkomma egna sådana och vill
orientera mig lite. Oavsett medium avnjuter jag helst verk med i någon mening
originell uppbyggnad. Sådant som jag inte ens hade tänkt på att man kunde
göra. Dostojevskijs Anteckningar från källarhålet är ett exempel på ett
sådant verk. Alan Moores Watchmen är ett annat. Framställningen bör även vara
skickligt genomförd. Skönlitterär text bör vara välskriven. Serier bör
dessutom vara estetiskt tilltalande. När det gäller film tillkommer ännu fler
krav.

# Fandom

Min väg till sf-fandom var tämligen slumpartad. Någonstans annonserades det
om Swecon i Uppsala ... hmm ... 99? Så jag gick dit, tittade på fåniga
paneldebatter och pratade strunt med Michael Swanwick. Just när jag var i
färd med att beskriva en novell som jag en gång hade läst traskade Ahrvid
förbi och snappade upp mina ord. "Var inte den novellen i Nova?" undrade han,
och jag sa att det kunde den mycket väl ha varit, varpå vi växlade några ord.
Samtalet avslutades med att han rekommenderade mig att gå med på
SKRIVA-listan, vilket jag också gjorde. Den har varit mycket betydelsefull.
Framför allt har novelltävlingarna utgjort en motivation till att skriva
något överhuvudtaget. Till att börja med hände det väl även att jag tog något
råd till mig.

Men jag hade inget fortsatt samröre med fandom förrän året därpå då jag läste
en kurs i planetgeologi med en ung kvinna som i fandomkretsar gjort sig känd
under namnet Åka. Hon upplyste mig om att fandom existerade, vilket inte var
något jag reflekterade över ens när jag var på kongressen, och hon drog med
mig till Williams.

Mitt mest bestående intryck av fandom är att toleransen för individuella
egenheter tycks vara högre än i de flesta andra sammanhang. Jag ser mest
fördelar med det. Individualist som jag är ogillar jag försök att stöpa om
människor i en och samma form. Stundtals kan väl dock tålamodet prövas av
personer med okonventionellt socialt beteende. Huruvida jag själv tillhör den
tålamodsprövande skaran eller inte överlåter jag nog helst till andra att
bedöma.

# Skapande

För ca tre år sedan väcktes hastigt och lustigt ett intresse för teckning som
bara har vuxit med tiden. För tillfället lägger jag mer tid på teckning än på
författande, och jag ska gå en konstlinje på Wiks folkhögskola till hösten.
Det känns lite bakvänt att börja på folkhögskola nu efter elva år som
universitetsstudent, men det kan säkert bli intressant. Får man tro
utomstående bedömare tycks jag, oväntat nog, ha talang för teckning, och om
det visar sig att jag även kan lära mig konsten att måla vill jag i framtiden
gärna framställa bildkonst av något slag. Serieskapande är till exempel något
jag har tänkt prova på. Eftersom jag även gillar spel i alla möjliga former
har jag funderat på att ge mig in i datorspelsbranschen. Där är det framför
allt en position som game designer som lockar.

I likhet med Alan Moore tycker jag inte att en skapande människa bör fastna i
en stil. Eller rättare sagt: Jag imponeras mindre av en skapande individ som
håller sig till en stil än av en med större förmåga att variera sig. Som jag
ser det ligger den största utmaningen i att bryta ny mark.

Hittills har det nog aldrig hänt att ett intresse för ett medium har
försvunnit. Nya intressen läggs till de gamla allt eftersom, med gradvis
förflyttat fokus. Mängden tid och energi är begränsad. Dessutom är vissa
projekt svårare än andra att ta itu med. För att kunna skapa datorspel eller
film skulle jag behöva en hel del folk och pengar, så jag håller mig mest
till texter och bilder för tillfället.

Överhuvudtaget har jag påtagligt svårt att begränsa mig till ett karriärval,
ett medium, en genre, osv. I mina studier tycker jag mig ständigt se samband
som löper kors och tvärs mellan olika ämnen, och i skapande verksamhet vill
jag helst inte låta tanken begränsas av existerande begrepp och
klassificeringssystem i butiker.

Du frågar om framtiden? Ja, om tio år bor jag förhoppningsvis inte kvar i
Uppsala utan ägnar mig först och främst åt skapande verksamhet i min herrgård
vid Medelhavet (eller på Mars).
