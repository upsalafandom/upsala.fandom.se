+++
title = "Stargate SG-1"
slug = "stargate_sg_1"
date = 2008-02-15

[taxonomies]
forfattare = ["Lennart Svensson"]
kategorier = ["Recensioner"]
taggar = ["teve"]
+++

Vi har lanserat en ny hemsida för Uppsalafandom, och till den ska höra en blogg – denna blogg. Och själv har jag ombetts att skriva i bloggen, gärna det...!

Detta är alltså mitt första blogginlägg i detta forum. Och jag tänkte jag skulle skriva om TV-serien [Stargate SG-1](http://imdb.com/title/tt0118480). Den går på Kanal 6 på vardagar kl 1800 och är väl värd att följa.

Jovisst är den det; ge den några chanser och du ska se att du är fast. Hursomhelst är det en smart serie, av flera skäl. Jag kan här förbigå sånt som bra rollbesättning (den har det, nog om det) och lagom påkostad scenografi, för det bästa med den är själva intrigerna, själva mångfalden i intrigtrådar.

Legenden är att det på 1900-talet hittats en gåtfull artefakt i Egypten, en hyperrymdsport, en port till stjärnorna som man med tiden lärt sig använda – men det är top secret, government denies knowledge! USA:s försvar tar sig via denna Stargate till främmande världar, möter främmande folk och får del av avancerad teknologi, och med tiden invigs övriga supermakters regeringar.

Det smarta intrigupplägget är nu att det hela utspelas i nutid, i tidigt 2000-tal, och därmed kan man relatera till figurerna på ett annat sätt än i exempelvis Star Trek. SG-figurerna kan otvunget skämta om aktuella filmer, populärkultur och aktuella förhållanden, vilket osökt får en att identifiera sig med dem. I sina civila liv kör de även dagens bilar, bor i vanliga hus osv.

Och det kan förekomma intriger i dagens värld med konflikter mellan flygvapnet och CIA, med Ryssland och annat dylikt. Men lika osökt kan ett avsnitt utspelas på en främmande planet, dit man kommit via den alltid så behändiga stjärnporten. Man kan med andra ord ha Star Trek-liknande avsnitt.

För det tredje kan man fara ut i rymden med rymdskepp byggda med alien-teknologi, vilket åter gör det Star Trekkigt – men utöver detta har vi som sagt nutidsförankringen med allt vad där tillhör. Och det är denna mix av nutid och oändlighet, av vardag och fantastisk verklighet, som ger "Stargate SG-1" sin osökta charm.
