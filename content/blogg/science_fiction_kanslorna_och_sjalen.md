+++
title = "Science fiction, känslorna och själen"
slug = "science_fiction_kanslorna_och_sjalen"
date = 2008-05-19

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["galen vetenskapsman", "kropp", "känslor", "medvetande"]
+++

På sistone har jag läst ett par saker som fått mig att fundera lite på det här med den vacklande och vinglande inställningen till känslor i science fiction. Det finns säkert väldigt mycket att säga om saken, men för tillfället knattrar jag bara ur mig ett par lösryckta tankar.

En bok jag läser nu (Final Theory, en vetenskapsbaserad thriller av Mark Alpert) innehåller en person som lämnat fysiken för att syssla med robotar och artificiell intelligens. Han säger vid något tillfälle att människan är för dum för att kunna hålla fred. Han började syssla med AI i förhoppningen att vi skulle kunna lära oss mer om oss själva på den vägen och så kanske kunna nå en högre form av medvetande eller mognad.

Jag läste också artikeln [Von allen guten Geistern verlassen?](http://www.gwup.org/skeptiker/archiv/2004/4/mad_scientist.html), som handlar om "galna vetenskapsmän" på film. Där sägs att det som skiljer de riktigt onda forskarna från de som bara är tragiska är känslorna. Det är ett exempel på det ganska vanliga sf-motivet att det är känslorna som gör oss mänskliga, och de som distanserar sig från känslorna (till exempel till förmån för att renodla sitt förnuft) också förlorar sin mänsklighet.

I det första exemplet är det känslorna som är farliga, och man är ute efter ett sätt att kunna vara smart nog att komma runt deras destruktiva tendens. I det andra exemplet är det förnuftet som är farligt, och känslorna som är den räddande aspekten. I båda fallen handlar det om kärnan i mänskligheten.

Det verkar också vara ganska vanligt i en viss sorts äldre sf att tillskriva människor en sorts speciell egenskap X, som gör oss mer överlevnadsfähiga än de främmande varelser vi konfronteras med. Jag har intrycket att den här egenskapen ofta är känslobaserad. Samtidigt är ganska mycket sf väldigt cerebral, fokuserad på att beskriva sätt att tänka sig ur problem. Man måste hålla huvudet kallt och göra det som är förnuftigt. Förnuft och självmedvetande får ofta det som definierar "mänsklighet". Jag tänker på C.S.Lewis begrepp hnau, en förnuftig varelse som är likvärdig människan. I annan sf finns det ganska ofta hnau som tillverkats av oss, som robotar, AI:er, eller "upplyfta" djur.

Sf är också mycket dualistisk: man kan dela upp en människa i kropp och själ, eller snarare hårdvara och mjukvara. En välanvänd kliché är det här med uppladdning, folk som överför sitt medvetande och sin personlighet till en dator (eventuellt försedd med en robotkropp för att kunna manipulera omvärlden). Man tänker sig möjligheter att kunna skriva om sin personlighet, att knoppa av minnen och gömma dem för sig själv, att förlänga sinnena till att uppfatta sånt som människor inte kan eller kontrollera andra sorters kroppar i form av rymdskepp eller vad det nu kan vara. Det är tydligt att man inte tänker sig att någonting essentiellt går förlorat om man gör sig av med kroppen, även om det finns något väldigt sinnligt i flirtandet med tanken på kroppar med utökade förmågor.

Jag har ett litet problem med hela den här uppladdnings-idén, och det är just det där med känslor. Hur mycket av vår personlighet sitter inte i den ständiga återkopplingen med kroppen? Alla manér, reflexer, instinkter. Ganska mycket självuppfattning, hur man bedömer sin relation till andra utifrån hur man fysiskt positionerar sig i närheten av varandra. Alla passioner, all längtan, motivation, sorg... Vi är fullständigt marinerade i hormoner, och det är en ganska komplex apparat det här med att reagera på saker. Vi känner känslorna i kroppen: pulsen, ansiktstemperaturen, muskelspänning. Jag kan inte se det som särskilt lättvindigt att översätta detta till något som liknar ett datorprogram, och jag kan tänka mig att det stackars amputerade medvetandet skulle dras med svåra fantomsmärtor. Man kan tala om själen som en aspekt av människan, men jag har så svårt att se den som en separat del.

Hela saken var roligt hanterad i Robert Sheckleys humoristiska (och tragiska) Mindswap, där en person färdas kring galaxen genom att byta kropp med andra varelser. Han förlorar ganska mycket av sin personlighet och känsla för sig själv, vilket känns rätt realistiskt. Det känns ändå som något rätt ovanligt.

Känslor är ju en sorts förnuftets genvägar, ett sätt att tänka som inte behöver passera det långsamma medvetandet varje gång ett beslut ska fattas. Känslor kan vara baserade i instinkter, men de är starkt formade av erfarenheter. Vi gillar ofta saker vi känner igen, vi bromsar och girar innan vi hinner bli medvetna om hindret på vägen, vi upprepar gamla misstag, vi kan träna oss i att bli mer optimistiska. Allt hänger på att vi har sett och reagerat på saker förr, och att hela systemet lärt sig reaktionen. Det har ju dessutom visat sig att känslor är i princip nödvändiga för att man ska kunna klara av att fatta ett rationellt beslut, det räcker inte med att förstå alla delar av problemet. Kanske är hela dikotomin känslor-förnuft en falsk uppdelning?

Å andra sidan kan man se det som att medvetandet är ganska onödigt, det skulle kanske gå att klara sig med bara förnuft genom känslor och instinkter. Peter Watts har visst utvecklat det tankespåret i sin _Blindsight_, men jag har inte läst den ännu och vet inte vad han gör av det förutom att det lär ska vara läskigt.

Det här är i stort sett gamla tankar, som jag haft sedan länge, fast lite i nytt ljus. Ska man ta populärkulturen på allvar som en spegling (eller förmedlare, orsak och verkan är väl inte alltid helt klar) av människors känslor får man säga att många är rädda för galna vetenskapare i någon form. Galenskap kan komma i form av för mycket känslor eller för litet känslor, men den kalkylerade ondskapen verkar ofta mer skrämmande på film och i böcker än den okontrollerade ilskan (hämndlystnad är ofta rent glorifierad).

För att knyta tillbaka till det första exemplet ovan, med fysikern som blev AI-forskare: jag skulle vilja beskriva andlig mognad som förmågan att känna sina känslor tillräckligt väl för att veta när man ska följa dem och när man ska sätta sig över dem. Förmodligen behöver man en _känsla_ för det, för annars skulle man redan ha följt känslan innan förnuftet börjar sätta sig emot.

Jag har väl ingen riktig slutsats att knyta ihop det här med, mer än att upprepa att jag tycker att det är intressant att se olika människors (författares) tolkning av vad essensen i vår mänsklighet är, och särskilt vad den har för koppling till kroppen.
