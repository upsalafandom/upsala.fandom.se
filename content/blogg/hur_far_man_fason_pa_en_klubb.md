+++
title = "Hur får man fason på en klubb?"
slug = "hur_far_man_fason_pa_en_klubb"
date = 2008-03-16

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = []
+++

Att prata med andra om böcker är nåt jag nästan alltid gillar. Det brukar vara en given öppning för ganska meningsfullt tankeutbyte, och så är det ju alltid skoj att träffa människor med gemensamma referensramar och intressen.

Som de flesta vet bor jag med min familj i Kanada sedan september. Jag saknar det fanniska umgänget i Uppsala (vilket jag skrev om [här](./blogg/upsala_i_mina_drommar.md)), och jag försöker göra vissa ansträngningar för att pigga upp den lilla fantastikläsargruppen vi har här. Det ger mig vissa insikter om både det ena och det andra.

Det finns faktiskt vissa fördelar med att ha en rätt liten grupp. Vi är fyra medlemmar just nu, och om alla kommer på ett möte är det ganska lagom många för att gå laget runt och prata om en och samma bok eller novell. Det går också rätt fort att lära sig varandras individuella smak, och det är ganska kul att se vad som händer när man konfronteras med varandras favoriter.

Nackdelen är förstås att det blir ganska tråkigt om inte alla dyker upp. Det är svårt att få riktig styrfart i en så liten grupp, och det är svårt att lyckas göra någonting utanför de månatliga mötena. Det är väldigt tydligt att det är två personer som drar i gruppen och får saker att hända. Kommunikationen mellan mötena är också nästan obefintlig, och det är nog en del av förklaringen till att det är svårt att värva fler medlemmar. När man får nys på potentiellt intresserade måste man nog fånga deras intresse omgående, så att man inte försvinner i strömmen av andra föreningar och evenemang som kämpar om allas uppmärksamhet.

Om de första medlemmarna är kompisar sedan tidigare, eller sådana som brukar diskutera med andra på nätet och därför är lätta att ha utbyte med mellan mötena, är det nog lättare att få nykomlingar att känna sig som en del av en grupp. Det har funnits många som verkat lite intresserade men som aldrig har hört av sig igen efter den första frågan. Tjejen som först drog igång gruppen jobbade tidigare på ett billighetsantikvariat där hon hängde upp plakat om mötena, och hon säger att många verkade tycka att det var en bra idé med en grupp av läsare som kan utbyta tankar och lästips -- men de dök aldrig upp på något möte.

Jag misstänker att det ganska mycket är en fråga om att känna sig engagerad och välkommen. Om man inte på förhand är riktigt intresserad av precis en sån här grupp kommer man kanske inte att söka upp den, men om man möter människor som man vill umgås med behövs det bara en förevändning för att träffas. För mig personligen är det ju umgänget jag är ute efter -- boktips kan jag få ändå, och jag har egentligen inget bokpratsbehov som kräver utlopp i den här formen. Jag vet bara att människor som läser är folk som jag har något gemensamt med och som jag förmodligen kan tycka om att umgås med. Jag vill ha spretiga och kaotiska möten, med många deltagare som pratar i munnen på varandra och som har så trevligt att de inte vill gå hem efter en timme.

Därför har jag försökt engagera mig i att locka till oss mer folk.

Jag började med att sätta upp en gruppblogg, och sen göra affischer med URL:en dit. Tanken var att det ska gå lätt att hitta information om tid och plats för nästa möte, så att man bara kan dyka upp om man är nyfiken, men också att det ska funka som en allmän online-resurs för dem som vill vara med, där vi kan lägga upp information som är bra eller kul att ha tillgänglig.

Ganska många har kollat in på [vår blogg](http://fearlessfantasy.wordpress.com), men bara två har hört av sig, och ingen har dykt upp på något möte. Vad saknas? Jag tror att det kan behövas lite mer tecken på att det faktiskt händer saker, så jag har bestämt mig för att uppdatera bloggen en gång i veckan. Det kommer förmodligen ändå inte att räcka. Min erfarenhet säger mig att det som behövs troligtvis är att vi som redan är med faktiskt möter och pratar med människor och skapar lite kontakt, IRL eller online. Synlig verksamhet och tillgänglig information räcker inte, det måste till lite mänsklig kontakt -- men hur hittar man de intresserade?

Vad tror ni? Hur värvar man folk egentligen?
