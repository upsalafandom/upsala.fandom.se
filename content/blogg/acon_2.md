+++
title = "Åcon 2"
slug = "acon_2"
date = 2008-02-13

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Inbjudningar"]
taggar = ["kongresser"]
+++

Jag måste få slå ett slag för en sf-kongress jag är med och arrangerar: [Åcon 2](http://acon2.wordpress.com). Den kommer att äga rum under Kristi Himmelsfärdslånghelgen, dvs 1–4 maj, i Mariehamn på Åland. Det innebär att den för oss i östra Mälardalen är mycket enkel och billig att ta sig till.

Det är den andra Åcon, det hölls en i fjol också. Då var hedersgästen Hal Duncan, en intressant ung skotsk författare. Det var en succé, ett 70-tal fans från framför allt Finland men även från Sverige umgicks, lekte, lyssnade på föredrag och drack öl i tre dygn. Det var kul att få lära känna de finska fansen och det var roligt att vara på Åland.

Det finns bilder här: [Åcon på Flickr](https://www.flickr.com/search/?q=%C3%85con&amp;w=all)

Årets hedersgäst är Ian McDonald som är aktuell med romanen Brasyl som kom i fjol: en av de roligaste tidsreseböcker jag har läst. Kom och lyssna på honom under en Ålandssemester du med!
