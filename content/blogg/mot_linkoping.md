+++
title = "Mot Linköping!"
slug = "mot_linkoping"
date = 2008-06-13

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Nyheter"]
taggar = ["Swecon", "kongresser"]
+++

Idag åker jag till [ConFuse/Swecon](https://www.lysator.liu.se/confuse) i Linköping. Riktigt intressanta val av hedersgäster, [kul program](https://www.lysator.liu.se/confuse/program.pdf) och bra människor. Det här skall bli roligt.
