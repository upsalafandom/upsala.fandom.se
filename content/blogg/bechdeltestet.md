+++
title = "Bechdeltestet"
slug = "bechdeltestet"
date = 2008-07-24

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Länkar"]
taggar = ["feminism", "film"]
+++

Randy Byers skriver på [Livejournal](http://fringefaan.livejournal.com/180628.html) om ett intressant lackmustest för att bedöma kvinnosynen i en film. Det kommer från den feministiska serietecknaren Alison Bechdels serie [Dykes to Watch Out For](http://alisonbechdel.blogspot.com/2005/08/rule.html). Något modifierad innebär den att en film som klarar testet ska uppfylla följande tre krav:

1) Den ska innehålla minst två namngivna kvinnor
2) som pratar med varandra
3) om något annat än män.

Som Randy (och kommentatorerna på hans livejournal) konstaterar går den att använda omvänt, för att se om en film kan tänkas vara en riktig _dick flick_, dvs riktad primärt till män. Intressant nog klarar **Alien** Bechdeltestet.

Nu kommer jag att sitta och fundera på detta varje gång jag i framtiden ser en film! ;)
