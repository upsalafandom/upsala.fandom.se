+++
title = "Första kapitlet av Hominids"
slug = "forsta_kapitlet_av_hominids"
date = 2008-03-20

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["böcker", "vetenskap"]
+++

I ett [tidigare inlägg](./blogg/vetenskap_och_sf.md) skrev jag om vad jag hört om
_Hominids_ av Robert Sawyer, bland annat att folk tycker att han tog sig
stora friheter med en person som antas ha en förebild i verkligheten.

Jag har inte tidigare läst något av Robert Sawyer, jag vet bara att han är en ganska populär författare sådär i största allmänhet. Nyligen läste jag också en [intervju med honom](http://www.sfquarterly.net/index.php/interviews/46-interviews/82-sfqs-interview-with-robert-j-sawyer), där han säger så här:

> I'm at one end in terms of accessibility: I have a large mainstream audience
> in Canada because of it -- people who don't read any other SF but Rob Sawyer.
> At the other end, there are people like Charles Stross and Greg Egan, who are
> writing for the hard-core SF buff; their stuff is impenetrable if you don't
> have a solid grounding in science and in the previous literature of SF before
> going in.

När jag nu till slut började läsa _Hominids_ var det två saker som framstod jättetydligt i första kapitlet:

* det börjar med en massa detaljer om neutrinodetektorn SNO
* en ung postdoc med fransk brytning visar sin spets-bh

Jag kan inte låta bli att undra om det här verkligen är så lättillgängligt,
även om jag själv tycker att det är rätt charmigt att läsa om ett labb där
jag själv jobbar. (Men det gör lite ont i mig att de förstör detektorn. Slå
på strålkastarna när man har högspänningen på till fotomultiplikatorerna, och
en massa blod och skräp i det ultrarena tunga vattnet!) Och så tycker jag att
Sawyer verkar lite gubbsjuk i sin beskrivning av den här forskaren, Louise.

Det första intrycket är inte odelat positivt. Undrar om resten av boken kan
kompensera för det, eller om det är som Tommy skrev i en kommentar, att
personskildringen i allmänhet är orealistisk. Jag får återkomma om detta.

Men jag kan nog ändå rekommendera den som vill veta något om SNO att läsa
bara de första 2-3 sidorna i boken.
