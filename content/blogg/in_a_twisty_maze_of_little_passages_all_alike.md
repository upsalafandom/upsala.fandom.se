+++
title = "In a twisty maze of little passages, all alike"
slug = "in_a_twisty_maze_of_little_passages_all_alike"
date = 2008-03-22

[taxonomies]
forfattare = ["Nea"]
kategorier = ["Rapporter"]
taggar = ["Eastercon"]
+++

Detta blogginlägg skriver jag sittande i en fåtölj i lobbyn på Radisson Edwardian Hotel, Heathrow, London. I hotellet ska enligt uppgift finnas över 1000 fans som har samlats till påskkongressen [Orbital 2008](http://www.orbital2008.org). Det är den största påskkongressen på flera år. Delvis beror det antagligen på att kongressen hålls i London i stället för Blackpool eller Hinckley eller Skottland eller Jersey eller så; det gör ingen större skillnad för oss svenskar som ändå måste resa en bra bit för att ta oss hit och lika gärna kan flyga till Manchester eller Glasgow som London, men för britterna spelar det förstås roll.

Hur som helst, det är mycket folk här, vilket inte riktigt märks. Hotellet är stort och har en intressant layout, lite labyrintisk med många korridorer som går i oväntade vinkar, lätt att tappa bort sig själv och andra i, men rätt charmig på det hela taget. Temperaturen är ett större problem: här är **kallt**. Nyss var jag på en paneldiskussion med Geoff Ryman, Neil Gaiman och Louis Savy; alla tre panelisterna och moderatorn med jackor på sig uppe på scenen, Ryman dessutom med halsduk.

Det var för övrigt en alldeles utmärkt panel, som handlade om London i fantastisk litteratur och film: varför just London, vilka aspekter av staden som används, vilka böcker som gör det bra (och mindre bra), berättelser som utspelar sig i London kontra berättelser i städer som baserar sig på eller lånar drag från London, och så vidare. De kom med en massa intressanta och underhållande inlägg, och en del fakta som jag inte hade någon aning om (till exempel att lyktstolpar och andra hinder framför Buckingham Palace kan skruvas bort på 25 minuter för att skapa en landningsbana, i händelse av snabb evakuering), och berättade om personliga intryck av staden, och litteraturen kring den. Ryman och Gaiman har bägge skrivit Londonrelaterade böcker, så de hade en hel del att säga. Fast en grundläggande sak som jag tycker att de missade litegrann var det faktum att London används i alla möjliga sammanhang, och är så lätt att identifiera även när det är kamouflerat, mycket på grund av att det är så stort. Det bor 12 miljoner människor i Storlondon och jag vet inte hur många miljoner turister som kommer hit varje år; det är självklart att litteratur och film om London blir mer allmängiltig än litteratur och film om Uppsala, för att ta ett exempel ur luften.

Det är det värsta med bra kongresspaneler. Man får en massa tankar och åsikter som det inte finns något sätt att få utlopp för. (I och för sig ett problem med att läsa bra böcker också, och det har aldrig hindrat mig hittills.)

Nu är det bäst att jag slutar skriva, innan jag stryper de små pojkarna som sitter i fåtöljen intill och spelar dataspel. Det blir nog fler rapporter, för helgen har bara börjat.
