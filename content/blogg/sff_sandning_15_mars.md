+++
title = "SFF-sändning 15 mars"
slug = "sff_sandning_15_mars"
date = 2009-03-03

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Inbjudningar"]
taggar = ["SFF", "fanzines"]
+++

_Beware the Ides of March!_ kraxar den åldrige siaren i Shakespeares
_Julius Caesar_. Som bekant aktade sig inte Caesar tillräckligt
mycket. Nå, nu är turen kanske kommen till dig, för denna dag eller någon dag
i veckan efter kommer Sveriges Fanzineförening att lägga en sändning fanzines
till medlemmarna på posten. Vill du vara med har du alltså tio dagar på dig
att knåpa ihop ett fanzine och få över det till Linnéa och mig.

Allt du behöver göra för att få vara med är att betala 125 kronor (150 kronor
utom Sverige) till föreningens plusgirokonto, **435 54 28-6**,
vilket innebär att du både får sex stycken sändningar och möjligheten att
distribuera så många fanzines du vill under den tiden med föreningen. Det är
inte så noga om du betalar innan eller efter att du skickar med ditt första
fanzine.

Gör det! Det är kul!
