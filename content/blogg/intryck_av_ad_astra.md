+++
title = "Intryck av Ad Astra"
slug = "intryck_av_ad_astra"
date = 2009-03-31

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier", "Rapporter"]
taggar = ["Ad Astra", "Toronto", "kongress"]
+++

Så var då ännu en kongress avklarad! Lite sliten och med en huvudvärk i tungviktsklass så ska jag försöka sammanfatta lite intryck. 

Kongressen det hela handlar om var Ad Astra, anno 2009. Evenemanget hölls på ett hotell någorlunda centralt i Toronto, Ontario. Med cirka fem miljoner invånare och många väldigt långa och raka gator känns stan med rätta ganska stor, och centralt ska förstås i det sammanhanget. Förra årets evenemang kändes lite trött, och många småsaker kändes lite taffligt hanterat. Speciellt tydligt var det programmet som led av brist på organisation. Detta år var det en kongress av liknande snitt, men det kändes betydligt fräschare och tight detta år. Programmet hade ett dussin parallella spår och det är ju onekligen ganska ambitiöst. Förvånansvärt nog var det god uppslutning på alla paneler jag besökte, och till skillnad mot förra året var uppslutningen av panelister god. Det var till och med så god uppslutning att det på en panel var sju personer i panelen!

Programmet som jag besökte handlade mycket om skrivande och skapande. Med tanke på att det var säkert femton författare närvarande (plus diverse mer eller mindre kända redaktörer) så kunde man få en hel del olika vinklingar på det hela. Många paneler var väldigt konkreta och gav tips och handgrepp man själv kan använda för att bl.a. bli fri från "writers block", skapa trovärdiga stridsscener eller effektiv struktur och styrsel mellan intern och extern konflikt och utveckling. Riktigt inspirerande även om jag inte längre är lika engagerad i eget skrivande. Själv deltog jag även i en liten workshop kallad "The Joseph W. Haldeman Memorial Seminar on the Redistribution of Economic Resources via the Application of Statistics &amp; Psychology". 

Dealers room var trevligt, och hade den vanliga blandningen av t-shirts, smycken, leksaker med skiffytema, tecknade serier och framförallt nya och gamla böcker. Utbudet av böcker var inte så tokigt, med Mick Sproule som ett trevligt återseende. Detta år pratade jag George R.R. Martin med honom. Det fanns gott om signerade godingar (PKD first editions för $10!) och fina hardcovers av gästerna och andra närvarande författare. Ed Greenwood kunde t.ex. ses signera i farten medan han småpratade.

Det som kändes bäst med den här kongressen var dock känslan av att återvända. Man kände sig hemma på hotellet, man kände igen Ian i Green Room, man kändes igen av Lloyd Penny som hälsade glatt på en när man kom och Mick (som sagt) kom ihåg en. Ad Astra måste väl sägas vara vår "hemmakongress" i det här landet. Lite roligt var det ju även att folk kände igen oss och ibland bara antog att vi varit med på den ena eller andra kongress de plötsligt mindes. Rebecka hade nog mer kul denna kongress också. Hon satt och målade i sin målarbok, sprang runt och lekte katt och barnen Hartwell/Cramer lekte lite med henne också. Som vanligt var hon populär och de utklädda personerna som man såg gå runt på kongressen vinkade gärna eller spelade lite apa för att roa henne. Maskerader och liknande är onekligen familjevänligt.

En annan sak som kändes trevlig och typisk nordamerikansk var ju con suite. Vi käkade inte frukost på hotellet, utan tack vare att det bjöds på mat i con suite av diverse förlag, bids eller intressegrupper så kunde vi käka både frukost och kalkonmiddag helt gratis på pr-budgeten. Onekligen börjar man bli tillvand vid detta högst oeuropeiska fenomen. Det skulle inte vara så tokigt med sånt på kongresserna hemmavid. Ölutbudet i baren på hotellet var inte direkt imponerande, men det var hygglig kanadensisk öl och fler än förra året kunde ses sitta i baren och prata med en öl eller två. 

Bytet då? Lite gamla Analog som delades ut gratis, en äldre roman av George 2R Martin, en Brust och en Dickbok som inte var sf. Dessutom var Writers of the Future åter representerade och det delades ut gratis böcker! Elron må varit galen och scientologerna livsfarliga som infiltrerar bl.a. amerikanska ambassaden i Stockholm, men det var en gratis bok och i alla fall en bra story däri, av Tony Pi.

Det är skoj med kongresser!
