+++
title = "Åter till Skottlands pipor"
slug = "ater_till_skottlands_pipor"
date = 2010-08-04

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["Finska", "Pipes of Scotland", "foto", "pubmöten"]
+++

Efter sommarens kaos kunde pubmötet denna dag åter avhållas på puben Pipes of
Scotland. Ett ämne i tiden är svenskans ställning i Finland. Många närvarande
var bekymrade, men Johan Jönsson kunde lugnande rapportera att än på 20-30 år
är det inte troligt att svenskan kommer att utrotas i Finland.

Neal Stephensons senaste roman Anathem var också ett ämne som diskuterades
flera gånger i min närvaro (kanske på grund av att jag för närvarande läser
den).

Några bilder blev det också.

![Åter till Skottlands Pipor 1](ater_till_skottlands_pipor_1.jpeg)

<!-- more -->

![Åter till Skottlands Pipor 2](ater_till_skottlands_pipor_2.jpeg)

![Åter till Skottlands Pipor 3](ater_till_skottlands_pipor_3.jpeg)

![Åter till Skottlands Pipor 4](ater_till_skottlands_pipor_4.jpeg)

![Åter till Skottlands Pipor 5](ater_till_skottlands_pipor_5.jpeg)
