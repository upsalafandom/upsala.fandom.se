+++
title = "Bokbytande"
slug = "bokbytande"
date = 2009-03-05

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["bokbytande", "bokbytardagen", "bookcrossing"]
+++

Idag är det stora bokbytardagen. Folk träffas i ett antal av Sveriges större
städer och byter böcker med främlingar. Man tar med sig en bok man vill byta
bort, och går till mötesplatsen för att ta emot en bok av någon annan. Jag
begriper mig inte på det.

Det är som BookCrossing. Dagens Sverige svämmar över av böcker. Gå till
Myrorna eller titta i lådorna utanför ett antikvariat. Hundratals, nej
tusentals, **riktigt** billiga böcker. Och där kan du välja själv. Det här
bokbytandet, där man entusiastiskt ska dyvla på en främmande människa vars
smak man inte känner en bok som man själv råkar tycka är bra, vad tjänar det
till? Jag är inte intresserad av andras boksmak. Ser jag en hylla med 300
böcker i är det förmodligen en eller två som jag skulle ha lust att äga eller
läsa själv. Då går jag hellre till biblioteket eller Myrorna och får tag på
en bok som jag verkligen **vill** läsa, så slipper jag sitta där med något av
Liza Marklund, Kajsa Ingemarsson eller Henning Mankell som jag är
fullständigt ointresserad av.

Jag skakar på huvudet. Visst, böcker är cool, jag älskar de små liven, och
jippon kan vara kul, men livet är för kort och bokhyllorna för trånga för att
jag ska göra plats för sådant jag inte har valt själv i mitt bokläsande och i
mina hyllor. Får jag boken av någon jag känner, som känner mig, är det en
annan sak. Men av en slumpvis person på gatan… _Gillar du fantasy! OMG,
då mååste du läsa Brisingr, den är såå bra!_
