+++
title = "Boken som försvann"
slug = "boken_som_forsvann"
date = 2008-06-11

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Kåserier"]
taggar = ["bokaffärer", "böcker"]
+++

De senaste två orderna jag lagt på SF-bokhandeln i Stockholm har strulat. Böcker som jag beställt har inte levererats och trots att de varit med på ordern som jag fått direkt jag beställt, har de inte kommit med resten av beställningen och inte varit med på ordern som följt med böckerna. Det här retar mig lite grann. Jag beställer oftast allt av en författare, får en stor hög med böcker och börjar plöja. Om då en bok i mitten av serien då saknas och inget syns på ordern så blir jag frustrerad.

Frustrerad är rätta ordet! Jag fick vänta på David Webers "bok 1" i hans serie om [Honor Harrington](http://web.telia.com/~u54504162/honor/index.htm) så länge att jag fick ettan när jag redan slukat typ 13 böcker i rad... och hur kul är det att då backa till ettan. Nu har jag fått Tanya Huffs sista bok i "[Valor-serien](http://www.sfbok.se/asp/serie.asp?Serie=2335)" (inbunden för jag bara ville inte vänta)   men, jag har inte fått den beställda "näst sista".  Så nu ligger den fina inbundna och frestar mig...

I-lands-problem så jag skäms att behöva klaga men det ÄR frustrerande för en bokmal som jag att behöva vänta.
