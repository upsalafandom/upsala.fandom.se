+++
title = "Utomjordingar och \"sense of wonder\""
slug = "utomjordingar_och_sense_of_wonder"
date = 2008-03-26

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Kåserier"]
taggar = ["aliens", "utomjordingar"]
+++

Utomjordingar i verkligheten, inte i SF böckerna, finns det massor av åsikter om ([läs lite här](http://aliens.monstrous.com/index.htm)) men om detta har jag inte tänkt skriva idag, jag tänker ägna mig åt utomjordingar (hädanefter kallade aliens) i böcker.

Jag läste "Egen rymddräkt finnes" av Heinlein när jag var ca 11 år gammal. Det var min första SF och den gav djupa intryck och massor av det som vi SF-fans kallar "sense of wonder". När aliens kidnappade bokens huvudperson, Kip Russel, när han för första gången provade sin egenhändigt helrenoverade lotterivinst, en rymddräkt, och jag läste om _maskansiktenas _avskräckande yttre och skrämmande matvanor var jag totalt uppslukad. Slutscenerna på _moderväsendets _hemplanet och senare, "den stora rättegången mot jorden" förstärkte min kärlek till aliens. Mina fantasier om aliens satte igång och visste inga gränser och ända sen dess har jag tänkt på främmande civilisationer och fantiserat om dem.

Hur är då aliens i regel beskrivna? Det finns massor av SF film som tyvärr ägnar sig åt att kreera någon typ av modifierad människa när de ska inkludera aliens. Eller så är de maskansikte-lika, människoätande monsters med drag av reptiler, insekter eller slemmiga sniglar...

Jag anser att de mest spännande aliens är de som mest skiljer sig från humanoider. [Fred Hoyle](http://www.nyteknik.se/efter_jobbet/kaianders/article15702.ece?service=print)s "Det svarta molnet" är ett bra exempel. I denna bok anländer ett kosmiskt stoftmoln till vårt solsystem och parkerar sig mellan jorden och solen. En ny istid hotar och allt görs för att avlägsna molnet. Det visar sig att molnet är en välvillig, intelligent alien och kontakt etableras och jag minns speciellt molnets reaktion när det får musik "uppladdat".

I de böcker av [S.L Viehl](https://en.wikipedia.org/wiki/S._L._Viehl) som jag läser nu (jag är tyvärr inne på den 10 boken av 10) behandlar författaren aliens både bra och dåligt. Dåligt är att många är _för _humanoida, människor med blå hud, 6 fingrar och helt vita ögon... Precis som om det bara är de olika små detaljerna, tex annan färg, som gör aliens annorlunda.  Det som är bra i hennes böcker är att Terraner beskrivs som bigotta "rasister" som slänger ut aliens från jorden så fort det bara går, som anser att jordens befolkning ska hållas "ren" och som predikar om jordmänniskornas överlägsenhet.  Inte för att jag själv skulle stå för en sådan separatistisk åsikt (tror jag) utan mer för att författaren visar vad denna politik leder till för jorden och för de Terraner som beger sig ut i rymden...  Vissa av huvudpersonerna är Terraner (en liten minoritet) och de drabbas av en slags "omvänd rasism" på ett ofta väldigt dråpligt vis. När aliens diskuterar Terranernas fula ovana att ejakulera kroppsvätska full med otäcka bakterier överallt (spotta), och oroar sig för att gå för nära en Terran, lugnas de av upplysningen att Terranen i fråga inte har fått en rödfärgad panna (ett tydligt och säkert tecken på att spottloskan strax ska följa) och ... ja då skrattar jag högt. I S.L. Viehls böcker är de aliens som är huvudpersoner ibland väldigt annorlunda och _det _uppskattar jag.

Jag kommer fortfarande ihåg den "sense of wonder" jag kände när jag läste en SF-bok om kärleken mellan en alien (en stor puma-liknande kattvarelse) och en människa på en planet som mest bestod av en djungel i vilken katten hörde hemma. Tyvärr kommer jag inte ihåg vad boken hette, det är säkert 30 år sedan jag läste den, men den var verkligt annorlunda. I den Viehl bok jag nu läser får jag samma känsla av glädje när en Terran skapar sig ett liv tillsammans med en väldigt stor, vattenlevande varelse och om hur deras kärlek överbygger skillnader som verkar oöverstigliga...

;o)

Mer aliens jag minns med värme? Hm... detta tål att tänkas på! Vilka kommer du på?
