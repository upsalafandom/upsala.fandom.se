+++
title = "De Vries on the possible death of Science Fiction"
slug = "de_vries_on_the_possible_death_of_science_fiction"
date = 2010-08-09

[taxonomies]
forfattare = ["George"]
kategorier = ["Länkar"]
taggar = []
+++

Jetse de Vries is a thoughtful SF commentator, reviewer, and editor of some of the best SF. Towards the end of last year he had his say in the current debate on the future character and very existence of Science Fiction.  The issues are many, complex, and interrelated. They transcend the genre and, I think, literature. Jetse's contribution is filled with links that flesh out his necessarily schematic presentation of what might be more of a social than a purely literary matter. [It's from his excellent blog](http://eclipticplane.blogspot.com/2009/12/should-sf-die.html).
