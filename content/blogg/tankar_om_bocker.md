+++
title = "Tankar om böcker"
slug = "tankar_om_bocker"
date = 2008-02-14

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["böcker"]
+++

Det finns för många böcker i verkligheten. Kanske är det fel, det finns för få böcker? Det borde i alla fall vara lättare att läsa alla böcker som skulle behöva finnas om de inte fanns. Vissa böcker innehåller så mycket dumheter att de inte borde läsas. Tur att de finns, dock.

Här i Kingston finns det en massa böcker. Som alla vet som försökt föra bok på vad de läser är det lätt att börja tävla med sig själv och andra. För man sedan bok på bok på dator, kan man plåga sig själv med en massa statistik. Hans Persson är bra på sådant. Man kan få ångest av mindre.

Känslan av att inte hänga med där det händer är ju också nåt som kan ge en ångest. Lyckligtvis hjälper bokbeståndet här mig att inte få ångest över sånt. Nu har jag i och för sig redan insett att jag har fullt upp med att hålla mig a jour med vad som händer i rollspelsvärlden, så att dessutom hålla koll på vad som publiceras av sf och fantasy är helt enkelt för mycket. Lyckligtvis kommer då stadens bibliotekarier till undsättning. Uppskatta din lokala bibliotekarie och deras gärning.

Vi har nämnligen en hylla för science fiction på biblioteket. Inget nytt, säger du? Smaka på det här då. Nyutgiven science fiction. Det är inte kattskit.

I Sverige har det ju länge funnits en hylla med böcker, ofta med gula ryggar, som man kunnat söka sig till, men här har vi alltså faktiskt något så modernt som böcker i andra färger än gult.

Så, nu kan man faktiskt skryta med att ha sett de nya sakerna när de fortfarande är nya, och att man läst den där nya omtalade volymen innan den ens kommit i pocket. Man kan svänga sig med författarnamn som låter nya och fräscha och behöver inte erkänna att man nyligen läste en bok som var 40 år gammal och tyckte den var bra.

Tyvärr så har jag ju vanföreställningen att det redan skrivits en massa bra saker och att jag kanske borde läsa dem först. Ibland händer det ju till och med att jag hittar en ny bok av någon författare och sedan köper en begagnad volym som denne skrev för 30 år sedan, bara för att den är känd och anses vara en "klassiker".

Plötsligt hinner man inte med att läsa de böcker som står på hyllan för nyinkommet, för man har en lista på saker man tänkte läsa först.

Så, då sitter man där. Det finns för många böcker igen. Det är så man kan få ångest. Det finns för många böcker i verkligheten.
