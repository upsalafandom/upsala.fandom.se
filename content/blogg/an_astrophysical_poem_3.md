+++
title = "An Astrophysical Poem"
slug = "an_astrophysical_poem_3"
date = 2010-07-15
lang = "en"

[taxonomies]
forfattare = ["George"]
kategorier = ["Länkar"]
taggar = []
+++

_Interzone_ is the foremost SF/F magazine of the UK. Its hard-working publicity person, Roy Gray, is an accomplished poet. Here is his "Astropoetica: Towards Darwin," which elegantly summarises our current account of the birth of stars. He gave me permission to [link to it on this blog.](http://www.astropoetica.com/Summer04/towardsdarwin.html)
