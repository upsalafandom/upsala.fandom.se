+++
title = "Världen utan oss"
slug = "varlden_utan_oss"
date = 2008-09-07

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Recensioner"]
taggar = ["böcker", "ekologi", "science fiction"]
+++


En sak i _Book of the New Sun_ som gjorde starkt intryck på mig var
känslan av att ha så mycket av mänsklighetens sönderfallande lämningar under
Severians fötter. Sand av nedmalna gamla städer, och gömda och glömda rester
av äldre tider.

![Bokomslag: The World Without Us](the_world_without_us.jpeg)

Just nu läser jag en bok som inte är sf, men ändå har stor sf-relevans:
_The World Without Us_ av Alan Weisman. Boken kom ut förra året, och
har sålt väldigt bra, men jag har inte kommit mig för att läsa den förrän nu
trots att jag genast blev intresserad när jag såg den. Det här är en
fantastisk bok, som handlar om människans påverkan på planeten och livet här,
och ett stort tankeexperiment med hur det skulle gå om vi alla plötsligt
försvann och övrigt liv fick breda ut sig som det vill och kan. Guldgruva som
källmaterial för världsbyggande sf-författare!

Vad skulle hända om människan försvann? Hur länge skulle våra byggnader stå
kvar, hur skulle livet klara sig, vad skulle komma efter oss? Ganska
fascinerande. Weisman går till undangömda och relativt opåverkade platser,
och tillbaka i tiden till före människan, för att hitta ledtrådar till vad vi
har för total inverkan på ekosystemen där vi befinner oss. Jag måste också
säga att han skriver väldigt bra och medryckande, det är en både läsvärd och
läsbar bok.

Det handlar också om hur våra byggnader och vår teknologi står sig med tiden.
Här har vi Severians sand.

Scientific American skrev en del när boken kom ut, bland annat hade de en
tidslinje för New Yorks sönderfall. [Här kan man hitta
artikeln](http://www.sciam.com/article.cfm?id=an-earth-without-people) om man
skulle ha missat den.

Läser man inte den här boken är det i alla fall värt att känna till vad man
missar.

(Med viss tvekan kategoriserar jag detta som en recension, trots att det
knappt är det. Men det handlar i alla fall om en bok.)
