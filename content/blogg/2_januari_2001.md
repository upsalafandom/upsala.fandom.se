+++
title = "2 januari 2001"
slug = "2_januari_2001"
date = 2001-01-02

[taxonomies]
forfattare = ["Zrajm"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Några svarta monoliter dök inte upp, men väl några (mer eller mindre suspekta) människor med följande namn: Anglemark (Johan), Ante (Andreas Gustafsson), Björn Lindström, Björn X Öqvist, Carl-Henrik Felth, Carl Johan Noreen, Joacim Jonsson, Luna (Daniel), Maria Jönsson, Marie Engfors, Samuel Åslund, Simon Åslund, Sten Thaning, Wahlbom (Anders) och därtill Åka (Anna Åkesson). En monolit har dock enligt pålitliga källor påträffats i Seattle, så helt besvikna ska vi kanske inte vara...

<!-- more -->

Diskuterades gjordes i vanlig ordning det mesta mellan rymd och jord (mest rymd dårå).

Homeopati blir ju allt populärare i våra dagar (hur utspädd ter sig t.ex. inte framtiden nu gentemot för några decennier sedan?) och ett exempel på detta som lyftes fram var Robert Jordan och hans homeopatiska litteratur, förfinad och ytterligare förfinad (och således effektivare och effektivare) i bok efter bok. Att Asimov dagen till ära fyllde år har ingenting med saken att göra, men kan med fördel stoppas in i­alla­fall.

Något som inte är oändligt utspätt är Chicken Vindaloo och därtill en viss reaktion därpå som enligt utsago uppvisades då Ahrvid intog en sådan.

Sent på kvällen när folk lullat tillse lite så började det gaggas matematik och annat sånt som bara dekadenta människor pratar om. Chi och epsilon gled in tillsammans med andra bokstavar ur grekernas lilla alfabet. Det svors högt över det faktum att sånt ser man minsann inte i matteböckerna i Sverje – iaf inte förrän i Universitetet och då ereju alldeles för sent juh!

Andra saker som nämndes var metapolitik (Appropå "nä, nu är jag trött och årkar inte tänka längre - nu gårja hem" - vilket minnde någon om att förutsättningarna för en vettig politik är att man orkar diskutera/tänka.), Wahlbomsmässoafton och hundra sätt att invadera Polen.

Omtyckta är vi kanske inte av alla, men iaf av personalen på stället då jag i förbigående lyckades föra en mindre konversation med någon som jobba på stället. Tydligen är vi sammankopplade med tisdagar, och betraktade som harmlösa (eller iaf lugna).

Nä, nu tog mina anteckningar slut och jag har legat av mig på lögnsidan så jag tänker bara säga en sak till. Och det är att Tussen var där också.

Snip snapp snut så var..

Ehh, ärsh. Bra så?
