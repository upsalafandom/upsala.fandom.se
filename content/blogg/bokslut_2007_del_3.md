+++
title = "Bokslut 2007: del 3"
slug = "bokslut_2007_del_3"
date = 2008-03-09

[taxonomies]
forfattare = ["Jesper"]
kategorier = ["Recensioner"]
taggar = ["Bokslut2007"]
+++

Här fortsätter jag min genomgång av 2007 års läsuppleveler. Tidigare delar kan läsas [här](./blogg/bokslut_2007_del_1.md) och [här](./blogg/bokslut_2007_del_2.md).

# Frederik Pohl & C. M. Kornbluth - _The Space Merchants_

Det här var en av de bästa och mest underhållande romaner jag läste under
året. _The Space Merchants_ handlar om en reklammakare i en överbefolkad
framtid som efter att ha blivit nerkastad på samhällets botten inser att det
han dittills sysslat med är omoraliskt. Handlingen är berättad i en mycket
spännande och medryckande thrillerform som hur lätt som helst skulle kunna
omvandlas till en bra Hollywoodfilm och kritiken som här är riktad mot ett
rovgirigt konsumtionssamhälle känns långt före sin tid.

Jag skulle nog till och med gå så långt att jag utnämner _The Space
Merchants_ till den bästa sf-roman från 50-talet som jag har läst och man kan
lätt se inflytandet som den måste haft på Philip K. Dick och en mängd senare
sf-författare. Det är en gjuten klassiker.

# Ronny Eriksson & Lasse Eriksson - _De norrbottniska satansverserna_ (ljudbok)

Likt _Svålhålet_ är även detta en samling med humoristiska berättelser
skrivna av (i det här fallet två) författare från norrland. Här finns dock
inte samma uttalade sf-vinkel, även om en hel del av historierna med lätthet
skulle kunna beskrivas som sf eller fantastik. Exempelvis smäller en atombomb
av ute bottenviken i en av berättelserna, men på grund av kylan fryser själva
explosionen fast och det stelnade svampmolnet blir snabbt ett populärt
utflyktsmål för skridskoburna helglediga.

_De norrbottniska satansverserna_ är lite av en bagatell och jag tvivlar på
att den skulle komma upp i hundra sidor i skriven form, men många av
historierna är väldigt underhållande och uppläsningen, som görs av
författarna själva, är mycket bra.

# Joanna Russ - _The Female Man_

_The Female Man_ är en väldigt svår bok att ha en åsikt om. Det är en bok med
en så tydlig ideologisk kärna att det känns svårt att ha en åsikt om boken
utan att samtidigt föra över det på ideologin. Vi får möte fyra varianter av
samma kvinna som lever i olika parallella världar: en som kommer från en
utopisk framtid där alla män är utrotade, en från en dystopisk framtid där
män och kvinnor har ingått i ett slags evigt krigstillstånd mellan varandra,
en som lever i något sorts utsträckt fyrtiotal och en som kommer från vår
värld. Dessa kvinnor stöter på varandra genom ett generöst hoppande mellan de
olika världarna.

Det som är bra med _The Female Man_: Russ kan verkligen skriva. Det finns
passager här som är ypperligt välkomponerade och sekvenser som är väldigt
roliga.

Det som är dåligt med _The Female Man_: Russ är arg, väldigt arg. Delar man
inte denna ilska är det svårt att inte se det som att hon bitvis är väldigt
orättvis. Man kan förstå hennes utgångspunkt, men här känns det mest som att
hon predikar för de redan övertygade, alternativt endast försöker ventilera
sin egen ilska. I vilket fall som helst så blir resultatet åtminstone för den
här läsaren mest alienerande.

# Philip Reeve - _Mortal Engines_

Mortal Engines är en Steampunk-artad ungdomsroman som utspelar sig i en
framtid där enorma städer rullar fram över ett postapokalyptiskt och
ökenartat Eurasien och försöker äta upp varandra. Huvudpersonen är en ung
pojke från London som blir indragen i en kamp kring ett uråldrigt
domedagsvapen.

Detta är en tämligen ordinär äventyrsroman, med många snygga och häftiga
idéer. Personligen hade jag dock ganska stora problem med min Suspension of
Disbelief och jag rekommenderar hellre Karl Schroeders _Sun of Suns_ (som jag
kommer recensera framöver) om man vill ha en pulpartad äventyrsroman men
steampunkinslag och fantastisk teknologi.

# Ray Bradbury - _Fahrenheit 451_

Jag har ett problem med Ray Bradbury. Så fort jag läser mer än femtio sidor i
rad av någon av hans romaner eller novellsamling börjar jag hata den. Han
funkar bra om man bara läser någon enstaka novell eller två, men efter det så
så blir hans överemotionella stil närmast outhärdlig.

Den här boken var bara dum, dum, dum och jag kunde inte tro ett ord av den.
Bradbury är här så sjukt gnällig och så komplett ovillig att verkligen
analysera samhällsproblem istället för att bara klaga på dem att _Fahrenheit
451_ mest blir pinsam.
