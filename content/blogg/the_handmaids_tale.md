+++
title = "The Handmaid's Tale"
slug = "the_handmaids_tale"
date = 2009-08-31

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["Margaret Atwood", "The Handmaid's Tale", "bokprat"]
+++

Här är en liten rapport från kvällens bokprat om _The Handmaid's Tale_
av Margaret Atwood. American Library Association har enligt uppgift rankat
boken som 37:e mest anmälda bok 1990–2000. Vi pratade en stund om varför, men
tyckte väl själva att vi läst värre.

En del av oss hade också läst Atwood's _Oryx and Crake_, en annan
dystopisk roman, och konstaterade att det finns en del likheter, framförallt
att båda på något sätt beskriver en miljökatastrof, om än mindre direkt i
_The Handmaid's Tale_ än i _Oryx and Crake_. En annan likhet är
ett den använder många flashbacks till perioden före katastrofen.

Vi var imponerade av Atwoods ekonomiska världsbeskrivning. Någon klagade på
att romanen inte lockade en att läsa vidare. Berättelsen har ett ganska
långsamt tempo, och verkar vara konstruerad för att ge intryck av en enahanda
tillvaro.

Vi pratade ett stund om huruvida den snabba samhällsomvandling som boken
skildrar är realistisk. Boken inbjuder till jämförelser med Iran; händelserna
där gör att tidsperspektivet inte verkar alltför orimligt, förutom att
författaren har lagt till en miljökatastrof som katalysator.

När vi pratat klart om Atwood forsatte vi med andra ämnen, som emotönten
Bradbury.

Nästa bokprat kommer att handla om _Toffs bok_ av Kalle Dixelius. Datum
kommer snart.
