+++
title = "Redan de gamla pulpförfattarna..."
slug = "redan_de_gamla_pulpforfattarna"
date = 2008-04-11

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["noveller", "singulariteten"]
+++

Jag läste just ett referat av novellen "The Microcosmic God" av Theodore Sturgeon, publicerad 1941. Den handlar tydligen om en man som skapar en sorts mikroskopiska varelser som lever mycket snabbare än människor, och låter dem evolvera under kontrollerade former. Meningen är att de ska passera människans utveckling så att man kan dra nytta av upptäckter och uppfinningar som de gör. Det går enligt planen först, men ganska snart drar de ifrån människorna så mycket att de inte går att förstå över huvud taget, än mindre kontrollera eller dra nytta av.

Det verkar lite [bekant](https://sv.wikipedia.org/wiki/Singulariteten) på något sätt.

Det finns många sf-idéer som har funnits längre än man tror.

Och för övrigt kanske jag borde läsa något av Vernor Vinge någon gång. Borde jag det?
