+++
title = "4 november 1997"
slug = "4_november_1997"
date = 1997-11-04

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Utpumpade efter svunna bataljer är möjligen Uppsalas sf-fans, för det var
inte många som dök upp på O'Connors när vi för elfte gången i år möttes på
allmänt pubmöte. Vi som satt där roade oss så gott vi kunde ändå.

<!-- more -->

Inspirerade av debatten om novellistik på Sverifandom-listan pratade vi en
smula om noveller och dålig tobak. Jag minns inte sambandet, men så står det
på den hopskrynklade pappersbit jag hittade i fickan. Dålig tobak rökte bl a
en bekant till Kristin, som samlade ihop restskrumset ur tomma tobakspaket,
vätte det med vatten, åsåg det mögla, rökte det ändå och blev till sin stora
förvåning präktigt sjuk. Den som vill dra paralleller till novellförfattare
utifrån detta må göra det, men skyll inte på oss. (Kanske kan man rulla
cigaretter av dåliga noveller?)

Jag berättade för Andreas att det inte är några större problem att få tag på
TV och videobandspelare i Sverige som klarar signalstandarden NTSC. Det fick
honom att börja grunna på om han skulle ta kontakt med den vilt främmande
amerikanska som skrivit till honom för att utbyta amatörporrfilmer. Jag lovar
att bevaka ärendet och låta er veta hur det går.

Magnus rapporterade från sin resa i Singapore där han inte ätit några
hamburgare, tydligen. Vi fortsatte diskussionen med att prata illa om David
Brin och Jerry Pournelle, och Matthias rekapitulerade handlingen i en
kortfilm som visades på kortfilmsfestivalen härstädes förra veckan, som
handlade om ett lejon som pratade tyska.

Till vår begeistring berättade Matthias också att Filmfestivalsledningen inte
returnerar de videoband de får med festivalbidrag för påseende. De torde ha
c:a 2500 band i en källare, som Matthias i egenskap av medarbetare kan skaffa
sig tillgång till. Bland dessa band kan alltså _The Adventures of Faustus
Bidgood_ befinna sig! The mind boggles. Att släppa Rutnätssanningen loss på
Uppsalas gator ännu en gång...

Jag fällde en kommentar som Andreas smickrade mig med genom att tycka vara
rolig, men bytte snabbt fot och förolämpade mig genom att likna den vid
Douglas Adams infantila så kallade humor. Linnéa satt troget vid min sida och
tröstade mig. Vi fortsatte in på religion, eller rättare sagt mormonism och
scientologi. Jag visste inte att L Ron hade varit medlem i Golden Dawn. Där
ser man!

Efter det urartade diskussionen till att handla om fåniga saker som jag inte
ska ödsla bandbredd med, så nu lämnar jag er åt ert öde. Fast först ska jag
nämna att vi förrättade en minipoll bland de närvarande om att flytta
tisdagsmötena, och resultatet blev 4 för Murphys, 1 försiktigt positiv och 1
emot. Kommer det flera i december och tycker ungefär likadant så flyttar vi
förmodligen mötena med början i januari.

ex pulpetra
— _Johan_

Vi var där: Johan Anglemark, Magnus Eriksson, Andreas Gustafsson, Linnéa
Jonsson, Matthias Palmér, Kristin Thorrud

Även nästa möte blir på samma plats (O'Connors pub, ovanför Saffets på Stora
torget) från 18.30 och framåt, tisdagen 2 december.

Väl mött!
