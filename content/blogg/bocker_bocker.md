+++
title = "Böcker, böcker"
slug = "bocker_bocker"
date = 2008-03-24

[taxonomies]
forfattare = ["Nea"]
kategorier = ["Rapporter"]
taggar = ["Eastercon"]
+++

En av påskkongressens hjärtpunkter är Dealers' Room, eller Book Room, där
försäljare av nya och begagnade böcker breder ut sina varor. Här finns även
möjlighet att handla T-tröjor, smycken, hantverk och krimskrams med
sf/fantasy/horroranknytning, och att köpa medlemskap i kommande kongresser,
men för mig är det böckerna som är huvudsaken, även om jag har köpt både
örhängen, mantelspännen och tröjor i Dealer's Room förr om åren. Det var väl
12-15 olika bokhandlare i år, hälften av dem stora etablerade försäljare med
tusentals böcker i lagret, resten folk som sålde av sina egna samlingar,
eller sålde böcker för att samla in pengar till välgörenhet. Totalt många
tusen böcker hursomhelst, framför allt sf, fantasy och skräck men även en del
annat. För några år sedan köpte jag ett dussin Gerald Durrell-böcker på en
påskkongress, till exempel.

Det låter lite på mina inlägg som om jag har gått på varenda påskkongress
sedan de instiftades, eller åtminstone sedan jag föddes - det stämmer inte
alls. Jag tror att årets Eastercon är min åttonde eller möjligen nionde, och
det är inte så mycket att komma med, men jag har iallafall lärt mig att resa
till England med en hel del tomt utrymme i väskorna, för att få plats med
alla böcker. Det är ju så att man köper böcker ändå, trots att det varken är
svårt att få tag på dem i Sverige, eller ens dyrare nu när momsen har sänkts;
Internet kan ändå aldrig ersätta ett Dealers' Room, jag skulle aldrig frossa
på samma sätt på Amazon. Jag har ärligt talat inte riktigt koll på hur många
böcker vi har köpt i år. Det visar sig. Har bara hunnit läsa en av böckerna
ännu, Caroline Stevermers _A Scholar of Magic_, som jag kanske
recenserar någon gång snart. (Den var bra!)

[Tillägg, något senare:]

Jag fick just syn på en av kongressens hedersgäster som gick förbi, och kom
på något jag hade tänkt skriva i dethär inlägget. Rog Peyton, som är
fanhedersgäst, är en av de försäljare som återkommer varje år, fast han är
pensionär och har sålt sin rörelse (det är en helt annan historia). När han
blev avtackad på avslutningsceremonin sa han ungefär att "det är märkligt,
det är det, att den som har lurat av er era pengar varenda år blir
hedersgäst". Vilket väl säger en del om hur viktigt bokrummet är.
