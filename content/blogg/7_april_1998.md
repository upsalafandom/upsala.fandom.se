+++
title = "7 april 1998"
slug = "7_april_1998"
date = 1998-04-07

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Tre trallande jäntor äntrade scenen på Europeisk Förening för Science Fictions fjärde månadsmöte för året tisdagen den 7 april på Fredmans pub.

Det här första mötet på Fredmans var lite trevande. Ganska få personer dök upp, Åka och Andreas var i Göteborg, Kristin går i väntans tider och andra var väl panka, kan tänka. Men vi som letade oss dit hade trevligt, drack vättar och glammade.

<!-- more -->

Ett rykte spreds vid bordet. Åkas namn nämndes. Andreas namn med. I samma mening. Tissel. Tassel. Jojo. Inte illa för ett möte som på sätt och vis hölls under Hårlemans huvars auspicier, i form av en bisarr monumental akrylbrushtavla i förment antikviserande stil. Mycket märkligt.

Vi pratade religiösa sekter. Kruse och en god vän hade dragit upp planerna för en kyrka vars Gud är konstant i tiden, men inte i rummet. En viss skillnad mot vår nuvarande statsreligion vars Gud verkar vara konstant i rummet (åtminstone stora sjok av rummet) men inte i tiden. Det här var tänkt som en kassako, men vigdes snart med Stens och Wahlboms tänkta roscoeistiska liturgi. Jag vet inte om jag tror att det äktenskapet har utsikt att bli särdeles lyckat.

Plötsligt invaderades vårt bord av unga flickor som var där för att lyssna på en skivdebuterande kompis som spelade på Fredmans denna kväll. De var nog så trevliga, men dessvärre helt ointresserade av science fiction. Konsert vidtog och vi började sakta troppa av, ut i den kyliga aprilnatten.

ex pulpetra
— _Johan_

Vi var där: Johan Anglemark, Mikael Jolkkonen, Karin Kruse, Jan Nyström, Sten Thaning, Anders Wahlbom, Tomas Winbladh, Björn X Öqvist.

Nästa möte blir på Fredmans från 18.30 och framåt, tisdagen 5 maj.

Väl mött!
