+++
title = "Mer om sf-definitioner!"
slug = "mer_om_sf_definitioner"
date = 2008-06-16

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["forskare", "litteratur", "science fiction", "vetenskap"]
+++

Detta ständigt återkommande ämne. Ni som har varit på ConFuse och träffat
Adam Roberts har säkert haft tillfälle att diskutera science fictions sanna
natur och sånt. För att ge lite mer näring till såna diskussioner vill jag
bara presentera en liten observation jag gjort.
[LabLit.com](http://lablit.com) är en webbplats som handlar om forskare i
litteraturen. Där finns en [lista över olika
verk](http://lablit.com/the_list), som inleds med en liten förklaring av vad
LabLit är för något. Där står så här:

> Please note that 'lab lit' is not 'science fiction’; briefly, lab lit fiction
> depicts realistic scientists as central characters and portrays fairly
> realistic scientific practice or concepts, typically taking place in a
> realistic – as opposed to speculative or future – world.

För att vara rättvis har man tagit med en "crossover"-kategori, som är
science fiction med extra realistiska forskarsituationer. Det lustiga är vad
man finner när man tittar närmare i huvudlistan, alltså inte
crossover-listan. Där hittar man flera författare vi känner igen som
sf-författare, och de flesta av deras verk är såna vi är vana vid att tänka
på som sf -- vilket de enligt redaktören här inte alls är. Som andra har
påpekat är ganska mycket av den heta nya sf som kommer ut nu sådan som
utspelar sig i nära framtid, eller i en alternativ nutid, så det är kanske
inte så konstigt att gränserna är flytande. Fast det är ganska intressant att
tänka på. _Antarctica_ av Stan Robinson, _Lust_ av Geoff Ryman -- inte
science ficton?

Det är kanske här man hittar nyckeln till att introducera sf för folk som
inte gillar sf: böcker som inte är så som de föreställer sig att science
fiction är.
