+++
title = "Fantasy från förr"
slug = "fantasy_fran_forr"
date = 2008-04-06

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["böcker", "fantasy"]
+++

Jag läste nyligen [den här reflektionen om att läsa Stephen Donaldsons verk
om Thomas
Covenant](http://mycupoftea.blogsome.com/2008/03/30/stephen-donaldson-the-chronicles-of-thomas-covenant-the-unbeliever)
och mäta den mot dess rykte. Det rimmar ganska bra med en del av mina intryck
av min pågående omläsning av Fionavar-trilogin av Guy Gavriel Kay.
Omslagstexten säger också att det är ett verk "in the tradition of J.R.R.
Tolkien and Stephen R. Donaldson."

Det finns en massa egendomliga anledningar som samverkar för att få mig att läsa de här böckerna igen.

* Jag tyckte inte så mycket om dem när jag läste dem första gången (i högstadiet), och jag har alltid undrat varför.
* Jag har hört många säga bra saker om de här böckerna, och jag har väldigt vaga minnen av dem. De kunde vara kul att komma ihåg, som fantasy-allmänbildning så där.
* Fantasybokgruppen jag är med i läste första delen för ett tag sedan. Jag känner utmaningen att lära mig läsa lite mer renodlad fantasy, om inte annat för att prova hur jag reagerar på det nuförtiden.
* Ett läsprojekt sen jag kom till Kanada har varit att läsa kanadensiska författare, och Kay är ju en sådan.

Fast min käre make skakade på huvudet och sade att de inte var bra böcker,
och att jag borde läsa något nyare av Kay. Det är ju ändå Fionavar-böckerna
som är mest kända, och läser jag för att allmänbilda mig är det ju dem jag
borde ta mig an. Nu har jag kommit halvvägs in i andra boken, och jag är lite
kluven, kan man säga.

I första boken tyckte jag fortfarande att det var fint och stämningsfullt med
det högtidliga språket och den kosmiska Signifikansen av allt som händer, men
nu börjar jag tröttna. Alla verkar ha någon fantastisk stor roll att fylla,
och allt hänger samman. Det är en känsla av Öde och Förutbestämmelse över
alltihop, som ett jättestort men ganska mekaniskt spel.

Den enda av de gamla storslagna berättelser som den här fantasytraditionen
inspireras av som jag faktiskt har läst ordentligt (två gånger också) är
Niebelungensången, men jag tror ändå att jag kan säga att jag fattar vad det
är för känsla de här författarna (Kay, Donaldson, Tolkien, m fl) försöker
återskapa eller bygga upp. Det biter bara delvis på mig som läsare, och i det
här fallet mindre än många andra.

De bitar jag tycker bäst om är de där de vanliga, små och vardagliga
människorna tittar fram. Det är sådant jag kan känna med, och kanske också
identifiera mig med. Det jag gillar med _Sagan om Ringen_ (får man
fortfarande kalla den så?) är att hoberna inte är särskilt märkvärdiga
egentligen, bara sega och uthålliga. De har sin storhet i att de överträffar
sig själva, och i grund och botten är väldigt jordnära. Det jag gillar hos
Lewis (ja, jag framhärdar i att gilla både Narnia och Rymdtrilogin, trots
allt folk säger) hänger väldigt mycket på samma kvalitet, att barnen är så
vanliga och strapatserna de är med om så vardagligt obekväma. Hjältarna
fryser och svälter och bråkar inbördes och är allmänt mänskliga. Det finns en
massa mänskliga svagheter i Fionavar också, men det är också av de mer
storslagna eller svårmodiga slaget (törs jag använda ordet _melodramatisk_?):
svartsjuka, självförakt, dödslängtan, och så vidare.

Jag tror att jag uppskattar berättelsen mer nu än när jag läste den första
gången, men det är på ett distanserat sätt. Det är omöjligt för mig att leva
mig in i händelserna, att känna mig särskilt engagerad av hela spelet om
status och att visa sig på styva linan som folk rätt mycket ägnar sig åt.

En sak jag också har svårt att förstå är vad det är som gör Fionavar till
ur-världen, den viktigaste av dem alla, själva grunden för existensen. Även
om det finns massor av magi och sånt finns det liksom inget som övertygar mig
om att Fionavar är särskilt speciellt. Folken där är väldigt stolta och ädla
och så där, men de verkar inte ha historia eller kultur eller något sådant
som ger intryck av att vara särskilt stort eller gammalt eller kosmiskt
centralt. Det är liksom en renässansliknande kultur (utan högteknologi) och
med alver och ädla vildar (nåja) som lever i områdena runtomkring. Inget
speciellt. Och jag får inte riktigt känslan av att världen är gammal, som
ändå Midgård väldigt tydligt är i _Sagan om Ringen_.

Vi får väl se om jag orkar läsa den tredje delen också. Egentligen vill jag
det, för annars vet jag ju fortfarande inte riktigt vad jag pratar om, fast
frågan är om jag känner mig tillräckligt intresserad. Det finns ju andra
böcker som lockar.
