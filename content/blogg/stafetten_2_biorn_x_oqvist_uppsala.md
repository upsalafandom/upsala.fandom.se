+++
title = "Stafetten 2: Biörn X Öqvist, Uppsala"
slug = "stafetten_2_biorn_x_oqvist_uppsala"
date = 2008-09-03

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Intervjuer"]
taggar = ["Kepler", "Okrand", "anarkism", "frihet", "hattar", "klingonska", "kläder", "uppväxt", "vänskap"]
+++

> Detta är den andra av mina personliga intervjuer med människor inom svensk
> fandom eller som på annat vis är involverade i det här med sf och fantasy.
> Biörn föreslogs av det förra intervjuoffret, Gabriel Nilsson, med
> motiveringen att han klär bra i hatt!

# Uppväxt

Jag växte upp i Storvreta. Flyttade dit ifrån Älvsjö när jag gick i ettan i
grundskolan, men minns egentligen ingenting av hur det var där bortsett ifrån
att vi bodde på Annebodavägen 58 i Älvsjö - vilket var lätt att minnas då min
far var 58 år gammal då.

Åldersskillnaden mellan min far och mor är rätt så stor - faktum är att min
mor träffade min far genom min bror. Mor och bror (de är bägge födda samma år
- 1950) gick på samma kurs på universitetet i Stockholm, i början på
sjuttitalet, och genom honom kom hon sedan att träffa min far (född 1924).

Tjugisex års åldersskillnad! Att min far har präglat mig råder det inget
tvivel om, men kanske inte främst på grund av sin ålder.

Han fick afasi när jag var cirka tre år gammal, och tappade därvid
talförmågan helt och hållet - den återkom sakta, men på grund av att jag i
den åldern plötsligen var den enda av oss som hade ett språk, blev jag i
somliga avseenden mer som en far åt honom än tvärtom. Mycket tolkande
gentemot omvärlden och så.

# Influenser

Mycket av mitt förhållande till **saker** kommer ifrån min far. Att ha få
grejor, men att se till att de grejor man har är användbara till mycket.
Hellre en dyr grej som är bra, än många billiga, halvdåliga grejor. Andra
människor som påverkat mig är Johannes Kepler och Marc Okrand.

När jag läste astronomi blev jag grymt imponerad av Johannes Kepler som ett
tag var den historiska person som jag helst av allt skulle söka upp i
händelse av en tidsmaskin. Hm, ingen har egentligen ersatt honom, så jag
antar att det fortfarande är så.

I mångt och mycket avundades jag saker som är kvalitéer jag såg hos mig själv
- mångsysslare, noggrannhet, intelligens - men som Kepler åstadkom under
mycket primitivare förhållanden. Att komma på att kvadraten på planeternas
omloppstider är proportionella mot **kuben** på avståndet till solen - utan
att använda miniräknare eller dator - är en enastående uppvisning i
besatthet/envishet.

Kepler var utan tvekan en ärkenörd. Men mitt intresse för honom har falnat
något.

Marc Okrand uppfann klingonska, språket som fick mig att börja intressera mig
för lingvistik. Och han är en trevlig och intellektuellt stimulerande person
att prata med (vilket han har gemensamt med många av mina favoritvänner)
vilket ju inte heller är så dumt.

# Uppsala och livet

Jag gillar Uppsala och skulle ogärna flytta härifrån. Det finns ett ständigt
inflöde av nytt folk (vilket är bra) och utflöde av goda vänner (vilket i och
för sig är mindre bra). Det är tillräckligt stort för att ha en vettig
syntklubb (dMz i nuläget) och -förening (Det Svarta Staketet), men
tillräckligt litet för att man ska kunna ta sig vart som helst medelst
apostlahästar eller velociped. Väldigt trevlig stad, helt enkelt… Om jag nu
bara jobbade i den.

Musik är viktigt i mitt liv, men det går i perioder. Perioderna handlar i och
för sig mest om vilken sorts musik jag lyssnar på (synt, punk,
trubadurigheter). Att gå ut och skutta runt på dansgolv gör jag dock mer än
gärna. Röjdans vill säga - ingen par- eller annan styrdans för mig. Och en
schysst spelning eller två är heller inget jag säger nej till. Det senaste
året har det blivit väldigt mycket punk.

Just nu gör jag hack åt ett företag i Stockholm. Jobbar 75 %.
"Driftstekniker" är min officiella titel. Tidigare jobbade jag på Uppsala
English Bookshop (och det är fortfarande en reflex att svara på frågan "Men..
**Var** har jag sett dig förut?" med "På bokhandeln?").

Men mitt jobb har aldrig varit mitt liv och jag kommer aldrig att flytta till
Stockholm. Möjligheten att sova över på jobbet, eller väldigt nära, vore
kanon. Men bara för att kunna riva av några dagar av **enbart**
jobba-äta-sova, och som medel för att få längre obruten fritid någon
annanstans. Att kunna lägga jobbet helt och sidan och tänka på inget annat än
mina egna galna projekt under en tid längre än bara en helg är guld värt! (på
en helg har man en dag får återhämtning, och en dag för återställning till
jobb - jag vill ha flera dagar instoppade däremellan för att överhuvud ens
tänka på det som "ledighet" snarare än bara "återhämtning").

Drömmen skulle ha ett helt portabelt liv; en väska med jobb och
förnödenheter, och tillräckligt med pengar för att kunna röra sig fritt. Det
och ett trevligt hem någonstans på gångavstånd från station i Uppsala
(väldigt nöjd med min lägenhet i nuläget!). En plats att sätta ned foten på,
när så behövs, förvara sina prylar, och ha en ständigt uppkopplad dator på.
Så länge jag kan nå mina data ifrån vilken nätad dator som helst är jag hemma
i hela världen! :)

Internet, och dess utveckling, är det största som har skett i världen under
mitt liv. När jag gick i gymnasiet (mitten på nittitalet) fanns det inte
mycket på nätet. Letade man efter datorinformation stod det mesta att finna,
men det är först nu långt senare som man kan hitta **allt**.
Stickbeskrivningar, instruktioner om växtbeskärning, och hur man renoverar
sitt badrum, etc. Vardagsprylar. Så'n info man verkligen behöver. Nu när "Ten
years of September" börjar gå mot femton finns allt!

Mina intressen spretar åt alla möjliga håll. Klingonska har jag ägnat mig åt
- skapat Klingonska Akademien med tillhörande webbsida
[www.klingonska.org](http://www.klingonska.org), ägnat mig åt självstudier,
undervisande, samt deltagande på möten - _qep'a'_ och _qepHom_).

Viktigast i mitt liv är förmodligen min individuella frihet, möjligheten att
göra lite som det faller mig in. Inte vara alltför uppbunden av jobb, plikter
och annat "styrt". Jag tycker bättre om att leva på sparlåga med lite
halvkrasslig inkomst än ha alla pengar i världen men ingen tid. Min tid är
värdefull, för värdefull för att ge bort i stor skala.

På den politiska kompassen hittar du mig någonstans i vänster nederkant. SAC
är mitt fack. Men aktivitet är det väl lite si-och-så med. På första maj går
jag i syndikalisternas tåg, och om jag hör talas om någon demostration för
något sympatiskt så händer det att jag går (senast Klimatkrock på Fyris torg
för några månader sen). När jag gick i gymnasiet var jag med och organiserade
ett par demon, men sen dess har jag inte gjort så mycket i den vägen.

# Vänner

Mitt nätverk av vänner är viktigt också. Under hela min uppväxt, fram till
och med gymnasiet, byttes alla mina "vänner" ut ungefär vart tredje år
allteftersom skolomständigheterna förändrades (lågstadium, mellanstadium,
högstadium, gymnasium). När jag började på komvux/universitetet insåg jag att
här måste man samla på vänner om man vill ha kvar dem, och började anstränga
mig lite mer för att göra det. Det tog skruv! Har under många år hållit
filmkvällar för attrahera/samla folk, men på senare tid har jag istället
ansträngt mig för att träffa folk på tu man hand. Kontaktat människor jag
träffat på i något sammanhang som verkat sympatiska, gått ut och fikat,
promenerat, eller umgåtts på annat sätt.

Förvånansvärt många människor svarar glatt "Ja" på frågan om de vill gå ut
och ta en fik, fastän man bara träffat dem i förbigående för flera månader
sen (men då skrivit upp dem på en lista över intressanta människor - någon
att sms:a för att se om de är vänmaterial). I dagsläget har jag en lista i
mobilen med kanske tio namn på, men det är en lista med folk jag är nyfiken
på, eller skulle vilja umgås mer med. Utöver dem på listan finns ju en massa
gamla vänner och bekanta som jag också umgås med. Lagom insnöade, kreativa
och pratglada människor får förtur! Nördar som man kan prata med, helt
enkelt. :) De behöver inte nödvändigtvis dela mina nördfält - fascinerade
människor fascinerar mig!

# Fandom

Science fiction-fandom hittade jag genom Åka. Hon ringde och lämnade ett
förvirrande meddelande där hon hävdade att hon hittat en klump med folk som
jag skulle trivas i, och att jag absolut bara måste komma dit. Det var en
kongress, och jag kom i tid att se en eller två programpunkter innan det var
slut. Minns inte vad kongressen hette, men den var i en källarlokal, i huset
brevid Missonskyrkan.

Jag minns att jag hörde någon som jag senare kom att identifiera som Ahrvid
Engholm göra en **lång** utläggning om när det är obligatoriskt att utelämna
ett komma ("aldrig komma före nödvändig bisats"), och vilka ingrepp han gjort
å författarnas räkning när han redigerat dylika texter.

Fandom får mig att tänka på Ramones "We're a happy family, we're happy
family". En energisk, kaotisk sammanslutning av människor. En anarkistisk
utopi, kanske? :)

# Kläder

Jag hörde att Gabriel tussade dig på mig för att han tycker att jag klär i
hatt. Jag har en ganska medveten klädstil. Jag fastnade för svart i
högstadiet, och hade några år senare fasat ut allt icke-svart ur min
garderob.

Hatt är praktiskt, särskilt om man (som jag) har rakad skalle. Det avvärjer
regn, liksom solsken och är utmärkt för att reglera temperaturen. Allvarligt
talat tycker jag nog att folk som inte har hatt/mössa när det är som kallast
är lite konstiga. Man vill väl hålla sig varm? Men så är det kul att sticka
ut lite från mängden också, och hatt ser ju rätt så bra ut därtill…

# Framtiden

Framtidsvisioner har aldrig varit min starka sida, men om tio år (eller
förhoppningsvis betydligt tidigare) kommer jag koll på vad jag kommer att
göra om tio år, men idag har jag det inte.

Förmodligen kommer jag att jobba med något mer kroppsligt krävande (än nu) på
deltid. Kanske undervisar jag - att förklara så att en nykomling till ämnet
förstår är intressant och utmanande - men i så fall vuxna personer.

Skulle kunna tänka mig att jag har tvåhundra galna fritidssysselsättningar,
och ett par-tre deltidsjobb, med stor frihet i.
