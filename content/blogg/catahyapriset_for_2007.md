+++
title = "Catahyapriset för 2007"
slug = "catahyapriset_for_2007"
date = 2008-11-27

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Nyheter"]
taggar = ["Catahya", "Catahyapriset", "noveller", "skräck"]
+++

Fantasyföreningen Catahya (som var med och hjälpte till att arrangera Kontext
nyligen) delar sedan förra året ut ett novellpris, som årligen skall
uppmärksamma den mest förtjänta fantastiknovellen. I år gick det till Johan
Theorins "Endast jag är vaken", ur antologin _Mardrömmar i midvintertid och
andra morbiditeter_. Juryns motivering:

> "Endast jag är vaken" är en morbid komedi som förenar 1800-talets
> sagotradition med den moderna blodiga skräckberättelsen. Invånarna i en
> nybyggd idyll får se sin strävan efter materiell och fysisk säkerhet slå
> tillbaka mot dem själva. Det är något både karikatyrartat och charmigt över
> dem, dessa representanter för en samhällsklass och ett sätt att leva som
> många eftersträvar, men även något överspänt med deras präktighet. Med
> skräckblandad förtjusning följer man Theorin när han stänker blod på deras
> livspussel.

Läs mer [här](https://catahya.net//nyhet.asp?id=2735).
