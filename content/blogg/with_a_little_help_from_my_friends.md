+++
title = "\"With a little help from my friends\"..."
slug = "with_a_little_help_from_my_friends"
date = 2008-03-07

[taxonomies]
forfattare = ["Lennart Svensson"]
kategorier = ["Kåserier"]
taggar = ["böcker", "datorer", "foto"]
+++

Tänkte be er om en tjänst, kära Uppsalavänner och fans.

Vissa av er torde ha ex av "Eld och rörelse" samt kokboken. Nu skulle jag behöva bilder av dem att ha på min egen blogg, i mina annonser, men jag saknar digitalkamera. Kan då någon med sån apparat ta bilder av novellsamlingen (fram- och baksida) plus kokboken (bara framsida, baksidan är ju blank) och maila mig?

Det behöver inte vara katalogbilder med 90-gradersvinklar. Det räcker om man ser boken liggande på ett bord eller så.

Och ni behöver inte konferera inbördes om "vem som ska göra detta", onej, jag hoppas på personliga initiativ, och förresten är flera bilder bara bra att ha. (Göra reklam för dessa böcker är och förblir ett av syftena med "Svenssongalaxen". Och jag har sålt en del ex tack vare dessa annonser, de har betalat sig...!)

Kul om någon lystrar till denna maning, tack på förhand.

För övrigt undrar jag om någon känner till en bra datakurs att gå? Själv kan jag så där halvlite, men skulle behöva bättre kunskap om Office, ja även Word och så. Och lite finesser för internet.

Den late i mig säger att jag klarar mig med den datakunskap jag har (och det gör jag på sätt och vis, jag sitter inte i sjön) – men mitt ambitiösa jag kickar mig där bak och säger att jag borde lära mig data bättre. Så vad ska man gå? Medborgarskolan? Kursverksamheten? Självaste ABF? Tacksam för tips.
