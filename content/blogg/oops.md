+++
title = "Oops!"
slug = "oops"
date = 2008-06-25

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["bloopers", "science fiction"]
+++

När man översätter något närläser man det på ett helt annat sätt än när man
bara läser för nöjes skull. Jag har just avslutat översättningen av **Paul di
Filippo**s kortroman _Ett år i den linjära staden_, nominerad till Hugopriset
2003, för [Nova SF](http://www.replik.se/novasf):s räkning. Den beskriver en
värld som består av ett New York City, eller rättare sagt ett Manhattan, som
är mycket långt, kanske oändligt långt. Möjligen ligger det inuti en lång
tub, kanske en torus.

Nå, denna värld begränsas på ena sidan av en flod och på andra sidan av
järnvägsräls. På bortre sidan av spåren respektive floden bor övernaturliga
varelser som kallas _rälstjurar_ respektive _fiskarhustrur_. Deras uppgift är
att hämta de döda. I dödsögonblicket dyker de upp, rälstjurar om man är en
syndare eller fiskarhustrur om man har levt ett dygdigt liv, och lyfter upp
kroppen - som då dematerialiseras - och flyger bort med den. Ingen kropp
kvar.

Så hur hade författaren tänkt sig följande passager ur berättelsen?

"Hördu, i din senaste novell, där huvudpersonen var tvungen att klä på sin
systers pestdrabbade **lik**…"

"I kombination med hennes långa kolsvarta hår fick utstyrseln henne att se ut
som en **begravningsentreprenörs** brud." (efter att tidigare ha låtit
huvudpersonen föreställa sig en värld där befolkningen tvingades "att ta hand
om lämningarna efter sina kamrater bäst de kunde" och en "yrkeskår av
dödsmäklare hade vuxit fram för att ta hand om denna obehagliga uppgift".)

Bloop, bloop!
