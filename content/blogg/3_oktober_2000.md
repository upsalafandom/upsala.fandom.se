+++
title = "3 oktober 2000"
slug = "3_oktober_2000"
date = 2000-10-03

[taxonomies]
forfattare = ["Karl-Johan"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Molntäcket var lågt över Upplandsslätten märkte jag när jag cyklade förbi Skånela på min väg till Upsalafandoms fyrtiosjätte månadsmöte på raken. Det är tydligt att jag inte lärt mig någonting ännu.

För en gångs skull så behövde jag inte gå in på Williams innan jag träffade fans — Björn L. och Mats var på väg uppför backen när jag kom ifatt dem. Inne i värmen hittade vi bland annat Johan (som mumsade på en hamburgertallrik), Åke B. (som undrade ifall jag cyklat igen), Magnus och några till, trots att klockan inte hade hunnit slå sju.

<!-- more -->

Johan hade gott om skadeglada kommentarer kring hur jag anställdes på Tanto, men jag frågar mig varför han inte talar om var han är anställd numera. Är det kanske Microsoft?

Biffen på Williams var ätbar även denna gång, men Johan tog inga chanser och åt, som jag noterade ovan, en hamburgertallrik.

Göstapristagaren utsågs enligt en lång, invecklad och ytterligt rättvis procedur. Nu kvarstår bara frågan om pristagaren kommer till Göstacon.

Gabriel, Olle och Maria var inne i en djup diskussion om evighetsmaskiner när jag frågade var jag skulle kunna sova någonstans. Maria föreslog en extra studentlya hon disponerade innan Björn X dök upp och jag kunde få sängplats hos honom. Jolkkonen blev också väldigt ursäktande över att han inte kunde ge mig sängplats eftersom hans nyinköpta lägenhet håller på att renoveras.

Gabriel ville veta hur långt man skulle kunna cykla eller gå på 20 timmar förutsatt att man inte blev trött i olika terrängförhållanden med tanke på en fantasyroman han håller på och skriver. Där ser man vad ungdomen nuförtiden inte lär sig nu när försvaret skär ner.

Vi diskuterade SFF-sändningen som jag hade med mig, "An Electrical Fairy Tale" av L Frank Baum som någon (jag tror det var Nisse)
hade med sig, hur Apple håller på att åka dit på grund av att Motorola inte satsar tillräckligt på att göra PowerPC-processorn snabbt och en massa annat som jag säkert glömt eller inte hörde.

Anders Wahlbom gick med i SFF, och det ser ut som om han flyttar till en andrahandslya i Hökarängen inom kort, när en kompis till honom sticker till Singapore för att hälsa på flickvännen.

Sist men inte minst väntade magister Jolkkonen med att föra fram sitt förslag på ny lokal för Orvarsmötena tills efter att Johan hade gått. Det rör sig om Flogsta-puben, där han har varit och pratat med den som driver den. Personen ifråga var inte bara positiv utan visade sig också vara sf-läsare.

Mot slutet av kvällen upptäckte jag också något som såg ut som en soppåse under bordet. Som tur var visade det sig att det bara var Mats påse med saker, och inte några kvarglömda sopor.

Närvarande var: Magnus Eriksson, Nisse Segerdahl, Johan Anglemark, Anders Wahlbom, Mikael Jolkkonen, Gabriel Nilsson, Björn Lindström, Samuel och Simon Åslund, Bertil Näsström, Sten Thaning, Åke Bertenstam, Olle Östlund, Joacim Jonsson, Maria Jönsson, Axel Röjheden, Mats Wistus, Björn X Öqvist, Tony Elgenstierna och eder ödmjuke krönikör, Karl-Johan Norén.
