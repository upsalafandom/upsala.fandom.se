+++
title = "3 november 1998"
slug = "3_november_1998"
date = 1998-11-03

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

OBS! EFSF byter mötesplats! Se längst ner i brevet.

Dramatiken var stor när Europeisk Förening för Science Fiction kapade Fredmans pub för sitt elfte månadsmöte för året tisdagen den 3 november. Det är mot denna dramatiska bakgrund den här fullständigt sanningsenliga ögonvittnesskildringen ska ses.

<!-- more -->

När jag anlände vid sjutiden tillsammans med Linnéa och Magnus var det första jag slogs av att det sista matalternativet under 100 spänn hade försvunnit från matsedeln, samt att ölsortimentet nu definitivt inskränkte sig till fem tråksorter på fat. När jag sedan försökte beställa fick jag vänta fem minuter på att servitrisen skulla snacka klart med någon i telefon, medan killarna i stekavdelningen två meter längre bort stod med armbågarna i bardisken och hade långtråkigt. Det var med andra ord uppenbart att bytet av mötespub inte kunde vänta tills årsskiftet, som vi tidigare hade tänkt oss. Här krävdes handling, och handling nu!

Ett förslag om att flytta mötesverksamheten till ett konditori röstades ned, likaså en tanke att flytta den till söndagseftermiddagar. Några anspråkslösa restauranger fördes på tal, men den vi först tänkte på visade sig stänga kl. 20.00 på tisdagar, och vi var rädda att det skulle gälla även andra ställen. Vi beslöt att bryta upp tidigt från Fredmans och promenera runt ett tag och se om vi hittade något lämpligt ställe.

Under tiden började vi prata kongresser. Vi diskuterade både "Upsala: 1999" och "Reconvene", nästa års brittiska påskkongress. Vi har hunnit få nej från ytterligare en hedersgästkandidat, men hoppas att snart få iväg ett brev till en amerikan som vi gärna skulle se i Upsala. Det konstaterades en förvånande motvilja mot bridge runt bordet (ett bord som för övrigt muterades hela tiden i takt med att fler anlände) medan schack och backgammon kom lindrigare undan.

Mycket tid upptogs av att diskutera Upsalafandoms nya webbplats, som Nils vänligt har ställt till vårt förfogande, och vi kom fram till några allmänna principer för hur vi ska utnyttja den. Snart, mycket snart, kommer ni att kunna besöka sfweb.dang.se. Sedan var klockan nio och vi drog ut på stan. I dörren träffade vi Matthias.

Det första stället vi provade var Williams i Ubbohuset, bredvid Universitetshuset (och inte långt från Jontes stuga, där Trøstcon och Verdenskongressen 1990 ägde rum). Det befanns vara tämligen tomt, ha sjyssta priser, ett gott sortiment öhl samt ett mycket smalt utbud käk till vettigt pris (och en indisk restaurang i rummet bredvid). Den lite väl bullrande jukeboxen konstaterades ha god musik, samt ett volymreglage som barpersonalen gärna skruvade ner lite när man frågade dem. Vi beslöt på stående fot att slå oss ner, och efter första rundan öhl att flytta mötena hit.

WILLIAMS
Åsgränd 5 (Universitetsparken)
Tel 018-14 09 20
Öppet 15-02 alla dagar

Sedan konstituerade vi en kongresskommitté för Upsala: 1999, och den befanns bestå av alla närvarande. Just nu är vi med andra ord 10 personer och fler lär vi bli. Mer öhl dracks, mer musik spelades och mycket babbel om allt mellan whisky, skiffy, raketer och fandom sipprade ut ur våra munnar.

Jag gick redan vid elva, men det stannade nog kvar folk i flera timmar till.

ex pulpetra
— _Johan_

Vi var där: Johan Anglemark, Linnéa Anglemark, Magnus Eriksson, Andreas Gustafsson, Mikael Jolkkonen, Matthias Palmér, Nils Segerdahl, Anders Wahlbom, Tomas Winbladh, Anna Åkesson, Olle Östlund.

Nästa möte blir på WILLIAMS från 18.30 och framåt, tisdagen 1 december.

Väl mött!
