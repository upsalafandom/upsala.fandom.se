+++
title = "Den lokala kulturen -- inga ölburkstorn"
slug = "den_lokala_kulturen_inga_olburkstorn"
date = 2008-09-20

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["hemmabyggen", "pubmöten", "öl", "ölburkstorn"]
+++

En del av läsarskaran har kanske hört talas om den fhaanniska bedriften att
bygga ett torn till månen med hjälp av ölburkar? Jag vet inte vem som först
kom på detta (kolla med [alvarfonden](http://www.alvarfonden.org/bok/sf.html)
om de har kvar _Fandom Harvest_>) men det kan ha varit Terry Carr. Detta
är ju bara en av de fanniska myter, legender och galna infall om folk genom
tiderna tagit sig för. Jag skrev ju nyss om en annan legend, Wilson Tucker.
Det finns att ta av.

Nåja. För att åter vända till ölburstornen så måste jag tyvärr meddela att
det inte setts byggas några ölburkstorn i Kingston. Inte heller har jag sett
folk bygga på ett hotell att ha kongresser i. Det är helt enkelt inte så
mycket fhaanniska geist här. Som på så många andra ställen.

Men, det har slagit mig att Upsalafandoms många försök att finna den perfekta
puben att ha våra pubmöten på kanske kunde lösas på något liknande sätt. Man
kunde väl åstadkomma nåt med fhannisk rhåkraft och en gnutta fantasi, eller?

Ölburkar och pubmöten, det fick mig att tänka på det där med att bygga ett
hotell ämnat enbart för kongresser. Kunde man inte bygga öl själv också? Då
får man ju vad man vill ha, om det lokala vattenhålet enbart serverar Pripps?
Eller hur? Nog finns det folk i fandom som brygger eget öl?

Här i stan har vi en
[pub](http://beerblog.genx40.com/archive/2005/january/kingstonbrewing) som
jag gillar, som faktiskt gör sånt. De säljer öl från många olika
småbryggerier, men dessutom kan man bakom bardisken se in i bryggeriet! Där
står kopparpannorna där ölet man dricker har bryggts! Det är ju utmärkt!
Borde man inte göra så för att få till den perfekta puben att ha pubmöten på?

När jag kommer tillbaka till Upsala så förväntar jag mig att ölet är
färdigbryggt, tappat och klart. Bygga en pub kan väl inte vara så svårt,
eller?
