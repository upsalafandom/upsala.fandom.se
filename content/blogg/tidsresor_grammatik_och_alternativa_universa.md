+++
title = "Tidsresor, grammatik och alternativa universa"
slug = "tidsresor_grammatik_och_alternativa_universa"
date = 2009-05-13

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Länkar"]
taggar = ["grammatik", "kronologi", "parallella universa", "språk", "tidsparadoxer", "tidsresor"]
+++

Stig W. Jørgensen har i dag publicerat en mycket intressant artikel på sin blogg Ekkorummet, om tidsresor, tidresans fysik och nuddar vid att skissa på hur ett språk vars tempus klarar av att hantera tidsresor på ett naturligt sätt skulle kunna se ut. Nu är skissen synnerligen grund, eftersom Jørgensen, som han säger, "imidlertid aldrig gjort noget forsøg på at konstruere et sådant sprog eller en formalisme der kunne udtrykke de nødvendige relationer, og jeg skal sige jer hvorfor: Tidsrejser er ikke virkelige. Nu ved jeg godt at nogle også betragter det som en udmærket hobby at lære sig klingonsk eller tolkiensk elversprog, men jeg synes ikke at resultatet står mål med indsatsen hvis det ingen relevans har for den virkelige verden."

Jag är inte säker på att jag håller med. Det skulle vara roligt, som ren hobby, att skissa på en sådan grammatik i alla fall, bara för skapandets glädje och för den kreativa problemlösningens skull. Man behöver ju inte fortsätta när man når de riktigt knepiga stadierna utan kan sluta där, på den punkten. (En annan fråga som i vart fall jag ställer mig är om det redan finns språk som är mer lämpade för syftet än vårt indo-europeiska modersmål.)

Artikeln refererar också några filosofiska och estetiska teorier om kronologi, och rekommenderas i sin helhet: [Tidsrejsens grammatik](http://ekkorummet.sciencefiction.dk/#post42).
