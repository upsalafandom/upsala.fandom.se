+++
title = "4 mars 1997"
slug = "4_mars_97"
date = 1997-03-04

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Skummande dryck och friterad fisk stod på menyn då Uppsalas science fiction-fans avverkade sitt tredje "Første tirsdag i måneden"-möte på O'Connors igår. (En hälsning hade inkommit från andra sidan kölen. Tack för den!)

<!-- more -->

Kruse har precis börjat jobba igen, vilket innefattar akrobatik på elevernas bänkar ("det finns ca 30 cm tillgodo att hoppa runt på utan att välta bänkarna") till den piercade rektorns förnöjelse. Gotiska förbundets månghövdade hydra stack upp nyllet igen, då någon i Uppsala studentkårs styrelse hade ringt upp Kruse och påpekat att det snart var försent att registrera sig, skulle inte vi vara med i år? Det ska vi.

S:t Ansgars församling, kvinnliga präster och sekulariserade dopföljen diskuterades, och viss bespottelse drabbade de icketroende som insisterar på rätten till en svartrock som officierar. Lenin nämndes inte alls, däremot Kurt Waldheim och Goebbels. (Inte i samband med religionsdiskussionen, utan i en tillbakablick över kända krogbråk i Upsalafandoms historia.)

En hemsida ska skapas för EFSF/Upsalafandom, med lite historik och möjlighet att kontakta oss. Vi pratade om att gå och se Mars Attacks när den kommer till byn, och bestämde att vi träffas hemma hos Anna onsdag 12 mars kl 19 för att se om vi vill göra ett fanzine tillsammans. Adress: Studentvägen 34.

ex cathedra
— _Johan_

Vi var där: Johan Anglemark, Joakim Bjelkås, Tony Brooks, Magnus Eriksson, Jan Nyström, Mattias Palmér, Kristin Thorrud, Anna Åkesson, Karin Kruse

Nästa möte blir på samma plats (O'Connors pub, ovanför Saffets på Stora torget) från 18.30 och framåt, tisdagen 1 april. No kidding.

Väl mött!
