+++
title = "Mot stjärnorna via Torontos lokaltrafik"
slug = "mot_stjarnorna_via_torontos_lokaltrafik"
date = 2008-03-31

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier", "Rapporter"]
taggar = ["kongresser"]
+++

Här kommer en liten snabb sammanfattning av intrycken från sf-kongressen Ad Astra i Toronto nu i helgen. Möjligtvis kommer det lite djupare tankar senare, om jag orkar.

Ad Astra är en kongress med lång historia. På en fest på lördagskvällen pratade jag med en man som tyckte att Ad Astra var bättre förr när det var mer fokus på litteratur, en smalare och mer genomtänkt kongress som författare gillade. Många som brukade gå på Ad Astra tycker inte att det är intressant längre, sade han, och han trodde inte att det gamla gardet skulle ha bjudit in en författare som Kevin J Anderson som den centrala författargästen (han är känd för att skriva media tie-ins, böcker som baserar sig på Star Wars och X-Files -- samt uppföljare till _Dune_).

I en korridor på söndagens förmiddag pratade jag med en annan man, som sade att Ad Astra är en kongress på uppåtgående, som blir bättre och bättre: bredare program, större maskerad och mer rutinerad kommitté. Så det är fråga om vad man är ute efter, antar jag. Det var definitivt ganska många unga utklädda människor där, som gick på dansverkstad och paneler om hur man tillverkar maskeraddräkter, men som inte syntes till på festerna eller i con-sviten.

Det där med con-svit (con suite heter det i original) är en lite ny grej för mig. I stället för att baren är kongressens hjärta, som vi är vana vid hemma (och på brittiska kongresser) är det en speciell svit man går till där det finns gratis mat -- det vill säga godis. Ibland finns det dryck också, och jag hörde folk skryta om andra kongresser där det minsann till och med finns öl i con-sviten. Fast jag hörde också en kille skryta om en fantastisk kongress där man hade femtielva sorters läsk, och hur fantastiskt det var. Hmm. Jag tror att jag har svårt att förstå grejen med så många sorters läsk.

Ante och jag funderade lite på om det är hälsosammare att ha det sociala umgänget kring chips och läsk snarare än öl, men i slutändan är jag inte så säker. De amerikanska fansen är i genomsnitt rätt så omfångsrika, och jag tror att jag ser ett samband mellan detta och den fria tillgången på sockerhaltig föda och dryck.

Jag var med i fyra paneler. Det allmänna intrycket var att det inte var någon riktig ordning och reda på panelerna. Att programmet ändras i sista minuten är ofta oundvikligt, men det var ett rent oerhört manfall bland panelisterna. Den enda fulltaliga panel jag såg var den som handlade om hur man rekryterar frivilliga till kongresser -- det var alltså en panel med engagerade personer på insidan. Det var heller nästan inga som visade sig i "green room" innan panelerna -- tanken är att man ska hinna hälsa på varandra och prata ihop sig först, men det verkade ingen bry sig om. Jag saknade svensk fandoms duktiga panelvallare, som ser till att folk vet var de ska vara och vad de ska göra.

Diskussionerna var rätt roliga ändå. Bäst var panelen "Through Rose-Tinted Goggles", om steampunkens rötter. Vi var två i panelen, men en kille i publiken fick komma upp och vara med eftersom han hade rätt bra synpunkter och tankar.

I stort sett tyckte jag inte riktigt att det var min sorts kongress, men jag hade rätt kul ändå. Och jag fick rabatt på medlemsskapet för att jag var med i fyra paneler, vilket satt ganska fint i plånboken. Vi bodde i partykorridoren, så det fanns massor av fester på både fredag och lördag kväll, och jag pratade med flera roliga och intressanta personer. Jag kunde ha partat mycket mer, men jag fånigt nog tyckte jag att det var oerhört skönt att krypa ner i en härlig hotellsäng och därför gick jag inte tillbaka till festerna igen efter att jag kom till vårt rum vid midnatt.

Det är lite jobbigt att vara på kongress med en tvååring, eftersom vi båda föräldrar måste turas om att vara barnvakt och hålla dottern på gott humör. Det går, men det blir liksom en halv kongress per person. Men vänta bara tills hon blir större, hon kommer att få en oerhörd världsvana.

Vi passade på att turista lite i Toronto också, hela fredagen. Det är en rätt skojig stad. Vi promenerade genom deras väldigt stora Chinatown, besökte en butik som är känd som en av nordamerikas få kvarvarande specialiserade sf-bokhandlar (såg rätt liten ut jämfört med sf-bokhandeln i Stockholm, men de hade _bara_ böcker och tidskrifter), och jag fick en privat visning av Judith Merril-samlingen på stadsbiblioteket -- en forskningssamling av sf och fantasy, som grundlades genom en donation från författaren och redaktören Judith Merril. Sen gick vi på Royal Ontario Museum och tittade på kinesiska gravar, dinosaurieskelett och uppstoppade fåglar, samt en rolig utställning med tidiga skrivmaskiner.

Vi kom tillbaka till hotellet rätt sent på fredagskvällen, så vi missade allt som hände då. Hotellet låg inte direkt centralt -- fast när jag sade det till en person i kommittén verkade han lite förnärmad, och påpekade att de valt hotellet för att det var så lätt att ta sig till. Hmm. Det är väl relativt som vanligt, men det tog ändå mellan en halvtimme och en timme att ta sig från centrum, först med spårvagn och sen med buss.

En rolig grej med Ad Astra var att det fanns ett helt programspår om rymden och naturvetenskap. Jag hann inte se så mycket av det, fast jag var ju själv en del av det. De hade programpunkter om vetenskapskommunikation, specialistpaneler som åhörarna kunde ställa frågor till, och massor av information om flyg och rymdprogram och sånt. Den lokala amatörastronomiska föreningen var också där och informerade, och hade en liten stjärnskådarstund på parkeringsplatsen utanför på lördagskvällen.

Storleken på kongressen var nog ungefär som de brittiska påskkongresser jag varit på, någonstans mellan 500 och 1000 medlemmar, fast det kändes mycket mindre. Lokalerna var ganska utspridda i hotellet, det fanns många parallella programpunkter hela tiden, och de författare som var där var mestadels sådana som jag inte läst eller inte känner till. Kevin J Anderson är förstås ett stort namn, men inte särskilt intressant i mina ögon. Robert Sawyer var där, men jag lyckades bara se honom på avstånd. Det var inte så särskilt hög nivå på det jag såg av programmet.

Just nu känner jag mig ganska upplyft efter en helg full av möten med folk, samtidigt som jag känner mig lite vagt nedslagen över det faktum att jag inte är så bra på allting som de fans jag mött. Jag skulle vilja kunna allt, komma ihåg allt jag läst, och vara händig med att bygga saker och duktig på att visa och förklara. När jag träffar roliga och kreativa människor blir jag ofta lite frustrerad av att jag inte är lika bra som de bästa på allting -- ett fullständigt orealistiskt mål, men ändå. Det kommer att lägga sig snart, och då kommer jag troligtvis att faktiskt göra en del kreativa saker trots allt. På mitt sätt och på min nivå.

Det var en rolig helg. Nu är jag bara lite ledsen över att jag inte kan fara på [Åcon](http://acon2.wordpress.com). Jag skulle verkligen vilja träffa Ian McDonald, och jag vet att det kommer att bli en supertrevlig kongress med riktigt trevliga människor. Det är billigt att åka dit från Upsala, men ganska dyrt från Kanada.
