+++
title = "Rapport från Kontext: \"Omtumlande bra\""
slug = "rapport_fran_kontext_omtumlande_bra"
date = 2008-11-03

[taxonomies]
forfattare = ["Bellis"]
kategorier = ["Rapporter"]
taggar = ["Kontext", "NoFF", "Nova", "kongress"]
+++

Nu är det nog meningen att man ska säga något om Kontext och de första ord
som dök upp i huvudet på mig står i ämnesraden. Det här var alldeles
omtumlande bra – mycket välarrangerad, mycket _intensiv_ kongress. Här fanns
inga döda punkter alls, i alla fall inte ur min synvinkel – det var liksom
konstant maaaangel, som vi brukade säga i Stockholm på den gamla goda tiden,
och det ska alltså uppfattas i allra bästa bemärkelse.

<!-- more -->

Några höjdpunkter, i ingen särskild ordning:

* Novapanelen, där jag fick hoppa in som ersättare för John-Henri som av familjeskäl tyvärr inte kunde komma, och som jag tyckte blev mycket intressant. Den här entusiasmen från de båda Novaläsande panelisterna – Carolina Gomez Lagerlöf och Tomas Cronholm – och, inte minst, den månghövdade publiken kändes värmande. Frågorna haglade, folk ville verkligen diskutera och de ville verkligen veta – och trots att vi inte hade ett enda Novanummer att visa upp (eftersom det ju var John-Henri som skulle haft dem med sig) fick vi fyra nya prenumeranter direkt efter panelen.
* NoFF-auktionen, som jag tror lärde oss något väsentligt – kör relativt få enheter och kör i rasande tempo. Jag auktionerade och Jonas Wissting skötte insamlingen av pengarna. På ungefär tjugo minuter drog vi in drygt 1200:- – stort tack till alla som tappert betalade höga belopp för eländiga böcker och urusla dvd-filmer! Och tack till Anders Holmström, som donerade den märkliga, lyxiga öl som jag vill minnas drog in mest pengar av någon enhet under hela auktionen. Att notera: Jonas Wisstings inramade p-bot från Finland såldes visserligen till Patrik Centerwall, men vi har kommit överens om att den blir vandringspris genom NoFF-auktionerna och varje gång ska auktioneras ut för ett utropspris som ligger strax över vad vi senast fått för den.
* Roompartyt i 210 på Muttern! En instant klassiker! Det här var ett alldeles autentiskt, alldeles lysande room party – för första gången på en svensk kongress på säkert femton år (med undantag för det i John-Henris hotellrum på SweConFuse i somras)! Ibland är okunnigheten en välsignelse. J. P. Melin från Göteborg, som besökte sin första sf-kongress förutom de båda i Göteborg han varit med om att arrangera, hade hört att det ska vara room parties på kongresser. Lyckligt ovetande om att det numera, när vi inte kan ha hotellkongresser i Sverige, strängt taget främst gäller brittiska och amerikanska kongresser, hade han bunkrat upp med whisky och vin i sitt hotellrum och gick runt och bjöd in folk i baren på lördagskvällen. Fast vi dröjde kvar i baren till efter stängningsdags – så när jag, som bodde på samma hotell några rum bort (206), anförde en stor skara fans genom Uppsalas nattmörker och med dem i hälarna knackade på J.P.:s dörr klockan något på natten, öppnade han i bara kalsongerna! Han och den andra fan han delade rum med hade gått och lagt sig, för de trodde att ingen skulle komma! Men J. P. fann sig direkt, bjöd in oss och började skvätta upp drycker åt alla håll – och room partyt var i full gång! Precis så stimmigt, trångt och absolut bara jävla fanniskt som ett room party ska vara. Det här var ett room party av den klassiska, urfanniska sorten – the real style! En alldeles extra metal-komplimang till vackra, charmiga Agnes, som är blott 19,5 år och som var på sin första kongress men ändå var så jävla tuff att hon hakade på till room partyt trots att truppen anfördes av en så markant livsfarlig person som den skränige Bellis!
* Minnesprogrampunkten för våra bortgångna. Som Johan Anglemark säkert minns var min omedelbara  invändning när han förra året frågade om kongressdatumet kunde funka att jag inte gillade att kongressen lades på de dödas dag, den dag då vi minns våra bortgångna. Jag tycker att kongresskommittén mycket värdigt och mycket vackert tog hänsyn till högtiden genom den stämningsfulla och stundtals rörande programpunkt där vi fick berätta om dem vi minns och som inte längre finns ibland oss. En lysande idé och en lysande programpunkt, inte minst därför att dem vi mindes var just lysande. Tack.
* Umgänget! Det här var en kongress med i allra bästa bemärkelse spirituellt, livfullt, givande, utmanande, roligt, värmande umgänge! Tack alla från mig i alla fall, för jag hade kanonroligt!
* Panelen om Obama och McCain som science fiction. Vi hade bara så jävla roligt, både vi som satt i panelen och, helt uppenbart, den våldsamt entusiastiska publiken. Jons avslutande betraktelse var helt jävla briljant!
* Dead Dog Party. Urgullig och hårt slitande ensam bartendertjej som helt överrumplades av att få sin vanligen söndagstomma pub fullkomligt invaderad (jag vräkte på med dricks till henne och det hoppas jag sannerligen att ni andra gjorde också!) och så hög och rolig stämning att man inte ens lyckades bli ledsen över att denna helt jävla lysande kongress var över!

Ett mycket uppriktigt känt och mycket stort tack till kommittén!

Kör hårt,

_Bellis_
