+++
title = "Sf-radio"
slug = "sf_radio"
date = 2008-03-31

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["mp3", "radio"]
+++

Oh, yum!

Jag har prenumererat på musik i form av högkvalitets-mp3:or från emusic.com i drygt två år. 25-30 kr per album - mycket billigare än att köpa cd eller att köpa från Itunes och andra nedladdningssajter. Dyrare än att ladda ned olagligt, men bättre kvalitet och så har jag inget att invända mot att upphovsrättsinnehavarna får betalt. Just nu upptäckte jag The Best Of Radio Science Fiction. Över fem timmar radio för drygt tjugo spänn! Det är ett gäng klassiska radiosändningar från mellan 1938 och 1950:

* The War Of The Worlds - The 1938 Radio Broadcast Starring Orson Welles
* Orson Welles Meets H.G. Wells - The 1940 Radio Broadcast
* The Time Machine - The 1950 Radio Broadcast
* Rocket From Manhattan - The 1945 Radio Broadcast
* Twenty Thousand Leagues Under the Sea - The 1953 Radio Broadcast
* Mars is Heaven - The 1950 Radio Broadcast
* To The Future - The 1950 Radio Broadcast
* The Meteor Man - The 1942 Radio Broadcast
* Donovan’s Brain - The 1944 Radio Broadcast Starring Orson Welles
* The Martian Chronicles - The 1950 Radio Broadcast
* Operation Tomorrow - The 1950 Radio Broadcast

Det här ska bli mums att lyssna sig igenom! Jag skulle i och för sig tro att de flesta av dessa går att hitta gratis och lagligt via archive.org och andra sajter, men jag orkar inte leta. Det här är billigt nog för mig.
