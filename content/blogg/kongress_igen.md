+++
title = "Kongress igen"
slug = "kongress_igen"
date = 2009-03-25

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = []
+++

Faktum är att jag nog inte är så bra på att få ut det mesta möjliga av en
sf-kongress. Ärligt talat glömmer jag snabbt bort det mesta också: vad jag
hört och sett, vem jag mötte, vad som hände. Många kongresser är bara ett
sammelsurium i huvudet. Förmodligen har jag absorberat någon sorts
erfarenheter från dem, även om det inte är något jag kan redogöra för eller
är riktigt medveten om. Hur som helst undrar jag ibland hur det kommer sig
att jag fortfarande är rätt intresserad av att åka på kongresser -- särskilt
kongresser där jag inte känner någon och alltså inte kan ha _träffa gamla
kompisar_ som förklaring.

Träffa nya kompisar kanske? Det lyckas tyvärr oftast inte så bra. Fast det är
definitivt väldigt uppiggande att vara bland en massa folk en hel helg. Det
är nog det som gör det. Även om jag inte alltid känner mig särskilt belåten
med förmågan att knyta kontakt med andra är det verkligen så att jag suger åt
mig energi av blotta närvaron av energiska och kreativa människor. En sorts
väldigt mild form av vampyrism? Även om jag har lätt för att känna mig
utanför blir jag alltid glad av att vara där något händer, och jag brukar ju
vara ganska bra på att fara omkring och försöka vara överallt och prata med
alla och se ut som om jag hör dit.

Den här gången är det [Ad Astra](http://www.ad-astra.org) i Toronto. Jag ska
hålla ett föredrag om mörk materia på söndag, och sitta med i en panel av
vetenskapare som svarar på frågor från publiken. Dessutom ska jag vara med i
en panel om kolonialism i science fiction, tillsammans med David Hartwell och
Timothy Zahn med flera om man får tro programmet. Förra året var det väldigt
manfall från panelerna, så vi får väl se vilka som faktiskt kommer att vara
där.

Rapport kommer nästa vecka.
