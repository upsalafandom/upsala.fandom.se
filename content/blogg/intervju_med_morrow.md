+++
title = "Intervju med Morrow"
slug = "intervju_med_morrow"
date = 2009-06-23

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Nyheter"]
taggar = ["James Morrow", "WordPress", "ateism", "intervju", "meta"]
+++

James Morrow, som bland annat skrivit _Only Begotten Daughter_, en
bokpratsbok vi diskuterade för något år sedan, blir intervjuad i
radioprogrammet _Atheists Talk_. Man kan [lyssna på intervjun
här](http://mnatheists.org/content/view/355/1).

PS. För er som skriver här på bloggen kan jag meddela att jag just
uppgraderat den till WordPress 2.8. Det finns en lite töntig [film som
sammanfattar de viktigaste
nyheterna](http://wordpress.org/development/2009/06/wordpress-28).
