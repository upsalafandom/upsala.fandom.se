+++
title = "The Library, Willy Ley, and Me"
slug = "the_library_willy_ley_and_me"
date = 2009-10-01
lang = "en"

[taxonomies]
forfattare = ["George"]
kategorier = ["Kåserier"]
taggar = ["New York", "autobiografi", "bibliotek", "böcker"]
+++

I was born in Far Rockaway NYC, the southernmost area of The Big Apple.
Except for the Carnegie Library, my high school, and my many friends, it was
an intellectual wasteland. You can see the Library here. It was built in 1906
and was stocked with good reading. As a youngster of 15 in 1957 my interests
were technical-scientific and my mind was empty. Then I got a card for the
“adult” section. I immediately became a bookworm.

![Bibliotek](library.jpeg)

<!-- more -->

The Carnegie had small but excellent SF (ground floor, left) and technology
(first floor, center) sections. I quickly devoured Olaf Stapledon’s classics
_Last and First Men_ and _Star Maker_, Asimov’s
_Foundation_ trilogy, and Clarke’s _The City and the Stars_. A
better introduction to SF was unthinkable at that time. These books turned me
into a lifelong, devoted, SF reader. Thanks to this initiation I was able to
take part in the development of modern SF into its current mature form.

The technology section had several wonderful books on spaceflight. The most
prominent and distinguished was the third edition (1957) of Willy Ley’s
_Rockets, Missiles, and Space Travel_. It is a book of a kind that is
no longer written and that, I believe, can today be published only after
overcoming stiff editorial resistance. Its 528 readable pages present an
historical, technical, and in part narrative history of its title subjects.
Its bibliography details texts in a number of languages and does not shy away
from reproducing titles in Cyrillic and Japanese notations. Try doing that
now! I love this masterpiece so much that I recently bought a used copy
online.

![Omslag till Rockets, Missiles and Space travel av Willy Ley](willy_ley.jpeg)

Dr. Ley was one of the founders of the _Verein für Raumschiffahrt_
(Association for Space Travel), the famous club of German astronautics
enthusiasts, which among other achievements experimented with liquid fuel
rockets at its _Raketenflugplatz_ (rocket airdrome) in the outskirts
of Berlin. He and his wife Olga fled to New York City after the Nazi seizure
of state power (the group dissolved a bit later, due to disagreements
concerning cooperation with the Nazis). My reading of SF and rocketry
literature moved me to contact him. We met twice in his modest house in
Jackson Heights, NYC. Our conversations convinced me to study mechanical
engineering and other subjects in college (CCNY), so that I could advance the
development of liquid fuel rocket engines. I admire them and Willy Ley to
this day. Indeed, he was revered in NYC SF circles and is now an American
cultural icon.

His immense scholarship in several fields, combined with my continual reading
of the best SF, helped save me when I was no longer able to study
engineering, mathematics, and physics. I switched to philosophy and
transferred to Queens College, which was near Far Rockaway. The importance of
studying the history of philosophy and literature – not only SF – was
impressed on me by Willy Ley’s pleasant but authoritative character. It has
never left me, although I left the Carnegie Library behind when I moved to
Manhattan to attend Graduate School at Columbia University. That magnificent
library, a monument to learning, burned down around 1970; it was replaced by
a postmodern monstrosity whose online photos fill me with disgust for our
dumbed-down age.
