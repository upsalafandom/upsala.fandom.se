+++
title = "Vetenskap och sf"
slug = "vetenskap_och_sf"
date = 2008-03-11

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["detektorer", "hård sf", "vetenskap"]
+++

Det slog mig plötsligt att jag jobbar på ett ställe som finns med i en sf-roman. I Uppsala jobbade jag med data från en detektor som också finns med i en sf-roman. Hur ska jag kunna fortsätta härifrån? Hur ska jag kunna hitta något nytt jobb som lever upp till den här underliga standarden? Kanske blir jag tvungen att skriva en egen sf-roman.

Stället där jag jobbar nu (eller i alla fall emellanåt, som förra veckan) är SNOLAB, ett underjordiskt laboratorium i en gruva, två kilometer under jord (2070 meter ska det vara, har det sagts mig). [Robert J. Sawyer](http://www.sfwriter.com) var där på studiebesök innan han skrev _Hominids_, så vitt jag förstår.  Det går ett rykte om att han tog stort intryck av en kvinnlig postdoc som visade honom runt, och folk tycker att han kanske använde sig lite för tydligt av den förebilden. Men det är som sagt ett rykte, och jag har inte läst boken ännu så jag kan inte säga något om hur Sawyer genomförde det. Det sägs att den är bra. Jag har den.

Varför i en gruva? För att skärma av så mycket som möjligt av den kosmiska strålningen, som stör våra väldigt känsliga detektorer. I en gruva detekterar man neutriner, och försöker detektera mörk materia-partiklar, sånt som passerar genom berggrunden mycket lättare än joner från rymden.

AMANDA, som jag jobbade med i Uppsala, är som ni kanske känner till ett neutrinoteleskop på sydpolen. Det finns med i _Antarctica_ av Kim Stanley Robinson. Den har jag läst, och den var bra.

(För övrigt hade jag lyckats lura mig själv med ett för bra lösenord, och kunde därför inte logga in igår. Sånt händer ibland, på webben där jag försöker tänka på säkerheten men inte orkar bry mig så jättemycket...)
