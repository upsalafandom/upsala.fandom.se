+++
title = "Det var svårt det här…"
slug = "det_var_svart_det_har"
date = 2008-05-27

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["Angel", "LibraryThing", "sf-kongresser"]
+++

…att lyckas blogga kontinuerligt. Just nu är min stora fritidssysselsättning
att föra över min och Linnéas bokkatalog från en FileMaker Pro-baserad
databas till [LibraryThing](http://www.librarything.com/catalog/anglemark).
Det går ganska geschwint, men det är ett par böcker, så det tar tid. Jag
upptäcker dubletter som jag inte visste om - vårt ena exemplar av Michael
Crichtons _Det stora tågrånet_ hade författarens namn felstavat på
omslaget (_Chrichton_) och hade därför lyckats smyga sig in obemärkt i
katalogen - men det är ju en av finesserna med LibraryThing: eftersom
databasen noterar att olika utgåvor är samma verk så får man hjälp med att
identifiera dubletter. Nu tror jag i och för sig inte att vi har så värst
många fler dubletter som vi inte visste om.

I övrigt fylls kvällarna av kommittémöten inför kongresser jag är med att
arrangera - [Kontext](http://kontext2008.se) och [Imagicon
2](http://www.imagicon.se) -, pubmöten samt
[Angel](https://en.wikipedia.org/wiki/Angel_%28TV_series%29). Vi har aldrig
varit särskilt förtjusta i Angel-figuren i Buffy, men tänkte att vi till slut
skulle ge serien en chans, och det har den väl på det hela taget visat sig
värd än så länge, även om den är ruskigt ojämn.
