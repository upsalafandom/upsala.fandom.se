+++
title = "Lömsk ström"
slug = "lomsk_strom"
date = 2008-07-28

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["Kate Atkinson", "fantasy", "noveller"]
+++

Vad kallar man det? Slipstream? Weird fiction? Fast det kanske bara gäller om
det är författare från genreghettot som skriver?

Jag hade ingen aning om att Kate Atkinson skrev fantastik. Det enda jag läst
av henne tidigare var den tragikomiska familjehistorian _I muséets dolda
vrår_, som visserligen hade en något fantastisk utångspunkt (huvudpersonen
minns saker från innan hon föddes, och vet en massa saker som annars bara
allvetande författarröster brukar kunna veta) men annars var gediget
planterad i en fullt igenkännbar version av vår verklighet. Jag köpte
novellsamlingen _Not the End of the World_ för att jag gillade Atkinsons sätt
att skriva och hade tänkt läsa mer av henne oavsett vad det var för något.
Dessutom var boken billig.

Och så visar det sig vara fantasynoveller. Jag har läst en hel del liknande
på senare år, ibland nominerat till Hugo eller Nebula: berättelser som
utspelar sig i en nästan vardaglig och realistisk verklighet, men där
någonting bryter igenom och antyder underligheter utanför det vanliga.
Regnet, är det bara ett litterärt grepp för att illustrera huvudpersonens
känslor, eller är det verkligen så att hennes tårar framkallar regnväder?
(Kan förresten någon identifiera den här novellen åt mig? Nominerad till
något för högst tre-fyra år sedan. Om en tjej som åker till Japan.) Faktum är
att de här Atkinson-novellerna hittills är mycket mer oblygt fantastiska än
så.

Jag som trodde att jag skulle läsa lite mainstream också.

Inte för att jag klagar!

***

Jag kommer till Uppsala imorgon.

För övrigt önskar jag att jag vore bättre på att komma på klatschiga rubriker
för mina inlägg.
