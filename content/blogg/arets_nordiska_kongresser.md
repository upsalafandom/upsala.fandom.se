+++
title = "Årets nordiska kongresser"
slug = "arets_nordiska_kongresser"
date = 2009-05-11

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Nyheter"]
taggar = ["Alcocon 3", "Aplakonen", "Fantasticon 2009", "Finncon 2009", "Imagicon 2", "Norcon 2009", "Swecon 2009", "Åcon 3"]
+++

Årets första sf-kongress i Norden har redan ägt rum, det var [Alcocon 3 i Göteborg](http://www.clubcosmos.net/Alcocon3), som avhölls 24-26 april. Bilder en masse därifrån finns på [Flickr](https://www.flickr.com/photos/tags/alcocon3). Resten av årets kongresser är:

* [Åcon 3 i Mariehamn](http://acon3.wordpress.com), 21-24 maj. Författarhedersgäst: Steph Swainston.
* [Aplakonen i Bredaryd](http://www.norensoversattningar.se/Aplakonen), 12-14 juni.
* [Norcon i Oslo](http://norcon.fandom.no), 19-20 juni. Författarhedersgäster: Richard Morgan och Iselin Alvestad.
* [Finncon i Helsingfors](http://2009.finncon.org/en), 10-12 juli. Författarhedersgäster: George R.R. Martin, Alastair Reynolds, Irmelin Sandman Lilius och Adam Roberts.
* [Fantasticon i Köpenhamn](http://www.fantasticon.dk/fantasticon2009), 29-30 augusti. Författarhedersgäster: Charles Stross och Gwyneth Jones.
* [Imagicon 2 (Swecon) i Stockholm](http://www.imagicon.se), 16-18 oktober. Författarhedersgäst: Liz Williams.

Jag hade tänkt att åka på åtminstone de två finska samt Swecon i Stockholm.
Kanske på Fantasticon i Köpenhamn, förmodligen inte på Norcon och tyvärr inte
på Aplakonen. Hur ser det ut för er andra som läser den här bloggen?
