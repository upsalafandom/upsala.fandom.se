+++
title = "Nyutkommet"
slug = "nyutkommet"
date = 2008-06-22

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["böcker"]
+++

På en kongress någon gång för länge sedan, förmodligen i slutet av 90-talet,
mötte jag en man som förklarade att han bara läste ny sf. Med det vidade det
sig att han menade sådant som skrevs i början av 80-talet, sisådär 15 år
tidigare. "Nytt" och "gammalt" är ju en fråga om perspektiv, och vad man har
för sorts gradering på sin måttstock.

Själv gjorde jag precis den intressanta iakttagelsen att de fem senaste
böckerna jag har läst är sådana som kom ut 2007 eller 2008. 22 av de 57
senaste böckerna jag har läst har varit högst två år gamla (det är allt som
finns i den läsjournal jag har med mig här i utlandet). Jag är helt säker på
att det inte var så om jag skulle titta längre tillbaka, då lutade jag mer åt
att läsa gamla klassiker och sådant jag blivit rekommenderad av andra, och
nästan ingenting som nyss kommit ut. Jag kände helt enkelt inte till de
aktuella författarna, och hade inte koll på vad som var nytt och bra.

Två saker bidrar till att jag läser fler nya böcker, särskilt det senaste
året.

1. RSS, som hjälper mig att hålla koll på [Locus online](http://locusmag.com) och andra källor till information online, så att jag vet något om vad som är på gång i bokvärlden. Det har jag använt mig av i tre år, eller vad det nu kan bli.
1. Hyllan för nyinkommen science fiction på biblioteket. Det här gäller bara det senaste året, för säga vad man vill: på svenska bibliotek finns inte en hylla med ny sf och fantasy i hårdpärm. På svenska bibliotek kan man (nästan) aldrig läsa engelskspråkig fantastik i tid att hinna nominera den till Hugon, men det kan man faktiskt här.

Senast läste jag _The Dragons of Babel_ av Michael Swanwick, och den kan jag
verkligen rekommendera. Läs också min [recension av _Final
Theory_](http://lablit.com/article/389), en sorts vetenskapsthriller.

För övrigt anser jag att det är märkligt att Upsalafandom online numera
verkar bestå till så stor andel av folk som bor i Kanada. Finns det ingen mer
därhemma som har något intressant att säga? (Jag har det inte alltid, men jag
harvar på eftersom Johan A gav mig en föreläsning om hur viktigt det är att
inte se bloggandet som ett tillfälligt åtagande ;-)
