+++
title = "Tisdag 1 juni 2010"
slug = "tisdag_1_juni_2010"
date = 2010-06-02

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["foto", "pubmöten"]
+++

Jag stannade inte så länge på pubmötet igår kväll, men jag hann åtminstone lägga ut texten om batteriers hållbarhet och vinpriser, och knäppa några bilder.

![Tisdag 1 juni 2010 1](tisdag_1_juni_2010_1.jpeg)

<!-- more -->

![Tisdag 1 juni 2010 2](tisdag_1_juni_2010_2.jpeg)

![Tisdag 1 juni 2010 3](tisdag_1_juni_2010_3.jpeg)

![Tisdag 1 juni 2010 4](tisdag_1_juni_2010_4.jpeg)

![Tisdag 1 juni 2010 5](tisdag_1_juni_2010_5.jpeg)

![Tisdag 1 juni 2010 6](tisdag_1_juni_2010_6.jpeg)

![Tisdag 1 juni 2010 7](tisdag_1_juni_2010_7.jpeg)

![Tisdag 1 juni 2010 8](tisdag_1_juni_2010_8.jpeg)
