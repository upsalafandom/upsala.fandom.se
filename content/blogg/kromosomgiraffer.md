+++
title = "Kromosomgiraffer!"
slug = "kromosomgiraffer"
date = 2004-03-02

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Det viskades kring bordet om möjligheten att skåla för nån död nazist, men ingen tyckte att det var nån riktigt bra idé. I stället skålade vi för kromosomgirafferna, som nämndes i en vältalig text av Roland Adlerberth (citerad i Jerry Määttäs licentiatavhandling som jag av en lycklig slump råkade ha i väskan). Kromosomgiraffer, rymdflygare och små krälande ormfåglar från tredje planeten till höger i Antares system. Jajamän.

<!-- more -->

När jag kom var det i sällskap med två glada medlemmar i min amnestygrupp.

Vi satte oss i ett hörn för att ha möte om mänskliga rättigheter, medan Nicklas och Tomas ensamma försökte hålla ett långt bord. De hade ett dåligt utgångsläge, eftersom det bord vi brukar använda som påbyggnad redan var ockuperat av ett annat sällskap. När MR-kämparna omsider gav sig av var fansens bord redan för litet. Fint främmande från söder hade anslutit sig, KJ och Therese och till och med Wahlbom. När folk fortsatte att droppa in och vi redan trängdes kring hörnen flyttade vi till långbordet närmare dörren. Två stackare vid bordet intill verkade känna sig trängda, för de flyttade längre bort. Så kan det gå när man försöker trängas med fandom!

Nicklas hade med sig nummer två av De elektriska elefanternas dal, till allmän förtjusning. Fanzines åt filket.

Wahlbom kunde med teknikens underverk upplysa oss om vad NASA sagt på presskonferensen om mars – Mars! Vatten minsann.

Dessa personer tycker jag mig minnas att jag såg (var vi inte fler?): Nicklas, Tomas, Maria, Samuel, Therese, Karl-Johan, Ante, Wahlbom, Sten, August, Gabriel, Luna, Marie, Björn X. Och så jag förstås.

/Åka

Det pratades, åtminstone till en början, om hur väldigt kallt det var ute i går kväll och det med rätta för det var bokstavligen ruskväder. Det gick att avgöra på grundval av kläderna vilka besökare på Williams som gått ut tidigt på tisdagsmorgonen och gått direkt till mötet och vilka som varit hemma och vänt innan pubbesöket. De senare var synnerligen påpälsade medan jag som tillhörde de förras skara knappast kan beskyllas för att ha tagit till i överkant med varma kläder och halsdukar eftersom morgonen utlovade en vacker senvinterdag med plusgrader. Nåväl, vägen till puben var inget problem, det hela började när jag skulle gå hem. Maken till oväder var det länge sedan jag upplevt så intensivt. Promenaden från Williams startade relativt lugnt men när jag nådde centrum så hade snöovädret slagit till med fullaste styrka. Total och fullständig motvind i kulingstyrka längs hela Vaksalagatan från stan upp mot Sala Backe är inget man önskar vara med om var dag. Jag lovar. Till och med kromosomgirafferna hade frusit.

/Tomas
