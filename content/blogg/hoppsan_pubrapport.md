+++
title = "hoppsan, pubrapport!"
slug = "hoppsan_pubrapport"
date = 2007-07-03

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Ah. Första tisdagen i månaden (och inte en helgdag). Det betyder pubmöte i Upsalafandom, sommar såväl som vinter.

Lennart Svensson var där ikväll (han som skrivit Soldat i universum, som jag inte har läst än), och jag fick ett exemplar av hans Den musiske matlagaren. Den finns recenserad i 12-timmarsfanzinet, som alla bör ladda ner och läsa. Fanzinet innehåller också bland annat en novell av Gabriel Nilsson, en recension av en bokhylla, och en del skojigt smått.

<!-- more -->

Vi skulle ha diskuterat lite seriösa saker, som kongressplaner, men det föll sig inte riktigt. Det kom andra samtalsämnen i vägen. Jag började prata lite om URSUS, vårt lilla resestipendium för folk som vill åka på sf-kongresser, fast det ledde ingen vart. Johan påpekade att ingen är intresserad av att åka på kongress som inte redan har varit med i fandom ett par år. Min förhoppning var ju från början att någon av dem som hänger med i fandoms utkanter skulle kunna lockas att gå på sin första kongress med lite pengahjälp, snarare än att helt nya skulle komma till -- fast det kanske inte funkar så. Hur som helst finns en fond med lite pengar i om det skulle komma till nytta någon gång, och någon måste ta över administrationen av detta. Ja ja.

Några killar från Catahya dök upp också. De chockerade mig med att inte alls förstå Sandman-referenser. En av dem pratade om en samling låtar om förstörelse, Destruction alltså. Jag kommenterade att det alltid snarare varit Delirium som varit min favorit, och kunde riktigt höra det ljudliga svischandet när referensen flög förbi över huvudena. Vad är detta? Fantasyfantaster som gillar konstig musik, men som inte läst Sandman? Det trodde jag var en allmängiltig gemensam referens, men det kanske inte är det längre.

Via sommar och över festivaler gled samtalet in på musik. Det var kul. Jag har inte lyssnat speciellt aktivt på musik på något år eller så, och har börjat känna ett visst intresse för sånt igen. Kul att höra vad andra har för sätt att hitta och välja musik. Jag har inte ens varit på festival sedan Hultsfred 2004, och har just nu varken riktiga högtalare till datorn eller någon riktig stereo hemma.

Man måste inte prata om science fiction för att det är sf-pubmöte. Det gjorde jag i och för sig i alla fall, litegrann, eftersom jag nästan alltid pratar om sf i alla fall. Men det var rätt lite.

Det var trevligt ikväll, fast lite konstigt. Dels hamnade vi av diverse anledningar vid skilda bord, och det var mindre rotation än vanligt (vi har en tradition att om man reser sig från sin plats för att till exempel gå till baren så tar någon annan den stolen så att man inte sitter på samma ställe hela kvällen). Dels hade jag dottern på 20 månader (och maken) med mig, och vi stannade rätt länge. Jag brukar tycka att det är dumt att övertala lillan att vara med på ett ställe hon tycker är tråkigt, och dessutom är det inte roligt för omgivningen heller -- men idag tog hon en lång eftermiddagslur och var jättepigg. Vi turades om att underhålla henne, och var ute och gick i universitetsparken med henne. På så vis kunde vi vara närvarande rätt länge.
