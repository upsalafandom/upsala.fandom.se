+++
title = "Var får de sina idéer ifrån?"
slug = "var_far_de_sina_ideer_ifran"
date = 2008-10-05

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = []
+++

Egentligen tycker jag inte att det här med idéer är så svårt. Det behövs bara
att man tar vara på alla associationer man gör, och spinner vidare på de
bästa. Att bli författare och faktiskt skriva romaner är en annan sak. Ibland
undrar jag ändå hur de gör för att avgöra vilken idé som faktiskt kan funka
och bli en säljande bok.

Följande beskrivning är till exempel tämligen… fantastisk.

> Det är jobbigt nog att vara succubus utan att dessutom vara bokhandlare. När
> man träffar på den hetaste killen den här sidan böckerna och det visar sig
> att han är ens favoritförfattare - då måste man ju hålla tassarna från honom!
> Om man äter honom kan han ju inte skriva mer…

(från [SF-Bokhandeln i Malmö](https://sfbokhandelnmalmo.blogspot.com/2008/09/who-would-you-not-kick-out-of-bed-1-mn.html))

Jag kan inte bestämma mig för om jag tycker att det här låter jättetöntigt
eller ganska kul. En bok om böcker och attraktiva författare. Det är förstås
fullständigt legitimt, men ändå: "titta, böcker är faktiskt jättesexigt,
särskilt när man kombinerar dem med söta och koffeinhaltiga drycker!" (Enligt
förlagets text dricker denna succubus många white chocolate mochas.)

Somliga böcker tycker jag att det är roligare att läsa om än att faktiskt läsa.
