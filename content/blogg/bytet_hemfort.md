+++
title = "Bytet hemfört"
slug = "bytet_hemfort"
date = 2008-05-10

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Dan Simmons", "Nathanial Hawthrone", "Poul Anderson", "Ramsey Cambell", "Robert Silverberg", "William Faulkner", "böcker", "droger"]
+++

Som jag berättat tidigare har ett antikvariat i stan rabatt för att de ska
slå igen. Jag har nu fyndat lite och kan inte låta bli att prata om fynden.

Väl medveten om att många verkar vara väldigt fascinerade av hans
författarskap hittade jag en volym med _The Scarlet Letter_ av Nathaniel
Hawthorne. Dessutom en bok av William Faulkner, som man ju nästan måste läsa
om man likt mig har haft en Hemingwayperiod. Faktum är att det är Robert
Silverbergs fel, eftersom han fick mig att bli fascinerad igen av litterär
stil och subtil språkmagi som en del av de här stora andarna ju är bra på. Vi
får se om jag kan snappa upp nåt, och njuta lite under tiden.

Efter dessa höglitterära köp så kunde jag inte låta bli att kasta mig över
_Agent of the Terran Empire_ av Poul Anderson, samt _The Boat of a Million
Years_ av densamme. (Jag borde ha börjat läsa Poul Anderson tidigare!)
Lyckligtvis fanns det även en _Songs of Kali_ av en annan favorit, Dan
Simmons, och Ramsey Campbells _The Long Lost_. De som någon gång hört t.ex.
Johan Anglemark prata om Ian McDonald kanske även känner igen _Desolation
Road_, vilket kändes som ett fynd även om den var lite sliten.

Som jag nämnde var Robert Silverberg inspirerande för vissa av inköpen, och
jag kunde inte låta bli hans _Downward to the Earth_ och en annan volym som
jag redan har i Upsala Jag vill läsa dem här och de finns inte på biblioteket
och jag kan ju alltid ge bort dem när vi åker härifrån. Jag har redan skänkt
bort en av böckerna jag köpte, nämligen _The Fifth Head of Cerberus_ av Gene
Wolfe. Den kostade $1.5 och jag visste att en av ägarna till spelbutiken där
jag håller till borde kunna gilla den. Hon är stor Aldissfan för övrigt.

Mycket skoj blev det! Det var billigt som bara den och det fanns flera små
godbitar jag inte skrivit om här. Detta måste vara en drog för mig, för det
känns som man trippar på tå och det bubblar lite i en när man köpt en
jättehög med böcker för ingenting, och bär hem bytet för att stapla dem och
gotta sig. Schysst drog, som inte verkar vara så farlig för kroppen.

Jag kanske återkommer med intryck när jag läst lite.
