+++
title = "Konstprat!"
slug = "konstprat"
date = 2008-11-17

[taxonomies]
forfattare = ["Torbjörn"]
kategorier = ["Kåserier"]
taggar = ["hobby", "konst"]
+++

_Torbjörn tar sig friheten att sprida sina vanföreställningar om konst, på
det att de torde besvära honom (relativt sett) mindre (än andra)._

Som vissa phanniska individer tvingats uppleva så är jag väldigt intresserad
av konst. Konstprat har en stark tendens att flumma ut och bli ointressant,
varför jag föredrar att producera det framför att konsumera det. Jag skulle
med andra ord vilja åsamka er några av mina tankar i ämnet i form av frågor,
för att sondera fhandoms åsikter:

* Hur kommer det sig att man ofta hör frågan "…men är det Konst?", men väldigt sällan frågan "…men är det Musik?"
* Kan en tavla med bara en röd fyrkant på vara ett konstverk?
* Nämn något du tycker är "Konst".
* Nämn något som kallas konst av somliga, men där du inte håller med.
* Är det bra med kulturstöd till konst?

För mina egna svar, se kommentar.
