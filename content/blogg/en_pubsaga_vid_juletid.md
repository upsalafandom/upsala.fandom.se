+++
title = "En pubsaga vid juletid"
slug = "en_pubsaga_vid_juletid"
date = 2000-12-05

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Denna saga utspelar sig på den tiden vintern ännu inte hade kommit till Upsala, när blidvädret härskade och Qvirinius ännu inte hade blivit utsedd till ståthållare i Slas uppläsning av Lukasevangeliet. Denna saga utspelar sig närmare bestämt tisdagen den 5 december 2000.

<!-- more -->

På Williams krog hade ett gäng av traktens ungdomar församlat sig, på det att Upsalafandom ej längre månde vara de enda gästerna denna tisdag, och den trängsel som därvidlag uppstod innebar att vi tvungos spilla över på andra bord än de vi i vanliga fall ockupera.

De Upsalafans som kommo hit för att trängas denna afton voro Anna Åkesson, Sten Thaning, Daniel Luna, Markus Persson, Anders Wahlbom, Simon Åslund, Karl-Johan Norén, Nils Segerdahl, Andreas Gustafsson, Gabriel Nilsson, Björn Lindström, Björn X Öqvist, Johan Anglemark, Pyro, Ragnar Hedlund, Joacim Jonsson och Jon Thorvaldson.

Fanmenigheten kom snart in på konversationsämnet teologi, vilket faller sig naturligt om man betänker det nära avståndet till domkyrkan och Helga Trefaldighetskyrkan i kvarteren om hörnet. Studentkårens än mer påtagliga närvaro insprirerar betydligt mer sällan till reflektioner av något slags djup, vilket inte heller är så märkligt för den insatte.

Men teologin inom science fiction-fandom, befanns det, skulle förmodligen dra stor fördel av lite relikhandel. Varför säljs aldrig något lillfinger från den sene och beklagade Isaac Asimov på auktionerna? Vad skulle en nitisk neofan icke giva för att komma i besittning av Walt Willis vänstra hörntand? Här, ansåg den församlade menigheten, fanns ett behov att fylla, en kommersiell niche som väntade på att uppstå.

Evangelisten Markus namne gjorde härvid effektfull entré, med ett sjutton gånger sjutton linjer stort bräde under ena armen. Vid detta slog sig sedermera en liten skara ned och spelade go, vilket säkerligen var uppbyggligt för dem, men innebar att de inte längre ingick i de konverserandes skara, något jag åsåg med blandade känslor. Säkert är dock gruppen är stor nog att kunna erbjuda denna sysselsättningsbredd utan att utarmas.

Pyro, som var en för de flesta ny bekantskap, avhöll sig från alkoholens lockelser i det att han misstänkte att en del andra substanser med vilka alkoholen ej trivs, fortfarande funnos kvar i hans kropp sedan föregående natt. Själv fördrev jag tiden med att sälja några överexemplar av Rickard Berghorns tidskrift Minotauren, som jag verkar ha blivit något slags lokal generalagent för.

Åka konstaterade att hon egentligen bort stanna hemma och ömt vårda sin förkylning, men hon hade hunnit tröttna rejält på att vara sjuk och tog därför med sig förkylningen ut så att den skulle få beskåda andra ting än hennes studentrums fyra väggar under sin korta levnads lopp. Karl-Johan redogjorde för detaljer i det tilltänkta programmet på nästa års Swecon, och flera av oss suckade trängtansfullt vid tanken på en komplett samling av Linda och Valentin. Åka avvisade efter någon bråkdels sekunds tvekan tanken på att gifta sig till Bertils samling, och därmed komplettera sin egen.

Karl-Johan delade ut Fandoms Kommunnytt 15, och avslöjade att det har hänt att han stavat fel i avsikt att stimulera läsekretsens locskrivande. Han provade även på att dra ett skämt samt lät en bomb brisera: Inte ens Eva Norman stod ut med att läsa hela Battlefield Earth.

Mycket mer hände denna minnesvärda kväll, men då er krönikör inte haft tid att överföra sina minnesbilder på tre hela veckor, har de flesta av dessa hunnit förflyktigas i etern och för alltid gå förlorade. Ve dem som lyssna.

Ex pulpetra

Johan

Nästa möte blir på samma plats (Williams pub, i samma hus som Upsala Studentkår) från 18.30 och framåt, tisdagen 2 januari. Det blir tredje årtusendets första möte! Gosh.

Väl mött!
