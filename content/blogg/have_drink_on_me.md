+++
title = "Have a drink on me"
slug = "have_drink_on_me"
date = 2008-09-17

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Bob Tucker", "Jim Beam", "fandom", "fanzines"]
+++

Fans i farten har naturligtvis redan läst [Chunga #14](http://www.efanzines.com/Chunga) där Dan Steffan skriver en minnesartikel om Bob Tucker. Jag läste den under semestern och insåg efteråt att som av en händelse var det just en flaska Jim Beam som jag köpte på flygplatsen i Amsterdam! Skål Bob!

Nu tittar jag in på [efanzines](http://efanzines.com) och naturligtvis hittar jag ett fanzine som dyker upp nästan samtidigt som jag läste Chunga. Det heter naturligtvis [BEAM](http://efanzines.com/Beam/index.htm). Kolla länken ovan.

Sånt här blir man ju törstig av...
