+++
title = "Den där boken"
slug = "den_dar_boken"
date = 2008-12-19

[taxonomies]
forfattare = ["Lennart Svensson"]
kategorier = ["Nyheter"]
taggar = ["böcker"]
+++

"Jag mötte en snubbe på en bar som kunde göra pdf av boken. Sedan gick jag runt och sålde den färdiga boken på samma bar..."

Så kan man ju sammanfatta "Eld och rörelse"s begynnelse. Det började på Williams pub våren 2007 då jag bad Anglemark layouta boken, och sedan delade jag ut friex och (senare) sålde ex till Uppsalafans. Om det sedan i framtiden kommer att låta som i det fiktiva citatet ovan, som om jag bara råkade träffa folk på Williams som var intresserade av boken, well, that's "such stuff that legends are made of"...

<!-- more -->

Nu, så här halvtannat år efter starten, har jag hunnit få en del respons på boken, av Uppsalafans och andra. "Nineves skatt", "Ett svenskt Roswell" och "Riddaren, djävulen och döden" tycks vara de mest populära novellerna.

Sedan har det kommit en veritabel recension av boken också, en bloggräka av en skribent som gått i närkamp med texten. Det är poeten Granlund på "Marmeladkungen" som funnit mycket att tala om, mycket att reagera på och en hel del att gilla också, bland annat titelstoryn. [Läs hela inlägget här.](http://marmeladkungen.wordpress.com/2008/12/13/bokrecension-eld-och-rorelse-av-lennart-svensson)

Det rör på sig litegrann får man säga. Och nu fortsätter äventyret; i februari (typ) lär jag ha min nästa bok klar, en roman, och då ska jag låta även den ha sin debut på Williams. Jag ska alltså "gå runt och sälja den bland folk på den där baren"...
