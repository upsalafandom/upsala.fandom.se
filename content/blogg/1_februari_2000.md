+++
title = "1 februari 2000"
slug = "1_februari_2000"
date = 2000-02-01

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

En rostfri ölback att slå folk i huvudet med vore ingen dum idé kanske vissa
läsare tycker, men denna tanke slog inte vår oefterhärmeliga Upsalafandom när
det 38:e pubmötet i Orvarsmöten TNG avhölls på Williams krog i går kväll.

<!-- more -->

Vädret denna vinter påminner väldigt mycket om en rollspelskampanj jag drev
någon gång i början på 1980-talet. Tärningarna rullade och i tabellen
konstaterade jag "idag är det minus tio och klart"; "idag är det plus fem och
snö"; "idag är det minus tio igen"; "oj, nu är det visst plus fem igen".

Med andra ord innebar gårdagens färd till Williams att jag riskerade liv och
jag vet inte vad över stadens glashala gator. Jag har full förståelse för
Sten som snart far till Malaysia, för att där beskåda kinesiska nyårsfirare.
Jag hoppas att deras gator är bättre skottade än våra. Stackars Åka har bara
erbjudits chansen att fara till England, men har å andra sidan ett hägrande
jobb som konstruktör av en chopper, som visade sig vara vare sig en
motorcykel eller ett hushållsredskap, utan en apparat för att hacka upp
strålar med.

Väl framme vid krogen fann jag att Andreas och Kristin hade hunnit före, och
satt redan med varsitt glas framför sig. Jag slog min ner och beställde en
Double Diamond. Efter ett kort tag, när även Karl-Johan hade anslutit, fann
vi oss inbegripna i en diskussion om spetsglas och deras autenticitet i den
svenska snapskulturen. Det framkom att Karl-Johan är i besittning av riktigt
fina gamla spetsglas, så ett inbrott hemma hos honom skulle inte vara
förgäves. Själv har jag inget av värde att stjäla, bara så att ni vet.

Smålänningarna vid bordet, båda enligt uppgift skapade av Gud, tyckte också
att det är märkligt att folk lägger ut stora summor för att köpa älgkött hos
slaktarn, när skogarna och hemmen svämmar över av varan. Däremot smakar älg
åtminstone distinkt, inte alls som häst som är lite tråkigt som stekkött och
smakar ganska likt struts. Kristin undrade varför inte våra förfäder blotade
struts till Oden i så fall, och vad Allfadern hade tyckt därom. Jag undrar om
de hade överlevt för att föra hans åsikt vidare, om de hade försökt blidka
honom med struts. Kräftor smakar garanterat *inte* som struts, men ska man
blota dem bör man inte köra dem genom papperstuggen, vilket så när skedde på
en arbetsplats jag en gång jobbade på.

Religion kom Karl-Johan in på även något senare, då denne förhärdade
kommeniss grymtande gick med på att alla präster inte behöver hängas i sina
tarmar, utan att de söta prästerna kan få klara sig. Vi får väl hoppas att
Åkas pappa är söt.

Cider är något som folk har dålig smak i, kunde jag konstatera, liksom när
det gäller champagne. Flera närvarande förklarade sin motvilja mot alltför
torra drycker. De gillar bergis inte Jever heller. Och dålig smak verkar ha
något med efternamnet Norman att göra. Lisanne och John är bara två exempel.
Håhåjaja.

Olycksöden var ett populärt samtalsämne. Åka kunde med innerlig övertygelse
avråda oss från att sträcka ryggmuskler samtidigt med att ådra oss en
besvärlig hosta. Kristin å sin sida hade tampats med ett paket om tre kg
vadmal som hon nyss fått levererat, och upptäckt att det billiga kilopriset
inte alls var lika roligt längre när portokostnaden för ett trekilos paket
tillkom.

Här slog en minnesbild Kristin och hon sänkte sedesamt blicken och berättade
om den dröm hon haft i natt, där hon kunde konstatera hur små smurfer, till
skillnad från hästar i fantasyböcker, bedrev ivrig fortplantning. Till sin
frustration kom hon aldrig tillräckligt nära i drömmen för att kunna se hur
de manliga smurfernas fortplantningorgan såg ut. Lustigt nog hade även
Jolkkonen drömt om smurfer natten innan, fast under betydligt mindre
skandalösa förhållanden. Jag lämnar fältet fritt för spekulationer.

Det här med fortplantning är ett kärt ämne, men riktigt kloka blev vi inte på britterna, som anser att högsta mode i kinky sex (när dessa ord uttalades lystrade Olle) är lättklädda högklackade damer som stampar på kackerlackor och små pälsdjur. Framför allt restes invändningar mot att det har med sex att göra. Lystra kan man göra av flera anledningar. När jag klagade på en programbugg spetsade Åka öronen, men medgav att hon hört fel och tyckt att jag sade programbok. Heder åt henne.

Och kära serconistiska läsare, fler sf/fantasyämnen diskuterades under kvällen. Exempelvis konstaterade Jolkkonen att alla raketer han under sitt stormiga liv har skickat iväg har exploderat efter någon futtig sekund i luften. En överlägsen Olle konstaterade att han minsann hade flera lyckade uppskjutningar bakom sig.

Den som tror att det enda som händer på våra möten är oansvarigt kannstöpande
får nu tillfälle att ändra uppfattning, ty vi diskuterade formalia. Ska vi
fortsätta vara på Williams? Ja, bestämde vi oss för, fast vi uppmanade alla
att hålla utkik efter en lokal som är mysigare än Williams och uppfyller
följande villkor: Central, inte för dyr, alla ska alltid kunna komma in utan
att diskrimineras, långsiktig lösning, någorlunda utbud av mat och dryck,
inte för populärt (det får inte bli fullsatt), inte för hög ljudvolym. En
tanke som väcktes var att hitta en central föreningslokal att låna/hyra en
kväll i månaden, så kan vi hålla med egen öl och egen mat.

Fler uppmaningar:

* Kom hem till mig och Linnéa från klockan 12 lördagen den 26 februari och spela spel! Rappakalja, TP, Axis & Allies, Junta, Britannia, poker eller vad ni vill. Spel finns, men ta med er skrynk och läsk själva.
* Kom hem till Sten måndagen den 28 februari kl 19 och diskutera Mervyn Peakes Gormenghast! Te serveras.

En längre tid har det på våra pubmöten bedrivits kommers med ett hoptrasslat
skosnöre, men den verkar ha nått sitt slut och maxpris när snörstumpen bytte
ägare för toppnoteringen 26,50. Dessutom glömde jag kvar snöret. Månne tog
någon annan hand om det.

Olle grunnade fortfarande på det här med kinky sex. Jag hade vidarebefordrat
ett brev till sverifandomlistan tidigare under dagen om att norska säpo
uppmanade folk som ville se sina säpoakter gärna fick tala om varför de
trodde att de **hade** en säpoakt. Olle föreslog då att man skulle kunna
skriva ett brev med följade lydelse:

> Hej säpo! Jag har hört att ni har ett register över folk med sexuella
> avvikelser Skulle jag kunna få komma i kontakt med några av dem?

En hastig blick på klockan nu avslöjade att hon närmade sig elva, så var det
dags att ge sig ut på ismassorna igen, som nu visade sig vara lätt pudrade
med pulversnö. Det första jag såg när jag steg utanför dörren var Mikael
Jolkkonen som mödosamt reste sig upp från marken med ansiktet förvridet i en
grimas.

Ex cathedra,

_Johan_

Vi var där: Johan Anglemark, Andreas Gustafsson, Mikael Jolkkonen, Joacim
Jonsson, Maria Jönsson, Björn Lindström, Gabriel Nilsson, Karl-Johan Norén,
Sten Thaning, Kristin Thorrud, Anders Wahlbom, Björn Wiklund, Anna Åkesson,
Samuel Åslund, Simon Åslund, Björn X Öqvist, Olle Östlund.

Nästa möte är i vanlig ordning första helgfria tisdagen i månaden på
Williams; i detta fall 7 mars 2000.
