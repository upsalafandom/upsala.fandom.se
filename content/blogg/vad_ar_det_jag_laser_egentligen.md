+++
title = "Vad är det jag läser egentligen?"
slug = "vad_ar_det_jag_laser_egentligen"
date = 2008-02-21

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["böcker", "tidskrifter"]
+++

För några dagar sedan kom jag att tänka på Locus. Åka är den i familjen som håller sig a jour med vad som är nytt på bokmarknaden, och jag i min tur håller koll på rollspelsbranschen. Det slog mig att jag har faktiskt köpt ett par nummer av Locus i min dag, men att jag knappast försökt få valuta för vad jag betalt för dem. Ni vet alla säkert att Locus för utom en rejäl intervju i varje nummer, också innehåller recensioner av allt nyutkommet som romaner, noveller, rubbet. Frågan är, vad har jag för relation till recensioner? Läser jag recensioner för att få tips om nya intressanta böcker? Skulle jag hitta mer kul att läsa genom att hålla koll på recensionerna i Locus?

En mycket enkel undersökning genomfördes. Eftersom jag har en bokjournal där jag skriver ner allt jag läser, med en kort kommentar, så kunder jag gå tillbaka till den och räkna efter hur många böcker jag läst, och av vilken anledning.

Nu visade det att förra året hade en signifikant del av de böcker jag läst kommit från den lista med kurslitteratur som jag betade av under mina nystartade studier. De är ju inte typiska på minsta vis så om vi lämnar dem därhän, och dessutom tar en titt på året innan, börjar trender visa sig. Det visar sig att väldigt många av de böcker jag läst är saker som jag läst på rekommendation från någon. Det kan vara en rollspelsförfattare vars intressen jag vet att jag delar, eller kanske en fan vars smak jag vet överenstämmer med min, eller ett fall av entusiasm på ett av de bokrekommenderarmöten vi hade. John Varley hade jag nog t.ex. aldrig läst om det inte var för att Torbjörn och Tobias varit så översvallande eniga om två av hans böckers förträfflighet. Clandestine av James Ellroy hade jag nog inte läst om det inte varit för att Greg Stoltze, vars underbart välskrivna rollspel Unknown Armies fascinerat mig så, rekommenderat honom. Kombinerat med en hel del läs på den trygga grunden att författaren är någon jag tidigare uppskattat, ger alltså intrycket att jag ofta läser saker som jag är trygg i förvissningen om att jag kommer att gilla.

The Harlot by the side of the road av Jonathan Kirsch är ett exempel på en annan intressant kategori. Det händer sig faktiskt at jag köper böcker bara för att jag råkar fastna för framsidan, baksides blurben, eller någon annan del av en boks rent fysiskt uppenbarelse. Sodoms dolk var en annan sådan bok. Hans Capelen som skrivit den är visst någon känd personlighet som skriver under pseudonym. Hade det inte varit för att den kostade en tia och hade en lockade mystisk framsida med en sinister dolk och en baksidestext som lovade spänning och en aning om övernaturliga mysterier, så hade jag säkert aldrig läst den. Nu var just den en intressant upplevelse för någon som växt upp med Dallas på TV. Folk rökte och drack som folk i nämnda TV-serie. Synnerligen osvenskt samtidigt som den dröp av Stockholmskänsla. Intressant uddahet.

Så, det verkar som om jag kan fortsätta att ignorera Locus, för tydligen lägger jag så stor vikt vid personliga rekommendationer och säkra kort, att jag med största sannolikhet inte skulle komma mig för att testa så värst många av alla de där nyutkommna sakerna de recenserar. När vi gjorde en liknande lista på Åkas läsvanor framkom ganska annorlunda tendenser, som du säkert anat. Prova själv, och ta reda på vad det är som styr dina läsval!
