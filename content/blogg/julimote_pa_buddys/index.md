+++
title = "Julimöte på Buddy's"
slug = "julimote_pa_buddys"
date = 2010-07-08

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

I tisdags möttes Upsalafandom på min kvarterskrog Buddy's, ett fall av omväxling förorsakat av sommaren. Bland samtalsämnena märktes kärnkraft, förlossningar och allmän livsleda. Jag och Magnus pratade också om våra kära kameror en god stund. Och jag använde min lite:

![Bild 1](julimote_pa_buddys_1.jpeg)

<!-- more -->

![Bild 2](julimote_pa_buddys_2.jpeg)

![Bild 3](julimote_pa_buddys_3.jpeg)

![Bild 4](julimote_pa_buddys_4.jpeg)

![Bild 5](julimote_pa_buddys_5.jpeg)

![Bild 6](julimote_pa_buddys_6.jpeg)
