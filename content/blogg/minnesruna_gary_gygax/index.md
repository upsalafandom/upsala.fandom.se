+++
title = "Vila i frid, ni som gått före"
slug = "minnesruna_gary_gygax"
date = 2008-03-06

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Minnesrunor"]
taggar = ["Gary Gygax", "fandom", "fanhistoria", "rollspel", "traditioner"]
+++

![Porträtt av Gary Gygax](garygygax.jpeg)

_1938–2008_

På morgonen för två dagar sedan dog E Gary Gygax. Det kan tyckas vara en
händelse som verkar vara relevant enbart för de närmast sörjande, men det har
visat sig vara en händelse som berört många. För de som inte vet det var det
alltså Gary och en annan person vid namn Dave Arneson, som skrev Dungeons &
Dragons, och därmed i mångt och mycket skapade hela rollspelshobbyn. Ser man
sig om i blogosfären så dyker det upp postningar om personliga minnen och
liknande. Vad som hänt är ju att en hobby, en rörelse, har tappat en av dem
som var med från början. En av dem som skapade det som man känner är ens
identitetsskapade grupp med barndomsupplevelser och likande.

Fandom är också något som skapat mycket sociala relationer, minnen och
upptåg. Precis som att rollspelsfandom nu reflekterar över sin historia och
sitt ursprung, så har sf fandom gjort det. Det har skrivits ganska mycket om
fanhistoria, och för många är det viktigt var fandom kommer ifrån, och hur
man kan hålla kontakt med traditionerna och folk som är bärare av dessa.

Själv har jag läst Rob Hansens "Then" och haft mycket nöje av det. Så vitt
jag vet finns det ingen liknande översikt över svensk fandomhistoria, men jag
kan ha fel. Jag har läst lite av Denis kolumner i VÄ, och jag tycker mig
minnas att en del av den reflekterade lite över Denis äventyr i den svenska
fandoms födelse. Denis dog ju härom året, och en del andra som var med i
början har väl även de börjat falla ifrån. Det är alltid en tid för
självreflektion när grundarna försvinner. Hur hamnade vi här? Håller vi
traditionerna levande? Vill vi? Vart är vi på väg?

En social sammanslutning som inte har traditioner att vårda, och känner sitt
förflutna kan lätt drabbas av generationsklyftor med åtföljande problem, tror
jag. Det finns alltid en fara om fansen blir äldre och äldre att det tillslut
inte finns någon fandom kvar. En del tycker detta inte är ett problem,
egentligen. Men, frågan om hur man får in nytt blod medan man samtidigt
håller kontrakterna med "det gamla gardet" upptar folks intresse, både inom
spelhobbyn och inom fandom.

Så, medveten om hur fenomenalt annorlunda populärkulturen skulle sett ut utan
Gary (t.ex. antagligen inget World of Warcraft), tänker jag på mina egna
kontakter med fandoms historia. Jag har ju träffat och pratat med Lars-Olov
Strandberg, som varit på i princip alla kongresser i Sverige sedan det
började, och jag har läst lite om Denis och de andras bedrifter. Fandom har
ju faktiskt påverkat världen en smula även den. En eller annan har ju som ung
läst sf och fanzines och bestämt sig för en karriär inom vetenskap.

Lite sorgsen känner jag mig allt, och kanske inte så sammanhängande som man
kunde önska. Dock kan man kanske både inom sf fandom och rollspelsfandom då
och då behöva en spark därbak och en påminnelse om vad det en gång var som
kickstartade folks fantasi. Om inte annat kan det ju sporra till nya
storverk.

Vila i frid, ni som gått före.
