+++
title = "Om någon nu missat den kommande järnhimlen (Iron Sky!)"
slug = "om_nagon_nu_missat_den_kommande_jarnhimlen_iron_sky"
date = 2008-06-27

[taxonomies]
forfattare = ["Torbjörn"]
kategorier = ["Nyheter"]
taggar = ["Star Trek", "film", "rymdkrig"]
+++

Jaaaa!

De galna finnar som tillverkade den charmiga StarTrek parodin StarWreck håller tydligen på att begå ytterligare en egensinnig produktion, utan hänsyn till ifall folk tänker ge dem pengar för det eller inte (är mitt intryck)

[Site med Teaser-trailer](http://www.ironsky.net/site/?p=16)

Tydligen körde de gamla pålitliga antagonisterna i nazityskland ihop en månbas precis innan det tredje riket föll samman - nu har de (i tystnaden på månens mörka sida) vuxit i styrka sedan dess, och tänker få resten av världen att förstå det fantastiska med vakuumtorkad knackwürst (får man anta), med hjälp av sina avancerade vapen och flygande tefat :) 

Jag kan inte förstå hur detta skulle kunna bli något annat än otroligt underhållande.. men det är klart.. jag tänkte likadant om den där filmen med nazi-zombies på en tropisk ö. Vi kan inte göra annat än hoppas..
