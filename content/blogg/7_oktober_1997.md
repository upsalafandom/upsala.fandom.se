+++
title = "7 oktober 1997"
slug = "7_oktober_1997"
date = 1997-10-07

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

I reptilens tecken förlöpte en del av tisdagskvällen på O'Connors krog i Uppsala då stadens science fiction-fans för tionde gången i år möttes på allmänt pubmöte.

Kruse och Janne hade fuskat och bänkat sig redan innan klockan blivit halv sju, och börjat äta och dricka. Jag hade ingen chans att hinna ikapp när jag kom, punktlig som en annan liten fyrkantig byrådirektör.

<!-- more -->

Vi började naturligtvis med att diskutera vår alldeles för snart infallande kongress (ett infall vi ofta drabbas av nuförtiden). Sven Christer hade hört av sig och meddelat att han trampat igenom en stegpinne i trädgården och skadat ryggen, så han har svårt att sitta still. I värsta fall får vi förse honom med sugrör för att han ska få i sig sin öl, bad han. No problems, svarade vi. (Sugrör och stark engelsk cider lär för övrigt enligt den brittiske fanen Mike Cheater vara ett oslagbart sätt att snabbt bli plakat på. Må det.)

Av någon outgrundlig anledning började vi sedan att tala om sköldpaddor, närmare bestämt bortsprungna sådana. Jag tror att det var Kristin som förde ämnet på tal, i det att hon berättade om helgens svampplockning då hon och Tobbe inte bara fått en hemul massa fingersvamp, utan dessutom plockat en sköldpadda som kravlade runt i skogen. Behöver någon verkligen fler bevis för att växthuseffekten är ett faktum?

Kruse berättade om en sköldpadda hon (?) hade haft som liten, som brukade springa bort med mycket jämna mellanrum och återlämnas mot hittelön. Sålunda brukade traktens ungar när de var godissugna titta förbi familjen Kruse och undra om sköldpaddan hade sprungit bort. Vi framförde här några konspirationsteorier, men de viftades bort. Karin var helt säker på att sköldpaddan kravlade ut i världen för egen maskin.

Leonard Borgzinner är död. Detta på intet sätt färska faktum sörjde vi över. Kanske en minnesutställning på nästa Upsalakongress? Det var några år sedan han dog nu, så en sådan skulle vara på tiden. Vem vet.

(Här kan jag inpassa att jag passade på att visa upp senaste SFF-sändingen för de närvarande som inte är medlemmar.)

Konditoripriser på tårtor drabbades av vår vrede, men Janne påstod att Walesbröd är hyggligt billigt. (För det var väl inte dyrt du sade?) Däremot lyckades han inte förklara vad Walesbröd är, men det var vare sig bara brith eller valnötsbröd, så det vet jag fortfarande inte.

Lennart muttrade spefullt nedsättande saker om den svenska kvällspressens neologismer. Spritfest, minsann. Hur många fester är spritfria?, undrade han med ett hånfullt leende.

Vi pratade också om att flytta tisdagsmötena till ett annat ställe, snarast till Murphys på Rackarbergsgatan (fd Rackis). Det har flera fördelar, främst kanske att det är billigare, har lägre bord och stolar med mer pubkänsla och saknar 22-årsgräns. Men vi sköt beslutet på framtiden och lovade varandra att flitigt titta in på Murphys för att fundera på vad vi tyckte. ;)

Mer öl, cider och Cola dracks och mer mat åts, men sedan drog vi oss faktiskt hemåt.

ex pulpetra
— _Johan_


Vi var där: Johan Anglemark, Joakim Bjelkås, Magnus Eriksson, Andreas Gustafsson, Karin Kruse, Jan Nyström, Matthias Palmér, Lennart Svensson, Sten Thaning, Kristin Thorrud, Björn Öqvist

Nästa möte blir på samma plats (O'Connors pub, ovanför Saffets på Stora torget) från 18.30 och framåt, tisdagen 4 november.

Väl mött!
