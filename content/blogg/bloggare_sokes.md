+++
title = "Bloggare sökes"
slug = "bloggare_sokes"
date = 2008-02-12

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Kåserier"]
taggar = ["meta"]
+++

Johan Anglemark kom med förslaget att några av oss skulle gå samman och tillsammans turas om att skriva något här varje dag.

Jag, Johan Anglemark, Anna och Andreas Davour samt Lennart Svensson har redan hoppat på. Vi är tillräckligt många för att sätta igång, men ett par skribenter till skulle inte göra något. Vi förväntar oss inga epos. Det går bra att skriva så mycket man är bekväm med. Vad gäller ämnesval är vi också flexibla. Tänk på vår gruppblogg som ett e-fanzine med högt i tak.

[Skriv till mig](mailto:bkhl@fandom.se) om du vill vara med.
