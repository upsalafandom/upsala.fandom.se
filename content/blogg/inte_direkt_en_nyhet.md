+++
title = "Inte direkt en nyhet"
slug = "inte_direkt_en_nyhet"
date = 2002-09-03

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

…för de flesta att Upsalafandom möts på en pub den första helgfria tisdagen i
månaden. Nu föll det på min lott att informera och dokumentera. Jag skriver
detta under viss begynnande tentastress, vilket får vara min ursäkt om det
inte blev särskilt skojigt och inspirerat. Håll till godo:

<!-- more -->

Plats: Williams

Tid: tidig kväll till sent, 3 september 2002

Jubileum! Jubel och klang! Denna dag var det på dagen 52 år sedan Max Born
skrev ett av sina brev till Albert Einstein. Jolkkonen läste vad Born i detta
brev sade om socialismens natur. Därpå fick vi även höra det roligaste skämt
Jolkkonen kan – den som vill höra det kan be honom dra det vid lämpligt
tillfälle.

Inte mindre roligt var det att Ante firade fem år i fandom – första tisdagen
i september 1997 kom han till O'Connors (där vi då höll till) och bar sedan
undertecknads resväska kring hela Upsala tills morgonen kom. Detta kan man
läsa om annorstädes. Detta firades grundligt, med resväska och allt.

Att lista alla närvarande glömde vi alldeles bort i allt glam och galej, men
jag kan försöka dra mig till minnes vilka jag träffade på.

Klaus Zeuge var där, av en planerad tillfällighet. Adam kom förbi, men sa
mest hej och gick igen. Han är en av klingonisterna och en av dem i
bekantskapskretsen som inte egentligen får vistas på pub. Synd för oss.

Fyra protoneos dök upp som ett resultat av vår lilla reklamkampanj vid
universitetets reccemottagning: August, Magnus, Dennis och Jonatan. Jag
försökte ge dem gamla nummer av Folk och fans i landet Annien, med klent
resultat. "Men alltså… vad handlar det om?" Försiktigt frågade jag om August
var påläst om fandom, för det finns ju neos som vid sin första IRL-kontakt
med fandom redan läst fanspalter i JVM, fanslangordlistor på nätet och
fanhistoriska redogörelser av alla möjliga slag. Det kan ju göra en lite
ställd när man märker att neos vet mer än man själv, men ingen av det slaget
verkar ha funnits bland dem som dykt upp nu.

Jonatan sysslar med teater och efterlyste science fiction-pjäser. Vi kunde
bara minnas två, varav den ena är "Spökflyg över Fyrisstaden", Ollies och min
fina hyllning till raketäventyren från förr. Den är av lite intern karaktär,
och kanske inte Stor Konst direkt, så vi rekommenderade den inte.

Ollie dök upp, tyvärr utan värja att dramatiskt spetsa fanzines på (detta har
han gjort förut, bildbevis finns på webben). Däremot diskuterade vi citat ur
_Egen rymddräkt finnes_ och försökte minnas namnet på Kips räknesticka.
(Detta var alltså innan Ahrvids lilla analys av boken dök upp på
sverifandomlistan.) Märket kunde vi inte dra oss till minnes, men att det var
en 20-tums log-log duplex
decitrig var vi ense om.

Kakan var där, men inte Luna. Maria var där, men inte Samuel. Jag är inte
riktigt säker på vad jag anser att ordet "tråkböp" har för nyans, men jag
provade att använda det om Samuel. Det lät inte riktigt rätt, men föranledde
en redogörelse för en kvalitetsskala för vad som räknades som bangning i
Upsalafandom på åttiotalet. Sedan upptäckte vi att det var en Magnus Eriksson
för lite i lokalen. Han fattas oss!

Få se nu, vilka fler var ändå på plats… Johan Anglemark, Björn Lindström,
Björn Öqvist. Marie Engfors kom sent, efter att ha skrivit en tenta i
Stockholm. Joacim visade sig också. Therese och Karl-Johan likaså. Sen var
det kanske inte fler. Verkar det rimligt? 18 personer verkar det bli när jag
kontrollräknar. Det var nog i stort sett alla, jag ber om ursäkt ifall jag
har missat någon – skrik mer nästa gång, så att jag lägger märke till dig!

Framträdande samtalsämnen under kvällen förutom dem som redan nämnts var
rollspel, Michael Moorcock, den kosmologiska principens observationella
underbyggnad, politik i praktiken och Vheckans Ävfentyr.

Eftersom alla numera är så trötta (Björn X hade en så fånig ursäkt som att
han varit vaken i nästan 30 timmar), ordentliga (Joacim menade att han skulle
upp nästa morgon) eller avlägset bosatta (i Storvreta, Ramsta eller Kista
till exempel) så var vi bara fyra tappra som drog vidare till Orvars krog.
Ante med resväskan i högsta hugg visade vägen, medan dr Jolkkonen försökte
sätta fart på Ollies och mina hjärnceller med ett resonemang om entropi. På
Orvar muterade samtalet flera gånger och vi fick bland annat veta lite mer om
Thåströms karriär. Alltför snart var alltsammans över, krogen stängde och vi
gick hem, trots allt. Jag kan glädja alla som bjöd Ante på dryck under
kvällen med att han var på mycket gott humör och förklarade många stora och
viktiga saker på vägen hem.
