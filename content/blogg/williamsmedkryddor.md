+++
title = "Williams med kryddor"
slug = "williamsmedkryddor"
date = 2006-03-07

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Det var en hygglig mängd folk på plats när jag kom, lite senare. Stockholm var representerat och alla sorter av Ufandom.

Jag och Mikael gladdes åt att Janne har avancerat i karriären och blivit näste chef för avdelningen. Vi och Simon suckade över att vissa får resa i jobbet så de blir less och andra inte alls. En forskningsingenjör, en arbetslös och en taxichaffis...

...och så Bellman.

<!-- more -->

August kom och berättade att han kommer att flytta till Södertälje, men hoppas återkomma. Sedan snackade vi hur distansförhållanden kan fungera och inte.

Tobjörn, Henrik och Trulsson och jag pratade musik och jag förklarade hip-hop som entartete kunst, end of discussion.

Maten var inte så tokig. Jag åt en rogan josh med lamm och även om de inte valt de bästa bitarna av lammstackarn så var det bra kryddor. Nästa projekt är en korma och en vindaloo.

Hof är bra efter indisk mat.

Sten, Jesper, Torbjörn, Henrik och jag pratade om märkliga och exotiska fynd med google.

Visste ni att en kinakrog kan heta Look ho Fuk?

"South Park: Bigger, Longer and Uncut" är en ganska bra filmtitel och bolaget hade redan stoppat en som de tyckte var oanständig. Tji fick dom.

Det är snart kongress. Imagicon. Be there or be a fyrkant. Ufandom hårt representerad. Conan med i programmet. Bra sak.

Inget är som ett Williamsmöte, utom möjligen ett Orvarmöte. Nästa gång faktiskt på Orvar.

Vi ses kanske där.

Hårdrock.
