+++
title = "Tillbakablickar"
slug = "tillbakablickar"
date = 2008-07-22

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Kåserier"]
taggar = ["historia", "pubmöten"]
+++

När vi startade den här bloggen lade jag ner viss möda på att lägga in alla
våra [arkiverade gamla pubmötesrapporter](/taggar/pubmoten). I
processen var det svårt att undvika att läsa någon rad här och där.

Eftersom nostalgi säljer tänkte jag här publicera några väl valda citat.

<!-- more -->

Barndomsminnen verkar ha varit ett populärt samtalsämne på våra möten. Exempelvis:

> …historien om hur Anna berättade för sin lillasyster om hur allt är gjort av
> atomer, varpå systern sprang till fadern och sade att hennes lögnaktiga
> syster påstod att Fantomen har gjort allt. Pikant pinsamt att bli anklagad
> för denna lögn var det eftersom Annas uppfattning om vad Fantomen var, var
> ganska vag. ([1997-02-04](./blogg/4_februari_1997.md))

> Kruse berättade om en sköldpadda hon (?) hade haft som liten, som brukade
> springa bort med mycket jämna mellanrum och återlämnas mot hittelön. Sålunda
> brukade traktens ungar när de var godissugna titta förbi familjen Kruse och
> undra om sköldpaddan hade sprungit bort. Vi framförde här några
> konspirationsteorier, men de viftades bort. Karin var helt säker på att
> sköldpaddan kravlade ut i världen för egen maskin.
> ([1997-10-07](./blogg/7_oktober_1997.md))

Teknik har alltid varit ett populärt ämne. En period vill jag minnas att de
mer datorintresserade i församlingen brukade bli förvisade till ett särskilt
bord, för att ge andra samtalsämnen en sportslig chans.

Även tekniska ämnen utan direkt koppling till datorer har förekommit:

> Jag berättade för Andreas att det inte är några större problem att få tag på
> TV och videobandspelare i Sverige som klarar signalstandarden NTSC. Det fick
> honom att börja grunna på om han skulle ta kontakt med den vilt främmande
> amerikanska som skrivit till honom för att utbyta amatörporrfilmer. Jag lovar
> att bevaka ärendet och låta er veta hur det går.
> ([1997-11-04](./blogg/4_november_1997.md))

Dolt i arkiven finns också beskrivningar av epokgörande händelser. Vad sägs
till exempel om det här?

> Det första stället vi provade var Williams i Ubbohuset, bredvid
> Universitetshuset (och inte långt från Jontes stuga, där Trøstcon och
> Verdenskongressen 1990 ägde rum). Det befanns vara tämligen tomt, ha sjyssta
> priser, ett gott sortiment öhl samt ett mycket smalt utbud käk till vettigt
> pris (och en indisk restaurang i rummet bredvid). Den lite väl bullrande
> jukeboxen konstaterades ha god musik, samt ett volymreglage som barpersonalen
> gärna skruvade ner lite när man frågade dem. Vi beslöt på stående fot att slå
> oss ner, och efter första rundan öhl att flytta mötena hit.
> ([1998-11-03](./blogg/3_november_1998.md))

Det är skönt att se att mötena också uppfyllt sitt huvudsyfte, nämligen
diskussioner om raketer och dylikt. Detta från det första pubmöte jag deltog
i, om närvarolistorna är att lita på:

> Exempelvis konstaterade Jolkkonen att alla raketer han under sitt stormiga
> liv har skickat iväg har exploderat efter någon futtig sekund i luften. En
> överlägsen Olle konstaterade att han minsann hade flera lyckade
> uppskjutningar bakom sig. ([2000-02-01](./blogg/1_februari_2000.md))
