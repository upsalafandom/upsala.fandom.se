+++
title = "Inte i Limerick"
slug = "inte_i_limerick"
date = 2008-09-03

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["Göstapriset", "Williams", "pubmöten"]
+++

Igår var det terminsstart för Upsalafandoms månadsmöten. Nå, mötena har
hållits varje månad utan undantag sedan 1 januari 1997, så några särskilda
terminer har vi knappast, men det lokala universitetet har kört igång och en
färsk filosofie student vid namn Erik från Vagnhärad hade sökt sig till
vitterhetens källa för att insupa visdom runt borden på Williams. Evelina som
gjorde ett nedslag på ett möte för fyra år sedan var också där, samt Bertil
och Otto - nykomlingar från förra mötet. Otto, som är den ende fan jag vet
som raggats till fandom av en taxichaufför i tjänst. Dessutom var ett urval
av the usual suspects närvarande; Lennart K, Gunnar, Lena, Johan J, Jesper,
Gabriel, Lennart J, Jolkkonen, August och Carl-Henrik. Har jag glömt någon
beklagar jag, jag förde inga anteckningar utan detta minne är silat genom - i
tur och ordning - en otillräckligt salt lassi, en Gem, en Hell, en Westmalle
Tripel samt en Tripel Karmeliet.

Däremot ingen Oscar eller Stina! Aufröhrend. Vi trodde att vi skulle
infiltreras och tas över i publikt försåt av Linköpingsfandom, men denna
invasion verkar utebli. Kanske var det etiketten _pubmöte_ som förvirrade;
LSFF:s bokmöten handlar ju inte om böcker så Oscar och Stina kanske inte
förstod att pubmötet skulle hållas på puben?

En fraktion av bordet diskuterade med pannor lagda i djupa veck den planet de
befann sig på, och de krigiska och primitiva grannar de försökte hålla sig
undan i väntan på att deras upptäcktsresande och uppfinnare skulle göra de
framsteg som krävs för världsherravälde och avancerad civilisation. Planeten
företer tydligen drag som för tankarna till Farmers flodvärld, och som av en
slump diskuterades Farmer - helt utan samband med detta - senare vid en annan
del av bordet, fast där var det mer hans Tarzan- och Doc Savage-pastischer
som var ämnet.

Williams nyheter var ett annat samtalsämne. En av servitriserna var helt ny
för kvällen (hoppas jag!) och behärskade varken sortimentet eller svenska
särskilt bra. Lite farsartat när jag försökte beställa ölen med gul etikett i
mitten på ölkylen när hon varken förstod fraserna "gul etikett" eller "i
mitten", men efter att hon i tur och ordning med frågande min hade pekat på
hälften av ölflaskorna, med början längst ner till höger, kom hon ju faktiskt
fram till den gula i mitten, så det var bara lite tålamod som krävdes.

De har också börjat med mat i bufféform. Maten är några av deras vanliga
rätter, och hade man kommit strax efter att de öppnade klockan fem hade den
säkert varit god. Som det var nu tror jag att jag kommer att återgå till à la
carte.

Tre Kontextarrangörer pratade om planeringen framöver, och avlade högtidliga
löften om att sätta sprutt på arbetet nu, med 58 dagar kvar. Otto tipsade om
YouTubefilmer med inspelningar från finlandssvensk lokal-TV:s sändningar från
älgjakten med mera. _Sjukt lokal lokal-TV_. Måste kollas upp.

Annars var det det vanliga sladdret om Star Trek, Orhan Pamuk, alkohol,
hurritisk meningsbyggnad, essäer om skiffy och annat som förgyller
Upsalafanens pubtimmar.

Till sist en uppmaning: På nästa möte, oktobermötet, är det dags för första
gången på fem år att utse en
[Göstapristagare](http://sfweb.dang.se/a/gosta/gostapriset.html)! Slut upp
och rösta fram en värdig vinnare!
