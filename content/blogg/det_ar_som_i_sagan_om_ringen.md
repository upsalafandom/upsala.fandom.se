+++
title = "Det är som i Sagan om ringen..."
slug = "det_ar_som_i_sagan_om_ringen"
date = 2009-03-04

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Länkar"]
taggar = ["Oscar Danielson"]
+++

"Har ni märkt att de mörka krafterna har börjat flyttat fram sina positioner igen? Det är som i Sagan om ringen", inleder författaren och musikern Oscar Danielson [dagens inlägg](http://www.oscardanielson.se/?p=694) på sin blogg, om mötet med mörkrets krafter som på E18 försöker att hindra honom från att köra om.

Jag ville bara dra en lans för [Danielsons blogg](http://www.oscardanielson.se), som ofta innehåller roliga små pärlor till kåserande betraktelser. Jag borde nog köpa en av hans böcker någon gång...

Notera också att Stockholmsfanen med Sheriffstjärnan, han som ramlade i ett bad med celluoid som liten, har startat en filmblogg: [Kinematografi](http://moviehead.wordpress.com).
