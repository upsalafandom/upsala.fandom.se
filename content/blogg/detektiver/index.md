+++
title = "Detektiver!"
slug = "detektiver"
date = 2008-04-27

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["böcker"]
+++

Den här veckan har både jag och maken kommit hem från biblioteket med spontana fynd av deckarhistorier med fantastiskt anslag.

Jag hittade [The Metatemporal
Detective](http://www.pyrsf.com/MetatemporalDetective.html) på bibliotekets
hylla med nya böcker. Jag hajade till och tog en extra titt eftersom jag inte
förväntat mig att se Michael Moorcock på _Mystery_-avdelningen, och sen blev
jag så nyfiken att jag bestämde mig för att ta med den hem och ta en närmare
titt. Moorcock kan ju vara lite
[svårtillgänglig](http://sfweb.dang.se/h/blog/arkiv/2005_09_01_archive.html)
ibland, men den här boken verkade hålla en ganska rak stil.

![Omslag till The Metatemporal Detective](the_metatemporal_detective.jpeg)

Nu har jag läst de två första berättelserna, och det är onekligen de gamla vanliga figurerna från Moorcocks Multiversum, fast i gammaldags deckarberättelsestil. Ganska trevligt, fast jag är inte säker på att jag tycker att det är mer än pastisch kombinerat med omtuggande av samma saker som de här personerna alltid gör.

Ante släpade hem [Shadows over Baker Street](https://en.wikipedia.org/wiki/Shadows_Over_Baker_Street), som är en antologi där olika kända författare har skrivit Sherlock Homes-berättelser i lovecraftiansk miljö. Jag som aldrig begripit mig på Lovecraft har lite svårt att förstå grejen, men konceptet verkar ändå lite kul på något sätt. Och omslagsbilden är lite cool.

![Omslag till Shadows Over Baker Street](shadows_over_baker_street.jpeg)
