+++
title = "People for the Ethical Treatment of SF"
slug = "people_for_the_ethical_treatment_of_sf"
date = 2009-06-05

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Länkar"]
taggar = ["Andy Remic", "Felix Gilman", "Jeff VanderMeer", "Science Fiction and Fantasy Ethics"]
+++

Jeff VanderMeer uppmärksammar på sin blogg, Ecstatic Days, en bisarr
författarsammanslutning som kallar sig [Science Fiction and Fantasy
Ethics](http://sffethics.wordpress.com), ledd av Andy Remic, och som har som
syfte att "slå ett slag för positiva recensioner av böcker, filmer och
tecknade serier". VanderMeer intervjuar sitt alter ego Evil Monkey, som
uppger sig tillhöra [People for the Ethical Treatment of
SF](http://www.jeffvandermeer.com/2009/06/04/evil-monkey-and-people-for-the-ethical-treatment-of-sf-petsf).
Den stora behållningen återfinns, som så ofta, i kommentarerna där
författaren Felix Gilman drar en växel på temat att behandla böcker väl och
skissar upp hur illa han själv behandlar böcker:

> on my desk right here i have a signed first edition of m. john harrison’s
> _The Centauri Device_
>
> i keep it in a 6-by-6 inch cage in which it cannot stand up or turn around,
> and i force-feed it ruthlessly in hopes of fattening its liver
>
> what are you going to do about it, huh, Remic? nothing, that’s what
>
> …
>
> if i want to take a bunch of _Solaris_ DVDs and drown em in a sack
> that’s my business, don’t meddle in things you don’t understand, Remic
>
> …
>
> since back in old country my people have gathered to watch two proud strong
> issues of _Asimov’s_ fight in a pit. it is an honorable death for the
> magazine, for _Asimov’s_ is strong and loves to fight. these are our
> folkways, Remic. who are you to interfere? how dare you
>
> …
>
> you know what the most beautiful thing in the whole damn world is? i’ll tell
> you what it is Remic, i’ll tell you good. its when a man and his son drive up
> into the mountains with a couple good huntin guns and maybe some beers, maybe
> the boy has a beer too why not, his mother dont need to know. you dont even
> need to talk much just enjoy all that silence. just wait real quiet until you
> see one of them _Star Wars_ prequels come shamblin on by. let the boy
> take the first shot. thirty aught six’ll drop that big hairy prequel right in
> its tracks, it aint cruel, it don’t feel no pain. the boy’s a man today. you
> can teach him how to cut off that prequel’s antlers and how to take its hide
> and how to get rid of them ugly hayden christianseny bits that ain’t no good
> to nobody. just like your daddy done for you with _A New Hope_, and
> his daddy done for him with _War Of The Worlds_. you’ll never
> understand what that means Remic and thats why i pity you
