+++
title = "Tirsdag"
slug = "tirsdag"
date = 2008-07-02

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Rapporter"]
taggar = ["Williams", "pubmöten", "tisdag"]
+++

Det var Tisdag, och som det lätt blir på Tisdagar samlades diverse löst folk
på Williams för att umgås. Ett gäng [catahyaner](https://catahya.net/)
(Therese, Christian, Mats, Cecilia och Peter) passade på att lysa upp
tillställningen med sin närvaro, och någonstans runt två dussin personer bör
ha närvarat under kvällen.

<!-- more -->

Det debatterades huruvida Hitler hade hållit sina vallöften eller ej, och om
han var mer eller mindre pålitlig än Gustav III. Vi konstaterade att det
skulle vara en Bra Sak att av rent estetiska skäl förflytta någon miljon
japaner till nordvästra, alternativt nordöstra, Afrika, och att man kanske
borde starta en _Japan åt ainu!_-kampanj för den goda sakens skull, och
Mattie berättade om sina politiska projekt och sin kamp för att lära sig att
böja lillfingret bakåt. Johan Anglemark lovade att köpa en kyrka och bli
galen om vi gav honom tio procent av våra inkomster, vilket med tanke på
_vilka_ det var som lovade det – som undertecknad – förmodligen var ett
ganska dåligt ekonomiskt beslut. Lennart Svensson delade ut _Eld och rörelse_
(ur vilken Peter sedan på hemväg glatt citerade slumpade meningar i en kvart
medan jag och Christian försökte få honom att antingen _läsa_ boken eller gå
och lägga sig) medan Lennart Jansson som vanligt hade underhållande
historiska anekdoter på lager.

August meddelade den glada nyheten om sitt nya stipendium, men ville trots
detta inte hälla Carlsberg på instiftarens grav utan lyckades av ren
olyckshändelse istället fälla årets, eller varför inte hela vår samtids,
sämsta raggningsreplik. Vi besvarade glatt hans frågor om vad Catahya är, hur
det fungerar och hur det uttalas, vilket kanske lämnade honom lika okunnig
men åtminstone något mer förvirrad än tidigare, och jag fick nämna mina
storslagna planer på att skriva Catahyas historia i den enda form som
verkligen skulle lämpa sig, nämligen krigskrönikan.

En bra kväll, på det stora hela.
