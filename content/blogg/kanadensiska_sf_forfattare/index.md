+++
title = "Kanadensiska sf-författare"
slug = "kanadensiska_sf_forfattare"
date = 2008-04-24

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["Kanada", "böcker", "författare"]
+++

**SPOILERVARNING**

I detta inlägg nämns en mindre detalj från en bok, en detalj som av somliga
kan anses av betydelse för handlingen (även om jag själv inte tycker att det
har någon som helst betydelse, och troligen skulle ha blivit mer intresserad
av boken om någon sagt det till mig innan jag läste den). Jag upprepar:
**SPOILERVARNING**

Ett av mina små projekt sedan jag flyttade till Kanada har varit att bekanta
mig med kanadensiska sf-författare.

[Rober Sawyer](http://www.sfwriter.com) verkar vara det största namnet, i
alla fall om man räknar i allmän synlighet: han nomineras till Hugo-priset då
och då, är aktiv på nätet och dyker ofta upp på kongresser. Han verkar sälja
rätt bra också. Jag känner att jag borde knyta ihop mina intryck av
_Hominids_, som jag började skriva om [efter att ha läst första
kapitlet](./blogg/forsta_kapitlet_av_hominids.md).

Jag håller delvis med Tommy, som skrev att han tyckte att personskildringen
var dålig, men det var inget stort problem för mig. Det kanske blir träigare
om man läser uppföljaren, men i den här boken är folks personligheter inte
särskilt framträdande eller betydelsefulla. Det enda som störde mig lite var,
som jag sade från början, det gubbsjuka porträttet av den vackra unga
forskaren — men det visade sig faktiskt att hon bara hade en blek biroll i
boken, så det gjorde inte så mycket. Tyvärr tyckte jag inte handlingen var så
intressant. Visst, en neandertalare kommer till vår värld från ett parallellt
universum där de blivit dominerande och homo sapiens har försvunnit. Det ger
möjlighet att betrakta mänskligheten lite utifrån och diskutera hur det
skulle ha kunnat vara istället, men jag tycker inte alls att det gav något
nytt eller var särskilt intressant. Beskrivningen av neandertalarnas värld
skulle kunna vara från någon Le Guin-bok, fast om hon hade gjort det hade det
varit roligare skrivet. Fånigast är när en av huvudpersonerna tittar på en
stol och reflekterar över att den som tillverkat den förmodligen känner att
hon bidragit till samhället. Hur som helst är handlingen mycket enkel: Ponter
Boddit kommer till vår värld av en olyckshändelse, lär sig lite om oss och
blir sorgsen över hur vi förstör jorden (ja, jag sade ju att det inte var
särskilt originellt), och lyckas ta sig hem igen.

Lite kul var det i alla fall att det var en kvantdator som öppnade vägen
mellan världarna, precis som i _Brasyl_. Jag får lust att skriva en artikel
om kvantdatorer och multiversum, för att bena ut begreppen. Vi får väl se om
jag kommer åt att göra det någon dag.

![FIXME](futuristic_tales.jpeg)

Övriga kanadensiska författare jag provat på under tiden här är [Guy Gavriel
Kay](http://www.brightweavings.com) (de två första Fionavar-böckerna),
[Claude Lalumière](http://lostpages.net) (novellen "This Is the Ice Age"),
och [Cory Doctorow](http://craphound.com) om han räknas ("When Sysadmins
Ruled the Earth" och ett par av serietidningarna med _Futuristic Tales of the
Here and Now_). Kanske har jag missat någon författare, men dessa är de jag
minns just nu. Nyligen har jag också lånat en bok av [Charles de
Lint](http://www.sfsite.com/charlesdelint), och [Tanya
Huff](http://tanyahuff.net) bor visst strax utanför Kingston så henne får jag
väl lov att läsa något av så småningom. Jag har också skaffat [senaste
Tesseracts](http://www.edgewebsite.com/books/tess11/t11-sample.html), en
antologi som verkar ganska trevlig.

Frågan är: spelar det någon roll varifrån en författare är? Ibland, ibland inte. Det beror väl på vad man skriver om. Och apropå det är det lika märkligt varje gång jag ser Åsa Larsson-böcker i bokhandeln här!
