+++
title = "En tidsmaskinsresa till Ryssland"
slug = "en_tidsmaskinsresa_till_ryssland"
date = 2009-04-20

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Ryssland", "foto", "tidsmaskiner"]
+++

Ibland kan man ju i sf hitta historier som handlar om någon som reser i en tidsmaskin eller nåt liknande och får uppleva forna tider. Jag hittade en gång en väldigt charmig länk till en tidsmaskin och letade jag fram den igen. Kolla [det här](http://www.loc.gov/exhibits/empire)!

Det är färgfoton från Ryssland, så som det såg ut innan revolutionen! Det är nog så nära en tidsmaskin man kan komma. Fascinerande.

