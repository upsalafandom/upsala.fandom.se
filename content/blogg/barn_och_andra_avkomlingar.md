+++
title = "Barn och andra avkomlingar"
slug = "barn_och_andra_avkomlingar"
date = 2009-05-07

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["fandom"]
+++

Jag ville så gärna att det skulle bli en fungerande fangrupp här i Kingston. Jag ville lära dem vad fandom är, jag ville samla folk och få dem att träffas och ha kul tillsammans över ett gemensamt intresse. Jag vill att det ska bli roligt, som i Upsala!

Tyvärr är folk kompakta tråkmånsar -- eller snarare är de hårt fast i sina vanliga mönster. Studenter umgås med studenter. Rollspelare träffar bara rollspelare. Folk som tror att de vet något om fandom för att de varit på några stora seriekongresser dyker om man pratar om Neil Gaiman men inte annars.

Våra möten har varit högst ojämna. Jag lyckas dra dit några en gång och sedan dyker de inte upp igen. Kärnan består av mig och två till, mötena brukar vara ungefär en timme och handla till hälften om någons tandläkarbesök.

Nu låter jag lite bitter. Jag känner mig inte särskilt bitter, för det är inte som om jag har särskilt mycket personligt investerat i det här gänget.  Jag tycker bara att det är synd. Folk är trevliga och roliga var för sig, men kommer aldrig riktigt ihop till ett gäng. På något sätt har jag misslyckats med att förmedla för folk vad det här skulle kunna vara. Jag tror inte att gruppen kommer att överleva att jag försvinner, inte om inget drastiskt händer de  tre närmaste månaderna. Fast jag hoppas litegrann på världskongressen, och på den inspirationsinjektion en sådan kan bli i områdena omkring.

Livet går vidare i alla fall. Och grodden i min mage är kanske viktigare än de orädda fantasy-fantasternas gäng, även om det också var ett projekt som skulle kunna vara värt status som ett av mina skötebarn.

Och jag kommer tillbaka till Upsala, där det finns en fandom som lever och frodas utan mig. Det är inte så dumt. 

Men först blir det som sagt världskongress! (Och innan dess bebis. Och kanske en artikel i Nuclear Instrumentation and Measurement om allt faller på plats, men det är en helt annan historia.)
