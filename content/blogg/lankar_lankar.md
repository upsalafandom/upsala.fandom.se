+++
title = "Länkar, länkar!"
slug = "lankar_lankar"
date = 2008-03-02

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Länkar"]
taggar = []
+++

Till förmån för den som egentligare har viktigare saker för sig, och behöver hjälp med att skjuta upp det en liten stund till, tänker jag dela med mig av länkar till en del saker jag har hittat på nätet på sistone.

* [Science  fiction stories with good astronomy and physics: a topical index](http://srb.npaci.edu/cgi-bin/nsdl.cgi?uid=/2006-01-31T18:08:47Z/606C9B327F2E88BB2DE68552EE5C8395/scifi.html). Det är inte en uttömmande lista, men en utgångspunkt om man är intresserad av berättelser som utforskar något visst fenomen eller så. Där finns två böcker som tar upp mörk materia, som jag inte har läst (ännu).
* [Centauri Dreams](http://www.centauri-dreams.org) är en blogg från något som heter Tau Zero Foundation, med nyheter och analyser av forskning och idéer relaterade till realistiska möjligheter att resa till andra stjärnor. Kanske intressant även för den som inte primärt är ute efter interstellära utflykter.
* [En intressant diskussion om Ted Chiangs novell "Story of Your Life"](http://scienceblogs.com/principles/2008/02/notes_toward_a_discussion_of_s.php), som förberedelse för ett gästseminarium på en sf-kurs. [Här är uppföljningsdiskussionen efteråt](http://scienceblogs.com/principles/2008/03/story_of_your_life_guest_lectu.php#c771056)
* 14 mars är [Talk Like a Physicist Day](http://talklikeaphysicist.com). Det blir förstås inte så intressant om det bara är fysiker som pratar som fysiker -- kom igen och var med! (Fysiker är roligare än [pirater](http://www.talklikeapirate.com), i alla fall ibland.)

Jag kan också meddela att jag [registrerade mig för att få Tor
Books](http://www.tor.com) nyhetsbrev. Nu har fått två coola e-böcker gratis,
_Old Man's War_ och _Spin_ -- återstår att se om jag kommer mig för
att läsa dem på något sätt. Dessutom laddat ner omslaget till _Escarpment_ av
Jay Lake (som kommer ut senare i år, se mina kommentarer om del ett,
_Mainspring_
[här](http://annien.wordpress.com/2008/01/14/mainspring-av-jay-lake)) att ha
som skrivbordsbakgrund på min jobbdator. Mer luftskepp lyser upp i vardagen!
Det var allt för mig för den här gången. Nu försvinner jag norrut ett tag,
till [jordens innandöme](http://www.snolab.ca).
