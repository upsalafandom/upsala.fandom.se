+++
title = "3 februari 1998"
slug = "3_februari_1998"
date = 1998-02-03

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Vit rioja smakar burkchampinjon slog Karin Kruse fast tisdagen den 3 februari då Upsalafandom slog rekord genom att belasta golvet på O'Connors pub med hela 19 varma kroppar.

Rekordet på 14 deltagare från förra månadens möte slogs med över 35 %! Fortsätter det på det här sättet måste vi fundera på ett rymligare och mindre populärt ställe att samlas på. Vi hade kunnat bli åtminstone fyra till igår om några uteblivna stamgäster samt en kompis som hade sagt att han skulle dyka upp (men aldrig gjorde det) hade visat sina nunor.

<!-- more -->

Skrävlande skrymtare skulle nu börja klia sig i skägget och mumla om succé för de återupplivade Orvarmötena, men eder korrespondent saknar såväl skägg som en skrymtande läggning och nöjer sig därför med att rapportera avskalade fakta sådana som de skulle se ut om vi drogs inför domstol och sanningen uppdagades.

Kvällens överraskning var Anna, som gjorde ett gästspel från Göteborg. Hon lovade att göra fler, och det ser vi fram emot. Flera av de närvarande fansen hade rapporter att avlägga. Kruse rapporterade som jag redan skrivit att vit rioja smakar burkchampinjon. Kristin rapporterade att Johan Schimanski trivs bra i Tromsø. Jag själv förteg att jag ännu inte skickat honom den skjorta han glömde hemma hos mig när han sov över under Konfekt i fjol. Woe is me. Mika Tenhovaara fyllde 35 år samma dag, berättade Tony, och tillade att Hans-Eric Hellberg hade besökt O'Connors kvällen innan och kanske till och med beträtt just de golvplankor vi satt ovanför!

Någon framkastade den absurda idén att ha en fannisk olympiad på nästa kongress, med ölhävning, frågesport, stencilerande och Rhoscoe vet vad, men det skriver jag inte här, för då kanske någon annan snor idén. (Nej, inte skulle ni väl göra det?) I ljuset av anklagelserna mot Arthur C Clarke och de dunkla krafter som kan tänkas ligga bakom dessa insåg vi vad hela den lankesiska konflikten handlar om. Tamiler är ett indiskt namn på pedofiler och lankeserna är alla småpojkar. Att vi inte har insett det förut!

Vi bubblade om forma tiders pubmöten, men för att förnya oss en smula utbringade vi skålar för såväl Bertil Ohlin som Karl Staaff. En tanke gick ut till alla folkpartister och kryptofolkpartister därute i buskarna. Olle avslöjade att i Gävle hackar alla teckenspråkstalare lök när de säger var de bor, och lyckades av någon anledning koppla samman detta med sina kamrater som planerar att sätta upp en opera baserad på Shelleys Frankensteins monster i Anatomiska teatern i Upsala. Eller så var kopplingen min, orsakad av den chock och förvirring jag drabbades av när Andreas beställde in en flaska Tennent's. När han smakade på drycken drabbades han av en liknande chock. Nästa flaska blev Bishop's Finger, vill jag minnas.

Ett slag slogs för skydd av de utrotningshotade sämskhonorna, och Matthias delade ut Nomicregler för vårt e-post-Nomic som vi snart ska påbörja.

Om någon har en åsikt om lämpligheten av att kalla vår nästa kongress för Supernova eller Världsutställningen kan de höra av sig till någon lämplig person, men annars får ni hålla till godo med denna lätt lakoniska rapport från tisdagens möte. Och om ni undrar så diskuterades det en hel del sf också, fast inte så mycket där just jag satt. Eller så har jag glömt det.

ex pulpetra
— _Johan_

Vi var där: Johan Anglemark, Tony Elgenstierna, Magnus Eriksson, Andreas Gustafsson, Linnéa Jonsson, Karin Kruse, Alexander Larsson, Daniel Luna, Björn Nilsson, Jan Nyström, Matthias Palmér, Lisa Sjöblom, Lennart Svensson, Sten Thaning, Kristin Thorrud, Anna Åkesson, Sam Åslund, Olle Östlund, Björn X Öqvist.

Nästa möte blir på samma plats (O'Connors pub, ovanför Saffets på Stora torget) från 18.30 och framåt, tisdagen 3 mars.

Väl mött!
