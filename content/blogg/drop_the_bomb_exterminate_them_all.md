+++
title = "Drop the bomb, exterminate them all"
slug = "drop_the_bomb_exterminate_them_all"
date = 2008-06-19

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["fascism", "totalitarianism"]
+++

Så skedde det tillslut att sveriges riksdag röstade igenom förslaget som ger FRA i uppdrag att spionera på medborgarna. Från och med denna dag ändras svensk rättspraxis till att du är förmodad skyldig tills motsatsen bevisats. Det är nog den mörkaste dagen i svensk historia.

Jag kommer aldrig mer ha nåt förtroende för politiker. I mitt medvetande ekar några ord sjungna av Thåström "Vi ska beväpna oss".

I sf böckerna är det så enkelt i dessa situationer. Man skapar en liten grupp av Heinleinskt dugliga som ställer saker till rätta. Nu önskar jag att man kunde leva i en sådan roman.

Vart ska man flytta nu, där de har ett öppet samhälle? Ryssland? Kina? Jag är så deprimerad att jag bara vill ge upp. Som Kurtz sa, "drop the bomb, exterminate them all".
