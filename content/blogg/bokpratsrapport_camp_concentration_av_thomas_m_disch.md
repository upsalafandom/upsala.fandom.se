+++
title = "Bokpratsrapport: Camp Concentration av Thomas M. Disch"
slug = "bokpratsrapport_camp_concentration_av_thomas_m_disch"
date = 2010-02-16

[taxonomies]
forfattare = ["Jesper"]
kategorier = ["Rapporter"]
taggar = ["Camp Concentration", "Thomas M. Disch", "bokprat", "science fiction"]
+++

I onsdags träffades vi hemma hos Björn Lindström för att diskutera [Camp
Concentration](https://en.wikipedia.org/wiki/Camp_Concentration) av Thomas M.
Disch. Närvarande var jag, Björn och George Berger. Jag hade precis läst ut
boken och Björn likaså, men han hävdade att han varit för trött när han läste
den och hade därför inte riktigt kunnat ta sig till den. George hade läst den
för många år sedan, men tyckte ändå att det var en så bra bok att den kunde
vara värd att diskutera. Eftersom jag då var den med boken bäst i minnet
inledde jag mötet med att kort sammanfatta dess handling (något som borde bli
en vana på framtida bokprat, då det är ett utmärkt sätt att få igång lite
diskussion).

_Camp Concentration_ handlar om Louis Sacchetti, en poet som hamnat i
fängelse pga vapenvägran i ett framtida, lätt dystopiskt USA (boken gavs ut
1969, men handlingen äger troligtvis rum någon gång på 70-talet). Han blir
mot sin vilja överflyttad från fängelset till en hemlig, underjordisk
forskningsenhet där man utför experiment på "frivilliga" fångar. Experimenten
går ut på att fångarna blir injecerade med något som får deras intelligens
att skjuta i höjden, men som även leder till döden inom nio månader.
Sacchetti har blivit ditskickad för att observera och beskriva de övriga
fångarna och hur de reagerar på sina nya förmågor. Fångarna leds av den
karismatiske Mordecai, och ivrigt påhejade av forskningsenhetens chef ägnar
de sig åt avancerade alkemiska experiment, med syftet att hejda
sjukdomsförloppet och att ge evigt liv.

Jag tyckte att boken var mycket bra. Björn kände som sagt att han inte
riktigt hade kunnat ta den till sig, men tänker läsa om den ganska snart,
något som även jag tror att jag kommer göra. Även George mindes det som en
utmärkt bok.

Vi diskuterade hur lättillgänglig boken var, eftersom den har ett visst rykte
om sig att vara svår och deprimerande. Jag tyckte varken att den var svårläst
eller deprimerande och blev lite förvånad över detta när jag läste den. Visst
innehåller den en mängd referenser till och citat ifrån äldre litteratur, men
jag tyckte inte att det gjorde det svårt att ta til sig boken, snarare gör
det nog att den kommer stå sig bättre vid omläsningar. Även vad det gäller
bokens påstådda tungsinthet så fann jag att den inte levde upp till ryktet.
Det är väl inte en glad bok direkt, men den innehåller en hel del humor och i
slutet tar den en vändning som är förvånansvärt hoppingivande.

Jag föreslog att boken kanske skulle kunna spegla den vetenskapliga
utvecklingen i västvärden, eftersom fångarna inledningsvis ägnar sig åt
alkemi och sedan övergår till moderna (riktiga) vetenskapliga göromål. Mot
denna tes framhölls att just övergången var alltför abrupt för att ha någon
historisk motsvarighet, men George påpekade att i slutet av 60-talet, då
boken skrevs, var vetenskapshistoria och perioden då äldre tiders alkemi och
hermetisk magi övergick i modern vetenskap inte lika väl studerad som den är
idag och att Disch därför ändå skulle ha kunnat menat att de skulle ses som
en parallell.

George berättade även om sin tid som student i New York på 60-talet och hur
den miljön kan ha påverkat Dischs roman. George gick på samma universitet som
Samuel Delany och berättade en del om kretsen kring honom, vad dom läste och
vilka idéer som var i omlopp i den miljön. Mycket intressant. Efter detta
gled diskussionen över till andra ämnen: Philip K. Dick, Stanislaw Lems
Cyberiaden, ekologisk mat och lite allt möjligt. På det stora hela var det en
mycket givande afton.

Nästa bokprat äger rum den tionde mars (även då hemma hos Björn) och boken
som ska avhandlas är Robert Silverbergs [Dying
Inside]http://tinyurl.com/yardqyg), en bok som rekommenderas starkt av alla
som har läst den. Vi ses då!

/ _Jesper_
