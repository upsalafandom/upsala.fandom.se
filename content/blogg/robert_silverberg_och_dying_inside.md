+++
title = "Robert Silverberg och Dying Inside"
slug = "robert_silverberg_och_dying_inside"
date = 2009-04-23

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["böcker"]
+++

En av mina favoritböcker alla kategorier är _Dying Inside_ av Robert
Silverberg. På den tiden jag var med i ett band som hette
[Elektrubadur](https://www.jamendo.com/artist/4363/elektrubadur) gjorde vi en
låt vars hela text var tagen rakt av från sista stycket i den här boken.
Väldigt tjusig. (Vi kontaktade Robert Silverberg, som tyvärr hade en ganska
tråkig attityd och satte oss i kontakt med sin agent så att vi skulle få veta
hur vi skulle betala pengar om vi tjänade något på låten i fråga. Som om!
Snarare hade vi väl tänkt göra lite reklam för boken. Men det blev nu inte så
mycket med det.)

Idag hittade jag (via [Locus Online](http://www.locusmag.com), som så mycket
annat) [en artikel om Silverberg och den här
boken](http://www.latimes.com/features/books/la-et-robert-silverberg21-2009apr21,0,7551767.story).

> "Dying Inside" never found a wide audience, but it's been hailed by those who
> know it. Michael Chabon has called it "one of those rare novels that manages
> to be at once dazzling and tender." The book, which the New York Times once
> called "the perfect science fiction novel for people who don't like science
> fiction," was reissued last month by Tor.

Mycket Robert Silverberg just nu. Jag läser precis hans _Science Fiction 101_
som jag nämnde i ett tidigare inlägg. Det är en antologi med viktiga
sf-noveller, de flesta från tidigt 50-tal, med kommentarer av Silverberg om
vad man kan lära sig om skrivande genom att studera de här berättelserna. Det
handlar också om hur han själv lärde upp sig till författare, ganska
intressant.

Artikeln jag länkade till ovan försöker också bena ut hur det kommer sig att
Silverberg inte är ett större namn, med tanke på hur bra hans bästa saker är.
Det finns förstås många anledningar, men jag tror att det kan spela in att
han skrivit så väldigt mycket som är kompetent men inte just mer än så. Det
vore hur som helst väldigt synd om hans bästa verk blev bortglömda.
