+++
title = "Swecon 2009 och Alvarpriset"
slug = "swecon_2009_och_alvarpriset"
date = 2008-06-14

[taxonomies]
forfattare = ["Nea"]
kategorier = ["Nyheter", "Rapporter"]
taggar = ["Alvarpriset", "Swecon"]
+++

Mer från ConFuse: [Imagicon 2](http://www.imagicon.se) vann budet om att bli 2009 års Swecon. Nästa års Swecon kommer alltså att hållas i Stockhom den 16-18 oktober, med Liz Williams som hedersgäst.

Alvar Appeltoffts Minnespris för 2007 vanns av Anders Reuterswärd.
