+++
title = "Julklappar"
slug = "julklappar"
date = 2008-12-11

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Kåserier"]
taggar = ["böcker", "fantasy", "julklappar"]
+++

Snart är det jul igen, och liksom så många andra har jag kämpat med att
försöka hitta lämpliga julklappar till diverse personer. En del med
fantasyanknytning: min bror skall få första delarna i Joe Abercrombies (obs!
Spoilervarning på länken) _[The First Law](http://vetsaga.se/?p=37)_ och
Naomi Noviks
_[Temeraire](https://catahya.net//litteratur/recensioner.asp?id=586)_. Jag
är inte överväldigad av någondera, men båda har sina poänger (Novik skriver
ytlig underhållning, men är rätt bra på vad hon gör, och Abercrombie för en
rätt intressant dialog med fantasygenren). Min syster skall bland annat få
Tanith Lees _Piratika_. Jag har inte läst den själv, men det är Tanith Lee,
så jag litar på att den är bra. Dessutom funderar jag på att ge bort en
[tidskriftsprenumeration](http://vetsaga.se/a/?p=225) eller två. Vad skall ni
– som inte har nära och kära som läser det här – ge bort? Vad _hade_ ni velat
ge någon?

Apropå någonting helt annat: missa inte [Vetsagas
essätävling](http://vetsaga.se/essatavling).
