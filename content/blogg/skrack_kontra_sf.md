+++
title = "Skräck kontra sf"
slug = "skrack_kontra_sf"
date = 2008-05-13

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["Lovecraft", "skräck"]
+++

Ett av de nyare tillskotten i makens ganska imponerande rollspelssamling är
_Trail of Cthulhu_. Jag bläddrade lite i den och råkade läsa beskrivningen av
egenskapen "Sanity". Det slog mig att det här nog sätter fingret på en aspekt
av vad som gör att den här sortens skräck inte funkar på mig. Så här står
det:

> Exposure to the truths of the Cthulhu mythos shatters the core of the human
> psyche by stripping away all illusions of human significance, benign nature,
> and loving God, leaving nothing but the terrifying vistas of stark, cosmic
> nihilism.

För mig ter sig det här fullständigt motsatt mot den grundläggande
inställningen i den mesta science fiction, nämligen att det finns en
signifikans -- till och med romantik -- i att upptäcka de enorma perspektiven
och själv vara en försvinnande liten varelse. Det finns äventyr mot en
kosmisk bakgrund, och själva livet ter sig desto mer viktigt i kontrast mot
rymdens tomhet.

När jag läste _At the Mountains of Madness_ (såvitt jag minns, det var ett
tag sedan) var min starkaste reaktion att huvudpersonen var neurotisk och
överspänd. Det mesta som de upptäcker verkar inte uppenbart farligt, och jag
fick inte intrycket av att folks reaktioner var motiverade.

Man ska förstås skilja på Lovecrafts egna verk och vad folk har gjort av det
efteråt, men för mig ter sig själva utgångspunkten i citatet ovan väldigt
främmande och lite fånigt. Det spelar förstås på den där känslan man kan få
när man lär sig något som gör att man måste justera sin världsbild, och som
somliga tycker är mest läskig medan andra gillar och tar till sig.

Det som skrämmer mig är vanlig mänsklig grymhet eller dumhet. Okända saker
som gömmer sig i mörkret tror jag bara inte på, helt enkelt, och eftersom jag
är präglad på (eller bara mer disponerad för?) sf är min grundinställning att
nya kunskaper och nya förutsättningar är mer utmaning än lamslående hot. Jag
kan uppskatta skräckfilm för att den triggar någon sorts
fly-eller-slåss-reflex, men skräcklitteratur funkar nästan inte alls på mig.

Så har det i alla fall varit hittills, men jag måste erkänna att jag faktiskt
är ganska okunnig om skräcklitteratur. Har jag fullständigt missat någon
aspekt, eller är skräck bara inte min grej?
