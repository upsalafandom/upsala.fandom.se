+++
title = "1 augusti 2000"
slug = "1_augusti_2000"
date = 2000-08-01

[taxonomies]
forfattare = ["Wahlbom"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Ur historiens djup kommer nu efter många om och men rapporten från pubmötet i
Uppsala den 1 augusti dennes. Mycket nöje.

När Sten och jag anlände till Williams, någon gång i den ödsliga 19-tiden,
befunno sig endast fyra i sällskapet där. De voro Magnus, Karl-Johan, Ante
och Åka. Sten och jag beställde mat (Sten med orden "Hej, jag vill ha mat",
varvid flickan bakom bardisken tittade förvirrat tillbaka), och sedan utsågs
jag till kvällens krönikör. Ske alltså.

<!-- more -->

Sten började med att bekänna, à propos någon diskussion jag missade början
av, att han inte läste serier och övervägde att läsa mer, varefter Ante
började överösa honom med tips – "Kabuki" intog där en framträdande plats –
och jag omtalade min motvilja mot amerikanska superhjältar och min förkärlek
för bandes dessinés, dvs företrädesvis franska och fransk-belgiska serier.
Därefter ventilerades hypotesen att Mike Myers (känd från bl.a. _Wayne's
World_) skulle inneha en roll i nästa Star Wars-film, Åka talade sig varm för
_Mission to Abisko_, och någon utropade att Asimovs språk var klart och rent
till skillnad från Frank Herberts.

Vidare talades det om tandläkarskräck, eftersom Sten (på förekommen
anledning) finner tandläkare obehagliga. Åkas tandläkare sägs vara bra, men
han är på semester; själv valde Sten sin med utgångspunkt från annonsen i
Gula Sidorna: "Personer med tandläkarskräck välkomna". Han kunde också
föreställa sig en behandlingsmetod: "Sitt still, annars kommer det här att
göra _väldigt_ ont..."

Magnus beställde en gin och tonic vari stod en drinkpinne med påskriften
"Galliano", vilket väckte farhågor om att det skulle ha iblandats just
Galliano. (Det visade sig snart att det endast var just drinkpinnen.) Ante
berättade i samband därmed om ett nyårsparty i Växjö där han hade befunnit
sig och där det hade konsumerats avsevärda mängder Galliano; dagen efter
("midsommardagen", hävdade Ante först, och rättade sig därefter, efter att vi
frågat, till "nyårsdagen" – fast det är ju allom bekant att där fen befinner
sig förvrängs tid och rum) hade en flicka som inmundigat ganska mycket av
varan täckt hela korridorsgolvet med Galliano. Sedan dess hyser Ante en viss
motvilja mot drycken, tycker jag mig minnas. (Det tog ett tag innan jag fick
reda på vad Galliano är; som bekant dricker jag inte.)

Och nu kom vår mat till slut. Efter att någon (vem framgår varken av mina
anteckningar eller mitt minne) observerat Sten äta framförde vederbörande
hypotesen att gaffeltekniken tyder på viss sexuell läggning - vilken återstår
dock att se!

Ungefär här dök Mattias Palmér, den där icke setts till på mången god dag,
upp, vilket föranledde Sten att fråga "Vad gör du egentligen och varför finns
du aldrig?" Han ansåg sig tvungen att förklara närmare, med tanke på att
frågan lät mycket likt "Vad har du för existensberättigande?" Mattias visade
sig, tycker jag mig minnas, ha börjat forska.

Vad är motsatsen till solipsism? Är det att man tror att alla andra finns men
man själv inte existerar, eller att man tror att någon annan specifik individ
existerar och inbillar sig resten av världen? Sten tillkännagav att han
trodde på Robert Lind i Kramfors, och att han allenast var en tanke i Robert
Linds hjärna. Detta spår ledde vidare till en diskussion om scientologerna,
som Magnus ansåg vara tråkiga - de har fantasi men på ett tråkigt sätt. Han
redovisade sedan sitt gudsbevis: "Gud finns, för jag skulle inte kunna komma
på en så tråkig värld själv." Om Magnus skulle skapa en värld skulle folk se
mycket mer olika ut, för Gud förefaller göra några enstaka originella
människor och sedan skapa folk med en enkel algoritm, ungefär som virus. Vid
närmare påseende är detta kanske snarare ett bevis för att Magnus inte är Gud
än för något annat, något som de horder som vill dyrka honom törhända borde
besinna. De flesta horder är betydligt fler än t.ex. Sten, hävdar Magnus och
vill erinra om att det bl.a. ligger i definitionen av "hord" - enmanshorderna
blir alltmer sällsynta. Mycket små horder som vill dyrka Magnus kan med
lätthet viftas bort, men de flesta horder är numera så civiliserade att de
ringer på framåt 2-3 på eftermiddagen efter att ha avtalat möte. Därefter
avstannade diskussionen om Magnusdyrkan, eftersom Magnus ville ta sista
bussen och avgick 21:23, följd av det muntra tillropet "Hej då, svikare!"
från Karl-Johan.

Sedan dryftades procmail, det faktum att Ante ännu inte har en leksak (en
Palm, då), Cryptonomicon och Apple Power Macintosh G4 Cube. Vid den här
tidpunkten strömmade också en tolkning av "Light My Fire", vilken inte föll
Ante på läppen, ur jukeboxen. Jag framförde hypotesen att det var Jose
Feliciano (vilken jag aldrig orkade verifiera), varvid Ante framhöll att han
föredroge Doors egen version. "Det kunde ha varit värre", påpekade jag; "det
kunde ha varit Tom Jones". Tanken föreföll Ante tillräckligt anstötlig för
att vara roande, varför han gjorde en improviserad imitation av Tom Jones som
bölar "Light My Fire". Mycket finns att säga om Tjuren från Wales, men
subtilitet och finstämdhet är inte de två första orden man tänker på i
samband därmed; kontrasten mellan sången och Tompas tänkta framförande roade
mig storligen. (M.a.o. höll jag på att garva läppen av mig. Jag har en ganska
barock humor, ja.) Därefter spekulerades, när vi ändå var i ungefärligen
samma musikaliska generation, om huruvida Tina Turner just nu var ute på sin
tredje eller fjärde avskedsturné.

Då stegade Olle Östlund in genom dörren, och höll (efter att ha läst mina
anteckningar om varför Magnus inte är Gud) inte med om att världen var tråkig
och att folk såg enahanda ut. Han menade också: "Man föds inte som människa,
man blir det. Utseenden sprider sig som memer, inte som virus. Å andra sidan
är ju memer virus..."

Kvällen fortskred, och diskussionen flaxade vilt omkring som en skadskjuten
kråka. Det föreslogs bl.a. att man skulle inrätta ett återkommande pris för
bästa hoax på framtida kongresser. Finns det förresten metahoaxer? Karl-Johan
hävdade att "Flowers for Algernon" var den bästa sf-novellen och en av de
bästa noveller överhuvudtaget som någonsin skrivits - eder ödmjuke berättare
är benägen att hålla med. "Vilken enhet använder man för att mäta
jordnötter?" undrade Sten, men fick inget svar.

Till sist dröp sällskapet av åt varsitt håll. Klockan var då ungefär halv
tolv, och vi hade spånat en del runt vad som skulle kunna bli en frågesport
till en kongress, men närmare detaljer ligger det i sakens natur att jag inte
avslöjar.

Dramatis personae voro Magnus Eriksson, Andreas "Ante" Gustafsson, Anna "Åka"
Åkesson, Sten Thaning, Karl-Johan Norén, Mattias Palmér, Olli Östlund, Maja
vars efternamn jag inte minns just nu och Anders Wahlbom, den senare
ävenledes krönikör.
