+++
title = "Bokslut 2007: del 2"
slug = "bokslut_2007_del_2"
date = 2008-03-02

[taxonomies]
forfattare = ["Jesper"]
kategorier = ["Recensioner"]
taggar = ["Bokslut2007"]
+++

Jag fortsätter här min  genomgång av alla böcker jag läste ut under 2007. Den första delen kan läsas [här](./blogg/bokslut_2007_del_1.md).

# Jasper Fforde - _The Fourth Bear_ (ljudbok)

Det här är den första romanen av Fforde som jag läst och jag kan definitivt
tänka mig att läsa mer av honom. _The Fourth Bear_ är en typiskt brittisk,
lättsam och humoristisk bok som kan vara väldigt trevlig att läsa om man inte
orkar ge sig på något tyngre, men som inte direkt lämnar något större
avtryck.

Det här är strukturellt sett väldigt mycket en vanlig deckare, med en
medelålders polis som går sin egen väg som huvudperson. Det som skiljer den
här boken åt är att den utspelar sig i ett England där sagofigurer är
verkliga och lever som en förtryckt minoritet. Fforde skriver ganska bra och
boken är bitvis riktigt rolig. Nu när Pratchett har övergått från att vara
fantasyförfattare till att bli propagandist så kan nog Fforde till viss del
ta hans plats.

# H. G. Wells - _Tidsmaskinen_

Under förra våren läste jag en kurs is science fiction på
litteraturvetenskapliga institutionen här i Uppsala. _Tidsmaskinen_ var den
första boken jag läste på kursen och alla böcker jag kommer recensera fram
till Bradburys _Fahrenheit 451_ (undantaget _De norrbottniska
satansverserna_) ingick även de i kursen.

_Tidsmaskinen_ är en av de allra mest klassiska sf-berättelserna. En namnlös
tidsresenär berättar för en samling viktorianska gentlemen om sin resa in i
en avlägsen framtid där barnsliga och lättjefulla Eloer hålls som boskap av
de vämjeliga och underjordiska Morlockerna. Dessa två folkslag är produkten
av att Englands över- och underklass har levt åtskiljda under årmiljonerna
och Wells använde detta scenario för att peka på de problem som han såg i sin
samtids klassuppdelning.

Som science fiction lämnar kanske berättelsen en del övrigt att önska;
tidsmaskinen förklaras på ett nästan filosofiskt plan som är ganska
underhållande, men den används ändå mest som ett redskap för att komma _någon
annan stans_. Fokuset ligger som sagt istället på den samhälleliga situation
som presenteras i boken. Har man sett den senaste filmen känns det som om
halva berättelsen saknas, men det kan man väl knappast skylla Wells för. I
vilket fall som helst så känns den en smula tunn, men eftersom den samtidigt
är tämligen kort (140 sidor i min utgåva) så är det något som alla som har
ett intresse av science fiction bör och kan läsa.

# Mary Shelley - _Frankenstein_

Brian Aldiss menar i bok _The Billion/Trillion Year Spree_ att science
fiction-genren startar med _Frankenstein_. Jag tror inte att jag kan hålla
med. Visserligen är skapandet av monstret science fiction-artat, men
samtidigt ligger tonen i boken mycket mer åt det gotiska skräckhållet.

På grund av tidbrist fick jag snabbläsa _Frankenstein_ och därför tyckte jag
inte att den var alltför hemsk. Hade jag lagt lika mycket tid på den som jag
vanligtvis lägger på böcker hade jag nog funnit den ganska tråkig och träig,
för den är tyvärr inte särskilt välskriven.

# Arthur C. Clarke - _Childhood's End_

Det här var andra gången jag läste _Childhood's End_ och jag tyckte nog
bättre om den den här gången. När jag läste den för ungefär tio år sedan
tyckte jag att den helt enkelt var för dåligt skriven för att kunna tas på
allvar, men nu så kunde lägga fokuset mer på idéerna och det underlättar
nästan alltid när det gäller Clarkes verk.

Jag har alltid tyckt att det har varit lite underligt att Clarke har varit så
kritisk mot religion när hans böcker för det mesta svämmar över i religiös
metaforik. Böcker som _Childhood's End_ och _2001_ må vara grundade i en
rationell världssyn, men samtidigt finns det alltid en tendens att vilja se
det gudomliga i universum och ge det en språkdräkt som nästan är hämtad
direkt ur en religiös tradition.

I det här fallet fungerar det dock ganska bra. Trots att det inte människorna
i boken inte reagerar realistiskiskt på något sätt så lyckas ändå Clarke
skapa en känslomässig resonans inför de kosmiska händelser som utspelar sig
mot slutet av boken. Jag anser fortfarande att det inte är någotlitterärt
mästerverk, men jag förstår nog bättre nu vad som har gjort att boken fått en
sådan hög status inom sf-världen.

Den här veckans inlägg blev ganska kort; jag får nog ta igen det nästa vecka.
Så häng i.
