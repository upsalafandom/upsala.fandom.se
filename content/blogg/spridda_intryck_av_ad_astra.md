+++
title = "Spridda intryck av Ad Astra"
slug = "spridda_intryck_av_ad_astra"
date = 2008-04-03

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier", "Rapporter"]
taggar = ["fandom", "knäppgökar", "kongresser", "skräck"]
+++

Nu har jag då tillslut varit på en kongress i nordamerika. Det var kul att gå på kongress. Saker var välskötta och rullade på så fansen kunde roa sig utan katastrofer och det är ju det viktigaste. Det som jag tyckte var lite dåligt var att det inte fanns någon från arrangörerna som vallade panelister och såg till att de var på plats och fick vad de kunde behöva. Det var ju inget jag personligen led av, men det hade varit en bra sak att ha.

Det som skiljer en svensk kongres eller en eastercon från den kongress vi var på är ju det där med baren. Många gånger när det planeras kongresser i Sverige är det ju viktigt att man har en centralt placerad bar som blir en mötesplats för folk på kongressen. Som Åka redan skrivit om hade de ju en con suite med snacks istället. Lite udda. Jag ska skriva lite om det som för mig kändes mer annorlunda.

Med risk för att verka som en gnällig gammal gubbe så måste jag erkänna att de personer som drev runt i diverse kostymer var för mig inte bara annorlunda, utan mer än lovligt märkliga. När det passerade förbi en stormtrooper som gick runt med nåt plastvapen och pratade med en burkig radioröst likt i Star Wars filmerna, fick jag konstiga idéer att vilja säga "get a life!" till honom. Kanske lite märklig reaktion från någon som ändå är på en kongress för folk som läser böcker och ser på film om små gröna män...

Jag upplevde min kongress väldigt dubbelt. Jag hade kul på kongressen, men blev då och då påmind om att folk omkring mig var tokbisarra. Märklig erfarenhet.

Nåja.

Vi kom till hotellet och installerade oss. Det tog ett bra tag att ta sig dit, och man insåg att Toronto är stort. Skulle man ut och äta på kvällarna fick man ta bilen/bussen ner på stan så jag tror inte folk gjorde det i någon större utsträckning. Vi installerade oss och insåg att i den längan vi bodde var alla roompartys och liknande. Jag brukar vara känslig för sånt, men lyckades sova aldeles utmärkt med öronproppar. Hotellet hade programrum lite överallt, men när de var utspridda på olika våningar var det ofta bara ett par rum precis intill hissen på varje våning. I vissa fall kändes det lite märkligt utspritt. Det funkade ganska bra, dock. Baren låg bra till i närheten av de stora "ballrooms" som vara de stora programsalarna, registreringen, dealers room och andra ställen man ofta ville komma till. Det tråkiga var förstås att det var inte så mycket folk som satt där, och att ölpriserna var som ett typiskt svenskt uteställe, dvs inte billigt alls.

Eftersom jag tog hand om Rebecka medan Åka var med i programmet, och vi fick tillträde till green room, så var vi ganska mycket där. Där fanns det läsk, godis och plockmat precis som i konsviten. Mycket sånt. Det var inte så välbesökt så vi kunde plocka i oss en hel del. Rebecka käkade ost och vindruvor och pratade telefon (hon gillar att trycka på knappar och lyfta luren), och jag snackade lite med den delen av arrangörsgruppen som hade grönrumstjänst. Riktigt trevliga filurer.

Några programpunkter han jag besöka och ska orda lite om här. Det första jag kommer ihåg var en panel om gränsöverskridande inom skräckgenren. Panelen alla var överens om att man måste ha någon sorts katarsis, moralisk poäng eller liknande för att det man gör inte bara ska bli pornografi. Vissa icke namngivna författare fick sig en slev kritik för brist på sådant. Man fick känslan av att detta var en panel som slängts in för att fylla en nisch, och att deltagarna suttit i den flera gånger. Dock så blev det intressant småprat om en del annat också. En annan handlade om författare som agerat redaktörer och vad den erfarenheten lärt dem. Lite känsla av fiskehistoria eller lumparminnen över den panelen, men den var i alla fall ganska roande. Det finns en hel del riktigt otrevliga och korkade människor både som författare och som redaktörer. Sist var jag på en panel om Writers of the Future. Det är alltså en tävling som startades av Scientologerna, antagligen för att bygga lite goodwill inom fandom och publicistsvängen för L Ron och hans [anhang](http://www.xenu.net). Panelisterna berättade dock att det var en toppenchans att få vara med på workshops och allt det man bjöds på som vinnare i tävlingen. Eftersom jag var den första på plats i ett ganska tomt rum med fler panelister än lyssnare så fick jag en gratis bok! Säga vad man vill om Elron, men hans initiativ har fört fram en del hyggligt intressanta författare!

Sammanfattningsvis kan man väl säga att jag hade trevligt även om jag var lite splittrad. Oavsett om jag ibland frågar mig vad fandom och kongresser med knäppgökar i konstiga dräkter är bra för så verkar jag ju bli på gott humör av dem. Jag blir inte klok på mig själv ibland. Bland bytet fanns förutom min gratisbok en skräcksamling samt en bunt Glen Cook. Sämre sätt finns att tillbringa en weekend.
