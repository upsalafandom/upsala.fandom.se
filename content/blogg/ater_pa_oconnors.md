+++
title = "Åter på O'Connors"
slug = "ater_pa_oconnors"
date = 2002-07-02

[taxonomies]
forfattare = ["Marie"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Upsala var vått och småljummet och jag stod inne på Åhlens och valde mellan ett ljuslila, ett nånting-annat-pastellfärgat och ett grått paraply. Det blev det gråa, såklart. Sträckan mellan Åhlens och O'Connors var dock inte så lång att jag behövde använda det. En ung person (under 22) och tre lite äldre satt i den rökfria avdelningen när jag klev in.

<!-- more -->

Naturligtvis handlade samtalet om ålder (naturligtvis därför att O'Connors tillämpar 22-årsgräns i dörren) och vi fick höra diverse historier om hur den evigt unge Johan Anglemark fick visa leg på systemet när han var 30 och liknande. Ålder **är** svårt att bedöma, så det är väl inte så konstigt.

Ganska snart blev vi många fler och bordet vi satt vid räckte inte riktigt till. Vi hade dock fått löfte om att flytta till ett annat bord, så det gjorde vi efter ett tag. Högljudda röster tyckte att vi skulle få flytta in i den avstängda delen av lokalen, men det ville inte servitrisen gå med på med motiveringen att hon inte skulle hinna med att servera då. Efter en stunds käbbel (för just då fick vi inte
heller plats vid det stora bordet vi nyss hade flyttat till, utan fick ta till ett bord till bredvid) var det bara Jolkkonen som fortfarande inte ville kompromissa. Han lär ha sagt: "Jag ska hitta mer koleriska människor att umgås med."

Visste ni förresten att Jolkkonen tycker att Magnus ska göra grus? Fast varför och hur och med vilket material och vilka verktyg fick vi aldrig reda på. Magnus drack Guinness och erkände att han har dåligt minne. Therese bet Johan i armen och det finns en bra förklaring, men den skrev jag inte ner, fastän jag vet att vi bemödade oss med att komma ihåg den en extra gång. Maria var också inblandad, fast hon bets inte. Jag tyckte att det var roligare att skjuta gummisnoddar (sådant man blir hyfsat bra på när man jobbar som brevbärare), fast det fick jag inte för de andra.

Magnus och Johan enades om att det inte hörde till allmänbildningen att känna till hur oktoberrevolutionen startade, medan Jolkkonen drog igång en redogörelse när jag frågade. Det var en av de få gångerna under kvällen som han inte pratade om sitt nya jobb ("förbättra produktionsprocesser för tillverkning av plutonium-zirkonium-mitrid". Han tycker att det är "skojigt") eller relaterade naturvetenskapliga ämnen. Fast Maria ville faktiskt veta vad teoretiska bottnar är, så då förklarade Jolkkonen snällt.

Therese är bra för mitt ordförråd, jag fick flera stycken okända ord uppskrivna i min lilla blåa bok. Nästa gång jag är i närheten av en vettig ordlista, ska jag slå upp dem och lära mig innebörden också. Therese var jättearg, för någons mamma hade städat
hennes lägenhet, inklusive de delar som inte är gemensamma utrymmen, och tyckte att hon ville ha ersättning för det. När Therese inte var arg (och hon berättade om varför hon var arg minst tre gånger), berättade hon hemligheter som jag inte får tala om och delade generöst ut pussar och kramar till folk som ville ha. Kramar är bra. Mer kramar!

Magnus berättade om en film som de visade på Anslagstavlan för 15 år sedan (?), som jag missade poängen med eftersom jag åt Chicken East. Mat ska man ta seriöst, och då kan man ju inte hålla på och lyssna på andra heller. Oartigt mot maten. Vad som helst på O'Connors är godare att äta än det de kallar mat på Williams, för övrigt. Karl-Johan höll med om det. Han åt en Club Sandwich ost och skinka.

Fakta om Karl-Johan: han vill inte ha tio barn. Inte fem heller. Speciellt inte om han måste föda fram dem själv.
Åka, Maria, Björn X och Luna diskuterade gener, social påverkan och homosexualitet och Ante lade sig i med en utläggning om den psykosexuella sfären, som vi nog inte blev klokare av.

I nästa diskussion konstaterade Ante och jag att det var bättre förr, för det var inte så läskigt att bada i havet om somrarna. Det fanns ingen algblomning och maneterna var inte lika läskiga och vattnet var inte lika kallt. Magnus pratade om de varma somrarna 1975 och 1976, som jag själv inte har något minne av (nyfödd, respektive ett år gammal).

Saker du inte visste om Åka: hon spelade minröj på sitt förra jobb, på arbetstid. Fast egentligen tycker hon att man inte ska spela spel på arbetstid. Man ska nämligen arbeta då. (Jo, det är sant!)

Vid ett tillfälle när Magnus, Johan och Ante satt och diskuterade något och plötsligt noterade att jag satt och lyssnade med pennan i handen, tystnade de. Johans kommentar: "Det här ska nog inte skrivas ner." Nähä, varför ska han vara censurmyndighet och inte
jag? Apropå myndigheter: om någon känner till vem eller vilken myndighet som sysslar med att inkompetensförklara folk, så är delar av Upsalafandom intresserade av att veta det. Kunskap är aldrig fel, motiverar vi den nyfikenheten med.

Två saker du inte visste om Björn X (ni vet, han som alltid har allting i svart): Han har tre trasiga videoapparater i garderoben och han har en förpackning pastellfärgade tandborstar hemma.

Johan smet iväg för att lyssna på trubaduren. Rökarna (två till antalet) lommade då och då iväg till den rökiga delen av lokalen för att förgifta sina lungor. Åka visade stolt en artikel med sitt namn på. Visserligen ett ganska stort antal andra namn också, men ändå. Artikelns titel: Search for Neutrino-Induced Cascades with the AMANDA Detector.

Diskussionerna fortsatte och övriga ämnen som behandlades lite eller mycket i närheten av mina öron och min penna och skrivhäfte: kongressande, Minority Report, datorer, dataprogram, trångboddhet (vi fick detaljer angående boendeytan hos en medlem i Åkas Amnestygrupp, vid det tillfälle då Amnestygruppmedlemmen var nygift), rättstavning av folks namn (stava rätt!), rätt längd på ett skägg för att det ska kittla på rätt sätt på ens partner, dop och konfirmation, flirt (den här diskussionen deltog Sten Thaning i, via telefon), mat, fanzine, vem som skulle betala vad på notan, m.m.

Ante och Therese delade ut fanzine och jag hade nyligen framkallade kort från Ghöstacon 2 att visa. Luna fick en bok.

Kvällen avslutades med att vi som var sist kvar stod och diskuterade om vi eller Emacs bäst tillfredsställde ens behov (eller tja, jag diskuterade typ inte, eftersom jag inte har någon åsikt i frågan), tills Jolkkonen sa åt oss att vi skulle gå.

Folk som var där: Samuel Åslund, Björn X Öqvist, Mikael Jolkkonen, Luna, Åke Bertenstam, Ante, Åka, Magnus Eriksson, Christina, Karl-Johan Norén, Maria Jönsson, Björn W, Therese Wikström, Johan Anglemark och jag (Marie Engfors). Sten Thaning deltog via telefon en kort stund.

/Marie

PS. Tack till Samuel och Maria för sovplats och frukost.
