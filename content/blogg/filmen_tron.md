+++
title = "Filmen \"Tron\""
slug = "filmen_tron"
date = 2008-02-22

[taxonomies]
forfattare = ["Lennart Svensson"]
kategorier = ["Recensioner"]
taggar = ["datorer", "film"]
+++

En gång i världen cyklade jag iväg längs en höstlig landsväg, nådde staden med alla sina ljus, parkerade cykeln, gick fram till ett biopalats och betalade 20:–, gick in och satte mig.

Och efter reklam för Göteborgs kex, Timotej schampo och "Wrigley's tuggummi, en smak av paradiset", förflyttades jag till ett samtida L. A. där en datadyrkande Jeff Bridges låg i konflikt med den drakoniske David Warner, chef för ett IBM-liknande företag. Bridges gör inbrott hos Warner – men fångas i klorna till superdatorn Tron, bestrålas och teleporteras i virtuell form in i denna dator, där han blir tvungen att spela dataspel tills han dör...!

Jag hade sett en snutt av detta på TV, det var lockande att se denna datoranimering, det var något nytt där och då, i detta tidiga 80-tal. Och nu satt jag där i biofåtöljen och följde äventyren, visuellt engagerande må jag säga, storyn var lite tunn, det ska medges – men helheten var läcker, färgerna och ljusflimret var något nytt.

Nytt, nytt, det ska hela tiden vara nytt! "Make it new!" heter modernismens slagord, vi ska alltid ha nya kickar i våra urbana bostadsceller, avskurna från naturligt kretslopp i elementens famn som vi är. Nåväl, denna film var ny och rolig för ögat, det var fantasi i nya former. Idag lever vi i en datavärld som 80-talsmänniskan inte kunde föreställa mig, i vart fall inte jag där jag 16-årig satt och glodde på "Tron" – och även om jag gillar denna nya datavärld så grämer det mig en smula att man inte kan bege sig in i datorn på det där syntetiska sättet, inte kan gå runt bland räta vinklar och med lysande dräkter och rejsa med virtuella motorcyklar.

Dagens dataspel har simulering så det räcker, icke förty. Men den där rymdkänslan man hade i Tron, parat med lysränder och virtuella väggar och allt, och svävande skepp över datanejden - detta ser man sig förgäves om efter idag, i denna sköna, nya datavärld...

För övrigt tycker jag de missade en skön, bildmässig poäng i slutet av Tron. På slutet har Bridges tagit sig ur datorn och fått hämnd på Warner i den riktiga verkligheten. Och så stiger hjälten ombord på en helikopter som flyger bort över L. A:s landskap, med nattljus i mörkret. Hur läckert, tänker jag då, hur läckert vore det inte om de då hade förvandlat denna verkliga nejd till en datanejd, ett virtuellt landskap som det man mött tidigare, med stadsljusen förvandlade till datagenererade ljuslinjer och allt?

Det hade varit rena Matrix före Matrix, så att säga.

Trots det förtjänar Tron sin status som klassiker i sf-filmgrenren.
