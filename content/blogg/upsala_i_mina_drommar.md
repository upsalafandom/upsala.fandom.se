+++
title = "Upsala i mina drömmar"
slug = "upsala_i_mina_drommar"
date = 2008-02-18

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = []
+++

Sedan några månader tillbaka bor jag i Kanada. Upsala är långt borta, och jag är usel på att använda sociala webbgrejor som [forumet på fandom.se](https://forum.fandom.se). Facebook är ett ständigt dåligt samvete: jag vill inte logga in där och konfronteras med alla jobbiga förfrågningar som jag måste ta ställning till, alla töntiga lekar och test, men det känns ändå som om jag borde hänga där och hålla kontakt med mina vänner. En kombination av dålig känsla för tidsskillnaden med Sverige och dålig kontakt med min kalender ledde också till att jag missade [hemsidesbyggarjippot](./blogg/12_timmar_hemsida.md) som jag hade kunnat delta i på avstånd. Jag är helt enkelt usel på det här med att vara social i cyberrymden. Jag vill träffa riktiga människor av kött och blod i den riktiga verkligheten!

Inte för att det heller är så enkelt alla gånger.

Det är ungefär tio år sedan den där galna tiden när jag umgicks med vänner dygnet runt, klättrade över [svarta staket](http://detsvartastaketet.nu) på vägen hem från synthklubbkvällar, [dansade lindy hop](http://swingkatten.se) flera gånger i veckan, [sträcktittade på tv-serier](http://2040.klingonska.org), och pratade om science fiction med alla som kom i närheten. Alla de här sakerna fortsatte förekomma i mitt liv, men inte riktigt i samma tempo. Det var länge sedan jag ofta brukade stanna sent på våra pubmöten och gå på eftermöte på [Orvars](http://www.norrlandsnation.se/?sc=sida&amp;sidid=46&amp;katid=20) när alla som inte hade kårleg hade gått hem. Redan innan jag blev mamma för ett par år sedan hade det sociala umgänget för min del minskat betydligt -- jag försökte jobba rätt mycket när jag var doktorand, och många andra blev också mer och mer uppbundna i sina respektive jobb och studier. Ibland kände jag mig nästan lite isolerad även där i staden där jag tidigare varit så hypersocial.

Upsalafandom fortsatte ändå vara andningshålet i vardagen.

Här har jag inget sådant liv som väntar på mig på fritiden. Jag trivs jobbet och kollegorna, men jag saknar den där sortens samtal som spinner av i oväntade riktningar och som handlar om saker som rymdhissar och döda forskare och möjliga katastrofer och knasiga måttsystem och gamla fanzines och böcker och datorer och antenner och konspirationsteorier och andra _saker_. Sånt där som fans i största allmänhet ofta brukar vara bra på. Och så saknar jag att ha mer folk inpå mig som har gemensamma referensramar i form sf- och fantasylitteratur. Tur att jag har Ante med mig åtminstone, som förstår att uppskatta att jag släpar hem sånt som [Dangerous Visions](https://en.wikipedia.org/wiki/Dangerous_Visions) från Book Market (hela boken i stället för de delar vi hade förut, och med originalomslaget -- fast bokklubbsutgåvan, så den var inte dyr) och som uppmuntrar mig att köpa Joe Haldeman (en gul bok, faktiskt!) på bokhandelns reabord.

Men jag traskar kring Kingston och sätter upp reklamlappar för vår lilla [fantasygrupp](http://fearlessfantasy.wordpress.com), och hoppas litegrann att det ska kunna växa till något lite mer vilt och obändigt än våra civiliserade entimmesmöten en gång i månaden. Och så dagdrömmer jag om fanzines, som jag inte riktigt kommer åt att få klart.

Kanske är inte fandom i Upsala riktigt så där livlig och kreativ i verkligheten som i mina drömmar, men jag har ändå ett ideal som spökar i huvudet. Och så tror jag att jag har vänner där, i Upsalafandom. Riktiga människor, som jag känner och bryr mig om mer än deras skyltfönster på Facebook.

I'll be back. Hoppas att ni är kvar då.
