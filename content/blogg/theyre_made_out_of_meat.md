+++
title = "They're Made Out Of Meat"
slug = "theyre_made_out_of_meat"
date = 2009-05-04

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Länkar"]
taggar = ["Björnar", "David Lynch", "Kött", "Terry Bission"]
+++

Terry Bisson är tyvärr inte en författare som det pratas så mycket om.
Michael Swanwick tipsade mig en gång om Terry Bissons _Bears discover fire_.
Swanwick undrade om den, som han ansåg, väldigt amerikanska berättelsen
funkade lite bra för svenska läsare. Det visade den sig göra och jag gillar
den starkt utan att kunna säga vad som är så amerikanskt med den (jag håller
med Michael), eller ens varför jag gillar den. Bisson har skrivit flera saker
jag gillar starkt.

Den här lilla novellfilmen är baserad på en annan av Terrys historier. För de
som gillar David Lynch så är det rena julafton. Jag käkar föresten ofta lunch
på ett ställe som ser ut ungefär sådär…

[They''re Made out of Meat](http://www.youtube.com/watch?v=gaFZTAOb7IE)

För övrigt så representerar detta min avskedspost på den här bloggen.
Engagemanget från Upsalafandom är lite för klent. Jag koncentrerar mig numera
på min egen rollspelsblog _The Omnipotent Eye_ istället. Besök den gärna. Jag
försöker updatera varannan dag, ungefär.
