+++
title = "Bokslut 2007: del 1"
slug = "bokslut_2007_del_1"
date = 2008-02-23

[taxonomies]
forfattare = ["Jesper"]
kategorier = ["Recensioner"]
taggar = ["Bokslut2007"]
+++

För två år sedan skrev jag en sammanställning över allt jag läst under 2005
som bland annat publicerades i Folk och Fans i Landet Annien. Förra året kom
jag aldrig till skott att göra det, men nu tänkte jag ta chansen igen och gå
igenom 2007 års läsupplevelser i kronologisk ordning.

Jag läste 33 böcker under 2007, vilket gör det till det första året sedan jag
började föra bok 2001 som jag tog mig över 30-boksstrecket. Även om det inte
är en särskilt imponerande siffra, så kommer jag dela upp min genomgång på
ett antal postningar.

Nåväl, låt oss börja.

# Mark Helprin - _Winter's Tale_

Ur en rent språklig synvinkel är nog den första roman jag läste ut under 2007
också den bästa. Helprin skriver lika bra som någon annan författare som jag
någonsin läst och inledningen i boken med beskrivningar av det tidiga
1900-talets New York är ett fantastiskt, lyriskt mästerverk. Tyvärr håller
inte hela denna bok lika hög kvalitet; ungefär halvvägs in i boken hoppar
berättelsen fram in i en nära framtid och berättelsens tonvikt skiftas till
en betydligt mindre intressant politisk och filosofisk allegori med religiösa
övertoner.

Boken har stora likheter med John Crowleys _Little, Big_. Båda gigantiska,
lyriska New York-skildringar med starka ickerealistiska inslag och båda har
nog spelat viktiga roller i att etablera Urban Fantasy-genren. Det här är en
tjock bok som kräver en hel del av sin läsare, men för den som är intresserad
av den mer litterära delen av fantasygenren och för den som lägger värde i en
författares språkliga handlag är den närmast ett måste.

# Joe Haldeman - _The Forever War_

Det är ganska märkbart hur ofta sf-klassiker är riktiga bladvändare.
Haldemans berömda militär-sf-roman om rymdsoldater som förlorar kontakten med
den värld de är satta att försvara pga relativistiska effekter, är
visserligen driven av starka sf-idéer med klar metaforisk kraft, men det som
slog mig mest av allt var hur ledigt den gled ner. Oavsett hur glada vissa är
att kalla science fiction för "the literature of ideas" så är det nog ändå
gammalt klassiskt pulpberättande som skapar de största avtrycken.

# Irmelin Sandman Lilius - _Bonadea_

Sandman Lilius brukar ofta räknas som en av de bästa svenskspråkiga
fantasyförfattarna, men trots att hon främst har skrivit ungdomsfantasy så
lyckades jag missa hennes böcker under min uppväxt. _Bonadea_ är en lågmäld
historia om en föräldralös flicka i den fiktiva staden Tulavall som tröttnar
på barnhemmet där hon bor och bestämmer sig för at flytta in på vinden
ovanför stadens bageri. Boken handlar främst om flickans liv i staden där hon
försörjer sig på att springa ärenden åt dess innevånare. Fantasydelen av
berättelsen är väldigt återhållen och består endast av några få scener med
övernaturligheter och på det stora hela känns berättelsen som en ganska
traditionell ungdomsroman, som inte är en miljon mil från exempelvis Astrid
Lindgren. Som vuxen läsare ter sig _Bonadea_ ganska mycket som en bagatell,
men den var trots detta en trevlig bekantskap och jag kan nog tänka mig att
återvända till Sandman Lilius värld.

# Gene Wolfe - _Exodus from the Long Sun_

Johan tog i det föregående inlägget på den här bloggen upp frågan om
favoritförfattare. Till skillnad från honom så har jag faktiskt en sådan:
Gene Wolfe. Det måste tyvärr här sägas att han inte blivit förlänad denna
status tack vare _Exodus from the Long Sun_ eller _The Book of the Long Sun_,
vilken är serien som _Exodus_ är den sista delen i. _The Book of the Long
Sun_ handlar om prästen Patera Silk, som blir indragen i ett inbördeskrig på
The Whorl, ett enormt cylinderformat generationsrymdskepp på väg mot ett
okänt mål. Serien börjar mycket lovande med _Nightside the Long Sun_, men
tyvärr blir varje efterföljande del mer långdragen och mindre intressant och
när Wolfe väl kommit fram till _Exodus_ är det närmast oläsligt. Den här
boken tog mig över fyra år att läsa ut, då jag sällan klarade av att läsa mer
än femtio sidor innan jag tröttnade och lade den åt sidan. Den enda
anledningen till att jag ville läsa ut den är att det sägs att uppföljaren
_The Book of the Short Sun_ är betydligt bättre.

Enligt min åsikt har Wolfe en benägenhet att bli väldigt pratig när han inte
riktigt är inspirerad och jag tycker mig definitivt se symptom av detta här.
Folk måste diskutera allt i oändlighet och beskriva saker in i minsta detalj
och detta blir som sagt väldigt påfrestande efter ett tag. När Patera Silks
käresta här börjar beskriva varför det inte är så kul att ha stora bröst som
man kan tro var jag SÅ HÄR nära att slänga boken i väggen.

Nej, läs _The Book of the New Sun_, _The Fifth Head of Cerberus_, _Peace_
eller _The Island of Doctor Death and Other Stories and Other Stories_
istället; de är äkta mästerverk.

# Mikael Niemi - _Svålhålet_

Den här boken lyssnade jag på som ljudbok, och den passade ypperligt i detta
format. Mikael Niemi läser själv ur boken som består av en räcka löst
sammanhängande berättelser som känns som de har sitt ursprung i en klart
muntlig humoristisk tradition. Inspiration från Douglas Adams Liftarserie är
uppenbar, men Niemi har ett tydligt norrländskt perspektiv och en personlig
berättarröst, vilket gör att han blir långt ifrån någon epigon.

Eftersom detta är en av de få svenska sf-böckerna som publicerats under de
senaste åren känns det som något som de flesta borde läsa och borde kunna
gilla.

# Gene Wolfe - _The Devil in a Forest_

_The Devil in a Forest_ är en kort ungdomsroman och handlar om en ung
föräldrarlös pojke i en namnlös medeltida by. En Robin Hood-liknande
stråtrövare bor i skogen utanför byn och pojken dras tills hans romantiska
livsstil. Wolfe utnyttjar denna utgångspunkt för skriva en moralistisk
berättelse om personligt ansvar och bedrägliga dragningskraften hos de
laglösa.

Som bäst är denna bok under en nattlig vandring till en uråldrig gravhög. Här
kommer Wolfes förmåga att blanda det övernaturliga med det vardagliga fram
som bäst, men boken innehåller även den långa sekvenser med detaljerade
beskrivningar av ytterst triviala detaljer. Inte heller detta är en av Wolfes
bättre romaner, men det slår i alla fall _Exodus from the Long Sun_.

Fortsättning följer…
