+++
title = "Och Honor Harrington tuffar på..."
slug = "och_honor_harrington_tuffar_pa"
date = 2008-04-18

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Kåserier"]
taggar = ["Honor Harrington", "Weber", "rymdkrig"]
+++

Nu har stackaren blivit en "prisoner of war", hon som bara själv har tagit POWar ska nu vara en själv. Deprimerad är hon också… Tänk, [Honor Harrington](https://en.wikipedia.org/wiki/Honor_Harrington) räddade "bara" ca 100 000 andra människor genom att försätta sig i den knipa som ledde till att hon blev tillfångatagen. Då måste man naturligtvis lägga hela skulden på sig själv och deppa ihop… Jag ser fram emot den dag som [David Weber](http://www.davidweber.net) avslutar serien om Honor Harrington. För läsa klart alla 17 böckerna MÅSTE jag ju! De har massor av bra delar också, och bra innehåll, och bra story, och… Speciellt alla stycken om [Honor's katt Nimitz](https://en.wikipedia.org/wiki/Treecats) gör att böckerna skiljer sig från mängden.

Eller hur?
