+++
title = "Nobelpriset i psykohistoria"
slug = "nobelpriset_i_psykohistoria"
date = 2008-10-17

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Länkar", "Nyheter"]
taggar = ["Isaac Asimov", "Paul Krugman", "nobelpriset", "psykohistoria"]
+++

Om nu någon har missat det: [Paul Krugman](https://en.wikipedia.org/wiki/Paul_Krugman), som får Nobelpriset i ekonomi i år, kom in på denna bana för att han [ville bli psykohistoriker](http://krugman.blogs.nytimes.com/2008/05/04/economic-science-fiction). (Om du -- liksom en kille jag pratade med häromdagen -- inte förstår vad jag syftar på har du läst för lite Isaac Asimov. Särskilt böckerna om Stiftelsen.)

[Via Making Light](http://nielsenhayden.com/makinglight/archives/010673.html)
