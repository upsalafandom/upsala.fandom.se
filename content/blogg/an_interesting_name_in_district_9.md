+++
title = "An Interesting Name in 'District 9.'"
slug = "an_interesting_name_in_district_9"
date = 2010-05-14
lang = "en"

[taxonomies]
forfattare = ["George"]
kategorier = ["Recensioner"]
taggar = ["District 9", "film"]
+++

Last Saturday evening I viewed 'District 9' on my new DVD system. Two days
ago I described the film to a Dutch woman who was active in the
Anti-Apartheid movement in the Netherlands. While telling her a bit about
this important film and its alien 'prawns,' I mentioned that the
protagonist's surname was van de Merwe. She burst out in laughter. When she
calmed down she told me that this name is South African slang for a shady,
untrustworthy, character. The sort of creep one wouldn't want to buy a used
car from.

This is a brilliant cinematic move. Wikus van de Merwe undergoes a series of
transformations: from the phonily-smiling, ever-optimistic-seeming,
bureaucratic spokesperson of a mercenary firm, to the firm's slimy
deportation boss, to a genetically modified violent human desperate for a
'fix' back to fully human form, to a person who, while motivated solely by
this powerful desire, perhaps does several good things for the Prawns. At
each stage he's as unreliable as his name suggests. The fit is perfect. Too
bad that few outside South Africa and the Netherlands ('Apartheid' is Dutch
for 'apartness') would get the devastating point.
