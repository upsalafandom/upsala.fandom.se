+++
title = "Sf-musikens labyrinter"
slug = "sf_musikens_labyrinter"
date = 2008-02-28

[taxonomies]
forfattare = ["Lennart Svensson"]
kategorier = ["Kåserier"]
taggar = ["Carl Sagan", "Michael Moorcock", "musik"]
+++

[Anglemarks inlägg om sf-musik](./blogg/det_finns_inget_som_heter_science_fiction_musik/index.md) för ett tag sedan fick mig att fundera.

Jag kom mig aldrig för att kommentera det, nej ämnet är så stort att jag
måste behandla det i ett eget inlägg.

Och visst har Johan principiellt rätt: sf-musik existerar inte som
självständig genre. Jag kan bygga vidare och säga att man aldrig i en
skivaffär kommer att få se kategorin "sf-musik". Och det kommer aldrig att gå
att fastslå teoretiskt vad sf-musik är, till skillnad från vad sf-litteratur
är (à la "Larry Niven är sf, Ivar Lo-Johansson är inte sf").

Sf-musik har något derivativt över sig, om vi nu ska bruka en lätt anglicism:
som Jeff Waynes rockopera som nämndes, samt i filmmusik och i enstaka
rocklåtar inspirerade av sf-böcker (exempel på det sista: Iron Maiden och "To
Tame a Land", om "Dune"). Först kommer boken, sedan musiken.

Sf-musik finns alltså inte. Men ändå…

Sf-musik har en viss existens i mitt psyke. Minnet av Williams musik till
[Star Wars](http://imdb.com/title/tt0076759) behåller sin makt över mig,
trots att det bara är Liszt-Wagner-epigoneri. Och Carl Sagans TV-serie
[Cosmos](http://imdb.com/title/tt0081846), där mannen sågs flyga genom
universum i ett prismaformat skepp till tonerna av Vangelis, ja det är
mäktiga tongångar, musik och bild gifter sig osökt. Och Queens soundtrack
till "Flash Gordon" från 1980 är stark suggo.

Men även bortom dessa derivationer kan man hitta musik med sann
fantastik-prägel. Som Jean-Michel Jarres "Ecquinoxe" och "Oxygène", där man
vid lyssnandet känner sig som gåendes genom en framtidsstad. Eller introt
till Stranglers "Toiler on the Sea", som är som att flyga över klippor på en
ökenplanet. Eller Jethro Tulls "Broadsword", som är den perfekta musikaliska
pendangen till Moorcocks fantasyromaner:

> I see a dark sail \
> on the horizon \
> set under a black cloud \
> that hides the sun… \

En låt med mer fantasykänsla än, säg, Blue Öyster Cults "Black Blade", med
text av samme Moorcock. Och som ju är exempel på sån där derivativ sf-musik.

Annars lyckades Moorcock bra med texten till BÖC:s "Veteran of the Psychic
Wars". Bra låt som helhet detta, en milstolpe i deras produktion: ödsliga
länder och fatalism. I brist på annan term kallar jag detta sf-musik när den
är som bäst.

Således: på det privata planet har jag ett fack för sf-musik. Men bara där.
