+++
title = "5 augusti 1997"
slug = "5_augusti_1997"
date = 1997-08-05

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Oanade konsekvenser för mänskligheten lär väl knappast gårdagkvällens
Uppsalafanmöte på O'Connors pub (O'Connors) få, men vem vet.

Så där förbluffande många blev vi inte i ett sommartomt Uppsala, men jag,
Magnus, Kruse, Kristin och Linnéa tog oss i alla fall dit för att sätta i oss
öl, cider, mat och cola. John Sören hade meddelat per brev att hans bror just
har flyttat in i en lägenhet bredvid sf-bokhandeln, varför han inte kunde
komma och den badrumsklättrande Sten skrev och talade om att han fortfarande
befinner sig i "den här staden på Europas undersida".

<!-- more -->

Inte snackade vi mycket om science fiction, men jag och Magnus försökte ge en
levande bild av Saltsjökongressen i helgen. Det var trevligt umgänge med
trevliga fans, det enda som saknades var några dussin deltagare till, en
bättre lokal och lite mer programpunkter. Även om det inte var den bästa
kongress jag besökt under mitt liv, är det i alla fall roligt att se Sigma TC
med i leken igen.

Vi babblade löst om att det vore kul att försöka få något näringsställe att
fira S:t Patricks dag nästa år, med musik och öl, men det projektet får nog
vila till åtminstone efter Konfekt i höst. Vi diskuterade också
programpunkter på Konfekt och bestämde tid för ett nytt
kongressplaneringsmöte.

Sedan var vi fullständigt utmattade och begav oss hem igen, efter att ha
konstaterat att det blir ett nytt Orvarsmöte på

**O'Connors kl 18.30 och framåt tisdagen 2 september**

Dit är du hjärtligt välkommen!

ex pulpetra

— _Johan_
