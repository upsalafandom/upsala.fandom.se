+++
title = "Science fiction-sällskapet i världens minsta land"
slug = "science_fiction_sallskapet_i_varldens_minsta_land"
date = 2008-03-10

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["Talossa"]
+++

Jag är överstepräst i vad som kan vara ett av världens minst kända science
fiction-sällskap, den kombinerade sf- och whiskyklubben
[The Talossan Science Fiction & Whisky Society](https://web.archive.org/web/20080513084220/http://web.telia.com/~u18120780/TSFWS/),
ett exklusivt sällskap kännare av destillerade drycker och sf, eller som det
heter på det lokala språket, _sientificziun és whisky_. Sällskapets aktivitet
är ganska låg för närvarande, men när det var ungt så utkom fortfarande
medlemstidningen Spaceships and Distilleries (_Vaißáis d'espaçál és
Stülvatôriâs_), som naturligtvis ägnades åt whiskyrecensioner och rapporter
från sf-provningar.

Sf är ett ganska vanligt intresse i det lilla landet
[Talossa](https://talossa.com/), som är beläget innanför den amerikanska
staden Milwaukees gränser. Talossa utropade sin självständighet 1979, och när
USA inte svarade med några kraftmedel tolkade man den uteblivna reaktionen
som ett defacto-erkännande. Landet har en europeisk provins också, den lilla
ön [Cézembre](https://web.archive.org/web/20070716001519/http://www.cezembre.org/cez/index.htm) utanför Bretagnes kust.
Invandring är välkommen och den invandrade har alla möjligheter att snabbt
klättra på karriärstegen. Redan efter ett par månader kan man sitta i
parlamentet och presidentposten är inte alltför avlägsen.

Går man med i TSF&WS så får man möjligheten att återuppliva medlemstidningen.
Om inte annat kan man prata språk, sf, och politik på medbrogarforumet. Det
behöver man inte ens vara medborgare för att göra, förresten. Man behöver
inte ens kunna tala talossanska! (Vilket annars är ett intressant språk,
romanskt i grunden men fullt med lån från andra europeiska språk, både
svenska, norska och finska.)

Det är kul att kunna vara nationalist utan att beblanda sig med de vanliga
flaggviftarna den sjätte juni!
