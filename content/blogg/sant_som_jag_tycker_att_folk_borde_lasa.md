+++
title = "Sån't som jag tycker att folk borde läsa"
slug = "sant_som_jag_tycker_att_folk_borde_lasa"
date = 2008-11-20

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Länkar"]
taggar = ["fantasy", "länktips", "reklam", "science fiction"]
+++

# [Vetsaga](http://vetsaga.se)

Det är förstås inte särskilt förvånande: om man driver en nättidskrift brukar
man tycka att det är bra om folk läser den. Men Vetsaga är någonting som jag
hade velat läsa även om jag inte hade varit den som drivit projektet, och jag
intalar mig att det faktiskt finns någonting att hämta där om man har ett
intresse för science fiction och/eller fantasy. Eftersom det inte alltid
dyker upp nya texter precis hela tiden, kan det vara värt att använda någon
av möjligheterna att [automatiskt få veta när det
sker](http://vetsaga.se/?page_id=4).

# [Nova science fiction](http://www.replik.se/novasf/main.html)

Därför att det är roligt med en ambitiös, svensk science fiction-tidskrift
och därför att en ambitiös, svensk science fiction-tidskrift behöver läsare –
eller i alla fall prenumeranter – för att överleva. Och för att det faktiskt
är värt pengarna, om man tycker om att läsa science fiction och inte har
någonting emot att göra det på svenska.

# [The Prilege of the Sword](https://www.catahya.net/litteratur/recensioner.asp?id=528)

Därför att Ellen Kushner är en bra författare. Därför att _The Privilege of
the Sword_ är en bra bok. Därför att det skulle vara roligt om mer fantasy
var så här välskriven.

# [Strange Horizons](http://www.strangehorizons.com)

Därför att det är roligt med en nättidskrift som tror på det litterära, det
nya, det genreöverskridande inom science fiction och fantasy. Därför att det
är kul att kunna läsa kvalitativ anglosaxisk science fiction och fantasy utan
att behöva få vänta på att det amerikanska postverket skall ha bundit fast
tidskrifterna på migrerande havssköldpaddor som skall ta dem till Europa.
Därför att tidskriften innehåller något av den bästa science fiction- och
fantasylyrik jag har läst.

# [Steph Swainstons böcker](http://www.stephswainston.co.uk)

Därför att Swainston kommer till [Åcon](http://acon3.wordpress.com) i vår,
och det vill man ju inte missa – och därför att en central anledning till att
vi bjöd in henne från första början är att de är bra.

Det finns fler saker jag tycker att folk borde läsa. Fler böcker, fler
tidskrifter, fler texter. De får vänta till en annan gång.
