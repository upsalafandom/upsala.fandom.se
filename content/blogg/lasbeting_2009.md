+++
title = "Läsbeting 2009"
slug = "lasbeting_2009"
date = 2009-03-08

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["böcker", "läsning", "pliktläsning"]
+++

Några olika faktorer styr hur jag väljer böcker att läsa. Jag försöker att:

* beta av favoritförfattare.
* hänga med i det allra mest omtalade av det som publicerats inom sf/fantasy senaste året.
* läsa in mig på hedersgäster på kommande kongresser.
* läsa böcker som någon jag litar på rekommenderar tillräckligt entusiastiskt.

Till det tillkommer "pliktläsning" av olika slag, om jag är med i någon jury
eller bokcirkel.

Problemet är att detta leder till att lejonparten av det jag läser är sf och
fantasy. Exempelvis är dessa de senaste femton böckerna jag har läst:

* Graham Joyce: Requiem
* Ursula K. Le Guin: Tehanu
* Kent Björnsson (ed): Krälande Cthulhu och andra bedrägliga blindskär
* Kent Björnsson (ed): Mardrömmar i midvintertid och andra morbiditeter
* Zoran Zivkovic: The Library
* China Miéville: Un Lun Dun
* John Garth: Tolkien and the Great War
* Nils Holmberg: Tusen och en natt 6
* Neil Gaiman: The Graveyard Book
* Dan Simmons: Song of Kali
* Peter Englund: Stridens skönhet och sorg
* James Branch Cabell: Figures of Earth
* Tim Powers: Last Call
* Ursula K. Le Guin: Tales from Earthsea
* Graham Joyce: Memoirs of a Master Forger

Jag funderar allvarligt på att ta paus från genrelitteraturen något år och
vältra mig lite i Joyce, Proust, Faulkner, Hemingway och annat som jag har
försummat alldeles för länge. Joyce har jag en del oläst av, framför allt
_Ulysses_, Proust har jag inte läst något alls av, Faulkner och Hemingway har
jag bara läst enstaka böcker av men älskat allt än så länge. Jag har inte ens
läst samtliga Shakespeares pjäser. Jag har olästa diktsamlingar av Seamus
Heaney som väntar. En massa Graham Greene som pockar. Kate Atkinson är jag
nyfiken på.

Men först måste jag "pliktläsa" mer av Liz Williams inför Imagicon i höst.
Tur att The Ghost Sister, som jag håller på med just nu, är bra. Sedan får
jag fundera på om det är dags att göra det där avbrottet. Kanske helt enkelt
genom att inte ställa någon mer sf eller fantasy på "Att läsa"-hyllan, så att
boktraven gradvis byts ut...
