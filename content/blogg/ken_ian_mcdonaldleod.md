+++
title = "Ken Ian McDonaldLeod"
slug = "ken_ian_mcdonaldleod"
date = 2008-03-23

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["författare"]
+++

Skottar har dålig fantasi. De borde ha mer olika namn, tycker jag. Varför
finns det en författare som heter Ian McDonald, en som heter Ian MacLeod, och
en som heter Ken MacLeod? (Ni ska se att nästa stora namn blir Ken McDonald.)
Jag är inte den första som ibland tar fel och blandar ihop de här herrarna.

Såg precis på [Locus Online](http://www.locusmag.com) att Ian McDonald fick
BSFAs (det brittiska sf-sällskapets) pris för _Brasyl_. Jag tar det som ett
tecken på att jag hänger med nuförtiden att jag precis läst den, men det
löser inte mitt problem med att komma ihåg vad han heter. Eller också gör det
det ändå, ifall jag bara pratar om det så mycket att namnet liksom nöts in.
Boken är i alla fall riktigt bra, och var av det slaget att jag fortsatt
bläddra i den då och då flera dagar efter att jag läst ut den. Parallella
världar, kvantdatorer, djungel, svärdsfäktande jesuiter, kaffe, fotboll och
capoeira.

Den andre Ian brukar ha en initial med: Ian R. MacLeod. Det kan vara
ytterligare en krångelfaktor, eller också hjälper det honom att skilja sig
från de andra litegrann. Av honom har jag läst en bok: _The Light Ages_. Jag
tyckte mycket om den, så det är troligt att jag kommer att plocka upp något
mer av honom i framtiden. Jag gillade beskrivningen av den väldigt fysiska
och farliga (förorenande) magin, aether-gruvornas industriella elände, och
revolutionens problem med att hålla sig på rätt köl. Lite romantik och
glamour fanns det också, som kontrast.

Av Ken MacLeod har jag läst flera böcker, som jag har väldigt dimmigt minne
av. De var inte så tjocka, men jag minns dem som komplicerade att hänga med
i. Ganska politiska också. På nåt sätt bet de sig inte riktigt fast. Han var
dock en trevlig hedersgäst.

Jag har i alla fall inget problem med att hålla författarskapen isär, bara
själva namnen. Har ni något bra sätt att komma ihåg och skilja på de här
författarna? Är det fler än jag som famlar i blindo ibland, och gissar
litegrann -- pratar om en fast använder namnet på en annan?
