+++
title = "2 november 1999"
slug = "2_november_1999"
date = 1999-11-02

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Skosulor och gamla kängor åts det inte när Upsalafandom i all sin prakt fylkades kring ölstopen för Orvarmöte. Däremot försökte vissa av oss på en närkamp med ett par präktigt sega biffar samt blog i lämplig mängd.

Mat var temat för kvällen under ett långt tag. Allas vår Lennart Svensson dök upp och pratade om kokböcker. "Krogkök man minns" var den boktitel som föreslogs på Lennarts kommande mästerverk i genren (råkar någon som läser detta ha en bostad över så är Lennart intresserad föresten).

<!-- more -->

Är sport tråkigt? Vild diskussion utbröt, men som alla vet så ägnar sig fans bara så kast med liten mimeo och andra shant faaaniska sporter.

Annars är blått framtidens färg som lyser om alla monitorer och radioaktiva vrak.

För er som är intresserade av en bra investering kan nämnas att den förbannade skosnörestorusen som måste säljas dyrare än man köpt den, nu är uppe i 18 kr och två frimärken. Slå till idag, du måste _tjäna_ på affären! Jag tror Sten Thaning är den nuvarande ägaren.

Diskussionens vågor böljade när Gabriel förfäktade den (bisarra?) iden att det vore bra om alla språk utom engelskan försvunne, eftersom alla översättare kunde ägna sig åt något mer produktivt.

Sedan har jag en anteckning om "Religion&Fonetik" jag minns inte vad det handlade om. Där ser ni vad som händer när man missar ett Orvarmöte! Det sägs saker som som är viktiga, roliga och glöms bort efteråt. Whittert wärre.

Som sagt, det var en del matprat och viktiga frågor blev ställda. Varför finns det inte gökurssopppa när det finns svalbosoppa? Ingen vet.

— Andreas

Vi var där: Anders Wahlbom, Karl-Johan Noren, Sten Thaning, Matthias Palmér, Kristin Thorrud, Eyolf, Johan Anglemark, Lennart Svensson, Anna Åkesson, Carl-Henrik Feldt, Gabriel, Marie Engfors, Maria Jönsson, Björn X Öqvist, Olle Östlund, Mikael Jolkkonen, Thomas Winbladh, Markus Persson, Sverker Wahlin, Samuel Åslund och Andreas Gustafsson
