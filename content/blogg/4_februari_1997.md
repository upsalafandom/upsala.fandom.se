+++
title = "4 februari 1997"
slug = "4_februari_1997"
date = 1997-02-04

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Ave fandom,

Fanatici upsalienses te salutant.

Upsalafandom rediviva samlades åter på O'Connors vid Stora torget i Uppsala för att kannstöpa. Mellan tuggor och klunkar babblade vi om universums expansion, Upsalafandoms dito samt kongressplaner inför hösten.

<!-- more -->

Jag hade med mig SFF-sändning 101, som föll i god jord hos våra neofans. Funderingar på Liverpool i påsk ledde till spirande planer och uppgivna suckar.

Jessica kunde inte vara med, hon satt med en konspiration på annat håll och planerade att attackera biskopen i ärkestiftet.

En diskussion om baksidestexterna på vår barndoms mjölkpaket (detta visade sig för vissa vara baksidestexterna på deras ynglingstids mjölkpaket och för andra vara "-vadå? Slikt hade vi ikke i Norge då jeg var ung-") ledde till historien om hur Anna berättade för sin lillasyster om hur allt är gjort av atomer, varpå systern sprang till fadern och sade att hennes lögnaktiga syster påstod att Fantomen har gjort allt. Pikant pinsamt att bli anklagad för denna lögn var det eftersom Annas uppfattning om vad Fantomen var, var ganska vag.

Naturligtvis menade hon att fandomen har gjort allt.

Nästa möte blir på samma plats från 18.30 och framåt, tisdagen 4 mars. Väl mött!

— _Johan_

PS. Närvarande var jag, Mattias Palmér, Tony (inte E.), Sten Thaning, Kristin Thorrud, Anna Åkesson och Björn X Öqvist.
