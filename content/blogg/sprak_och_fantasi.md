+++
title = "Språk och fantasi"
slug = "sprak_och_fantasi"
date = 2008-07-15

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["fantasi", "fantasy", "språk"]
+++

Det finns en del "hjälpvetenskaper" för fantasyförfattare. Ett typiskt fantasyverk som utspelar sig i en sekundärvärld à la Tolkiens Midgård, Lewis Narnia, Jordans "Randland" osv, är ju ofta försett med kartor, egna språk eller fragment därav, utdrag ur fiktiva historiekrönikor osv. Tanken slog mig just att detta kanske inte bara är en konsekvens av att författaren försöker att måla upp en trovärdig sekundärvärld, utan närmare förknippat med den kreativa processen än så?

> _Marzhin, Marzhin, pelec'h it-hu Ken beure-se, gant ho ki du_
>
> _Bet on bet kas kaout an tu, Da gaout dre-man ar vi ruz_
>
> _Ar vi ruz eus an naer-vorek War lez an aod, toull ar garreg_
>
> _Mont a ran da glask d'ar flourenn Ar beler glas ha'n aour-yeoten_
>
> _Koulz hag uhel-varr an dervenn E-kreiz ar c'hoad, 'lez ar feunteun_

Jag är inte ensam som fantasyläsare om att sedan tidiga år ha fascinerats av historia, vexillologi, heraldik, historia och lingvistik. Jag började rita kartor, flaggor och skissa på egna språk innan jag blev biten av fantasy. Kanske är det samma del av min hjärna som stimuleras när jag lär mig glosor i ett nytt språk, bläddrar i en historisk atlas med mera, som när jag läser en fantasieggande berättelse som utspelar sig i en fantasivärld? Jag undrar om det finns något sätt för mig att gå vidare med denna tanke, peta på den och se om den innehåller korn av sanning.

Vad tror ni? Är jag något på spåren eller gör jag misstaget att tro att några personliga intressen måste ha ett samband, där inget samband finns?
