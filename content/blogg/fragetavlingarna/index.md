+++
title = "Frågetävlingarna"
slug = "fragetavlingarna"
date = 2013-08-07

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["frågetävling", "pubmöten", "quiz"]
+++

Vi firade ju den 200:e pubträffen i obruten följd i går på Pipes of Scotland.
Det var kul, jag räknade till 41 vuxna och ett barn, och jag kan ha missat
någon. Största mötet någonsin och det överlägset största augustimötet. Av de
elva som var på[ första mötet 4 januari 1997](./blogg/4_januari_1997.md), var
tre närvarande: jag själv, Sten och Zrajm.

Den stora aktiviteten på mötet var en frågetävling i fyra delar med fina
priser, både prasslande (böcker) och flytande (öl). Jag lovade att lägga upp
frågor och svar i efterhand. Här kommer de:

<!-- more -->

# Deltävling 1: Frågor

1. Strax före andra världskriget sändes i USA en radioteater om invasion av utomjordingar som skrämde slag på stora delar av publiken. Vem stod bakom?
1. Vad heter Michael Moorcocks albinohjälte?
1. I vilken amerikansk nittiotals-tv-serie är huvudpersonerna utomjordingar som förklädda till en människofamilj försöker att förstå sig på hur Jorden fungerar?
1. Hur många episoder spelades in med den åttonde doktorn?
1. Vilken författare har skrivit både en roman tillsammans med Neil Gaiman och en roman tillsammans med Stephen Baxter?
1. Varför var Peter Capaldi i ropet nu i helgen?
1. Vad har Dune, Ringworld, The Dispossessed, American Gods och Among Others gemensamt?
1. Vad gjorde DC Comics för två år sedan som slog serievärlden med häpnad?
1. Sortera följande tv-serier efter hur många säsonger de gick:
    * Buffy the Vampire Slayer
    * Caprica
    * Firefly
    * Fringe
    * Heroes
    * Lost
    * Terminator: The Sarah Connor Chronicles
    * Veronica Mars
    * X-Files
1. Vad är Bender’s Game?
1. Vad har Jim Baen, Don Wollheim och Lester Del Rey gemensamt?
1. I vilken bransch var företaget TSR verksamt och vad stod förkortningen för?
1. Vad heter novellen, sedermera romanen, om den ointelligente Charlie som via ett medicinskt experiment förvandlas till geni på samma sätt som en labbmus före honom, bara för att få se effekten klinga av igen?
1. Vem har skrivit böckerna om pandionriddaren Spjuthök?
1. Vilken bransch utsätts för Frederik Pohls och Cyril Kornbluths satir i romanen Venus är vår?

[Facit till deltävling 1](facit1.pdf)

# Deltävling 2: Författares födelseår

Sortera följande sf- och fantasyförfattare efter födelseår:

* Catherine Asaro
* Karin Boye
* David Brin
* CJ Cherryh
* Lord Dunsany
* Neil Gaiman
* Robert Heinlein
* Diana Wynne Jones
* Dénis Lindbohm
* HP Lovecraft
* Stephenie Meyer
* Christopher Paolini
* Hannu Rajaniemi
* Justina Robson
* Mary Shelley
* Bram Stoker
* James Tiptree, Jr.
* H G Wells
* Kurt Vonnegut

[Facit till deltävling 2](facit2.pdf)

# Deltävling 3: Författarinitialer

Vilka sf-/fantasyförfattare döljer sig bakom följande initialer?

* ACC
* CDS
* CLM
* CSL
* DAW
* DNA
* DWJ
* EFR
* EMF
* ERB
* HGW
* HPL
* HRH
* JBC
* JCG
* JKR
* JWC
* KEW
* KJA
* KJB
* KSR
* MWW
* MZB
* OSC
* PCJ
* PJF
* PKD
* PZB
* RAH
* RAW
* REH
* SCS
* SJL
* TÅB
* UKL

[Facit till deltävling 3](facit3.pdf)

# Deltävling 4: Kasta om bokstäverna

Vilka filmer döljer sig i följande anagram?

1. Kirk Embraces Epithets
1. A Butt Fuck Hetero
1. Soften The Appeal
1. Her Huge Magnets
1. A Little Baccarat Stag
1. Grim Toothy Borneo
1. Mob Hacking Evil John
1. Heard Trekking Shits
1. Taffy He Died Of Thirst
1. A Bed Refiner Often Thinks
1. Handmade Boxed Dummy Tenor
1. Reunite Font Hygiene
1. Cows Prefer A Monkey
1. Cheetah Nap Moment
1. Alienate Unjust Tangent Term

[Facit till deltävling 4](facit4.pdf)
