+++
title = "Att se film som ett barn"
slug = "att_se_film_som_ett_barn"
date = 2009-03-11

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["fantasy", "film", "kina"]
+++

Jag har förmodligen nämnt min fascination för asiatisk film förr. Jag har egentligen inte läst in mig, så jag _vet_ inte mycket om det. Jag bara uppmuntrar maken, och han hittar en massa roliga filmer. Sedan tittar jag. Jag förstår ofta inte riktigt vad som händer, och ofta är det inte alls uppenbart vilken sida man ska sympatisera med och varför. Jag anar ibland syftningar till händelser och företeelser jag inte känner till. Det är roligt, och lite utmanande. Det är lite som att upptäcka en ny värld helt från början.

Jag misstänker att det är just där lockelsen ligger för mig. Det där att bara kasta mig in i en annan berättartradition och en annan kultur, och bara se -- utan några speciella förväntningar eller krav eller ens analyser.

Det är lite som att vara barn på nytt. När jag var liten hade jag inte så utarbetad smak, helt enkelt därför att jag inte sett så mycket. Allt var nytt, och jag höll på att bygga upp en karta över verkligheten. Jag undrar ibland om intresset för fantasy och science fiction knyter an till den där bekanta men alltmer avlägsna känslan av att upptäcka världen. I fantastiken får man nya världar presenterade på silverfat. Men det finns ju genrekonventioner också, och man känner sig ofta märkligt hemtam även i de främmande världarna. Det är strömlinjeformat, kompetent utfört, och fast rotat i en och samma grundläggande kultur (vit, angloamerikansk, medelklass, o s v).

Efter att ha sett ganska många historiska filmer, fantasyfilmer och gangsterfilmer från Hong Kong, Kina och Korea börjar jag se en del mönster och ha en del förväntningar, men det är fortfarande nytt och ganska okänt och därför intressant på ett helt annat sätt än motsvarande filmer från Europa eller Nordamerika skulle vara.

En gång såg vi en kinesisk fantasyfilm gjord i amerikan för amerikaner. Den hade samma ingredienser som många av sina förebilder, men en berättarteknik och en sensmoral som känns igen från Hollywood. Fortfarande rätt skoj, men inte alls lika intressant eftersom jag fundamentalt känner igen alltihop.
