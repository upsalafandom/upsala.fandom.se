+++
title = "Favoritförfattare"
slug = "favoritforfattare"
date = 2008-02-22

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["böcker", "författare", "listor"]
+++

"Vem är din favoritförfattare?" Hur många gånger har man inte fått den frågan
och suckat lika djupt varje gång. Det är ju bra böcker man är intresserad av,
inte författarna, och de bra böckerna kan man hitta varsomhelst, av
vemsomhelst...

Ickedestomindre finns ju det några författare som jag läser allt av. Jag
skulle vilja dela in dem i två kategorier; författare som jag har läst
praktiskt taget allt av, vars böcker jag skaffar så fort de dyker upp, och
författare som jag har läst det mesta av och som jag planerar att läsa resten
av, men som jag inte har någon direkt tidplan för. Författarna är:

# Jag läser allt när det kommer ut

* Susanna Clarke
* Graham Joyce
* Guy Gavriel Kay
* Ken MacLeod
* Christopher Priest
* Geoff Ryman

# Jag tänker läsa allt i sinom tid

* Peter Beagle
* Italo Calvino
* William Faulkner
* Diana Wynne Jones
* Howard Norman
* Mervyn Peake
* Colm Tóibín
* Connie Willis
* Zoran Zivkovic

Jag kan också nämna författare som jag brukade läsa troget, men som med tiden
av olika anledningar halkat ner i mina prioriteter: Iain Banks, Jasper
Fforde, Alasdair Gray och Patrick McCabe.

Och, för att avsluta, författare som **borde** befinna sig under punkten
sådant som jag tänker läsa allt av (eftersom jag har tyckt mycket bra om allt
av dem jag läst), och som jag inte riktigt vet varför de inte gör det: Jane
Austen, Stig Dagerman, Roddy Doyle, John Fowles, Graham Greene, Ernest
Hemingway och China Miéville.
