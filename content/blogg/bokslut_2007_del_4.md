+++
title = "Bokslut 2007: del 4"
slug = "bokslut_2007_del_4"
date = 2008-03-16

[taxonomies]
forfattare = ["Jesper"]
kategorier = ["Recensioner"]
taggar = ["Bokslut2007"]
+++

Nu till del fyra i vår [odyssé](/taggar/bokslut2007/) genom det
litterära landskapet.

# Ursula K. Le Guin - _The Left Hand of Darkness_

_The Left Hand of Darkness_ är även den en riktig klassiker. När den gavs ut
1969 lyckades den vinna både Hugo- och Nebulapriset och tillsammans med _The
Dispossessed_ får den ses som Le Guins mest kända sf-roman.

Huvudpersonen är en är utsänd från rymdimperiet Oekumenen till en nyupptäckt
vinterplanet där alla människor är hermafroditer. Handlingen är en blandning
av en klassisk äventyrsroman (med politiska intriger, fångläger och en lång
fotvandring genom den frusna ödemarken) och en antropologisk fallstudie. Det
förefaller mig vara ganska typiskt för Le Guin, som ofta dras mellan det
vetenskapliga och det romantiska och som i många andra av hennes tidigare
böcker så tar ofta det romantiska här bitvis överhanden.

_The Left Hand of Darkness_ ses ofta som en feministisk roman, med en analys
av människans könsroller som det centrala, men personligen tyckte jag inte
riktigt att boken levererade om man läste den ur detta perspektivet. Här tar
det manliga perspektivet allt för mycket överhanden för att boken riktigt ska
fungera som en utforskning av könsroller och istället tror jag att man få gå
till Le Guins intresse för taoismen för att riktigt komma åt vad hon vill med
boken.

Jag får nog säga att jag var en aning besviken över denna bok. Man kan
visserligen se att den har haft ett stort inflytande på mycket senare sf, men
den känns trots detta aningen simpel. Visserligen är det en bra bok som är
väl värd att läsa, men Le Guin gick vidare till att skriva betydligt bättre
saker än det här.

# Maureen F. McHugh - _China Mountain Zhang_

Om jag skulle välja ut en bok som 2007 års bästa så skulle det nog bli _China
Mountain Zhang_. McHugh har här skrivit en lågmäld och elegant roman om
Zhang, en ung man i ett framtida kommunistiskt USA som står under kontroll av
den nya supermakten Kina. Zhang växer upp i New York, men har turen att få
ett stipendium som gör det möjligt att studera i Kina, detta trots att han
inte är en riktig kines (något som lite plastikkirurgi lyckas dölja) och
trots att han är homosexuell. Vi får följa hans resa från New York, via
Kanadas tundra, till universitetsstudier och jobb i Kina och till sist
tillbaka till New York i avskilda episoder. Insprängt mellan dessa episoder
finns kapitel berättade ur perspektivet av olika personer som Zhang på något
sätt kommer i kontakt med på vägen; allt från chefens dotter som han blir
ihopparad med i New York, till kolonisatörer på Mars som han håller
korrenspondenskurser för.

Detta låter kanske inte så spännande, men det som gör den här boken så bra är
McHughs förmåga att gestalta de personer vars historier hon berättar. Hon
lyckas på ett lågmält sätt ge dem röst och form, samtidigt som en
fascinerande värld träder fram.

_China Mountain Zhang_ gavs för första gången ut 1992, och man kan se
inflytandet från cyberpunken. Men även om också detta är science fiction från
gatuplanet, så saknar den cyberpunkens hårdkokta manér. Jag kan också se ett
släktskap med senare böcker, som exempelvis Ian McDonalds _River of Gods_.

Man kan kanske ha åsikter om rimligheten i att USA blir kommunistiskt, men
det ser jag som ett sekundärt problem, då personerna fungerar så väl ihop med
sin värld. I slutändan är det dock främst ett ypperligt exempel på välskriven
science fiction som klarar av att vara konstnärligt högstående utan att göra
avkall på tillgängligheten. Varmt rekommenderad.

# Diana Wynne Jones - _Deep Secret_

I _Deep Secret_ tvingas Rupert Venable, en magid eller interdimensionell
polis om man så vill, rädda multiversum på en science fiction-kongress. Det
är ungefär den blandning av humor, originalitet och skickligt fångade unga
rollfigurer som man har kommit att förvänta sig av Jones, även om
perspektivet här är aningen vuxnare än i de flesta andra av hennes böcker.
Idén med att låta boken till stora delar utspela sig på en sf-kongress är
förstås god, då den både tilltalar den presumtiva publiken och passar väl in
i handlingen. För vem skulle kunna gissa folk i magikerkåpor faktiskt kommer
från en annan värld och inte bara är fans på väg till maskeraden?

_Deep Secret_ skrevs på nittiotalet, vilket placerar den något efter den
period som jag skulle karaktärisera som Jones höjdpunkt som författare (dvs
sent sjuttiotal fram till slutet på åttiotalet). Den når därför inte rikigt
upp den nivå som hennes bästa böcker håller (_Archer's Goon_, _Howl's Moving
Castle_, _The Lives of Chrisopher Chant_, _Fire and Hemlock_, med mera), men
den är ändå en mycket rolig och underhållande bok som jag tror de flesta kan
få ut något av.

# Terry Pratchett - _Strata_ (ljudbok)

Denna bok utspelar sig på en värld som är platt som en pannkaka och fylld med
mytiska väsen, men det är inte Skivvärlden. Strata är även den en komisk
roman, men här skriver Pratchett space opera med inspiration från Larry
Nivens _Ringworld_.

_Strata_ skrevs 1981 och kan ses lite som en förstudie till Skivvärldsserien.
Det är en tidig Pratchettbok, so fokus ligger mer på parodi än på satir, men
jag tycker att den håller god klass i jämförelse med hans övriga verk och det
är kul att se honom tackla samma material som i Skivvärldsböckerna, men från
ett annat perspektiv.
