+++
title = "Stafetten 4: Matthias Palmér, Uppsala"
slug = "stafetten_4_matthias_palmer_uppsala"
date = 2009-02-09

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Intervjuer"]
taggar = ["Matthias Palmér", "e-lärande", "framtiden", "frihet"]
+++

> Detta är den fjärde av mina personliga intervjuer med människor inom svensk
> fandom eller som på annat vis är involverade i det här med sf och fantasy.
> Matthias föreslogs av det förra intervjuoffret, Kruse, med motiveringen att
> hon är nyfiken på vad han gör nuförtiden. "Han behöver en liten puff för att
> komma till ett månadsmöte!"

<!-- more -->

# Ursprung

Större delen av min uppväxt tillbringade jag i Garphyttan utanför Örebro.
Garphyttan är en gammal bruksort med cirka 2000 invånare, på kanten till
Kilsbergen och tämligen naturskön.

Min mamma flyttade till Sverige från Österrike med min mormor och diverse
släkt när hon var sex år. Min mormor upplevde en del hemskheter under andra
världskriget och hennes inställning verkar vara att krig och elände är
normaltillstånd, kriget kommer snart igen. Min mamma verkar ha brutit med
denna fatalism till fördel för ibland överdrivet kritiskt förhållningsätt
till olika samhällsfenomen som television, mobiltelefoni, byråkrater,
konsulter osv. Som halv andra generationens invandrare bemöter jag detta
muntert med optimism och utopiska visioner.

Min pappa växte upp i lärarbostaden till skolan där farfar var magister.
Farfar var en väldigt nyfiken person men samtidigt en riktig besserwisser och
kunde lätt trötta ut folk med sina föreläsningar, även utanför skolans
väggar. Denna gen verkar vara dominant och ledde till en revolt bort från det
akademiska för min pappa och sen en kontrarevolt tillbaka till det akademiska
för mig.

Jag har två yngre systrar som står mig nära.

I min subjektiva bild av vad Garphyttan erbjöd i form av fritidsaktiviteter
kunde man välja mellan nykterismen och idrottsrörelsen. Då jag hade astma och
var minst i klassen (med undantag för lilla Anna; jag vägde 35 kilo när jag
gick i åttan) var valet inte så svårt. I korthet kan man säga att med
undantag av nykterhetsrörelsen var jag inte särskilt social av mig under
denna period av mitt liv. Jag läste helt enkelt mycket böcker.

Efter att ha gjort diverse nedslag bland de 5 hyllorna med ungdomslitteratur
som fanns på skolbiblioteket beslöt jag mig för att gå mer systematiskt
tillväga och började läsa från A (viss filtrering utifrån baksidetexterna
förekom). Efter en tid kom jag till bokserien Universums öde skriven av
George Johansson och jag fick direkt mersmak. Ganska snart var jag inne på
vuxenlitteraturen och slukade Asimov, Clarke, Lundwalls antologier Det hände
imorgon osv.

# Till Uppsala för studier och fandom

Efter att ha valt bort Kiruna och rymdingenjör på grund av dess för praktiska
karaktär kom jag fram till att jag ville läsa naturvetenskap och matematik på
ett friare sätt efter gymnasiet, dvs inte en civilingenjörslinje. Uppsala
stod för kvalitet och tradition, dessutom hade jag en släkting i staden.
Alltså blev det Uppsala och matematik, fysik, datavetenskap,
litteraturvetenskap och en liten smula retorik och filosofi. Jag ångrar att
jag inte tog mig tid och läste statsvetenskap eller sociologi eftersom jag är
mycket nyfiken på de frågeställningar som dessa ämnen behandlar. Samtidigt är
jag helt nöjd med att ha fritidsintressen som handlar om helt andra saker än
det jag arbetar med och vill inte ändra denna ordning.

Hur kom jag då i kontakt med Upsalafandom? Uppsalafandom var banbrytande och
började spamma redan 1996! Skämt åsido, så sökte Johan efter ”science
fiction” och ”Uppsala” på Altavista och fann min hemsida där jag skrev lite
kort om mina intressen. Johan tipsade om kongressen 1996 som jag av någon
anledning inte kunde vara med på. Jag i min tur tipsade istället min kursare
Åka som genast engagerade sig och drog med en mängd människor, mig
inberäknad.

# Fafierad

Jag minns fortfarande de många diskussionerna på Williams och kongresserna,
men tvingades efter ett par år till en gradvis nedprioritering som jag hoppas
kan reverseras i framtiden. Främst handlade det om att jag blev doktorand och
jobbade på tok för mycket samt att jag hittade sporten capoeira som också tog
upp mer och mer av tiden.

Jag hade alltid velat testa på någon kampsport men skrämts bort av inslag av
auktoritet och stryk, kanske omotiverat. Capoeira, med sitt sydamerikanska
ursprung, är mer en akrobatisk dans med starka kulturella inslag, t ex sång
och gemenskap. Det finns inte heller någon tydlig tävlingstradition utöver de
individuella mötena i _rhoda_ (en ring av människor som klappar händer,
sjunger och spelar instrument). Jag är särskilt förtjust i de mer akrobatiska
inslagen (även om jag inte kommit så långt själv); sök på Youtube så förstår
ni vad jag menar.

Tyvärr finns det en del konflikter mellan olika utövande grupper som till
viss del förtar glädjen. Min grupp, en lokal filial till den
världsomspännande Capoeira Brasil, upplöstes för ungefär två år sen då vår
tränare flyttade till Stockholm. Jag har inte upptagit utövandet sen dess,
men det kan hända att jag gör det i sällskap med min son Vilmer om han visar
intresse när han blir äldre.

För tillfället är jag föräldraledig och ägnar mig åt långrandiga och smått
absurda diskussioner med min 4-årige son. Till exempel: "Man måste skjuta
grisen först, annars springer den sin väg, för den vill ju inte att man skär
stjärten av den." Min snart ettåriga dotter kräver också rätt mycket
uppmärksamhet.

När jag inte är föräldraledig växlar jag mellan rollen som projektledare för
vidarutvecklingen av Uppsala Universitets studentportal och forskning inom
teknologi förstärkt lärande inom olika eu-projekt. Arbetet med
studentportalen innebär konkret att identifiera de behov som universitet har
och därefter specificera hur dessa kan förverkligas genom tekniska lösningar.
Jag ägnar mig också åt att skjuta på min disputation.

Jag forskar på hur den semantiska webben kan användas inom teknologiförstärkt
lärande (kallas även ibland e-learning). Mer konkret skulle man kunna säga
att jag forskar på system där individer har stor frihet i att utrycka sig med
hjälp av metadata, överenskomna vokabulärer, begreppssystem osv. för att
bättre kunna samarbeta och beskriva för sig själva och andra vad de vill/ska
ha/har lärt sig. Förhoppningvis leder detta till att man kan själv välja sina
verktyg och personligen hantera sitt lärande oavsett om det sker formaliserat
på ett universitet, på arbetstid i ett företag eller helt privat för sitt
höga nöjes skull. Dessutom måste man ta hänsyn till att lärandet inte är en
isolerad aktivitet utan ofta sker "just in time" eller som en del av andra
aktiviteter. Gränsdragningen mellan en väldigt omfattande vision och det som
ska in i min avhandling är som synes ännu en aning otydlig.

Den tid som blir över ägnar jag åt att programmera, se på föreläsningar
online (t ex sajterna [fora.tv](http://fora.tv) och [ted.com](http://ted.com)
är stora källor till glädje) samt förstås konsumera science fiction i olika
format.

Jag bor med sambo och två barn i Rickomberga i en fin lägenhet som dock
börjar kännas för liten.

# Engagemang

Privat är jag först och främst engagerad i min familj och de utmaningar det
innebär att uppfostra och samtidigt ge utrymme för självständighet,
nyfikenhet och kreativitet.

På ett annat plan är jag engagerad i att försvara våra rättigheter och
friheter på nätet som blir allt mer ifrågasatta av lobbyister, särintressen
och oförstående politiker. För närvarande handlar det mest om att bekämpa
FRA-lagen, IPRED, och datalagringsdirektivet. Dessutom är nya dumheter som
polismetodsutredningen och Acta (från EU) på gång. Jag har en känsla av att
debatten ofta reduceras till kampen om vilka ord som ska användas och vilken
betydelse och emotionell laddning dessa ord har, på grund av frågornas
komplexitet. Nyspråk, skeva förenklingar och konstiga liknelser är de vertyg
som används. Tyvärr leder detta till att kampen inte blir på samma vilkor då
försvararna oftast inte är lika skrupellösa (som lobbyister) eller
oförstående (som politiker), utan är noggrannare med sitt språk (t ex med att
upphovsrättsintrång är inte detsamma som stöld).

Närbesläktad är frågan hur vi hanterar kunskap i samhället i stort. Vi har
erövrat oerhörda mängder kunskap om oss själva och vår omvärld samtidigt som
vi har möjligheten att i stort sett utan kostnad obegränsat dela med oss av
den via nätet. Detta skulle kunna leda till en kulturell och samhällelig
explosion där vi gemensamt löser de oerhörda problem som ligger framför oss i
form av resursbrist, energibrist, pandemier samt mindre allvarliga sjukdomar.
Tyvär ser vi hur detta hejdas av patent och vinstintressen som är uppkomna ur
de traditionella affärsmodellerna. Jag tror inte att svaret är att kasta ut
kapitalismen, men kanske bör den kompletteras på något sätt, särskilt när det
finns starka inslag av allmännytta. Nå, jag har inga svar, men frågan
engagerar mig.

Jag är också fascinerad och lite engagerad av de många övriga kriser som
verkar hänga över oss, särskilt intressanta är finanskrisen och de
närliggande energi- och resurskriserna, då de har potential att förändra
samhället i grunden (jag rekommenderar
[www.chrismartenson.com/crashcourse](http://www.chrismartenson.com/crashcourse)
för er som orkar). Jag erkänner att jag gillar domedags-science fiction (vem
gör inte det?) och att dessa kriser är nästan lika underhållande. Men det
förminskar inte det faktum att vi verkar ha allvarliga problem framför oss
och att det vore idiotiskt att i alla fall inte utvärdera riskerna och
fundera på om traditionella skydd som att ha en hemförsäkring är nog.

# Förebilder

Så undrar Johan om det finns någon jag beundrar. Min tolkning av vad det
innebär att beundra en person innefattar att man personligen skulle vilja
besitta personens egenskaper och visioner, samt ha uppnått liknande
framgångar som den personen. För mig känns det också naturligt att det är en
samtida person.

Som första person som jag beundrar måste jag nog slå ihop tre personer i min
närhet. Det är Mikael, Anna och Magnus som varit drivande bakom nätverket
stoppaFRAlagen.nu och till viss del symboliserat FRA-motståndet. Tillsammans
har de kämpat dag och natt mot etablissemangets arrogans och inkompetens
genom att outtröttligt sätta sig in stora mängder dokument och argumentera
med folkvalda och medier om vår digitala framtid. Att denna långkörare sen
spillt över till debatten om IPRED, Acta, Datalagringsdirektivet och
Polismetodutredningen gör bara att jag beundrar deras ork ännu mer.

Den andra personen jag beundrar är Richard Dawkins för att han med seriöst
med fakta, logik och en underbar brittisk accent försvarar de icke-troendes
plats i samhället. Då en värld där vi blir allt mer beroende av varandra och
uppseglande konflikter kring våra begränsade resurser blir alltmer troliga är
mitt intryck att de sekulära och humanistiska perspektiven är viktigare än
någonsin att försvara.

Den tredje personen jag beundrar är Craig Venter för att han är en
framgångsrik entrepenör med fantastiska visioner. En person som tar sig an
att sekvensera människans genom, snabbare än en internationell allians av
forskare med gigantisk budget, och sen växla om till att försöka skapa
syntetiskt liv (och är på god väg) och därefter vilja lösa energiberoendet
genom syntetiska framställda bränslen, är som taget ur en science
fiction-roman.

# Framtiden

Om tio år lever jag och min familj i ett fortsatt kulturellt, socialt och
tekniskt rikt samhälle där vi kan glädjas åt små och stora saker. Jag har
också haft modet att bli entrepenör och satsat på en idé som kombinerar min
forskning med ett engagemang i någon viktig fråga. Jag har också återknutit
kontakterna med fandom och börjat skriva sf-noveller som ingen läser, om en
främmande civilisation där invånarna är svampar som tänker och kommunicerar
genom att deras mycel växer och samverkar i komplexa mönster.
