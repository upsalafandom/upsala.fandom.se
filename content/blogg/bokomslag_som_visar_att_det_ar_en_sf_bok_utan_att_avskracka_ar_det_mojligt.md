+++
title = "Bokomslag som visar att det ÄR en SF bok utan att avskräcka. Är det möjligt?"
slug = "bokomslag_som_visar_att_det_ar_en_sf_bok_utan_att_avskracka_ar_det_mojligt"
date = 2008-02-20

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Kåserier"]
taggar = ["bokomslag", "böcker"]
+++

Hur ska ett bra bokomslag se ut? Det har jag ofta funderat på när det gäller SF. Ett allt för påträngande omslag kan ju få den tilltänkte bokköparen att backa medan ett allt för diskret kan få en att helt missa boken. Hur ser ett typiskt SF-omslag ut? Hur vill jag att det borde se ut? Borde? Mja, jag har som sagt funderingar men absolut inga självklara svar.

För att du också ska fundera tar jag här ett litet exempel ur den flod av omslag som jag har åsikter om. Det är Elisabeth Moons bok "Command decision" som kom ut för något år sedan. Den är fjärde boken i en serie om "Vattas krig". Du kan se flera av omslagen [här i LibraryThing](http://www.librarything.com/work/1448619/covers).  Jämför nu omslaget med en militärisk kvinna som bredbent står och funderar på en plattform och det med ett rymdskepp mot en turkos-grön-silvrig bakgrund. Vilken av omslagen skulle DU välja om du skulle köpa en bok och bägge låg framme? Vilket får dig sugen på att läsa boken innanför omslaget?
