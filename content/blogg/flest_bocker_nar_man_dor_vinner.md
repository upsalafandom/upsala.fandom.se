+++
title = "Flest böcker när man dör vinner"
slug = "flest_bocker_nar_man_dor_vinner"
date = 2008-05-15

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["böcker", "läsjournaler", "readers block"]
+++

Jag har under de senaste åren fört en läsjournal, och dumt nog jämfört mig med andra läsare. Förra året blev det 30 volymer och årentinnan det 24. Naturligtvis såg det bra ut med 17 lästa verk efter 17 veckor på året. Nu skulle jag kanske klara den magiska gränsen 52 på ett år!

Detta är naturligtvis helt förryckt. Borde jag inte läsa böckerna för att jag gillar dem? Varför tävlar jag egentligen? Nu har jag dessutom insett en sak, när vi har nått vecka 20 i kalendern, och det är att jag inte "har råd" att läsa noveller! En novellsamling tar ofta längre tid för mig, eftersom jag vill idissla varje avslutad berättelse en aning och därför inte läser mer än en eller två om dagen. Det kan ju bli max 25 sidor på en dag i värsta fall!

Man behöver inte tänka speciellt länge för att se hur dumt det där är. Kanske är frestelsen att tävla helt enkelt för stor, och en del av oss borde helt enkelt låta bli att skriva ned vad vi läser? Jag ska försöka tagga ner lite, men det är inte lätt.

Det hela har sin upprinnelse i en upplevelse av "readers block" som jag drabbades av. Det var i augusti för några år sedan som jag insåg att jag bara hade läst 5 volymer under det året! Jag fick spel och satte igång att pressa så mycket lättlästa saker jag kunde hitta, och nu har jag fortfarande inte lugnat ner mig! Vissa är rädda för att bli gamla, och andra för att inte hinna läsa alla sina böcker innan de dör...
