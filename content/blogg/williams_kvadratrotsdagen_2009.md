+++
title = "Williams, kvadratrotsdagen 2009"
slug = "williams_kvadratrotsdagen_2009"
date = 2009-03-04

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Inbjudningar", "Rapporter"]
taggar = ["Square root day", "Västgötaskolan", "Williams", "pubmöten"]
+++

Ja, kvadratrotsdagar kallas de dagar då produkten av dagens datum och månadens ordningstal blir lika med årets sista två siffror. Det finns bara nio per sekel, och de är alltså 1/1 -01, 2/2 -04, 3/3 -09, 4/4 -16,  5/5 -25, 6/6 -36, 7/7 -49, 8/8 -64, samt 9/9 -81.

Vi blev cirka tjugo personer i går, eller något fler. Flera av besökarna var där för första gången. Anders A. från Västerås som hade med sig en bunt egna alster, både i ord och bild, samt en fin tvåhundraårig bok av sf-karaktär ur sitt exklusiva personliga sf-bibliotek. George, vår filosofiske amerikanske expat och flykting undan det nederländska sjukförsäkringssystemets brister. Miriam, som hängde med sin kompis Henrik för att få tips om hur hon skulle kunna få sin roman förlagd. Hans, som befinner sig i exil från Linköping i några dagar, för att undervisa i Stockholm.

Det stora irritationsmomenten på Williams är som vanligt de bisarrt klumpiga och feldimensionerade stolarna och borden, som passar bättre i ett kälkborgerligt vardagsrum än på en pub. Allt är trångt och bökigt, precis hela tiden. Maten är dock god, ölsortimentet hyggligt (fast ovanligt mycket var slut - jag undrar om de har hittat en ny ägare till slut och börjar tömma lagren?) och den beklagligtvis ensamma servitrisen var ett charmtroll som lyfte kvällen.

Lennart var där för att kränga sin Antropolis, hoppas att han blev av med något exemplar eller två. Vi diskuterade dagens medielandskap med Facebook och mobiltelefoni och allt, och försökte förklara för Saga hur det var förr i tiden, när man var beroende av uppgjorda möten för att kunna ses på stan och ofta fick gå från en uppgjord mötesplats utan att veta varför den man skulle träffa aldrig hade dykt upp.

George förklarade varför han flyttade till Sverige i januari, och hur mycket bättre behandling han erbjuds för sin sjukdom här jämfört med i Nederländerna, där han var bosatt från 1972 fram till nu. Han berättade också om sin uppväxt i 50- och 60-talens New York, om hur han började läsa science fiction - fr.a. tack vare biblioteket där han hittade böcker av Clarke, Asimov och Stapledon, och om sina favoriter bland dagens författare: Al Reynolds, Ken MacLeod, Peter Watts. George hade också med några böcker som han ville donera till Alvarfonden.

Anders A. ville visa en fin sf-bok från 1786 som han hade köpt som yngling, men hittade den inte. Efter att vi hade letat ett tag visade det sig att han hade tappat den i snön utanför, och att den där upphittats av några andra gäster på puben. Den var rejält blöt och det är bara att hoppas att den torkar och inte tar alltför mycket skada. Imponerande var den i alla fall, med en fantastisk kopparstickskarta som inklistrat utvik längst bak. Hans medtagna reproduktioner av sin bildkonst gjorde också intryck på mig. Han hade rest hit enkom för pubmötets skull och skulle övernatta på hotell innan det var dags att bege sig hem till Västerås igen.

Marie dök upp från Stockholm, men drack bara mjölk för att inte skada den främmande varelsen som växer inuti henne. (Insert obligatory Alien reference.) Vi tvistade lite om huruvida hon var Simon eller Fantomen, världshistoriens två mest berömda mjölkdrickare-på-pub. Hon fick tillbaka den **Vietato Fumare**-skylt som hon gav till mig för fem år sedan för att jag skulle skanna in den. _Host_. Sten dök också upp från Stockholm, och hade med sig en uppsättning fanzines från Tomas Cronholm, som ska skickas ut med SFF. (Fanzinen, inte Tomas.)

Hans noterade att Oscar och Stina inte var där och vi andra undrade vilka dolska fejder som gjorde att de undvek honom. Den stora chocken för kvällen var i alla fall att det avslöjades att götarna anser att Uppsala är huvudstad i Götaland. Dags att göra upp räkningen med de där fake-svenskarna. Var är alla nationalistiska politiker som försöker att vinna poäng på att spela ut svear och götar mot varandra?

Det var en massa andra människor där också som jag knappt kom åt att prata med: August, Ylva, Lena-Marie, Lennart J, Erik G, ja, försöker jag räkna upp alla så glömmer jag garanterat någon, så jag låter bli. Ni vet vilka ni är! Fast jag måste nämna att August kom dit nyförlovad och svävade på de karakteristiska små molnen. Grattis!

Jag delade ut Ansible, samlade in pengar för den som skulle skriva rapport från mötet, samt försökte värva fler skribenter till denna blogg. Att hitta skribenter till bloggen, både rapportskrivare och stadiga, gick uselt, men det gick lätt att skramla pengar.
