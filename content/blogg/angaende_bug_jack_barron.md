+++
title = "Angående Bug Jack Barron"
slug = "angaende_bug_jack_barron"
date = 2010-04-14

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["Norman Spinrad", "bokprat", "cyberpunk"]
+++

Kvällens bokprat handlade om _Bug Jack Barron_ av Norman Spinrad. Boken gavs
ut 1969 och är starkt influerad av den tidens politiska klimat,främst av den
amerikanska medborgarrättsrörelsen.

Det var inte ett så välbesökt bokprat, detta vårt bokprat för kvällen. Det
var jag och Åka som pratade, och Maria, som inte läst så mycket av boken, som
andäktigt lyssnade.

Vi lyckades ändå få en hel del sagt. Det är en bok som det finns mycket att
lägga märke till i. Mest var vi nog intresserade av de olika drag i boken som
gör den till ett protocyberpunkverk. Huvudpersonen är en tevepersonlighet,
och ett genomgående tema i boken är kontrasten mellan honom själv och sin
skärmpersona. Han kallas också för "black shade", och får vid ett tillfälle
ett par svarta solglasögon att ha på sig, fast det är nog ett
sammanträffande.

Boken handlar om nedfrysning av människor och odödlighetskurer, och vem som
har rätt till dem. Upplösningen har inget definitivt budskap, även om den har
en starkt moralisk ton. På sätt och vis är budskapet att sanningen alltid
måste fram, vilket ju också är ett cyberpunkdrag.

Nästa bokpratsbok är _The Affirmation_ av Christopher Priest, som vi ska
prata om onsdag 12 maj. (Se [kalendern](./sidor/kalendarium.md) för detaljer.)
