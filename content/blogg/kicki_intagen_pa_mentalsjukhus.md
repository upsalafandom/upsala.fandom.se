+++
title = "Kicki intagen på mentalsjukhus!!!!"
slug = "kicki_intagen_pa_mentalsjukhus"
date = 2003-02-04

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Intagen på mentalsjukhus blev ingen under det förra pubmötet, däremot lyssnade drivor av storögda neos uppmärksamt på Augusts redogörelse för Moorcocks allmänna förträfflighet.

<!-- more -->

Under tiden, vid andra änden av bordet, tittade Simon Åslund på över ett stort glas mjölk medan folk diskuterade forna stordåd. Johan Anglemark berättade tårmilt om slika hjältedåd han och Magnus Eriksson utförde under den tid de torrlagde Irlands pubar. För att skänka ytterligare trovärdighet till sin berättelse förevisade Johan stående på bordet danssteg som fått Riverdance att blekna i jämförelse. När Linnéa så småningom dök upp steg han ned från bordet med ett lätt generat ansiktsyttryck. Mer svenska dryckesvanor förevisades framgent.

Under tiden kom Petter Bergström in med en synnerligen iögonfallande T-shirt. Detta föranledde Björn X Öqvist att inleda en längre diskussion om högteknologiska 3D-animerade framtidskläder. Detta avbröts dock då Gene Roddenberry dök upp, och han bjöd honom på en öhl för sina sista pengar.

Under tiden dök Karin Kruse upp och berättade livfullt om maken Jans senaste dryckesexperiment, vit Rioja som smakar flugsvamp.

Under tiden satt några för författaren okända personer och diskuterade Ken Macleods senaste bok och hade säkert massor av intressanta saker att säga om den. Glatt inspirerade av denna roman planerade de nästa års världsrevolution.

Under tiden skedde skandalösa ting av onämnbar natur då Schwa, dvs Björn Lindström, XXXXXXXXXXXXXXXXXXXXXXX.

Under tiden spelade jukeboxen som vanligt Ace of Spades. I egenskap av metal-hippie headbangade Ante och delade ut blommor till alla iförd batikfärgad tröja direktimporterad från San Fransisco.

Av ren osannolikhet dök Lennart Uhlin upp, vilket ledde till väldigt osannolika positiva saker.

Maria Jönsson bestämde sig nu, för att avhjälpa bostadsbristen i Uppsala, genom att sticka ett hus till Samuel och sig själv.

Vid ungefär denna tidpunkt insåg krönikören att han fått i sig lite för mycket, lade ifrån sig skrivmaskinen, och gick hem för att veva ut rapporten. Han somnade därefter nöjd.
