+++
title = "Reseläsning"
slug = "reselasning"
date = 2008-07-17

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Ian McDonald", "Poul Anderson", "Ramsey Cambell", "böcker", "resor"]
+++

Så, då var det snart dags för oss att återvänta till den eviga ungdomens
stad. Naturligtvis är en av de viktiga saker man måste packa någonting att
läsa på vägen. Men vad?

Det är en svår balans det där att få med sig tillräckligt med läsbart, men
att inte släpa med sig ett mindre bibliotek. Oftast brukar jag i sista stund
stoppa ned något extra, "för säkerhets skull". Denna gång riskerar att bli
som de förra, men jag ska försöka hålla mig i skinnet.

Vad är det då som gör en bra resebok? Till att börja med måste den ju vara
lagom lång att räcka resan ut, men även en del andra kvaliteter kan vara
värda att fundera på.

Är boken en bok jag tycker verkar intressant, men inte lättläst nog att ta
slut på en gång? Kommer jag bli väldigt besviken om den blir borttappad eller
stulen? Är det en bok som kräver lite motstånd, men som kanske man kan komma
in i när man sitter en längre stund och bara kan läsa? Kan man ge bort den
till någon när den tagit slut, så man slipper släpa den hem igen?

Du har kanske fler saker du funderar över när du väljer reseläsning? Vilka är
de i så fall?

Nu ser det ut att bli _Desolation Road_ av Ian McDonald, _Long Lost_ av
Ramsey Campbell samt _The Boat of a million Years_ av Poul Anderson.
Förutom den där saken som åker med i sista minuten, förstås.
