+++
title = "Ian Graham on Gaming"
slug = "ian_graham_on_gaming"
date = 2010-11-22
lang = "en"

[taxonomies]
forfattare = ["George"]
kategorier = ["Länkar"]
taggar = ["Ian Graham", "spel"]
+++

Before I moved from Amsterdam to Uppsala, all I knew about gaming was that many people engage in it. Later I was surprised to learn how intellectually challenging and deep some of  today's games are. Several months ago I started to see items online about the possible effects (good and bad) of the Net in general and of games in particular. I know enough neuroscience to be reasonably certain that we do not know the truths of such things. A British FB Friend, fantasy writer Ian Graham, has written a perceptive piece about some of the issues. One needs no scientific background at all to read [this interesting paper](http://www.wordpunk.co.uk/index.php/2010/10/reveries-of-a-gaming-non-gamer/#more-1454).
