+++
title = "Soldiers Live -- farväl till några gamla vänner"
slug = "soldiers_live_farval_till_nagra_gamla_vanner"
date = 2008-04-11

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier", "Recensioner"]
taggar = ["fantasy", "främlingslegionen", "psykotiska trollkarlar"]
+++

Jag har nu läst slut på Glen Cooks serie om The Black Company. Sträckläst i det närmaste, faktiskt. Det känns lite sorgset att det är över, men ändå skönt att veta att det inte blir mer. Hur folk kan läsa bokserier som består av ett större antal volymer ter sig för mig ofta som lite märkligt. Vill man inte möta nya figurer? Vill man inte ha slut på intrigen så man får veta hur det går? Tappar man inte modet om man aldrig verkar avsluta berättelsen?

Vanligtvis tycker jag bäst om att läsa ganska korta och avslutade berättelser. Det har hänt mer än en gång att jag läst ett verk så fort att jag missat massa subtila detaljer, vilka sedan jag får utpekat för mig till både min och utpekarens förvåning. I själva verkar är det en ganska naturlig effekt av hur jag ofta läser. Det jag vill åt är att få veta gåtans lösning och mysteriernas utredanden. Om en berättelse handlar om en märklig artefakt som hittas på månen och antyder att det funnits intelligent liv som planterat den där för tusentals år sedan vill jag läsa så fort jag kan till slutet så jag får veta hur artefakten kom dit, och vem som gjorde det. Fanns det en parallellhandling där det förekom kommentarer och reflektioner om samhälle och politik? Fanns det en relationshistoria? Det missade jag, när jag rusade vidare efter _Mysteriets_ lösning.

Det finns naturligtvis undantag från den aningen förenklade bild jag skissade ovan. Inte alla böcker går ens att läsa på det viset. Men, att läsa en lång episk saga och gå upp i miljön och bara njuta av sceneriet, som jag vet att en del läsare gör, det gör jag väldigt sällan. Att nu i ett sträck läsa fyra volymer som utspelar sig i samma värld med samma personer var något av ett undantag för mig, och man kan ju fråga sig varför jag gjorde så.

Cooks böcker var för mig, när jag läste de första tre volymerna i serien, ett nytt namn och hans böcker en ny typ av läsupplevelse. Det var storslagen fantasy med ondingar som kunde få jorden att skälva, med huvudpersoner som inte var några duvungar de heller och det var skrivet med en kall och torr och precis stämma. Kompaniet, det sista av de fria kompanierna från Khatovar, var en mötesplats för utstötta och missanpassade och de var inte några muntra hjältar. Det skitiga och leriga soldatlivet kändes mer äkta än de idealiserade questromaner jag läst, och humorn och det faktum att det var gott om dramatiska scener gjorde att de gick hem. Det fanns klara och tydliga hot, och det gjorde ont när det gick illa och när kompaniet åstadkommit nåt så kändes det tillfredställande samtidigt som det inte på något vis var ljust, glatt och "alla levde lyckliga i alla sina dagar" i slutet.

Nu när jag läst de sista böckerna är nära nog alla ursprungliga personer i kompaniet döda. Många dog en meningslös död, en brutal död och en död långt bort från allt kan skulle kunna kalla ett hem. Man förstår att Cook nyttjar en del av den soldatromantik och det exotiska skimmer som omger [Främlingslegionen](http://www.legion-etrangere.com) när han skriver om The Black Company. Som pacifist kan man ju kanske tycka att sådant är lite fånigt, men tanken att även utstötta kan finna hemhörighet i sin lilla grupp är ju ganska tilltalande. Jag kommer faktiskt inte riktigt ihåg när jag senast känt sådan saknad när några grånade, gamla och gnälliga personer i en bok tillslut kilat vidare. Mitt bland alla palatsintriger, främmande världar och monomaniskt kämpande och psykotiska trollkarlar blev folk riktigt mänskliga. Jag fick tillslut veta hur det gick, och fick under tiden följa ett gäng knäppgökar och komma dem inpå skinnet så att jag nästan började känna mig lite hemma i den där världen, samtidigt som jag är glad att jag kan lägga den åt sidan. Livet går vidare, precis som i vår värld.

Soldiers live, and wonder why.
