+++
title = "Den ungdomen..."
slug = "ungdom"
date = 2008-07-14

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["Cory Doctorow", "identitet", "litteratur"]
+++

Jag läste nyligen [en essä av Cory Doctorow om att skriva för ungdomar](http://www.locusmag.com/Features/2008/07/cory-doctorow-natures-daredevils.html). Han skriver där att unga människor "really use books as markers of their social identity". Det var ganska fint formulerat, även om jag inte är säker på att jag vet vad det betyder i verkligheten alla gånger. Fast jag minns en tid då jag alltid hittade naturare (såna som gick naturvetenskaplig linje på gymnasiet) vart jag än kom, och man vanligen kunde prata med dem om miniräknare (snabbt uttömt, men kärt samtalsämne) och bolla gamla skämt ur _Liftarens guide till galaxen_. De där böckerna hade verkligen ett starkt värde som gemensam referensram.

Eftersom jag aldrig har knutit mig själv särskilt starkt till någon särskild musikstil har böcker alltid varit mycket viktigare än musik när jag försökt definiera mig själv. [Du är vad du läser](http://www.duarvaddulaser.se)? Kanske det.

Jag har säkert nämnt någon gång att jag blev lite förbluffad när en kille berättade att William Gibson gav honom en identitet, men sedan insåg att det var helt rimligt och vettigt. Killen var någon sorts datornörd, som när han läste lite cyberpunk insåg att det fanns ett sätt att se på det som han gjorde som coolt.

Några andra reflektioner? Hade ni andra några böcker som gick laget runt i bekantskapskretsen i tonåren, eller som blev kult på något plan?

(Egentligen hade jag tänkt skriva om mina första planer på att skriva en sf-roman i 14-årsåldern, och vad de stupade på, men det får bli nästa gång.)
