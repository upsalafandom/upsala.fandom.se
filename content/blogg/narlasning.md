+++
title = "Närläsning"
slug = "narlasning"
date = 2009-04-16

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["Lägg till ny etikett"]
+++

Det är väldigt fascinerande att följa med i [Kate Nepveus omläsning av Lord of the Rings](http://www.tor.com/index.php?option=com_content&amp;view=blog&amp;id=13692). På ett helt annat sätt, men i samma omfattning, är det kul att läsa [den mycket grundliga sågningen av Left Behind på Slacktivist-bloggen](http://slacktivist.typepad.com/slacktivist/left_behind) (observera att man måste läsa baklänges om man vill börja från början, krångligt nog). Jag kan inte låta bli att både beundra och fascineras av den här typen av närläsning. Ibland är ju kommentarerna väldigt givande också, det är fördelen med att göra sånt här i bloggform. (På tor.com finns också på samma sätt omläsning av Wheel of Time och omtittning av Star Trek, men de har inte intresserat mig eftersom jag inte riktigt tagit del av de här verken till att börja med.)

Skulle jag kunna göra något liknande? Jag har grunnat på det ibland. Man vet ju aldrig. En vacker dag, om jag fastnar för något värdigt föremål för uppmärksamheten.
