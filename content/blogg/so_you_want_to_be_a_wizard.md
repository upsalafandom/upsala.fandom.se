+++
title = "So You Want to Be a Wizard"
slug = "so_you_want_to_be_a_wizard"
date = 2008-06-11

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Recensioner"]
taggar = ["Diane Duane", "fantasy"]
+++

1-3/8 i år äger [sf-kongressen](./sidor/vad_ar_fandom.md) [Stocon
08](http://stocon08.warp10.se) rum i Stockholm. Hedersgäst är science
fiction- och fantasyförfattare Diane Duane, vilket är anledningen till att
hennes ungdomsfantasybok _So You Want to Be a Wizard_ är vad som skall
diskuteras under kvällens [bokpratsmöte](./sidor/bokprat/index.md). _So You
Want to Be a Wizard_, som gavs ut för första gången 1983, inleder Duanes
serie _Young Wizards_, men kan utan problem läsas fristående utan att man
fortsätter med de övriga böckerna i serien. Lyckligtvis, eftersom det gott
och väl räcker med en bok.

Egentligen kanske det inte beror så mycket på boken som på att jag har svårt
för några av de drag som utmärker många ungdomsböcker, som hur storyn
fungerar: inom ramen för den överhängande handlingen får bokens båda
huvudpersoner, Nita och Kit, en rad problem som alla klaras av på ungefär
lika många sidor och där det, hur hotande de än är, alltid går att ta sig
igenom svårigheterna genom någonting protagonisterna tidigare lagt sig till
med, oavsett om det är en månskensrönnkvist eller en tacksam Lotus Esprit.
Det känns lite som ett tv-spel: ett gäng banor de skall ta sig igenom för att
komma fram till och besegra bossen. Det är ett berättande som kräver mer
delaktighet från min sida för att faktiskt engagera mig.
