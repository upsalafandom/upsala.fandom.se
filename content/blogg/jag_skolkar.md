+++
title = "Jag skolkar"
slug = "jag_skolkar"
date = 2008-05-26

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Länkar"]
taggar = ["meta"]
+++

Hej alla glada Upsala-fans! Idag har jag inte hjärnceller över till den här bloggen, eftersom de allihopa är sysselsatta med annat (klä om stolar, bada barn, redigera fanzine, skriva recension, få galna idéer om vad man skulle kunna konstruera av gamla väckarklockor...). Jag får lite mindervärdeskomplex ibland när jag ser de riktigt ambitiösa bloggarna på webben, de som lyckas skriva flera djupa och välformulerade inlägg om dagen, själv orkar jag inte på långa vägar posta om alla intressanta saker som passerar min hjärna. Jag vill stanna upp och suga på tankarna först, och det hinns inte alltid riktigt med.

I stället för att skriva något idag skickar jag er vidare till  [kolumnisterna på SciFi.com](http://www.scifi.com/sfw/column/index.php), de skriver också en massa intressanta saker som jag oftast inte tar mig för att läsa.
