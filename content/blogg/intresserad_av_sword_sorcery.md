+++
title = "Intresserad av sword & sorcery?"
slug = "intresserad_av_sword_sorcery"
date = 2008-03-05

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Länkar"]
taggar = ["noveller", "sword &amp; sorcery", "webbmagasin"]
+++

Jag hittade en länk till ett magasin med s&amp;s berättelser. De finns i tryck och på nätet. Kanske kan vara värt att besöka?

[Flashing Swords
](http://flashingswords.sfreader.com/titlepage.asp)
