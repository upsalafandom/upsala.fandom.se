+++
title = "Paging Mr. Murphy"
slug = "paging_mr_murphy"
date = 2008-02-12

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["meta"]
+++

Jag undrar om jag någonsin har varit med om att lansera en ny webbplats utan
att servern har brutit samman just när man gått ut och meddelat världen att
de kan komma och titta på den nya skapelsen.

Murphy är den engelska formen av det gäliska efternamnet
[Surnames](https://en.wikipedia.org/wiki/Surnames) _Ó Murchadha_ eller _Mac
Murchaidh_ - ättling eller son till _Murchadh_ (som betyder sjökrigare - man
undrar om det ursprungligen var vikingar som åsyftades? I så fall är Murphy
en av oss, och det är ju passande i sammanhanget.)

I vilket fall är Upsalafandoms webbplats uppe igen, och vi får hoppas att den
så förblir. Det ska ha varit en kritisk säkerhetsuppdatering av Linuxkärnan
som sänkte servern. Själv tänkte jag äta semla ikväll.
