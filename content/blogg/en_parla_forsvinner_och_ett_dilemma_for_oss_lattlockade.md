+++
title = "En pärla försvinner och ett dilemma för oss lättlockade"
slug = "en_parla_forsvinner_och_ett_dilemma_for_oss_lattlockade"
date = 2008-04-25

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["absurditeter", "böcker", "frestelser"]
+++

Vardagens små märkligheter fortsätter att förbluffa mig. För ett litet tag sedan såg jag att en av Kingstons antikvariat hade utförsäljning och skulle läggas ner. Har man bott ett tag i Upsala så ter det ju sig otänkbart att inte vara omgiven av ett flertal ställer som säljer gamla, dammiga, rara eller bara udda böcker. På det viset är Kingstons stadskärna mycket trevlig, för här finns gott om möjligheter att titta på böcker, och att nu en av dem ska försvinna känns väldigt tråkigt. Att just detta antikvariat dessutom har en rejäl samling med fantastik är gör ju det ännu tråkigare. Nu är det halva priset på allt i butiken, och vi som inte tänk slå ner bopålarna alltför djupt frestas alltså att köpa böcker för halva priset. Hur ska man kunna motstå?

Nu kommer det som gör situationen riktigt absurd. Varför slår denna boklåda igen? Ja, det är det gamla vanliga om att folk inte köper mycket nog från oberoende försäljare utan vänder sig till de stora kedjorna. Det är förstås inte så konstigt, men ändå lite tråkigt. Jag har vid tillfälle faktiskt valt att köpa saker lokalt till lite högre pris för att det ger mig möjligheten att ha en lokal butik. Jag önskar fler tänkte så. Det riktigt tråkiga är dock att lokalen nog kommer att stå tom framöver! Tydligen är det så att man i Kanada precis som i Sverige betalar fastighetsskatt. Dock har de här den mycket udda regeln att om en butikslokal står tom, så får du en rejäl rabatt på skatten! Det är ett mycket märkligt sätt att stimulera en levande stadskärna...

Jag har redan sett ut en bunt med böcker av Poul Andersson, men frågan är ju om jag ska leta efter nåt nytt och okänt nu när det är billigt värre. Eller kanske hålla igen, och inte samla på sig än mer man måste skeppa till Sverige eller avyttra när vi ska lämna landet. Det är inte lätt att vara "bookaholic"!
