+++
title = "Märkliga främmande världar"
slug = "markliga_frammande_varldar"
date = 2009-03-17

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Glen Cook", "M.A.R. Barker", "R. Scott Bakker", "Tekumel", "asiatisk kultur", "fantasy"]
+++

Jag satt och tittade tillbaka på vad jag läst under 2008. En av de böcker jag
läste var R Scott Bakkers _The Darkness That Comes Before_. Som den del av er
säker vet är det del ett i en serie om tre (i sin tur bara en bit av hela
sagan, naturligtvis), som det så ofta är numera. Boken var inte en odelat
positiv upplevelse, men det var inte dålig. Signifikant är kanske däremot
varför jag fick för mig att läsa den. Vi hade nämnligen pratat om fantasy jag
och Robert, som jag spelar rollspel med. Av okänd anledning, kan ha nåt med
Steven Erikson att göra, pratade vi om fantasy som inspirerats av andra
kulturer än europeisk medeltid. Då kom det sig så att jag lånade Bakkers
böcker av Robert.

Fantasy med lite nya inspirationskällor brukar intressera mig, och en annan
sådan läsupplevelse kan man finna i den serie böcker av Glen Cook jag också
läste förra året. Böckerna i _Glittering Stone_ utspelar sig i en miljö som
snarast för tankarna till indien och centralasien. Sånt är lite skoj, tycker
jag. Kanske är det inte så illa med inspiration från det hållet. Vem tycker
t.ex. inte att Kali är riktigt ruskig? (jo, jag vet att det finns folk i
indien som dyrkar henne som en snäll modersfigur. Inte min typ, kan jag bara
säga)

Så en annan fantasy med lite udda miljö är professor M.A.R. Barkers skapelse
_Tekumel_. Professorn han använt denna sin värld som rollspelsvärld sedan
sjuttiotalet, då någon introducerade honom till Dungeons & Dragons.
Barker är precis som Tolkien språkprofessor och har naturligtvis kokat ihop
alfabet och ord för sina främmande kulturer. Det exotiska med hans skapelse
är inte bara att det handlar om en grupp med människor som från ett framtida
samhälle lämnar jorden och koloniserar en främmande planet långt borta.
Dessutom är människorna i huvudskap från sydamerika och sydostasien, så den
kultur de exporterar till stjärnorna är allt annan än den vardag vi känner i
västerlandet, eller ens kan extrapolera bakåt eller framåt ur vår historia.
Det är trappstegspyramider, ett myller av gudar och en samhällstruktur som
inte uppfanns i europa.

Naturligtvis har folk skrivit en massa saker om den rika värld och dess
exotiska kultur. Det finns t.o.m. en APA! Professorn, som nu är pensionerad,
utvecklar fortfarande världen och skriver om den i romaner (jag läste en
förra året, riktigt hygglig), språkliga utläggningar och rollspelssessioner.
Varje torsdag träffas de i Barkers källare och spelar spel och utforskar
världen Tekumel. Klart fascinerande! För den nyfikne på klart annorlunda
fantasy kan jag rekommendera en titt på
[Tekumel!](http://www.tekumel.com/index.html)
