+++
title = "Nomineringar till Hugopriset 2008"
slug = "nomineringar_till_hugopriset_2008"
date = 2008-03-22

[taxonomies]
forfattare = ["Jesper"]
kategorier = ["Nyheter"]
taggar = ["Filmer", "böcker", "fantastikpriser"]
+++

Den här veckan blir det inget [Bokslut 2007](/taggar/bokslut2007), då jag glömde
listan med böcker när jag reste hem till mina föräldrar över påsken. Istället
kan jag meddela att de nominerade till årets Hugopris nu har
[tillkännagivits](http://www.denvention.org/hugos/08hugonomlist.php).

Romankategorin är den som åtminstone jag tycker är av störst intresse, men
den ser tyvärr inte jättespännande ut i år. Ian McDonalds _Brasyl_ och
Michael Chabons _The Yiddish Policeman's Union_ verkar vara
intressanta romaner, men utöver dessa är det inget som direkt lockar.
Noterbart är att Charles Stross är nominerad i denna kategori för för femte
året i rad; vi får se om detta utnötningskrig kan betala sig till slut.
