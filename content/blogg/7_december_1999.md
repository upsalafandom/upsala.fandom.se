+++
title = "7 december 1999"
slug = "7_december_1999"
date = 1999-12-07

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

På andra sidan skogen satt en kråka, men på denna sida satt Upsalafandom när det 36:e pubmötet i Orvarsmöten TNG avhölls på Williams krog i tisdags kväll.

<!-- more -->

Jag anlände till ett Williams som låg begravt i illavarslande slask. Som vi alla vet är snöns enda uppgift att producera slask. Hur fint och vitt det än månde te sig, så slutar det alltid med slask. Liksom alla människor måste dö, måste all snö smälta. Inuti den relativt slaskfria lokalen var det fantomt, men redan efter fem minuter anlände Karl-Johan Norén, tätt följd av Åke Bertenstam och Magnus Eriksson. Mötet var igång.

Magnus skulle ha flyttat i början av december, men denna flytt blev inställd. Det visade sig att anledningen var att chauffören, en bekant till Magnus svärfar med bräcklig hälsa, drabbats av en elakartad influenseliknande attack och med största sannolikhet kommer att ligga däckad ett tag. Alla krafter som vill hälpa Magnus att till fots flytta hans bohag 3 kilometer i Heby ombeds ta kontakt med honom. Om Magnus själv ska utföra flytten beräknas den vara klar någon gång i juni, förutsatt att vädret inte jävlas allt för mycket.

Möjligen kan hugade flyttare som belöning någon gång i februari få smaka Jolkkonens då färdigjästa öl, som går mot färdigjäsning i en takt motsvarande en halt elefants språngmarsch i djup kvicksand. Betydligt större lycka har Jolkkonen haft med tekniskt trolleri, som att rädda data från en 12 år gammal Atariformaterad SyQuestskiva. Om fru Jolkkonens mors sydländska utseende talas det tyst, Formellt är hon 100 % finsk-ugriska. Jojo.

En litet frö till split på puben brukar kunna vara ljudvolymen i lokalen. På Williams är de dessbättre relativt lyhörda för vad man tycker. King of the fairies, som de har i sin jukebox, får de gärna spela på hur hög volym som helst vad mig anbelangar, men annat skräp som stör konversationen betackar jag mig för och gjorde så även i tisdags, varpå personalen snällt sänkte volymen något (även om de hävdade att de hade andra gäster som kom dit för att lyssna på musik). Må det.

Har du varit arbetslös hela ditt liv och lider av psykisk störning? I så fall ska du prata med en bekants bekant till Jolkkonen som länge lidit av just dessa problem. Samhället har funnit ett användningsområde även för honom och utbildar honom just nu till syokonsulent. Heja staten! Däremot är det ingen idé att vända sig till Jolkkonen för att köpa amfetamin, eftersom råvarorna för amfetamintillverkning numera är belagda med svåra försäljningsrestriktioner.

Höger-vänsterskalan är ju ett populärt samtalsämne varhelst kannor stöps och även så på Williams. Åka förklarade att hon finner att symmetriska ansikten ser konstiga ut, stick i stäv med samtida vetenskap som funnit att symmetri är attraktivt. I sammanhanget må nämnas att frågan om huruvida Simon skulle vara mer eller mindre odräglig om han vore fysiskt tilldragande ställdes. Sten konstaterade också att man borde tillverka pennor i par; till varje högerpenna skulle det följa med en vänsterpenna. Då skulle man nämligen med hjälp av strumpeffekten kunna vara säker på att alltid ha en penna kvar och aldrig förlora båda. Detta gäller tyvärr även handskar, muttrade Åke som nyligen råkat ut för ett handskrelaterat missöde.

Nå. för min del hade kvällen börjat dra sig mot sitt slut när Olle stormade in och viftade med en värja. Han spetsade elegant Karl-Johans fanzine till publikens förtjusning och avbröt mig och Ante i vår diskussion om Jim Morrison. Ante hade sagt sig vara ganska gramse på den förljugna kulten av det tragiska människoödet Jim Morrison, och gav Michael Petersén en släng av sleven.

När jag trädde ut genom dörren, ut i slasket, pågick fortfarande en livlig diskussion om man skulle gå någonstans och spela poker eller inte. Magnus, hur gick det? Du dök aldrig upp hos mig, så jag hoppas att pokerpartiet blev av.

Science fiction, då, grymtar en surmulen serconist i Bräcke, diskuterar ni aldrig science fiction? Jodå, käre surmulne serconist, vi konstaterade att när det gäller Dicks författarskap så är att läsa en bok detsamma som att äga den.

Ex cathedra,

Johan

Vi var där: Johan Anglemark, Åke Bertenstam, Magnus Eriksson, Andreas Gustafsson, Mikael Jolkkonen, Joacim Jonsson, Gabriel Nilsson, Karl-Johan Norén, Sten Thaning, Kristin Thorrud, Anders Wahlbom, Anna Åkesson, Björn X Öqvist, Olle Östlund.

Nästa möte är som vanligt första helgfria tisdagen i månaden på Williams; i detta fall 4 januari 2000.
