+++
title = "Det var en Shakespearesk afton"
slug = "det_var_en_shakespearesk_afton"
date = 2002-10-01

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

...när den upsaliensiska skiffyskocken flockades kring öltapparna på det lilla gästgiveriet i Ubbohuset. Allt var som vanligt, jag hade med mig Minotaurer till försäljning samt ett nytillverkat Obduktion och Åka hade med sig backish av Folk och fans och överex av lite spridda zines som hon hittat i sina lådor. Dessutom låg där några utskrifter av Ahrvid Engholms faanfiction Storm i Fantarktis.

<!-- more -->

Williamsburgare med pommes frites beställdes in och när de anlände till bordet förklarade Therese att även om hennes pommes frites förvisso var jordnötter (vilket skulle vara lite svårt att förneka), så tänkte hon ändå sätta gaffeln i den hand fick för sig att förse sig av dem. Den blodsockerlåga blicken injagade skräck och respekt i bordsgrannarna, så de pommes frites hon inte ville ha lämnades att kallna på den övergivna tallriken under ett tjockt, kolesterolstint lager sauce béarnaise.

En blick i Obduktion påminde Åka om nästa bokpratarmöte, då vi ska diskutera Hope Mirrlees Lud-in-the-Mist. Åka har redan börjat läsa den och varnade alla - med en blick kastad åt Antes håll - för att hasta igenom den för fort, eftersom den innehåller mycket som bör avnjutas ordentligt och inte bara skummas, exempelvis skildringar av träd och natur i olika årstider (om jag minns och hörde rätt).

Simon gjorde entré och överräckte till Samuel något som bestämt måste ha varit en mindre handgranat av den här typen som tyskarna har i amerikanska krigsfilmer, alltså en som ser mer ut som en ficklampa än en ananas. Ingen drog ur någon sprint ur den eller räknade till tre, så läget kändes inte särskilt farligt, trots allt. Dessutom är Simon en pålitlig karl när det gäller motorer och vapen, fast jag tvivlar på att han är någon stjärna på att sprinta.

Jukeboxen tog upp temat från TV-serien Helgonet och det undrades hur det förresten skulle bli med det där Prisonertittandet hemma hos Nils. Nils förklarade sig villig att arrangera kvällen bara någon sparkade lite grann på honom, men frågan var om vi ville se bara de sju första eller alla sjutton avsnitten i en maratonsittning. Frågan bollades lite fram och tillbaka i ett parti table volleyball men utmynnade inte i särskilt mycket och rätt vad det var fick Karl-Johan syn på en övergiven halvpint Guinness. Tidigare på kvällen hade Guinnessglas flyttats lite fram och tillbaka i takt med att hela havet stormade, och just detta glas hade en gång tillhört Åka, som sedermera trott sig ha förlorat det för alltid och köpt sig en lager som tröst. Hon betvivlade sin förmåga att dricka både denna halvpint och den halvpint lager som stod framför henne med Pripps Blå-logotypen på sned, och förklarade att hon gärna skänkte bort den rumstempererade avslagna Guinnessen till någon annan.

Karl-Johan grep glaset och utannonserade raskt en fanfondsauktion, låt oss så här i efterhand kalla den för STUFF, the Stockholm-Upsala Fan Fund, avsedd att frakta Ylva Spångberg till ett upsaliensiskt pubmöte, utfodra henne med en öl och sedan sätta henne på tåget hem igen. Halvpinten såldes för 32 kronor, vilket jag (köparen) tyckte var ett fynd för en öl som förlorat såväl sin obehagliga källarkyla som de sista resterna av irriterade kolsyra, och det andra auktionsobjektet - Björn Lindströms cd "I helvetet en tid", som jag spelar i skrivande stund och får mig att inse varför genren heter noise - drog in 25 kronor. Till detta kom de pengar som alla som var med i men inte vann budgivningen tvingades betala; slutsumman blev över 180 kronor tror jag, så det är bara för Ylva att boka in tisdagen den 5 november.

Böcker är fina, men är man amerikansk doktor får man inte ha det fint. Eller för att förtydliga mig: sedan andra världskriget trycks inte amerikanska doktorsavhandlingar automatiskt längre, meddelade Åke som ett à propos i en annan diskussion. Upprörande. Dessutom har fransmännen nyligen börjat med liknande tilltag, och tecken tyder på att även Sverige är på väg in i den bokfientliga snålfållan. I bästa fall kan man nuförtiden få avhandlingarna i de berörda länderna tryckta på beställning i enstaka exemplar. Det var tyvärr inte det enda som var upprörande: Magnus Eriksson saknades oss åter. Magnus, kom tillbaka, vi saknar dig. Allt är förlåtet! Många andra ansikten som vi saknar skulle kunna nämnas, Kristin, Joque, T&J med flera, men en frånvarande fan som orsakade konkreta problem var Björn X som trots löften att dyka upp (det senaste avgivet tio minuter innan mötet började) uteblev och hindrade Maria från att ta mått på de vantar hon stickar åt honom. Den late fryser om händerna, som Aisopios säger (och här kommer Bellis hävda att namnet stavas helt annorlunda. Jaja.)

Det sades mer om böcker. Torbjörn efterlyste ett Sverifandomfattande bokutlåningssystem där man kan se vad andra fans och föreningar har för böcker, eftersom alla bra böcker som han får nys om undantagslöst brukar visa sig vara utlånade, förkomna eller aldrig ha funnits. Jag tipsade om Linköpingsfandoms bibliotek samt förevisade min och Linnéas bokdatabas, men det vore ju skoj om alla svenska fans böcker fanns katalogiserade i en gigantisk databas. Jag undrar hur många böcker vi har tillsammans? Om
vi lite löst utgår från att det finns 250 aktiva svenska fans, kan det bli 250.000 böcker tillsammans? Det finns ju vissa som drar upp snittet något alldeles hemskt, men de flesta har väl betydligt färre än tusen per person.

Vid det här laget upptäckte Ante Luna som varit närvarande i en timmes tid och utbrast att Luna måtte vara en sån där smygande SMOF, en som tar över hela fandom i hemlighet utan att någon märker det. Jag är lite osäker på bevisföringen, men Luna förnekade det i alla fall inte så förmodligen stämmer det inte. Det konstaterades i sammanhanget att IB absolut inte finns längre och Jan Guillou är enligt Ragnar inte medlem i några körer överhuvud taget, med undantag för Regensburger Domspatzen och Wiener Sängerknaben. Och det känns ju betryggande att veta.

Bakom Williams disk stod som vanligt en ny blondin, eller så var det en gammal blondin i ny tappning eller en halvgammal blondin som nyligen tappats. Sysslar de med vit slavhandel på det här stället? undrade Jonatan och Dennis nyfikna. Ett indicium på detta skulle kunna vara den fina lapp på en ölkran som förkunnade att de inte sålde John Bull på tapp utan istället slinkor. Så tolkade jag det i alla fall, på en engelskspråkig pub.

Och så lovade Maarten att ta oss ut på belgisk ölkväll på Ghuben om vi bara e-postar honom och spikar en tid. Dags att plocka fram kalendrarna och ghrubla!

Ex pulpetra

Johan

Nästa möte är som antyds den 5 november, även denna gång på Williams kl. 18.30 och framåt. Alla är välkomna! Och skicka Upsalafanniska nyheter till mig och Ante, så kanske de kommer med i nästa nummer av Obduktion.

Vi var där: Johan Anglemark, August Aronsson, Åke Bertenstam, Jan Buckli, Andreas Davour, Anna Davour, Ragnar Hedlund, Torbjörn Josefsson, Maria Jönsson, Dennis Lindgren, Björn Lindström*, Jonatan Lindström*,
Daniel Luna, Gabriel Nilsson, Karl-Johan Norén, Nils Segerdahl, Maarten van Wildemeersch, Therese Wikström, Stefan Zgrablic, Samuel Åslund, Simon Åslund.

WAHF: Marie Engfors och Sten Thaning, som via e-post dagen innan meddelade att de avsåg att ha pubkväll i Turin samtidigt som vi skulle sitta på Williams.

WACHBN: Mícheál Ó Flaithearta och Michael Srigley, med vilka jag kort diskuterade Yeats, poesi, skapande, ockultism och det falnande intresset för iriska i Upsala.

* ej släkt
