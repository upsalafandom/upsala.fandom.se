+++
title = "13 januari 1998"
slug = "13_januari_1998"
date = 1998-01-13

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

75 kg tjurtestiklar och Chicken vindaloo var bara två av de sällsport
fascinerande samtalsämnena när Upsalafandom slöt upp vid öhlpumparna
O'Connors för första gången i år för att skölja julen ur fjädrarna.

Först en sammanfattning. Vi var fjorton stycken sammanlagt, vilket är rekord.
Två fanzines delades ut. Och nu över till väsentligheterna.

<!-- more -->

Först avdelningen sorgliga nyheter. Zorro, Kristins och Tobbes katt, kördes
på av en smitare tisdagen den 6 januari, och avled senare på djursjukhuset av
skadechock och (antagligen) svårare inre skador än förmodat. Han fick
skelettfrakturer och lungskador, men bedömdes ha en 40 %-ig chans att klara
sig. Han avled emellertid efter några timmar under observation. Mer dramatik
var det under själva mötets inledning, då utryckningsbilar vräkte sig fram
genom gatmassorna nedanför pubfönstret, men ingen brandrök kändes någonsin,
och efter några minuter återvände stegbilen snopet med svansen mellan benen
så det var kanske bara ett falsklarm. Möjligen hade de hört att Anders
"bordspyromanen" Wahlbom, Jan "pyrex" Nyström och Andreas "bensinbombarn"
Gustafsson var i faggorna men tog fel på adressen.

Kruse labioalveloarsmackade nedlåtande (vad **heter** "tut-tut" på svenska?)
och jämförde med när Uppsala Energis torvlager brann någon gång 1989–90, då
hon och en vän for dit i bil med ett paket grillkorv och bröd och frågade de
vaktande poliserna om de fick komma in och grilla? Det var en brasa det! Som
trivia kan ju detta vara kul att veta, men kommer "tri via" verkligen från
romarnas sätt att anslå meddelanden i trevägskorsningar? Det är vad Molle
fått berättat för sig skrev hon till mig häromdagen. Ingen av oss runt bordet
visste säkert, men Kristin visste däremot att de gamla grekerna brukade offra
lök till Hecate vid just trevägskorsningar.

Hecate heter inte många nuförtiden. Inte Gullspira Skelleftiana heller, men
det namnet känner (eller kände) Kruses far någon hemifrån Ångermanland som
ståtade med. Jisses. Att uppkallas efter en get och en stad. Fler intressanta
namn som samma källa visste att berätta om var Seagull (uttalas som det
skrivs - på svenska) efter bonden som var inne i hamnstaden och såg sitt livs
vackraste skepp och prompt skulle döpa dottern efter det, och Tittit och Bobo
Harding, som misslyckades med att hyra bil i USA eftersom biluthyrarna
vägrade tro att de uppgav sina riktiga namn.

Kvällens citat kom även det från Kruse som hade kastat ögonen på _Handbok
i barnindoktrinering_ och därifrån burit med sig den vackra uppmaningen
"Berätta tråkiga saker om bilar och berätta spännande saker om gräs!" Om fler
gjorde det skulle det inte finnas några krig förmodligen, vi skulle alla
sitta snällt på ängen och idissla. Ungefär som vegetarianen Andreas. Hans
vegetarianism sträcker sig emellertid inte till Chicken vindaloo, det är för
gott för att avstå från. Tjurtestiklar, undrar alla nu, när kommer de? Det
var Jolkkonen som berättade att 75 kg tjurtestiklar var en tämligen typisk
ingrediens i vissa biokemiska experiment. Science fiction, undrar kanske
någon annan. Pratar ni aldrig om science fiction i Upsalafandom? Jo, det
händer, när samtalsämnena tryter.

Starship Troopers hade folk sett och sågade glatt. Sten (tror jag) citerade
tjejen två rader framför honom på bion som reste sig upp efter filmen och
sade förundrat (inte alls ilsket) "Oj! Det var nog den sämsta film jag sett i
hela mitt liv!". Harlan Ellisons namn nämndes som tänkbar hedersgäst på vår
nästa kongress, och vi skrattade gott åt Ellisons min när han upptäckte att
den fina inbjudan han fått var till en liten sak i en svensk småstad med 70
deltagare.

Andreas berättade om ett projekt han har hållit på med en längre tid, att
räkna ut pi med n decimaler och låta 10=A, 11=B osv, och sedan hitta
sekvensen ELVISISALIVE så många gånger som möjligt. Andreas upplyste också om
att Nietzsche är det enda kända exemplet på någon som var expansivt galen
utan att omedelbart gå överstyr som läkarvetenskapen känner från hela
1800-talet. Månne är det det han menade med übermensch?

Psykologtråden fick en vacker avrundning när Sten beskrev sin mönstring.
Psykologen var en herre med pipskägg och glasögon nedskjutna på näsan som
talade med tysk brytning. Sten lyckades hålla sig för skratt, men inte utan
ansträngning. Vid detta lag hade universum närmat sig sin undergång så pass
mycket att jag och Linnéa drog oss hemåt.

ex pulpetra

— _Johan_

Vi var där: Johan Anglemark, Magnus Eriksson, Andreas Gustafsson, Mikael
Jolkkonen, Linnéa Jonsson, Karin Kruse, Jan Nyström, Matthias Palmér, Lisa
Sjöblom, Lennart Svensson, Sten Thaning, Kristin Thorrud, Anders Wahlbom,
Björn X Öqvist.

Nästa möte blir på samma plats (O'Connors pub, ovanför Saffets på Stora
torget) från 18.30 och framåt, tisdagen 2 februari.

Väl mött!
