+++
title = "Liveblogging Confuse"
slug = "liveblogging_confuse"
date = 2008-06-13

[taxonomies]
forfattare = ["Nea"]
kategorier = ["Rapporter"]
taggar = ["Confuse", "Swecon", "böcker"]
+++

Några meter till höger om mig håller Johan på att prismärka böcker för Alvarfondens antikvariats räkning. Böckerna kommer från en stor donation av (mestadels) "mjukfantasy" som Alvarfonden fick genom ett arv för en liten tid sedan. Runt bordet med böckerna står en intresserad klunga människor som ömsom kommer med spydiga kommentarer om titlar och omslag, ömsom köper böcker i stora travar och diskuterar olika författares relativa förtjänster (eller brist på förtjänster). Några meter åt andra hållet ligger den kombinerade registreringen och baren, där en annan klunga står och hänger och dricker öl och kaffe. En halvtrappa ner ligger programrummen. Just nu pågår en hedersgästintervju som jag egentligen hade tänkt gå på, men jag blev så sömnig att jag satte mig här uppe i stället.

Förra programpunkten var en panel om en sf-kanon, inte en musikalisk utan en litterär sådan. Det blev en ganska bra diskussion tyckte jag, som både berörde vad en kanon egentligen är (är det bara "bra" böcker som ingår, eller kan man säga att Normans Gor-böcker är kanoniska på grund av att de är så usla att alla borde läsa 20 sidor från en Gor-bok för att se hur djupe människan kan sjunka?), vem eller vilka som skulle kunna definiera vad som skulle ingå i en sf-kanon, om det är författare eller verk som blir kanoniska, och om genren är tillräckligt gammal för att det över huvud taget skulle gå att kanonisera vissa böcker.

Oj, nu kom femtio fans uppvällande - intervjun måste vara över. Alla kanske inte är här, men rätt många iallafall, och det är kul! Kul är också att se hur folk omedelbart graviterar till bokbordet och börjar prata böcker med alla som står omkring: "Eva, du har läst denhär författaren, vad säger du?" "Ha, nu har jag fyllt på min samling kvalitetslitteratur!" "En halvnaken karl OCH en enhörning, vad kan man önska sig mer? *fniss*" "Den där boken? Nja, del ett i serien var rätt hyfsad men jag tycker den har gått utför".

Kongressen hålls i Nationernas Hus mitt i Linköping. Det är å ena sidan trevligt att vara inne i Linköping i stället för i Ryd, så att man kommer åt restauranger och sådant; å andra sidan har det varit skolavslutning i dag, så stan är full (i flera bemärkelser) av folk i studentmössor och vita klännningar eller kostymer, och de hörs väldigt väl in i kongresslokalen. Vårt hotell ligger ett stenkast härifrån, alldeles vid ett torg med en gigantisk uteservering. Lyckligtvis har Johan och jag ett rum som vetter mot en sidogränd.

Nu blev jag invecklad i ett kort intermezzo med ett par finnar som beskrev svårbegripliga lekar med svärande mobiltelefoner. På det hela taget en väldigt trivsam kongress såhär långt.
