+++
title = "Vi vandrade i Sweconskuggans dal"
slug = "vi_vandrade_i_sweconskuggans_dal"
date = 2003-09-02

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Tisdagen den 2 september var det dags för det 81:a rekonstituerade Upsaliensiska pubmötet som denna gång drog inalles 20 personer. Dessa voro J. Nicklas Andersson, Johan Anglemark, Åke Bertenstam, Martin Dalsenius, Andreas Davour, Anna Davour, Marie Engfors, Mikael Jolkkonen, Torbjörn Josefsson, Maria Jönsson, Daniel Luna, Gabriel Nilsson, Sten Thaning, Erik Trulsson, Kalle Walestrand, Tomas Winbladh, Samuel Åslund, Simon Åslund, Biörn X Öqvist och Ollie Östlund. Kilgore Trout Jr var också där enligt närvarolistan, men jag såg aldrig till karln.

<!-- more -->

Jag var inte först för Nicklas Andersson satt i ett hörn och väntade på mig när jag ramlade in på Williams denna allt annat än sensommarlika tidiga kväll. Jag hade misstänkt att han skulle ha mig sig en kartong böcker åt Alvarfonden och jag hade rätt. Att jag bara **misstänkte** det och inte **visste** det förvånande Nicklas eftersom jag faktiskt, påminde han mig om, hade bett honom att ta med den. Ah, javisst ja. Snart därefter ramlade fler fans in, däribland Kalle och Martin, ett par entusiastiska nykomlingar som lockats dit av Magnus och Maries bejublade uppträdande på universitetets recentiorsdag veckan innan.

Jag beslöt mig för att följa den pålitlige Winbladhs exemepl och beställa in en Staropramen, men det skulle vi inte ha gjort, för den smakade inget vidare. När vi klagade fick vi varsina nya som besynnerligt nog smakade klart bättre, men inte så bra som de borde. Skit i slangarna? Vem vet. På det hela taget har Williams blivit ett betydligt drägligare hak allteftersom de har utökat ölsortimentet. Numera har de ett brett register och priserna är inte värre än någon annanstans. (Utom på studentnationerna, förstås. Än så länge, för de ska förmodligen snart momsbeläggas. Ett hårt slag mot studentalkoholismen, men jag antar att privata fester kommer att få ett uppsving.)

Sten vill få fört till protokollet att han inte nämnde Nethack en enda gång, trots att vi ägnade tid åt att med frenesi diskutera Diplomacy och rollspel, inspirerade av nytillskottet Martin som visade sig vara göteborgare med ett förflutet i såväl Sveriges ädlaste del som i Luleå. (Sverige består nämligen av följande delar från söder till norr: Den ädlaste delen, en massa barrskog, Linköping och sjöar, och därefter Norrland.)

Märkliga smakdomare fördes på tal; Värnpliktsnytts videorecensenter, systemvetare och Jonas Wissting. Systemvetare befanns vara moraliskt oklanderliga, även om idén med en datautbildning för folk som vill slippa närkontakt med datorer sågs med skepsis. Jonas Wissting slapp däremot undan med en imponerad vissling; han har nämligen tagit ett nummer av Stens sax & PC, satt det i LaTeX och skickat Sten resultatet med uppmaningen att fortsättningsvis använda detta layoutprogram. Sten vägrar och håller fast vid Word. Sålunda utkämpas dagligen små duster mellan det stora imperiet och de fria programmerarna. Sten utvecklade sin tes vidare: "Jag tycker väldigt mycket om... jag tror att det var amerikanska försvaret." Ögonbryn höjdes.

Religion och EMU nämndes i största hast (nja, EMU diskuterades halvlänge, men jag ska bespara er den diskussionen så här i dessa de ytterligaste av dagar), religion framför allt eftersom Luna fick på sig Marias tjusiga hatt och genast liknade en fanatisk jude. ("Menar du inte ortodox?" "Är inte det samma sak?" Nej. Ö. a.) Den hastigt mosaiske Luna beklagade att han inte hittade till lärarhandledningarna på Valsätraskolan; de verkade vara slugt nedgrävda på diverse obskyra ställen. Den arbetssökande matematikdoktorn O. Östlund visste dock att berätta mer om dessa gömmor och satt och ritade skattkartor till den tacksamme Luna. (Lär man sig automatiskt var lärarhandledningar finns om man är topolog?)

Sten förklarade att han drömmer om framtida stora Upsalakongresser och nekade frankt Åka rätten att avlägga prov för doktorsgraden i fysik förrän tidigast hösten 2005. Alternativt börja jobba någonstans där man kan hålla en stor kongress. Eller i Dublin. Vi diskuterade statyer, såväl de i Dublin millenniemonumentet och Floozie) som de i Washington DC, som föreställer dygder men förmodligen är modellerade efter konstnärens prostituerade bekanta. Att höra ordet Irland nämnas tände den ikonoklastiska glöden hos Jolkkonen som med hetta förklarade att Cromwells enda misstag på Irland i allmänhet och i Drogheda i synnerhet var att han inte var tillräckligt noggrann. Det visade sig att Jolkkonen tycker om länder vars namn börjar på S men inte länder vars namn börjar på vokal. (Bakgrunden var diverse blackguards och patetiska rånare som han råkat ut för på den gröna ön.)

Sedan hände inte så mycket mer värt att nämna, öl dracks och folk småpratade till dess att nästan alla hade gått hem. Därefter gick nog resten hem med, men det vet jag strängt taget inte så mycket om, för då var jag på väg i säng i mitt hem. Under natten drömde jag att jag och Magnus satt på något slags lektion, en studiecirkel eller något, och frågan om vad anarkism är kom upp. Handledaren blinkade åt oss och gav ordet åt en medelålders kvinna som skulle berätta, för hon var nämligen den enda arvtagerskan till den lilla frihetligt socialistiska organisation som bildades när hela styrelsen för Alvarfonden avgick någon gång i början på åttiotalet, med Harald Berthelsen och Janne Forssell i spetsen, för att bilda en politisk kampgrupp. Senare i drömmen muterades ursprunget till att ha varit hela kommittén för en tidig NaSaCon, för jag drog Magnus oroligt i skjortärmen och sade att vi måste göra något åt den där kvinnan; hon var ju en vandrande kongresskommitté och kunde kapa vilken kongress som helst om
man inte såg upp.

Jag anklagar Williams dåliga Staropramen!

Ex pulpetra

Johan

Nästa möte äger i laga ordning rum tisdagen den 7 oktober. Samma plats, samma kanal.
