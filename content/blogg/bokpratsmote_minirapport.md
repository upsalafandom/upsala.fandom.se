+++
title = "Bokpratsmöte – minirapport"
slug = "bokpratsmote_minirapport"
date = 2008-06-12

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Rapporter"]
taggar = ["Diane Duane", "bokprat", "böcker", "fantasy"]
+++

Igår var det, som [påpekat](./blogg/so_you_want_to_be_a_wizard.md),
bokpratsmöte hemma hos Jesper. Närvarande var Jesper, undertecknad, Björn och
och Biörn X. Vi skulle prata om Diane Duanes _So You Want to Be a Wizard_,
vilket vi också gjorde i någon mån. Ingen var förskräckligt imponerad –
episodisk, för enkel utveckling, för enkla lösningar, könsrollscementerande
och överdrivet kursiverad var några av omdömena som fälldes. Fungerande
språk, intressanta miljöer och konstaterandet av vi nog tyckt bättre om den
om vi befunnit oss någonstans i närheten av målgruppen var förlåtande
faktorer. Ryktet säger att hennes Star Trek-böcker är bättre.

Mer tid ägnades förmodligen åt Neal Stephenson, hans idéer, hans förmåga att
slänga in så många som möjligt sådana i en och samma roman och hans oförmåga
att låta bli att skriva om udda religiösa sekter. Dessutom Cory Doctorow,
J.K. Rowlings språk och värld och roliga namn på saker och ting, vilken
science fiction som egentligen säljer, Homeland Security och amerikanska
underrättelsetjänster.
