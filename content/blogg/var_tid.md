+++
title = "Vår tid i backspegeln"
slug = "var_tid"
date = 2008-09-28

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["kultur", "lajv", "tidsresor"]
+++

Sen jag fick den i huvudet första gången har den här tanken kommit tillbaka till mig gång på gång: undrar hur det skulle vara att färdas till framtiden (hoppa över tiden emellan, alltså) och hamna i ett lajv eller något reenactment-sällskap som handlar om vår egen tid. Det skulle förmodligen vara en konstig blandning, och en del detaljer som vi se som ganska perifera skulle stå ut som väldigt viktiga. Har ni sett några såna där roliga teckningar som framställer människokroppen omskalad efter hur mycket känsel man har i olika delar? Jättestora läppar och fingrar, korta små ben.  Att se sin kultur omskalad på det sättet, efter hur stor betydelse olika saker haft för en specifik senare epok, det skulle kunna vara väldigt kul -- och lite läskigt.
