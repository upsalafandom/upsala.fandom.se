+++
title = "Några intryck från Con*Cept"
slug = "nagra_intryck_fran_concept"
date = 2008-10-20

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Rapporter"]
taggar = ["David Brin", "Montreal", "kongresser", "vetenskap"]
+++

Här kommer en sammanfattning av mina intryck från
[Con*Cept](http://www.conceptsff.ca).

**Det var litet.** Det vill säga, det var en rätt stor kongress jämfört med dem
jag själv arrangerat, men mindre än de flesta kongresser av den sort den här
försöker vara. Antalet medlemmar var kanske 200-300, utspridda på fyra
våningar i hotellet. Fyra programsalar, ett konstutställningsrum med
fanzinehörna, en con suite, ett spelrum, ett dealers room, ett programrum för
barnen, och ett green room (dit programdeltagarna kan gå för att förbereda
sig eller bara slappna av). Consviten (det är ett rum med läsk, snask och
smörgåsar, där det är meningen att folk ska kunna sitta och umgås) låg
faktiskt ganska centralt, men den var inte särskilt utnyttjad ändå. Många
människor satt i korridoren där registreringsborden stod, men man såg aldrig
särskilt många på samma ställe samtidigt. Programsalarna såg alltid lite
halvtomma (om inte nästan helt tomma) ut.

<!-- more -->

**Jag kan göra ett bättre program.** Nu dömer jag hårt med ganska litet
underlag, men både Ad Astra i Toronto i våras och den här kongressen hade
allvarliga problem med att få programmet att fungera. Det hände ofta att
panelister inte dök upp där de skulle vara, eller att de inte riktigt visste
vad panelen skulle handla om. Det verkar väldigt valhänt hanterat, och när
jag frågade lite verkar det inte vara ett engångsproblem. Inte gick det att
veta vad programpunkterna skulle handla om heller, för det enda som stod
tryckt någonstans var rubrikerna, som ibland inte var så beskrivande.
"Writers and writing". Vad handlar det om? "Vi skulle ha haft med det i
programboken, men vi hann inte", förklarade en av arrangörerna. Förutom det
tycker jag också att programmets litterära del var ganska svag, men det
kanske hade sett bättre ut om jag faktiskt kunnat lista ut vad varje punkt
handlade om.

**Montreal laddar för världskongressen**. Det är väldigt uppenbart. Det
handlade mycket om att informera om värdskongressen, och se till att alla får
veta vad de behöver. Väldigt många av de närvarande är involverade på ett
eller annat sätt. Det kändes kul, förväntan hänger redan i luften.
Ordföranden var extremt entusiastisk, och pratade också gärna om varför
fandom är så bra och hur roligt fans kan ha medan de gör vettiga saker.

**Starkt vetenskapsinslag**. Kanske för att David Brin var det stora namnet,
men också för att den lokala amatörastronomiska föreningen RASC har goda
kontakter i fandom, och för att det finns några andra lokala entusiaster som
drar i såna här saker. Det fanns ett föredrag av en kvinna som jobbat med
simulerade mars- och månresor, till exempel. Hon berättade lite om praktiska
arrangemang, och hennes tvåveckors fälttest av kosthållning för deltagarna i
en sådan här simulerad marsresa. (Fast de fuskade litegrann, och hade
sophämtning och vattenleveranser bland annat.)

**Nätverk**. Jag var verkligen inte särskilt utåtriktad den här helgen, och det
kändes väldigt mycket som en kongress där alla känner alla och pratar om sina
interna projekt. Ändå hade jag flera väldigt trevliga konversationer, och
träffade folk som jag nog kommer att ha kontakt med igen. Jag stötte också på
några fanzine-bekanta, som jag mötte första gången på Ad Astra. En del trådar
jag plockade upp kommer nog att vävas ihop i den lokala lilla
proto-fangruppen här i Kingston. Jag känner mig jättenöjd, trots att
egentligen inte alls lyckades umgås med folk egentligen.
