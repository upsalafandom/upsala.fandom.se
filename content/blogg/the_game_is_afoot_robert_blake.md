+++
title = "The game is afoot, Robert Blake!"
slug = "the_game_is_afoot_robert_blake"
date = 2008-06-18

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Recensioner"]
taggar = ["H.P. Lovecraft", "London", "Sherlock Holmes", "pasticher", "victoriana"]
+++

För ett tag sedan gick vi här i Kanada igång ordentligt på victoriansk
kultur, gammal och nygammal. Åka har sin fascination för steampunk, och jag
har återupptäckt den främste av detektiver, Sherlock Holmes.

När jag var ordentligt mycket yngre, kanske runt 12 år gammal, slukade jag
den första boken om Sherlock Holmes -- En studie i rött -- och fastnade
direkt. Det dimmtäckta Londons gator utövade en förtrollande charm på mig,
och jag kände att detta var en civiliserad tid av gentlemen och proper
engelsk Kultur(tm). En stor mängd böcker av Jules Verne och Conan Doyle
senare och jag var totalt dränkt i det sena artonhundratalets kolonialistiska
idévärld. Jag har nog aldrig riktigt slutat att fascineras av den sedan dess.
Nu sprang jag nyligen på ett par böcker som fick mitt slumrande intresse att
vakna igen.

Nicholas Meyer skrev 1974 en bok som på svenska publicerades under titeln
_Den sjuprocentiga lösningen_. Det är en Sherlock Holmes pastisch, som
berättar vad det egentligen var som skedde under den tid då Holmes försvann
efter att han drabbat samman med Moriarty och han ansetts som död. Den läste
jag ett bra tag efter det att jag slukat allt Conan Doyle skrivit, och efter
det att minnet av dessa historier hunnit sjunka undan en aning. Någon gång
hade jag väl försökt att läsa om en favorit, men de framstod som formelartade
och dammiga på ett sätt som fick mig att tro att jag vuxit ifrån dem. Meyer
lyckades dock åter få gnistan att tändas och den gamla charmen att återvända.
Ganska nyligen så sprang jag på ännu en an Meyers pastischer _The West End
Horror_, som denna gång inte befann sig på deckarhyllan utan under
rubriken skräck. På den hyllan fanns en annan volym, som är den som jag nu
precis avslutat och tänkte orda lite om.

På framsidan möts vi av en klatschig blurb med den oförglömliga frasen
"Sherlock Holmes enters the nightmare world of H.P. Lovecraft"! Att
Lovecrafts värld inte är samma sak som hans fiktiva värld, och att en fiktiv
person knappast kan besöka en verklig persons värld verkar ha gått
blurbförfattaren totalt förbi. Boken är en antologi med mer eller mindre
lyckade noveller om Holmes och hur han löser märkliga fall där märkliga
varelser och kulter, i stil med de i Lovecrafts verk, figurerar.

Hur funkar det då? Jo, ganska hyggligt. En typiskt berättelse av Doyle är ju
ganska formelartad, och det är inte så svårt att bygga mer arkana och
suspekta hemligheter som bas för mysterierna. I princip alla berättelser är
Holmes väl bevandrad med den occkulta världen, och känner t.ex. till
Necronomicon. Nog hade det varit mer effektfullt om Holmes tvingats
konfrontera sin logik mot det oerhörda som Lovecrafts varelser representerar?
En av författarna gör dock något litet extra av sakerna och det blir först på
sista sidan klart hur mycket han vänt upp och ned på ens förväntningar. Sånt
gillar jag. Som affischnamn ser det ju inte tokigt ut heller att ha Neil
Gaiman först ut i listan av förattare, men tyvärr tyckte jag inte att hans
bidrag var ett av de bättre. Det finns flera andra kända författare, som
Elizabeth Bear och Brian Stableford, men jag såg inga tecken på att de
författare jag kände till skrev bättre eller sämre. Tim Lebbon skrev kanske
den mest obehagliga av dem alla och hans övriga produktion kanske kan vara
värd att håla utkik efter.

På det hela taget är det riktigt charmigt med att läsa om den viktorianska
eran. Det är en era av upptäckarlust och framstegstro som är främmande för
dagens samhälle. Väldigt annorlunda är ju också det strikt skiktade
samhället, med var person på sin plats på stegen. Detta börjar ju dock komma
tillbaka i dagens Sverige så det kanske bara känns hemtamt. Dock gillar jag
den lite långsammare värld som porträtteras i Holmes äventyr. Det är inte
fråga om att vara uppkopplad hela tiden, eller att skicka SMS eller tjata i
mobiltelefon. Ska man åka tåg till en avsides by för att godsherren där bett
en komma och lösa ett mysterium så läser man en dagstidning, röker pipa eller
tänker kluriga detektivtankar. Det är tilltalande. Naturligtvis kom det sig
så att jag en dag, medan jag fortfarande höll på att läsa _Shadows over
Baker Street_, när jag passerade förbi bordet med billighetsböcker
utanför Indigo råkade se en bok för $5 som hette _Sherlock Holmes - the
man and his world_. Det visade sig vara en biografisk skiss över Holmes
och hans era. Mycket trevlig liten skrift med fina bilder och en intressant
vinkel på den viktoriska erans liv och leverne som om Holmes faktiskt
existerat. Nu vet jag mer än någonsin tidigare om England under 1900-talets
gryning!

Nu är då frågan om jag ska ta mig an originalet. Vågar jag läsa om Conan
Doyles berättelser, och kanske förstöra de fina minnen jag har av dem? Det är
ju enklare att vifta undan pastischer som formelartade och banala. Jag känner
mig dock riktigt sugen, om inte annat för att nu kunna se dem som dokument
över denna svunna era om inte annat. Vid horisonten reser sig dock ett stort
moln, jag har börjat bli sugen på att läsa om en mastodont. Silmarillion. Vi
får se vad det blir. Man har ju sällan problemet att man inte har något att
läsa i alla fall.
