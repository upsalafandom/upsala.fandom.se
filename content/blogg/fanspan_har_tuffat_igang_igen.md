+++
title = "Fanspan har tuffat igång igen"
slug = "fanspan_har_tuffat_igang_igen"
date = 2008-07-07

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Nyheter"]
taggar = ["Fanspan"]
+++

Jag har det inte alltid lätt att motivera mig att uppdatera Fanspan, med tanke på hur många bloggar det numera finns som rapporterar nyheter från sf-fältet. Finns behovet fortfarande? Nåja, jag har börjat skönja lite mer obunden fritid, så då passade jag på att uppdatera Fanspan med sådant som har inträffat senaste månaden, kongresser, Tolkienistjubileer, Alvarpriset, filmfestivalnyheter och annat.

[http://sfweb.dang.se/a/span.html](http://sfweb.dang.se/a/span.html)
