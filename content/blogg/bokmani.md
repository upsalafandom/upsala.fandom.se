+++
title = "Bokmani"
slug = "bokmani"
date = 2008-04-21

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["LibraryThing", "bibliofili", "bibliomani", "böcker"]
+++

[Precis som i Åkas fall](./blogg/manga_bocker_blir_det.md) kretsar mina tankar runt böcker för närvarande. Jag har ett både komplicerat och okomplicerat förhållande till böcker. Jag gillar böcker, jag gillar att läsa, jag gillar de fysiska exemplaren. Bibliotek var magiska platser för mig som liten, kanske för att vi inte hade några enorma mängder böcker hemma. Något hundratal, inte fler. Ganska normalt, antar jag.

Jag kan egentligen inte motivera varför jag vill behålla volymerna efter att jag har läst dem. Visst är det praktiskt att kunna plocka fram en bok igen och slå upp något man letar efter, men det är mest ett svepskäl - det är egentligen inte värt den plats de tar. Det rummet skulle kunna användas till mycket annat, och man kan som bekant ingenting ta med sig när man dör. Men böcker är något slags kombination av snuttefilt och statussymbol för mig, antagligen en direkt följd av hur jag såg på böcker som liten. Det är nog säkrast att fortsätta att samla på sig dem, för sinnesfridens skull.

Att tankarna löper i bibliofila banor just nu beror på att jag till slut har börjat att föra över vår bokkatalog från FileMaker till [LibraryThing](http://www.librarything.com/catalog/anglemark). Vårt exemplar av FileMaker Pro är gammalt, en ny licens är dyr, och så är det ett meckel med att exportera posterna så att jag kan lägga upp dem på webben. Och på webben vill jag ha dem, så att jag kan komma åt listan enkelt när jag är ute och reser och står och begrundar huruvida vi redan har en bok eller inte. Vad jag däremot inte insåg innan jag började, är hur viktigt det är för mig att ha omslagsbilder på så många böcker som möjligt i katalogen. De tar alldeles för lång tid att leta reda på i de fall där det inte finns någon färdig bild i LibraryThing, men jag lägger ändå ned den tiden. Suck.
