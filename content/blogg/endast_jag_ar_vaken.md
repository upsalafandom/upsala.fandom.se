+++
title = "\"Endast jag är vaken\""
slug = "endast_jag_ar_vaken"
date = 2008-12-13

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Nyheter"]
taggar = ["Catahya", "noveller", "skräck"]
+++

För ett par veckor sedan [påpekade](./blogg/catahyapriset_for_2007.md) jag att [Catahya](https://catahya.net/) delat ut sitt årliga novellpris, som gått till Johan Theorins novell "Endast jag är vaken" – som trycktes i en antologi som är i princip omöjlig att få tag på längre (själv läste jag den en natt hemma hemma hos [en av jurymedlemmarna](http://www.vildvittra.se) efter att ha firat hennes fördelsedag, till exempel).

Nu har novellen lagts upp på [Catahyas hemsida](https://catahya.net//litteratur/endastjagarvaken.html), så att de som vill kan läsa den.
