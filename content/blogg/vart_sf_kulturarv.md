+++
title = "Vårt sf-kulturarv"
slug = "vart_sf_kulturarv"
date = 2008-09-22

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = []
+++

Jag läser en bok. Här är en rad:

> covers. The washroom feels like a sleek spaceship, a milennium

Fast nästa rad börjar inte alls med det ord som min hjärna förväntar sig.
Nästa ord är "away".

Ganska lustigt. Säga vad man vill om Star Wars, men vist har de ursprungliga
filmerna lämnat sina bestående avtryck i vår kultur. I min hjärna också!
