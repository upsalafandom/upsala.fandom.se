+++
title = "Upsala i mina inälvor"
slug = "upsala_i_mina_inalvor"
date = 2008-03-03

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["Upsala", "antikvariat"]
+++

Jag vet inte riktigt vad jag tycker om Upsala. Slätten staden ligger på är platt som slätter plägar vara och jag vet inte om hon, staden, är vacker. Enskildheter är rackarns tjusiga, som kvarteret Historicum–konsistoriehuset–Gustavianum, och jag tror aldrig att jag har vandrat på en mysigare protestantisk kyrkogård. Men strängt taget tror jag nog att min uppväxtstad var vackrare, i alla fall som hon var när jag växte upp. Sedermera har fula byggnationer besudlat henne.

Jag upptäckte för några år sedan att jag får en fantastisk sensawundah-känsla av att stanna till vid Dômen några minuter på väg hem från Williamsmötena och ställa mig nedanför ett av tornen och titta rakt upp. Jag älskar också årummet om hösten. Ja, strängt taget är den tid då löven börjat gulna och rödma, innan novemberregnens malande tagit vid, den vackraste av alla i staden.

Men egentligen är det inte den fysiska staden som fått mig att stanna här, vore det fysiska attribut som styrde bodde jag nog i Lund redan. Det är människorna. Tolkiensällskapet och de alldeles fabulösa, fantastiska fester jag varit med om i sällskapet, pubmöten och irländska festivaler med sf-fans, kongressplaneringarna. Upsala är där alla mina vänner bor. Och antikvariaten. Alla Upsalas sjutton antikvariat – eller hur många de är numera – fulla med gamla luntor, sf, vitterhet, historia. Böcker att köra ned nosen i och dra i sig doften av gammalt papper. Viktor finns inte längre, men de andra är kvar, med fornsaxiska grammatikor, reseskildringar från 30-talet och debattskrifter mot 1906 års stafningsreform.

Antikvariatsrunda, anyone?
