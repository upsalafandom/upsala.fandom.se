+++
title = "Bokprat: Dying Inside"
slug = "bokprat_dying_inside"
date = 2010-03-10

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["Dying Inside", "Robert Silverberg", "bokprat", "telepati"]
+++

Kvällens bokprat behandlade _Dying Inside_ av Robert Silverberg, som en del
av vårt pågående tema, science fictions bortglömda mästare. Deltog gjorde
Anna Davour, Jesper Svedberg, George Berger och jag själv.

Boken handlar om en man som lever i New York i mitten av sjuttiotalet
(författarens nära framtid), och försöker hantera sin trytande telepatiska
förmåga. En del av oss tyckte att den påminde om vår förra bokpratsbok, _Camp
Concentration_, bland annat i det att den båda försöker behandla det som
_verkligen_ händer i allas vårt inre.

Ett annat tema är frågan om huruvida kunskap om andras tankar verkligen
skulle hjälpa oss att förstå andra. Är huvudpersonens egna attityder och
fördomar för starka för att hans förmåga ska kunna öka hans förståelse för
andra?

Jag tror att vi alla tyckte om att läsa boken, och fascinerades av dess
genomtänkta struktur. Flera av oss (särskilt Åka) hade gjort observationer de
andra missat.

Nästa bokprat handlar om _Bug Jack Barron_ av Norman Spinrad. Se
[kalendariet](./sidor/kalendarium.md), [e-postlistan](./sidor/epostlista.md) eller
[Facebook](http://www.facebook.com/event.php?eid=397916361368) för detaljer.
