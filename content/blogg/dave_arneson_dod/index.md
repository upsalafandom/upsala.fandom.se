+++
title = "Dave Arneson död"
slug = "dave_arneson_dod"
date = 2009-04-09

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Minnesrunor"]
taggar = ["BNF", "Blackmoor", "Dave Arneson", "rollspel"]
+++

![Porträtt av Dave Arneson](dave_arneson.png)

Vila i frid, Dave

Lite mer än ett år efter det att Gary Gygax lämnade oss har nu den andre giganten från rollspelshobbyns gryning, Dave Arneson, också förlorat sista striden mot sin cancer. Nu har alltså mannen som skapade konceptet Dungeons i _Dungeons & Dragons_ tillslut lagt tärningarna på hyllan.

Som jag sade förra året har alla kulturer någon sorts BNF, hjälte eller föregångsfigur, vars visioner en gång lade grunden till det man idag lever i och uppskattar som en del av ens liv och vardag. Ibland glömmer vi bort att tacka dem medan de ännu är ibland oss för vad det gjort, eller vi kanske inte ens inser hur mycket vi har föregångarna att tacka för det vi idag tar för givet. Jag kommer osökt att tänka på vad jag läste i _Banana Wings_ där Mark skriver om hur han under flera år avfärdat Jack Speer som en fan som inte gjorde annat än att kritisera folks stavning i FAPA utskick. Naturligtvis insåg han när Jack väl vad död hur mycket han gjort, och hur mycket han betydit för fandom. Tyvärr är jag rädd att David Arnesons frånfälle kommer bli precis ett sådant tillfälle att upptäcka att det var han som låg bakom alla de där saker som andra senare slog mynt av och gjorde till allmängods. Saker är ju den att många av de innovativa greppen med den nya typ av spel som rollspelen var vid 1970-talets början var just Arnesons uppfinning. Själv var han helt enkelt inte den som gjorde någon större affär av det. 

Precis som jag skrev för ett år sedan är det oräkneliga sätt som dessa kreativa genier influerat så mycket av poulärkulturen och därmed även flera av de som idag skriver fantasy eller skapar fantastik på tv, film eller i datorspel. Det sistnämnda gäller väldigt konkret för Arneson, som under senare år undervisat i speldesign för datorspelsutvecklare. 

Själv har jag blivit inspirerad av Daves _Blackmoor_ kampanj för mitt eget rollspelande. Faktum är att den inte bara är den längsta kontinuerliga rollspelskampanjen (1970-2009), utan även en skådeplats för tre generationer rollspelare som i sin fantasi skapat och upplevt förunderliga äventyr i främmande världar. Drömmar och visioner om äventyr binder samman oss alla som läser, skriver och spelar fantastik. Drömmen lever.

> Uther, Once And Always
>
> The Father of Roleplaying is dead.
>
> Requiem aeternam dona ei, 
> Domine: et lux perpetua luceat ei
