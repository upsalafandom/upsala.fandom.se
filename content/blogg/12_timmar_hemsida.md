+++
title = "12 timmar hemsida"
slug = "12_timmar_hemsida"
date = 2008-02-10

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Kåserier"]
taggar = ["meta"]
+++

Idag har vi ägnat 12 timmar åt att bygga klart Upsalafandoms nya hemsida.

Vi har mödosamt flyttat över allt relevant material från de gamla sidorna, och en del irrelevant.

<!-- more -->

Närvarande var förutom jag själv Jesper, Johan Anglemark, Kakan, Luna, Maria Hagelin, Maria Jönsson, Samuel, Zrajm, Filemon och Jonathan.

Jag och Johan koncentrerade oss på att flytta gammalt material till den nya sidan, och  Johan skrev lite nya sidor för att presentera oss.

Nicklas Andersson telependlade, och hjälpte till med den nya designen av sidan, och Zrajm deltog lokalt i samma strävan.

Jesper skrev beskrivningar av bokpratsmötena och pubkvällarna.

Övriga närvarande stod till tjänst med matlagning och spridda klagomål.

Vår förhoppning är att den nya hemsidan ska bli en mer integrerad del av vår verksamhet. Rapporter från pubkvällar, bokprat med mera kommer att hamna här.
