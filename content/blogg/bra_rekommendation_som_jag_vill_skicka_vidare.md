+++
title = "Bra rekommendation som jag vill skicka vidare"
slug = "bra_rekommendation_som_jag_vill_skicka_vidare"
date = 2008-03-12

[taxonomies]
forfattare = ["Katrin"]
kategorier = ["Kåserier"]
taggar = []
+++

Härom veckan blev jag rekommenderad att läsa böcker av författarinnan [S. L. Viehl](https://en.wikipedia.org/wiki/S._L._Viehl). Snabb som jag är beställde jag 10 st böcker från SF-bokhandeln som alla verkade lockande, bland annat en serie med 8 st böcker om Dr Cherijo. Rymdresor, nya världar, aliens och en kvinnlig läkare mitt uppe i smeten var något att sätta tänderna i. I början av [den första boken](http://www.sfbok.se/asp/artikel.asp?VolumeID=43217) blev jag lite rädd över att jag redan hade funderat ut hela intrigen (vilket ju är ganska vanligt...) men ack vad jag bedrog mig! Historien tar nya vändningar hela tiden och karaktärerna utvecklas åt nya håll som inte ens jag med min fantasi kan fundera ut på förhand. KUL!

Visst, det är ingen djupare SF men ibland behöver jag bara bra och underhållande läsning, speciellt när jag som nu har det jobbigt med familjemedlemmar och vänner som alla råkar ut för svåra och tunga saker och när mitt arbete dessutom är extra krävande. Då orkar jag inte med att läsa böcker med allt för stort allvar och tyngd.

Jag är nu precis i början av den fjärde boken i serien och härligt nog har jag behållit mitt intresse för böckernas alla karaktärer. Vissa aspekter är ganska uppskakande, till exempel all tortyr som förekommer i den tredje boken, och skildrat så grovt och ingående att jag mådde illa, men tonen i böckerna tilltalar mig ändå och jag ser fram emot många fler timmar med underhållande läsning.
