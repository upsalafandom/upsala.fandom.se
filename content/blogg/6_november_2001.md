+++
title = "6 november 2001"
slug = "6_november_2001"
date = 2001-11-06

[taxonomies]
forfattare = ["Sten"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Mitt lilla papper, där jag hade antecknat allt roligt, intressant och viktigt som hände på williamsmötet i tisdags, finns inte. Åtminstone har jag gett upp försöken att hitta det, så jag tänker istället skynda mig att rapportera från minnet. Alla eventuella felaktigheter är alltså inte mitt fel, utan den onda fysiska världens.

Raskt alltså över till rapporten:

<!-- more -->

Tjoho! För första gången i upsalafandomtisdagsmötenas historia var jag först till ett dylikt! Anledningen till denna häpnadsväckande bedrift var enkel: jag hade lovat att möta Timmy och Saija klockan halv sju, för att ledsaga dem till Williams. Trots tappra försök lyckades jag inte gå vilse, så fem över halv sju intog vi ett bord och tittade oss förundrat omkring i den tomma lokalen. Det **var** väl tisdag? Jo, det var det, vilket bevisades av att Emanuel Berg plötsligt stod vid bordet och undrade om det var vi som var fandom. Det var vi, tyckte vi. Saija tillhör visserligen Jyväskyläfilialen av upsalafandom, och Timmy blev inte assimilerad förrän i tisdags, men det är betydelselösa detaljer. Lättad slog sig Emanuel ner och förklarade omedelbart att fantasy bara går att läsa om man är en flicka i yngre tonåren. Detta hindrade oss inte från att faktiskt diskutera lite fantasy, och snart började fler människor droppa in.

Jag hela-havet-stormade runt bordet, som bara verkade växa och växa mellan varje förflyttning. Swish, så diskuterade jag datorer med de vanliga misstänkta. Swish, så förevisade Simon stolt sin motorcykelhjälm för mig. Swish igen, så sade plötsligt Jolkkonen: "Du pratar om Nethack, va? Nej, jag hörde inte ett ord av vad du sade, jag bara antog det". Jolkkonen känner mig. Eller så är jag bara förutsägbar. Ännu ett swish, så satt jag mittemot Anna, som beskrev sitt jobb med "Jag översätter Wendy. Om du känner till denna tidning så vill jag inte prata med dig." Jag gladde henne med att Wendy tillhör kategorin saker som jag inte känner till, och fick veta att det var en tidning för hästtokiga flickor i tioårsåldern. Jag funderade ett tag på huruvida det faktum att jag numera kände till Wendy diskvalificerade mig från ett fortsatt samtal, och kom fram till att jag förmodligen gjorde bäst i att glömma bort denna referens så fort som möjligt. För att uppnå detta skaffade jag mig en ny Guinness - alkohol lär vara bra för att glömma, har jag hört. Eller hur det nu var.

När jag kom tillbaka till bordet fann jag mig lyssnandes på Maria, som hade blivit intervjuad om vilka dagstidningar hon läser, samt hur ofta hon har sura uppstötningar. Vi väntar nu med spänning på resultatet av denna undersökning - ger DN eller SVD flest sura uppstötningar? Spelar det någon roll om man hoppar över ledarsidan? Har seriesidan någonting med läsarnas hälsa att göra?

BjörnX tar inga risker. Han läser inte dagstidningar alls, men hade med sig några direktimporterade Bruno-album som han stolt visade upp. Spridda utrop av beundran hördes från dem i sällskapet som han redan lyckats frälsa. De andra lovade att titta på http://www.brunostrip.com för att bli hängivna Bruno- älskare de också. Börja från början, var vårt råd till dem. Eller på , det är ett annat bra ställe att börja på, men då kan man ju lika gärna börja från början och läsa även de fyra månader som kommer innan.

I takt med att kvällen blev kvälligare och kvälligare blev stämningen gradvis högre. Den enda sång jag hörde stod dock Johan Anglemark för, alldeles i början, då han gladde finnconarrangören Saija med att ljudligt (eller iallafall ljudlikt) mässa "Finncon, finncon, finncon" i olika tonlägen.

Vad ska man se när man är på besök i Uppsala? Tja, man kan ju inte missa Uppsalas största turistattraktion, den 1,4 ton tunga stenbollen som ligger och flyter på villavägen. Så vi gick givetvis dit, och jag fick ännu en gång bekräftat för mig att alkohol har en negativ inverkan på balanssinnet.

Åtminstone Marie Engfors, BjörnX, Saija, Timmy och Jorrit var på Williams, för de befann sig plötsligt på något sätt hemma hos mig och drack te. Jag vill minnas att det fanns fler uppsalafans där, men närvarolistan finns som sagt inte längre. Nåja, den var iallafall oläsbar.

Nästa möte hålls tisdagen den 4/12. Se till att vara där, så kanske just **du** blir vald till kongressarrangör för höstens uppsalakongress!

— Sten
