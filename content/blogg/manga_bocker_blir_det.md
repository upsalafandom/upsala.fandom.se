+++
title = "Många böcker blir det..."
slug = "manga_bocker_blir_det"
date = 2008-04-20

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["böcker"]
+++

 Inledande förklaring: Jag tror inte att jag har postat det här inlägget tidigare, fast jag borde ha gjort det. Det skrevs nämligen för en vecka sen, men jag verkar ha glömt att klicka på "publicera".

Jag räknade just: strax över femtio böcker har vi köpt sen vi kom hit (för nytillkomna: familjen är i Kanada, av jobbanledningar, sedan september). Somliga har vi köpt nya till fullt pris, men de flesta är köpta på billighetsantikvariat och loppisar och så för mellan 25 cent och ett par dollar. Vi har inte skaffat någon bokhylla än, så de trängs i en garderob och ovanpå ett linneskåp.

När vi kom hit hade vi fyra resväskor med oss, resten av alla våra saker lämnade vi i Sverige. På många sätt har det varit skönt att inte ha så mycket prylar, saker man måste ta rätt på och flytta runt och som är i vägen när man städar. Det är faktiskt inte så ofta jag verkligen har saknat något som vi lämnat. Det är symaskinen, några barngrejor som skulle ha varit med men i hasten blev kvarglömda, en vettig visp (verkar vara svårt att hitta vispar här som funkar för varm choklad och bottenredningar) -- och så böckerna.

Ibland blir jag riktigt frustrerad över att inte ha dem. Böcker som jag plötsligt känner för att läsa och inte har i närheten. Böcker jag vill kolla upp saker i eller undersöka något som jag skulle vilja skriva om. Programmeringsböcker, kokböcker, uppslagsverk. Det finns ju bibliotek här, och jag är där en gång i veckan eller så, men det är inte alls samma sak som att bo ihop med en samling böcker jag känner väl. En viss brist på svenska böcker har de också.

Många av de böcker vi skaffat här kommer vi inte att ta med oss tillbaka till Sverige, bara en utvald andel. Det är en svår balansgång det där att hindra böckerna från att ta över hemmet totalt, samtidigt som man lyckas ha alla betydelsefulla böcker tillgängliga när man vill ha dem.
