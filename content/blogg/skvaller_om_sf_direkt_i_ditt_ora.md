+++
title = "Skvaller om sf, direkt i ditt öra!"
slug = "skvaller_om_sf_direkt_i_ditt_ora"
date = 2010-08-18

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Länkar"]
taggar = []
+++

Ibland får jag faktiskt ha min mp3-spelare ifred (när jag inte tappar bort den bakom strategiska hyllor i hemmet eller råkar visa den för den musiktokiga ettåringen), och då händer det att jag lyssnar på science fiction.

Jag har just börjat lyssna på en serie podcasts med samtal mellan Jonathan Strahan och Gary Wolfe. Det finns 15 timslånga avsnitt vid det här laget och jag har bara hört ett och ett halvt, men jag tycker att det är värt att rekommendera. Känns litegrann som en bra men ganska vagt definierad programpunkt på en kongress, och jag känner ofta att jag vill föra anteckningar (dålig avkopplingslyssning för min del!). Fullt av kommentarer om vad man kan läsa, vad som är på gång, och vad som är vad i historisk kontext. Hur somliga författare är bättre än andra på att överraska, och hur somliga läsare inte vill bli överraskade. Vad Locus och recensenter har för betydelse. Kan man förstå Joanna Russ utan att läsa den sf hon läste och stod i dialog med? Vad är det som gör "new sword and sorcery" ny, och hur är det med alla andra "new"-subgenrer? Och vem blir nästa Paolo Bacigalupi?

Det här är två snubbar som är grundligt pålästa och belästa, och som slänger namn och titlar ur sig på löpande band. Ofta gör de som man brukar när man är väldigt bekant med folk och kallar författare vid enbart förnamn, så man får spetsa öronen ibland för att hänga med. I stort sett har jag inte haft några problem med det ändå, och det är kul och inspirerande att höra på. I några avsnitt har de tagit in fler personer i konversationen också. 

Alla avsnitt kan laddas ner från [Jonathan Strahans blogg](http://www.jonathanstrahan.com.au/wp). Jag hittade dem först via [en länklista på Torque Control (Vector-bloggen)](http://vectoreditors.wordpress.com/2010/07/09/no-present-like-links), där det finns direktlänkar till de första nio avsnitten. Sedan får man leta i arkiven efter resten.

Jag tänker nog lyssna igenom dem i tur och ordning. Får se om jag kommer ikapp!

