+++
title = "Sf-musik igen"
slug = "sf_musik_igen"
date = 2009-03-11

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Kåserier"]
taggar = ["musik", "sf"]
+++

Jag har planerat ett tag att skriva ett genmäle till Johans artikel [Det finns
inget som heter science
fiction-musik](./blogg/det_finns_inget_som_heter_science_fiction_musik/index.md).
Min huvudsakliga invändning gäller påståendet att musik inte är narrativ.
Snarare skulle jag säga att merparten av all musik är är skapad just för att
framföra berättelser.

Till att börja med har vi musik satt till poesi. Min erfarenhet från
komponering är att tonsättningen spelar stor roll för hur berättelsen, och de
flesta sångtexter är berättelser, framstår. Sedan har vi musik för dans. Det
är förstås ovanligt med dans utan musik, och även dans är för det mesta
narrativ, ofta genom att representera myter, eller som exempelvis i våra
pardanser rituellt spela upp våra sociala konventioner. Vid det här laget har
jag täckt in merparten av all musik som producerats, men till och med modern
konstmusik, även om den inte representerar en specifik berättelser, följer
ofta en narrativ mall med en inledning, ett klimax, och ett slut.

Följaktligen tycker jag inte att det är fel att påstå att ett musikstycke är
science fiction om det representerar en science fiction-berättelse.

Det är heller inte svårt att finna exempel på musikalbum som i sin helhet
utgör science fiction-berättelser. Österikiska
[mind.in.a.box](https://en.wikipedia.org/wiki/Mind_in_a_box) gör inget annat.
David Bowie's [1. Outside](https://en.wikipedia.org/wiki/Outside_(album)) är
ett annat exempel på en cyberpunkberättelse i musikalisk form.

Jag tycker inte heller att det är orimligt att referera till musik som ingår
i t.ex. sf-film för sf när den är skriven för att kunna ingå i den
övergripande berättelsens sf-värld. Ett bra exempel är [The American
Astronaut](https://en.wikipedia.org/wiki/The_American_Astronaut), en
rymdvästernhistoria där ett av de element som gör världen främmande för oss
är människornas relation till musik.
