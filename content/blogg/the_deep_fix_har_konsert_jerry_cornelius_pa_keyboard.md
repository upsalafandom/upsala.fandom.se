+++
title = "The Deep Fix har konsert – Jerry Cornelius på keyboard"
slug = "the_deep_fix_har_konsert_jerry_cornelius_pa_keyboard"
date = 2008-06-27

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Elric", "Jerry Cornelius", "Stormbringer", "psykedelia", "rock", "video"]
+++

Ni minns kanske det stora party i _The Final Program_ där alla som är nåt samlas och partajar med Jerry i London? **The Deep Fix** underhåller och Elric själv spelar keyboard!

Det var ett maffigt party och jag kommer ihåg hur bl.a. Una Persson pratade om det länge med nåt längtande i rösten. Höjdpunkten var förstås när Jerry duellerade med hela artilleriet och när Elric och Stormbringer ylade fram toner som bubblade över multiversums alla plan. Dagen till ära var Stormbringer klädd i guld. Det var en riktig fest det.

Nu finns showen fångad i kondenserad form, direkt från den andra etern: [video på YouTube](https://www.youtube.com/watch?v=_l_8N0Jy9QU)
