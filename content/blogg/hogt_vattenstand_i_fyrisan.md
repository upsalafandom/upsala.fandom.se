+++
title = "Högt vattenstånd i Fyrisån"
slug = "hogt_vattenstand_i_fyrisan"
date = 2008-03-04

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Inbjudningar"]
taggar = ["pubmöten"]
+++

Kära vitterhetsvänner!

Jag har äran att inbjuda er till professor Wiedenborgs föredrag ikväll om
Michail Sjolochov och den upsalienska litteraturparnassens kritik av
Akademiens val av litteraturpristagare på 1950- och 60-talen, betitlat "Högt
vattenstånd i Fyrisån och andra slamkrypare".

Platsen är WAS - **Williams akademiska salonger, Åsgränd 5**, Uppsala. Tid:
**18.30 och framåt**.

Professor Wiedenborg är aktuell med boken _Spuds and Bacon_, om Samuel
Becketts och Michail Sjolochovs samlevnadsförhållanden under det kalla
kriget, där han driver tesen att potatispest och ransoneringar förklarar både
motivval och erotiska problem hos dessa författare, samt spekulerar i
sanningshalten i de rykten som var i svang under 1940-talet om att Samuel
Beckett egentligen var Gertrude Stein i förklädnad.

Varm välkomna. Mat och dryck serveras mot separat avgift.

Giovanni Cramelgna

Koordinator, WAS
