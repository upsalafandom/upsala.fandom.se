+++
title = "Spoileri, spoilera, spoileralla..."
slug = "spoileri_spoilera_spoileralla"
date = 2008-04-27

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = []
+++

I kommentarerna till mitt förra inlägg dök den gamla diskussionen om
"spoilers" upp igen. Hur mycket får man säga om en bok innan man förstör den
för folk som inte läst den ännu?

När jag skriver seriösa recensioner brukar jag försöka undvika att avslöja
viktiga saker om handlingen. (Mer om recensioner har jag [redan sagt på annan
plats](http://sfweb.dang.se/h/blog/2007/01/hur-jag-skriver-recensioner.html),
se även [den stora diskussonen i sf-bloggvärlden
nyligen](http://vectoreditors.wordpress.com/2008/03/30/well-that-makes-life-easier).)
När jag inte skriver seriösa recensioner tycker jag att det är allra roligast
att prata om just de där spännande detaljerna och de kritiska punkterna i
berättelsen. Slutet, särskilt oväntade vändningar. De där sakerna som fångar
mitt intresse, det är ju dem jag vill prata om!

Jag tycker också mycket om att läsa kritik, texter där man dissekerar
litteratur och hittar alla intressanta aspekter. Jag läser gärna böcker efter
att jag fått reda på vad det är som är häftigt med slutet. En period i
högstadiet lusläste jag boken _Fantasi och vetenskap i science fiction_ som
inte skulle ha varit hälften så kul om den inte tog upp mängder med exempel
från kända böcker och filmer, och därmed diskuterade kritiska saker i
handlingen. Klassiker är böcker som alla tar för givet att man vet vad de
handlar om, även om man inte läst dem, och någonstans kommer ju den kunskapen
ifrån.

Så man kan ju säga att jag har en ganska respektlös inställning till det där
med att avslöja saker. Jag vill ju prata och skriva om det jag tycker är
coolt och intressant! Om jag verkligen går in på detaljer försöker jag komma
ihåg att varna, men jag tycker verkligen att det är orimligt att hålla inne
med små jämförelser och kommentarer i förbigående bara för att handlingen i
böcker ska vara någon sorts helig hemlighet. Inte bara orimligt, ärligt talat
tycker jag att det är fånigt. Jag inser att det kommer att göra folk
irriterade ibland, men det må i så fall vara hänt.

Om man verkligen inte vill veta något om en bok innan man läser den undrar
jag hur man avgör vad man ska läsa och inte.

Vad tycker ni andra om saken? Låt oss nu få en riktigt hetsig debatt!
