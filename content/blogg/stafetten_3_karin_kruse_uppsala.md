+++
title = "Stafetten 3: Karin Kruse, Uppsala"
slug = "stafetten_3_karin_kruse_uppsala"
date = 2008-10-09

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Intervjuer"]
taggar = ["Dalai Lama", "Hallstahammar", "Kruse", "aktivism", "rädda världen", "utbildning", "Östtimor"]
+++

> Detta är den tredje av mina personliga intervjuer med människor inom svensk
> fandom eller som på annat vis är involverade i det här med sf och fantasy.
> Kruse föreslogs av det förra intervjuoffret, Biörn X Öqvist, med motiveringen
> att hon är en ganska skön person!

# Begynnelsen

Jag är ursprungligen norrlänning, kommer från Gällivare, men växte upp i
Hallstahammar dit familjen flyttade 1968. Pappa Stig var gymnasielärare
(filosofi, psykologi och samhällskunskap). Mamma Gunilla arbetade med textil
tillskärning, även hon på gymnasiet. Trist, för hon var utbildad tillskärare
och skräddare, men ingen använde kostymer i Gällivare... Brorsan Olof, sju år
äldre och förvillande lik Jan Guillou, gick i skolan. När den sprängdes för
ca 30 år sedan bodde vi inte ens där, så det var **inte** han
som sprängde den, vad de än säger!

<!-- more -->

# Fandom

Under fritiden under tonåren hängde jag en period mest med det beryktade
Hallstahammarsfandom, bestående av undertecknad, Mika H Tenhovaara och Mikael
T I Andersson. Det kom sig av mitt intresse för min klasskompis Marias bror
Mikael... och när jag blev ihop med honom när jag var 14 år så fick jag
fandom med på köpet. Det gjorde ju absolut ingenting. Vi gjorde en massa
fanzines och hade rockband och en massa drömmar.

Jag var SF-intresserad redan innan jag träffade TIMA (som var hans fan-namn).
I familjen och släkten lästes det en hel, **även** Olof Möller
(som fanns i sommarstugan). John Carter på Mars var en familjefavorit ett
tag, och Tolkien fick man med modersmjölken. Egen rymddräkt finnes, Jules
Verne, **Häpna!** (lästes hos moster), Kosmos-serien...

Fandom fångade mig för att det fanns spännande människor där. I början var jag också överväldigad av den uppmärksamhet jag fick. Senare blev jag överväldigad av alla kreativa, nytänkande, intelligenta och fantasifulla personer som drogs till just fandom. Tänk, passar jag in där? De senaste många åren har jag varit en fringe-fan, och passar nog bäst där. Jag har inte längre samma driv till annat än att långpendla och snyta mina barn. Men vänta bara tills ungarna flyger ur boet och flyttar hemifrån...!

# Musiken

Senare hamnade jag i rollspelsträsket, och efter det i rockbandseländet och
arrangerade konserter. Jag sjöng, körade, spelade keyboard, bas och
percussion. Det ballaste var att spela röd rutschkana av glasfiber. Den var
en liten spädbarnsmodell som gav ifrån sig många spännande ljud. Jag har
alltid velat ha **mer**, så jag och några till startade Y-dur, ett nätverk
med band så att vi kunde fixa konserter och sådant. Det slutade som vanligt
med att jag gjorde nästan allt (ja, jag spelade ju inte solo för alla band
under konserterna...).

Det var kul att spela i band, men trots att jag kan hålla tonen när jag
sjunger och att jag kan räkna ut hur man spelar de flesta instrument så är
jag inte vidare musikalisk. Jag kan inte improvisera eller spela på gehör, så
för allas trevnad tar jag inte upp det igen. Jag hade en kort romans med
bluegrass och lirade femsträngad banjo med HP Burman och Anders "Lillen"
Eklund en kort period, och de människor man träffade i de sammanhangen var
fantastiskt musikaliska och hade **det**. Det har ju inte jag då, men plinkar
nuförtiden lite ukulele i lönndom... Men inte offentligt. Ännu.

Något körkort hann jag aldrig ta...

Det var väl OK att växa upp i Hallstahammar. Man hade många vänner där,
vänner jag träffar än idag. Under den period jag bodde där fanns ingenting
att göra för tonåringar, det var därför vi startade alla rockbanden. Jag
skulle **inte** vilja flytta till Hallstahammar efter pensioneringen. Uppsala
känns som mitt **hem**, och jag tror jag gärna bor här resten av mitt liv.
Qui vivra verra...

# Uppsala

Hösten 1985 flyttade jag till Uppsala för att studera. Jag började med
religionshistoria, fastän jag hade sökt in till juristlinjen. Helst ville jag
bli marinbiolog med inriktning på däggdjur. Kära nån!

Istället blev det religionshistoria, religionsfenomenologi,
religionssociologi, filosofi, astronomi, civilrätt, offentlig rätt,
immaterialrätt, jiddisch, paleontologi, historia, ekonomisk historia,
redovisning, företagsekonomi, företagsrätt, pedagogik, geologi,
kulturgeografi, meteorologi, mineralogi, samhällskunskap... **äsch**. Cirka
300 universitetspoäng av gamla sorten, i alla fall. Men bara en fil. kand.
Kanske borde jag ha struntat i juridiken och läst mer ekonomisk historia.
Tror jag. Men nästan allt har varit kul!

Marinbiologin, däremot, den hade jag aldrig de kvalifikationer som behövdes
för. Jag hamnade rätt från början med religionshistoria. Det är fortfarande
min favorit! Juristlinjen hade jag bara fått för mig att jag skulle in på.
Och jag kom ju in där. När jag var på en världsräddarkonferens i Kairo
träffade jag mitt i natten två flickor som tiggde. Den äldre flickan, 4 år
gammal med avskuren tunga, tiggde med lillasyster i handen. Jag tog dem till
ett matställe och de fick äta tills de kräktes, och sedan äta igen. Jag insåg
då att juridik inte kan förändra så mycket som jag skulle hoppas på. Sedan
fick jag fly hem till Sverige igen med Gulfkriget i hälarna och Mossad på
halsen. Jag tjatade mig in på lärarhögskolan och har sedan dess berättat om
dessa flickor för alla mina elever, och flera av dem räddar idag världen.

När jag tittar tillbaka var Uppsala var ett bra val. Staden är lagom stor och
här finns alltid nya människor. Synd bara att de bygger så mycket i
stadskärnan just nu att "lungorna" försvinner.

# Rädda världen

Bortsett från att plugga ägnade jag det mesta av studietiden och en tid
därefter åt att försöka rädda världen. Jag är en riktigt priviligierad jävel
som får ha de åsikter jag behagar, och som har tillgång till sjukvård, dagis,
utbildning och annat. Möjligheten till att jag skulle ha fötts till sådana
förmåner är försumbart liten om man tittar på den globala födelsestatistiken.

**Om** det nu ska vara så att den förbaskade slumpen avgör om man ska få ha en
dräglig tillvaro eller inte här i världen så tycker jag det väl vore
trevligast för oss alla på jorden om chanserna var lika bra för alla. Jag kan
inte stoppa världssvälten eller göra andra mirakel, så för att inte utmatta
mig och förändra **allt** i **hela världen** på en gång så
mognade beslutet att inrikta mig på åsiktsfrihet. Mitt fokus i slutskedet av
min rädda världen-period låg på Östtimor.

Mitt favoritminne från de här åren är från en konferens i Krakow, när
deltagarna kom - iklädda sina folkdräkter - till en polisstation där de
godtyckligt hade arresterat några miljöaktivister som jag kände. Alla dessa
olika nationaliteters dräkter och hudfärger skrämde poliserna så att de
trodde det kunde bli internationella förvecklingar. De släppte aktivisterna
pronto.

Att sitta och hacka sönder Berlinmuren var också en underbar upplevelse. Och
att hålla handen tillsammans med flera tusen andra människor genom de
baltiska staterna. Och vi blev bara fler och fler och fler...

Eller att bli slagen i huvudet av Portugals gamle president general Nuñes med
en ihoprullad affisch. Eller att flörta med Dalai Lama. Charmerande och
karismatisk man, men så har ju den kontext som han vuxit upp i format honom.
Det var han som började flörta på ett oskyldigt sätt, och vi samtalade en
stund tills hans livvakter avbröt för att det kom en massa annat folk. Jag
var plaskvåt, för det var skyfall i Wien under konferensen för mänskliga
rättigheter 1993. Jag var den enda som fanns i närheten av hans skyddade
talarpodium, och jag var redan blöt så jag kunde lika gärna stå i regnet och
fånga vattendroppar på tungan. Han provade också och vi fnittrade hysteriskt.
Sedan upphörde skyfallet plötsligen, solen bröt fram med 30 graders värme,
tusentals människor strömmade till och förtrollningen var bruten...

Men allt var inte kul. Att vara omringad av indonesiska flottan ute vid
kusten vid Östtimor och bli hotad att de ska skjuta vårt skepp i sank om vi
närmade oss Östtimor med vårt skepp var jobbigt. Hoten innefattade också att
de skulle arrestera oss allesammans och ta oss till fängelseön Bali, våldta
oss tjejer och tortera killarna på annat sätt tills vi blev hämtade. Nu tror
jag inte att de skulle ha verkställt hoten, för det skulle ha blivit ett
jäkla liv. Men att stå på fel sida av en kanon var inte så värst trevligt.

Inte för att det avskräckte mig. Jag körde flera race tills jag fick fast
anställning och började pendla till Sala 1994. Efter det har jag bara gjort
kortare strandhugg, mestadels i Bryssel där mitt förlag har anordnat en del
konferenser med fokus på "u-lands"-kvinnor i världsekonomin. Att stå mitt i
ett gäng nynazister i Berlin på _Anschluss_-dagen med min "judiska näsa" var
inte heller riskfritt. Jag klarade mig med mitt svenska pass och
tyskklingande namn (då bar de till och med mitt bagage...)

Just nu är det Västsahara jag arbetar mest med, särskilt genom det lilla
bokförlag i form av stiftelse som jag sedan många år driver med ett par
kompisar. Kolla vår hemsida
[www.globalpublications.org](http://www.globalpublications.org). Där kan man
läsa en hel del artiklar (gratis) och köpa böcker. Mina två titlar om
Östtimor är slutsålda tror jag, men den första av dem håller just nu på att
bli översatt till bosniska. Kul!

Men mina barn Anna och Jarl, 10 respektive 4 år, går naturligtvis före allt annat.

# Arbetet

För att försörja mig plågar jag tonåringar! Härligt att vara lärare i
SO-ämnena (religion, historia, samhällskunskap och geografi). Det har jag
gjort i över tio år nu, i lilla Sala (på samma skola som David Nessle gick
på).

Det är kul att vara lärare, för att man ser hur de personer man arbetar med
växer och utvecklas till resonerande människor. Man lär sig själv en massa
saker under resans gång också, och det är väldigt belönande när man når fram
och får en person bli stolt över sina egna prestationer. Det är en egoboo som
är mastig!

Det blir, tvärtemot vad en del tror, aldrig tjatigt och upprepande!
Visserligen lär man ju ut de saker som man ska enligt lag, och man återkommer
till samma områden år efter år, ibland flera gånger per år, men de grupper
man undervisar är så olika att man måste attackera stoffet på flertalet sätt.
Jag har till exempel undervisat om franska revolutionen i 14 år, men banne
mig om jag har gjort det likadant varje gång.

Det går absolut att lära ungarna något, och lyssnar **du** på vad **de**
säger så lyssnar de på vad du säger. Så enkelt som bara den!

Vad jag gör du om tio år kan man tack och lov aldrig veta. Men det ska bli
mycket spännande att se vad det blir!
