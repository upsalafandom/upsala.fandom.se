+++
title = "Macleod on Uppsala"
slug = "macleod_on_uppsala"
date = 2010-10-03
lang = "en"

[taxonomies]
forfattare = ["George"]
kategorier = ["Länkar"]
taggar = ["Ken Macleod", "Uppsala"]
+++

I moved to Uppsala on 8 Jan, 2009. The main reason for this move was medical, but SF played a significant role in motivating me to choose Uppsala over my other possibilities, Helsingfors and Leuven.  In 2003 I read some quite complementary words about the SF scene in Sweden. They were written by Ken MacLeod, a person whom I admire very much. Since I've mentioned this text to Upsalafans and Uppsala philosophers many times, I decided this morning to [display it here](http://kenmacleod.blogspot.com/2003/08/seeing-mars-from-uppsala-ive-recently.html). It's from Ken's excellent blog, but has appeared in other places as well.
