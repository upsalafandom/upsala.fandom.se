+++
title = "När sanningen överträffar verkligheten"
slug = "nar_sanningen_overtraffar_verkligheten"
date = 2008-04-17

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Adolf Hitler", "Arkiv X", "frihet", "konspirationsteori", "politik", "teve"]
+++

Jag har på sistone gjort ett återbesök i det förflutna. I detta fall innebar
det en märklig impuls att titta på tv-serien _Arkiv X_ igen. Den kunde när
den var som sämst vara lite sådär småfånig, och när den var som bäst ett
stycke ganska intrikat paranoia och konspirationsteori. Inget kittlar väl
fantasin lika mycket som tanken på främmande liv i universum och om man
dessutom får en liten adrenalinkick av invecklade försök att hålla sanningen
hemlig så är det ju en given vinnare. På något vis kändes serien som en
ganska typisk nittiotalare, med misstro mot etablissemang och lite
anstrykning av "alternativa" livstolkningar. Men, det som slår en när man ser
om den är ju hur spöklikt det känns som om den handlar om nutiden.

Oavsett vad man tycker och tror om UFOs och tanken på att det skulle finnas
en konspiration för att hålla kontakter med dem hemliga, så är själva
konceptet med maktmissbruk och vardagliga intrång i privatlivet från
myndighetsmakt inte bara fantasi utan realitet. I serien kommer man i säsong
fem till den intressanta situationen att det verkar som om folk med makt och
pengar inom militär och säkerhetstjänst inte haft en hemlighet de underhållit
allmänheten, utan att de för att hålla saker hemliga så hittar de på en
hemlighet som de sedan sprider ut antydningar om att de försöker hålla
hemligt, så att själva hemligheten dols av förvirringen om deras fabricerade
hemlighet är sann eller inte när de nu verkar jobba så hårt på att hålla den
hemlig. Blev du lätt yr av det där? Det är en oerhört komplex soppa där
sanningen är så väl dold av lögner att den inte längre syns när själva
sanningen har slutat vara meningen med vad folk tror är sant. Det bästa
sättet att gömma något är att göra det i det öppna har det sagts. Ska du
ljuga så ljug så stort och mycket att folk inte kan föreställa sig att du har
mage för så grova lögner, som en expert sa. Mycket intrikat, och nästan lite
beundransvärt i sin illvillighet.

Det var riktigt underhållande att se några av de bättre avsnitten, där Chris
Carter med medarbetare tar i från tårna. Man inser att skådisarna som
vanligtvis inte gör mer än en hygglig insats faktiskt kan bli riktigt bra om
det vill. Om man dessutom tycker om att få en liten ilning längs med
ryggraden både från sensawunda och från en lätt känsla av skräck och fasa.
"Njutbart" faktiskt.

Vardagen så. Tyvärr är det inte bara underhållning. Nu idag har vi så efter
nittiotalets lite populariserade auktoritetsskepticism (om den nu någonsin
var så stark som sades vete sjutton, men ändå) fått ett samhälle där vi
godtar precis den typ av våldtäkt på individen som i _Arkiv X_ används för
underhålla oss med lite ilningar. Bara tanken att man ska kunna ta DNA prov
på "tänkbara brottslingar" som man i Storbritannien diskuterat och nu även i
USA pratar om är skrämmande. Att man i Sverige på fullt allvar diskuterat att
polisen ska få befogenhet att övervaka kommunikation innan det ens begåtts
ett brott är också skrämmande. Samhället har blivit så genomsyrat med rädsla
för alla möjliga verkliga och inbillade hot att folk glömmer bort att byta in
sina friheter mot säkerheter inte gör dem ett dugg säkrare, bara mer
kontrollerade.

John-Henri Holmberg skrev i en ledare i Nova om hur sf är inte bara roligt
och intressant, utan meningsfullt och direkt nyttigt för att man inom science
fiction länge skrivit böcker som diskuterat utopiska och dystopiska
scenarier, och att det där länge funnits en diskussion om samhälle, individer
och frihet. Naturligtvis gör jag inte på något vis John-Henri rättvisa genom
att så kortfattat sammanfatta hans ledare, men ni förstår kanske vad jag far
efter. Jag tror definitivt att han har rätt, och att en öppen debatt om
frihet och makt är något som kanske borde föras även utanför genren. Filmer
som Gattaca har ju setts av många, likt tv serien jag pratar om, men jag vet
inte om folk brytt sig tillräckligt mycket för att inse att även dystopiska
scenarier serverade för underhållningssyfte faktiskt kan säga nåt om vår
framtid. Själv är jag illa berörd av utvecklingen, och det gjorde ironiskt
nog upplevelsen av att titta på _Arkiv X_ än mer effektiv i att ge mig de där
ilningarna.

Frågan är du förstås om det finns en konspiration, och om det finns några
aliens? Antagligen inte, men när man ser hur direkt förnuftsvidriga saker
sker i vardagspolitiken kan man ju börja undra. Är det någon som försöker
dölja nåt genom att dra ut det så långt i strålkastarljuset att folk inte
tror på det längre? Är vardagspolitiken där makten våldför sig på oss en
postmodern "reality show" där själva vardagens sanning blivit ett skämt?

Jag vet inte riktigt om man ska dra den tankekedjan till slutet, men det
finns något skrämmande i allt det här. Science fiction kanske kan få en att
tänka, fundera och reflektera över vardagen trots allt?

_The Truth Is Out There_
