+++
title = "Bokprat"
slug = "bokprat_2009"
date = 2008-12-13

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Inbjudningar"]
taggar = ["Catahya", "bokprat", "böcker"]
+++

Jag kan inte låta bli att tycka att det är lite roligt, på ett ironiskt vis,
att diskussionen kring Upsalafandoms bokprat har (som Björn Lindström
påpekade på [listan](./sidor/epostlista.md)) flyttat från Upsalafandom till
[Catahya](https://catahya.net//forum/svar.asp?tid=9260&amp;forumid=13).
Inte för att det inte finns en stor poäng med det. De aktiva, som i alla fall
har försökt att läsa varje bok och dyka upp på varje bokpratsmöte (Björn,
Jesper, Johan Anglemark, jag) finns där allihop och sett till erfarenheten av
att försöka få med fler fans på bokpratsmötena på sistone så lär det vara
betydligt större chans att få någon eller några till att börja komma där.
Inte för att det inte har droppat in andra personer av och till, förstås, men
ingen annan som har kommit regelbundet.

Vilket inte betyder att ni inte skall börja nu, förstås. Det är roligt att
prata böcker. Särskilt bra böcker. Det är inte direkt svårt att gå med i
Catahya, om man har åsikter om vad och hur och när vi borde läsa och vill
skriva på forumet där. Vill man av någon anledning inte göra det går det
förstås att påpeka dem här eller på listan också. Dyka upp kan man alltid
göra oavsett.
