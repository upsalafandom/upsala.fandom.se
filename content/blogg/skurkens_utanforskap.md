+++
title = "Skurkens utanförskap"
slug = "skurkens_utanforskap"
date = 2009-08-20

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Nyheter"]
taggar = ["Erik Granström", "fantasy", "litteratur", "rollspel"]
+++

Freke Räihä, som är student vid Växjö Universitet, har skrivit en uppsats i litteraturvetenskap om Erik Granströms serie av rollspelsäventyr kända som konfluxsviten. Uppsatsen [finns att läsa här](arkiv/freke_raiha-skurkens_utanforskap_i_erik_granstroms_konfluxsvit.doc), och Erik [kommenterar artikeln på sin blogg](http://erik-granstrom.blogspot.com/2009/08/utanforskap-i-konfluxsviten.html).
