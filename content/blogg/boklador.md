+++
title = "Boklådor"
slug = "boklador"
date = 2008-02-26

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Kåserier"]
taggar = ["bokaffärer", "böcker"]
+++

Igår blev jag sugen på att konsumera lite rymdopera, men det fanns ingen i min hylla att läsa. Följaktligen promenerade jag ner till [The UEB](https://bookshop.se) och såg efter vad de hade inne. Min första plan var Iain M. Banks nyutgivna _Matter_, men den hade de sålt slut av.

Det blev alltså till att botanisera i hyllorna, och jag fastnade till slut för novellsamlingen _Galactic North_ av Alastair Reynolds. Titeln skadade inte.

På väg hem slog det mig att det är väldigt sällan jag handlar i bokaffärer nuförtiden. Priserna men kanske främst utbudet tenderar att locka mig till webbhandlarna.

Men då och då händer det som inträffade igår. En ledig eftermiddag blir man sugen på rymdopera och två dagars leveranstid duger inte.

Jag tänker knappast börja köpa alla mina böcker i butik, men någon slags princip kommer nog att införas. Den första bok jag köper varje månad som finns på UEB ska jag köpa där, tror jag.
