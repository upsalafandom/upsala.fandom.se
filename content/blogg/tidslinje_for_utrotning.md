+++
title = "Tidslinje för utrotning"
slug = "tidslinje_for_utrotning"
date = 2008-03-05

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Länkar"]
taggar = ["framtiden", "humor", "spådomar"]
+++

Ross Dawson har på sin blogg [Trends in the Living Networks](http://www.rossdawsonblog.com) publicerat en underhållande [tidslinje](http://www.rossdawsonblog.com/weblog/archives/2007/10/extinction_time.html) över hur saker och personer har utrotas och kommer att utrotas åren 1950-2050. Den är en blandning av allvar och småleende satir.

Vi lär oss att det sista ångloket försvann 1968, billig olja blev ett minne blott 1973, Jugoslavien försvann 1991,  Tony Blair 2007 och att följande saker kommer att försvinna i framtiden: askkoppar 2010, telefonkataloger 2015, Taiwan 2020, rätstavnig 2022, dokusåpor 2030, Bangladesh 2035, koldioxidutsläpp 2040, Google 2049, framtidsforskare 2050, listor med förutsägelser 2051, fulhet några år senare och slutligen döden ytterligare några år efter det.
