+++
title = "7 november 2000"
slug = "7_november_2000"
date = 2000-11-07

[taxonomies]
forfattare = ["Björn"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

Uppslutningen till Upsalafandoms samkväm tisdag 7 november 2000 var god. Faktum är att antalet närvarande uppgick till det heliga talet 23.

Kvällen hade inte lidit långt innan frågan "Varför är du här?" ställdes till samtliga närvarande.

<!-- more -->

Björn Wiklund svarade att han var ute efter öl och skådespel. Han kunde gå därifrån nöjd efter att både ha avnjutit några glas öl
och dessutom ha bevittnat ett slagsmål mellan Karl-Johan och Magnus, vilket orsakats av att Magnus ifrågasatt om K-J, Tokholmare som han är, verkligen hör hemma i Upsalafandom.

Andra tappade helt greppet när de ombads förklara varför de egentligen kommit. Ante utropade exempelvis: "Elefanter!
Elefanter!". Visste han över huvud taget var han var?

Ytterligare andra förklarade sig genom att refererare till olika SF-TV-serier. Björn X ställde motfrågan "qathl bIghel?" och
Wahlbom yttrade: "If I go to Z'ha'dum I will die." – Bättre då att vara på William's, antar jag.

Efter allt detta ståhej kände de flesta sig lättade då Samuel i vild öhlextas utropade: "Tradition!"

Uppmuntrad av detta initiativ krossade Åka sitt (urdruckna) ölglas i golvet, så att det slogs i tusen bitar, varpå hon citerade kommunisten C-J Norén genom att säga:

"Någon jäkla ordning ska det vara i fandom!"

Det är tur att vi har dessa traditionsivrare. Genom sina eldiga tal lugnade de äntligen Magnus. Han lämnade K-J ifred och började
istället planera Fantastika 2002 tillsammans med Johan. K-J kunde snart ta sig på benen igen och tog tåget till Stockholm för att
plåstra om såren. (Med tanke på hans utmattade tillstånd blir man tvungen att ursäkta detta svek mot cykelfandom.)

Vid det laget fann även jag det för gott att lämna sällskapet till förmån för bussen. Vad som därefter hände får ni alltså aldrig veta.

Närvarande var Johan Anglemark, Åke Bertenstam, Marie Engfors, Magnus Eriksson, Andreas Gustafsson, Ragnar Hedlund, Mikael
Jolkkonen, Joacim Jonsson, Maria Jönsson, Björn Lindström, Daniel Luna, Marcus, Karl-Johan Norén, Gabriel Nilsson, Bertil Näsström,
Nils Segerdahl, Sten Thaning, Anders Wahlbom, Björn Wiklund, Anna Åkesson, Samuel Åslund, Simon Åslund och Björn X Ökvist.
