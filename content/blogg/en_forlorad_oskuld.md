+++
title = "En förlorad oskuld"
slug = "en_forlorad_oskuld"
date = 2008-04-14

[taxonomies]
forfattare = ["Johan J."]
kategorier = ["Kåserier"]
taggar = ["Cory Doctorow", "bokprat", "böcker"]
+++

Ibland är man dålig och beställer inte böcker i tid för att få hem dem innan
man borde ha läst dem. Det är någonting med det där att man _borde_
beställa en bok, istället för att man _inte borde_ beställa den utan
lägga pengarna på saker som, tja, mat, som gör det hela så mycket mindre
tilltalande. Då får man stå sitt kast, och läsa den [på
skärmen](http://craphound.com/someone/Cory_Doctorow_-_Someone_Comes_to_Town_Someone_Leaves_Town.htm)
istället, någonting som man några timmar och en roman senare inser kanske var
en måttligt god idé eftersom huvudet känns alldeles tomt på ett sätt som det
inte brukar göra efter att man har läst en bok. Man är åtminstone en
upplevelse rikare och vet vad det faktiskt innebär att läsa en så lång text
på datorn utan paus. Om inte annat så är det en god påminnelse om varför man
vanligtvis betalar för de där pappersbuntarna med trycksvärta på.

Det finns förstås förmodligen långt värre romaner att läsa på skärmen än Cory Doctorows _Someone Comes to Town, Someone Leaves Town_. Den är inte för långt, den är inte svårläst och den är tillräckligt engagerande för att man skall dras in i läsningen. Jag är ändå glad att det inte är någonting jag hade tänkt göra om de allra närmaste dagarna.
