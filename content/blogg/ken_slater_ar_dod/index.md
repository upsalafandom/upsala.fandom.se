+++
title = "Ken Slater är död"
slug = "ken_slater_ar_dod"
date = 2008-02-19

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Nyheter"]
taggar = ["Ken Slater", "brittisk fandom", "dödsfall", "fanhistoria"]
+++

Ken Slater var 20 år gammal när Storbritanniens sf-fandom kom igång på
allvar. Det var 70 år sedan. Jag träffade honom senast för två år sedan, på
den brittiska påskkongressen i Glasgow. Då stod han som vanligt och sålde
böcker, och såg inte ut att vara en dag äldre än 75. En makalös människa.

Han har fått äran av att vara den som fick brittisk fandom på fötter igen efter andra världskriget. Så här skriver Rob Hansen:

> Many who lived through this period credit Ken Slater with British fandom's
> eventual revival nationally. In September 1947 he published OPERATION
> FANTAST, which [...] sparked off the first post-war boom in fanzine
> publishing. OF remained associated with the BFL and Slater found himself
> running a department in the organisation that he describes as:
>
> >  _"...a sort of 'bring-and-buy' agency for the BFL's surplus magazines and
> >  books — and, of course, my own material. Officially, the original OPERATION
> >  FANTAST was the liaison department of the BFL and it remained that way
> >  through most of 1948."_
>
> […] Beginning to find their feet again British fans began to think about
> holding a convention. Ted Carnell appears to have been the first to push the
> idea, and in the January 1948 OF Trading Supplement, Slater urged interested
> fans to get in touch with him and passed their names along to Carnell. In the
> event, however, neither Slater or Carnell would be responsible for the
> convention that actually came about.
>
> The WHITCON, Britain's first post-war convention, took place on Saturday
> 15th May 1948. It was put on with little advance publicity but still
> succeeded in attracting 50 attendees, quite good for those days, and was held
> at The White Horse. Its organiser was John Newman:
>
> >  _"In 1948 I decided that I would like to attend an SF convention and came to
> > the conclusion that I would have to organise it myself. Ken Chapman gave
> > (calling it a loan but refused to accept repayment) money to arrange things
> > and be certain of covering the expense of the buffet and hire of the upstairs
> > room at the White Horse."_
>
> The youngest attendee was undoubtedly Gillings 14 year-old son, Ron, and
> one of those who couldn't be there, Ken Slater, sent £2 along so that
> everyone present could have a drink on him.

![Porträtt av Ken Slater från 1980](ken_slater_1980.jpeg)

_Bilden tagen av Lars-Olov Strandberg på Sfancon 11 i Gent, september 1980_

Kens **Operation Fantast** utvecklades till en bokhandel  och det var så man stötte på honom på senare dagar, som en oförtruten bokhandlare på brittiska kongresser, äldre för varje år utan att det syntes. Uttrycket _the end of an era_ är slitet, men mycket mer sant än så här blir det inte. Denne man var aktiv inom science fiction innan Michael Moorcock föddes och Arthur C. Clarke slog igenom.

Han dog nu i helgen.

![Porträtt av Ken Slater från 2005](ken_slater_2005.jpeg)

_Bilden tagen av Chaz Baden på världskongressen i Glasgow, augusti 2005_

> _"I like to take a book, a glass or cup of some liquor appropriate to the
> time and season, a comfortable chair, cigarettes or pipe ...and a few
> hours."_
>
> (Ken om sina läsvanor)
