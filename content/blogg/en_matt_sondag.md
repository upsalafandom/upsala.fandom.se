+++
title = "En matt söndag"
slug = "en_matt_sondag"
date = 2009-05-10

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = []
+++

Jag känner mig lite slö så här framåt kvällen, så dagen kommer nog att avslutas med ett par avsnitt av Mupparna framför teven. I går disputerade Linnéa med den äran på en [avhandling](http://urn.kb.se/resolve?urn=urn:nbn:se:uu:diva-100108) om tilltalsfraser i datorförmedlad kommunikation. Efteråt var det disputationsfest, men vi var faktiskt hemma igen inte alltför lång efter klockan ett, så det är inte därför jag känner mig seg.

Jag försökte [stödköpa lite böcker](http://www.tor.com/index.php?option=com_content&amp;view=blog&amp;id=27098#preview) av Peter S. Beagle tidigare i dag, men hindrades av att man måste uppge vilken _State/Province _man bor i. Korkade amerikaner. Nä, dags att släpa mig bort till teven. Det är en lång vecka framför mig: Två [Imagicon](http://www.imagicon.se)-möten, middag med några arbetskamrater för att fira en release samt ytterligare ett möte. Alla kvällarna måndag-torsdag inbokade. Suck.
