+++
title = "Pengar och inflytande"
slug = "pengar_och_inflytande"
date = 2000-09-05

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Rapporter"]
taggar = ["pubmöten"]
+++

En liten hög med pengar bildades på bordet och växte sakteliga under kvällen tills det var ett verkligt torn av glittrande mynt som hotade att välta över oss och krossa flaskor och glas – kanske jukeboxen också. Just då hade ingen sörjt om den hade strukit med, för ingen uppskattade musiken som spelades. Ingen i den muntra skaran kunde göra något åt saken heller, eftersom alla mynt var offrade till den imponerande stapeln. Och vem fick fylla fickorna med skrammel och ta med sig det hem? Det fick jag, eftersom jag skrev upp namnen på alla som var där och lovade att se till att världen fick veta.

<!-- more -->

Så det gör jag nu, berättar om vad som tilldrog sig igår kväll på en krog i Uppsala som inte har någonting med Orvar att göra. (Orvar var för övrigt snerking, fick jag veta. Det betyder inget mindre än att han tillhörde en viss studentnation som inte är Norrlands. Det hela verkar mycket underligt.) Den har kanske nån anknytning till nån som heter William, men det vet jag inget om. Vad jag vet är att jag blir oemotståndligt lockad att gå dit när solen står i väster på den första tisdagen som infaller efter att en ny kalendermånad börjat. Inte för att jag är så mycket för astrologi, men ibland ser man samband...

Underliga saker inträffar vid såna här tillfällen. Många sedlar bytte händer och inte alla omvandlades i öhl. (Transmutation? Det är för nybörjare...) Somliga gick därifrån med tjocka plånböcker. Även andra saker vandrade från en väska till en annan i ett bytesmönster som förstås bara av de invigda. Här förevisades en stor del av en Linda och Valentin-samling, en Conan-bok och andra små saker. Man talade om en dator som också skulle få byta ägare. Mitt på långbordet hukade Ante och Joacim över förbjuden kunskap (_Forbidden Lore_), för att Joacim ska kunna invigas i rollspelarnas krets i en värld där solarna slocknar.

Det som ingen fick med sig hem var fanzines. Av "Folk och fans i landet Annien" återstod bara kopieringsoriginal. Inte var Karl-Johan där heller, med sina gula lappar. Det skickades runt lite handskriven reklam för GöstaCon och det var allt. Någon blev undervisad om vad SFF är för något, så att vägen är beredd när det finns fanziner igen.

Någon som hastigt sedd i ögonvrån påminde mycket om Ahrvid gick förbi några  gånger och fick ett par av oss att haja till. Det var nu inte han, inte ens nån som verkade vilja ha nåt med oss att göra alls. Vi klarade oss ändå. Vi hade förstärkning av författande människor som annars träffas i cyberrymden. Jag kunde inte minnas vad Per Jorner hette utan att fråga, fastän han har fått en hel bok utgiven. Jag funderar återigen på hur lämpad jag är för det här med skvallerskriverier.

Nå, han är ju inte lika känd som Neil Gaiman i alla fall. Det viskades att herr Gaiman och herr Pratchett ska komma till allas vår bokhandel i Stockholm och signera böcker när _Good Omens_ kommer på svenska. Där ser man, vilken mängd fördold kunskap som kommer i dagen när vi samlas.

Det viskades en hel del annat också. Mycket handlade om böcker. Väldigt mycket. Det tänker jag inte berätta om nu, den som vill veta får komma och prata med oss själv.

Dessa var där: Anna Åkesson, Johan Anglemark, Simon Åslund, Bertil Näsström, Andreas Gustafsson, Anders Wahlbom, Björn X Öqvist, Mats Wistus, Sten Thaning, Björn Lindström, Olli Östlund, Gabriel Nilsson, Joacim Jonsson, Samuel Åslund, Ragnar Hedlund, Mikael Jolkkonen, Alexander Larsson, Per Jorner, Marie Engfors -- summa 19 personer.

Slut för idag!
