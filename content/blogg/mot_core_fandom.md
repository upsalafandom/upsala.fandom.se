+++
title = "Mot Core Fandom..."
slug = "mot_core_fandom"
date = 2008-03-27

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["fandom", "ghalenskaper", "kongresser"]
+++

Idag bär det av mot Toronto och vår första kongress i nordamerika. Jag läste lite fanzines igår, och försökte skaffa mig lite koll på debatten om "core fandom". Det är ju en term som har bubalt lite i fandom, och jag har hört det diskuteras på påskkongresser jag varit på.

Nu är det visst dags igen med debatt, och frågan är åter vad som är kärnan av fannisk verksamhet och vem som är en "riktig fan". Jag har ingen aning om det finns "riktiga fans" i Toronto, men ser jag någon ska jag rapportera det.

Själv är jag lite fascinerad av att man kan elda upp sig så mycket över vad en "riktig fan" är för något, men kanske är det så med starka känslor och identitetsskapande grupper.

Tänk själv på svensk fandom på åttiotalet, med en massa tonåringar som sprutade ur sig fanzine och levde fhaanniskt på det överdrivet energiska sätt som bara tonåringar kan. En hord av supande, fanzineskrivande galningar som anser det fhaanniskt att härja, bråka och vråla "Heinlein" och muttra om ufon. En herrans tur man inte är en "riktig fan", för jag skulle aldrig orka i mer än några timmar i taget!

Rapport om kongressen kommer nästa vecka!
