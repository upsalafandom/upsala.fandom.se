+++
title = "Ståldoktorn"
slug = "staldoktorn"
date = 2008-06-02

[taxonomies]
forfattare = ["Åka"]
kategorier = ["Kåserier"]
taggar = ["Doctor Steel", "leksaker", "musik", "utopier"]
+++

Världen innehåller för mycket dumt och dåligt. Låt oss fylla den med skoj,
och vara mer lekfulla! Leksaker åt alla!

Det låter bra, om än en smula för sötsliskigt kanske. Fast inte om man är en
galen vetenskapsman, och kombinerar budskapet med att hävda att man tänker ta
över världen. Med robotar.

Gulliga saker är suspekta, inte riktigt trovärdiga. När Doctor Steel
kombinerar visionen om en roligare värld med sitt fascistiska bildspråk och
sin emellanåt skräckfilmsaktiga musik blir resultatet lustigt nog inte bara
ett ironiskt uttalande om utopins omöjlighet, utan en mer äkta känsla av att
drömmen om Bra Saker trots allt är giltig.

Doctor Steel är musiker, artist, designer, kulturpersonlighet. Han lever på
samma populärkulturella metanivå som till exempel [den maskerade
proggaren](https://sv.wikipedia.org/wiki/Den_Maskerade_Proggaren) (och allt
annat som förekommer i [Kapten Stofils
tidning](http://www.kaptenstofil.net)), fast är mer multimedial. Definitivt
[befriad från gullighet.](http://www.youtube.com/watch?v=QBrQ9Hkd6h0)

Han är också ledare för en armé av leksakssoldater. Alla kan gå med.

![Propagandaposter för Doctor Steel](enlist.jpeg)

Jag gillar Doctor Steel. Jag har tittat på hans roliga propagandafilmer en
massa gånger, och spelar hans musik då och då (hittade den först på hans
[Myspace-sida](http://www.myspace.com/drsteel)). Någon leksakssoldat blir jag
ändå aldrig. Folk som marscherar i takt, även om det är i ett försök att vara
i otakt med alla andra, ger mig alltid skräckrysningar.
[Obehagskänslor](http://www.thewave.tk). Jag kan inte med att gå med i någon
armé, där går gränsen för mig.

Men jag kan ju uttrycka vissa sympatier för Saken. Och cylinderhattar. Och
labbrockar i blank PVC. Och robotar. Jag
[citerar](http://worlddominationtoys.com/drsteel/about/emperor.htm):

> As World Emperor, Dr. Steel strives to rebuild the world, free from
> boredom, fear and war. Creating a place where fun is the top
> priority.

(Fast jag föredrar förstås i praktiken att ersätta hans "singular vision" med
en massa spretiga parallella visioner.)

Gå nu allihop och roa er med
[webbskojigheter](http://worlddominationtoys.com/drsteel/enter.html) i Doctor
Steels bistra och flashifierade leksaksland!
