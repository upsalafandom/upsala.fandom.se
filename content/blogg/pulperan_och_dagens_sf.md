+++
title = "Pulperan och dagens sf"
slug = "pulperan_och_dagens_sf"
date = 2009-03-09

[taxonomies]
forfattare = ["Ante"]
kategorier = ["Kåserier"]
taggar = ["Barssom", "Edgar Rice Burroughs", "Jack Williamson", "pulperan"]
+++

Jag har ett intresse av att lite då och då ta en titt på hur sf traditionen skapades under 20 och trettiotalen. Förvånandsvärt många av dagens typiska genreelement skapades ganska tidigt, och det är lite kul att se var de kommer ifrån. En del av dessa små literaturarekologiska utfärder gräver fram pärlor, och andra saker man helst vill glömma.

En författare som ibland fascinerat mig är Jack Williamson. Han var verksam ända från sf barndom på 1920-talet till sin död på 200-talet. Tänk att ha fått leva genom hela den tiden! Han fick ju i princip se hur världen förändrades och hur science fictions drömmar blev verklighet. Han är bl.a. känd för Legion of Space Series, vilken jag köpte i en volym från [Alvarfonden](http://www.alvarfonden.org). Det var en intressant upplevelse, för här hade vi en av de där stilbildande verken som folk lystet köpte sf magasin för att få läsa. Hur skulle berättelserna te sig? Tyvärr måste jag säga att det nog är bland de värsta sf jag läst! 

Supervapen introduceras som helt oövervinnerliga, och för att det ska gå att skriva uppföljade så blir det sedan dags att uppfinna konstlade kringmanövrar kring det han först beskrivit som oövervinnerligt. Saker händer fullkomligt på slump, och när huvudpersonerna är i knipa så är det alltid så att det "bara händer" nåt i rätt stund som räddar dom. Det verkar liksom inte finnas minsta plan bakom storyn alls! Dessutom är personerna sådana karrikatyrer att man storknar. Regelbundet kraschar maskiner och efter en massa fånigt ojande så tar sig Mekanikern sig an problemet och kan naturligtvis laga saken. Naturligtvis efter att en reservdel bara plötsligt dyker upp utan rim och reson. Det är melodramatiskt och erbarmligt banalt. Hur överlevde genren det här skräpet!?

En annan författare som också är intressant är Edgar Rice Burroughs. Han skrev sina böcker om Barsoom innan sf traditionen hade börjat, och öven om han många gånger uppenbarligen skriver mer fantasy än sf är han när han ägnar sig åt tekniska förklaringar förvånandsvärt flyhänt! Första volymen i serien är intressant. Ramberättelsen ger oss John Carter liggandes i koma i en grotta i Arizona, där han inandats märkliga ångor från en skumt grönt pulver och han upplever under tiden äventyr på Mars. Det intressanta här är hur hela berättelsen är drömsk och lite overklig i framtoningen, och med en inramning som antyder att allt kan ha varit en dröm eller en droginducerad hallucination. Väl på mars är det hyggligt driv i berättelsen och även om John Carter är en banal figur, med komisk förmåga att alltid ta i "lite till" och ta ett steg till in i den mytiske hjältens gestalt och rädda fagra damer och vinna strider, så funkar det. Kanske är det för att han skriver en berättelse som mer liknar Homeros än nåt annat, men det är ju ingen tvekan om att hans val av sceneri och tema definitivt är science fiction, och har inarbetats in i genren i efterhand. Den otränade Edgar Rice Burroughs lyckas frammana mytiska kvaliteter i de fruktansvärt osofistikerade verktyg han arbetar med. Plötsligt inser jag varför det här skräpet faktiskt varit inte bara omtyckt utan också influerat och inspirerat.

Man kan hitta mycket spännande om man gräver i de gamla källorna. Berätta gärna om dina egna fynd, käre läsare.
