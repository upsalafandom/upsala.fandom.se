+++
title = "Litterära förfalskningar"
slug = "litterara_forfalskningar"
date = 2008-02-25

[taxonomies]
forfattare = ["Anglemark"]
kategorier = ["Kåserier"]
taggar = ["bluffar", "böcker"]
+++

Något som roar mig storligen är lärda bluffar. De är inte bara roliga att läsa om, utan det finns något alternativhistoriskt över dem. Tanken spinner loss och man börjar fundera på hur det hade varit om den eller den boken verkligen hade funnits. Och man börjar planera sina egna ståtliga förfalskningar. (Ni har glömt det här inlägget den dag jag hittar Strindbergs okända uppföljare till Fröken Julie uppe på vinden, va?) Inte bara en intellektuell lek, utan också en utmaning för händerna. Detta blir lättare i framtiden, när originalen är elektroniska, eller så kanske dör genren ut eftersom det aldrig går att bevisa något.

[The Guardian på webben](http://books.guardian.co.uk/news/articles/0,6109,594060,00.html) listade vad man menade var historiens tio mest kända litterära förfalskningar för ett par år sedan och Loren Rosson III på [bloggen The Busybody](http://lorenrosson.blogspot.com/2005/10/top-20-literary-hoaxes.html) fyllde på.

Detta är skilt från de öppet deklarerade pastischerna, som också är roliga men inte alls har samma romantiska tjusning över sig.

Jag undrar hur mycket man skulle bli hatad om man placerade Mormons bok och Koranen, som båda väl till skillnad mot Bibeln uppges vara dikterade av Gud själv, på en sådan här lista... och Castaneda borde kanske också hamna där, fast den gode Carlos påstår ju inte att det är den med största säkerhet uppdiktade Don Juan som har skrivit böckerna.

Man undrar också hur många verk som idag hålls för äkta som i själva verket inte är det. Det perfekta brottet är som bekant det som aldrig uppdagas.
